//
//  ForgotPasswordViewModel.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/03/2023.
//

import Foundation
protocol ForgotPasswordDelegate {
    func didReceiveForgotPasswordResponse(forgotPasswordResponse: ForgotPasswordResponse?)
}
class ForgotPasswordViewModel {
    var delegate: ForgotPasswordDelegate?
    func  getForgotPasswordResponse(email: String) {
        let forgotPasswordResource = ForgotPasswordResource()
        forgotPasswordResource.getForgotPasswordResponse(email: email) {  getForgotPasswordApiResponse in
            if getForgotPasswordApiResponse.status ==  200 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveForgotPasswordResponse(forgotPasswordResponse: getForgotPasswordApiResponse)
                }
            }
            if getForgotPasswordApiResponse.status ==  422 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveForgotPasswordResponse(forgotPasswordResponse: getForgotPasswordApiResponse)
                }
            }
        }
    }
}
