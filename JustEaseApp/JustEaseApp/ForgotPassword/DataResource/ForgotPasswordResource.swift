//
//  ForgotPasswordResource.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/03/2023.
//


import Foundation
struct ForgotPasswordResource {
    func getForgotPasswordResponse(email: String, completionHandler: @escaping (_ result: ForgotPasswordResponse) -> Void) {
        let httpUtility = HTTPUtility()
        let forgotPasswordUrl = "\(ApiEndpoints.forgotPasswordEndPoint)?email=\(email)"
        let url = URL(string: forgotPasswordUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType:ForgotPasswordResponse.self) { result in
                completionHandler(result!)
            }
        }
    }
}
