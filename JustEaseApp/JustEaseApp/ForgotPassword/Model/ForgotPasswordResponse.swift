//
//  ForgotPasswordResponse.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/03/2023.
//

import Foundation
struct ForgotPasswordResponse: Decodable {
    let status: Int
    let message: String?
    let data : [UserData]?
}


struct UserData: Decodable {
    let email: String
    let password: String
}
