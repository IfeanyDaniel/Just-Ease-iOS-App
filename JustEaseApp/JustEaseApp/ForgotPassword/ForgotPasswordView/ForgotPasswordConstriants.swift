//
//  ForgotPasswordConstriants.swift
//  JustEaseApp
//
//  Created by iOSApp on 09/02/2023.
//

import UIKit
extension ForgotPasswordViewController {
    //MARK: - Layout subviews for the scroll views
    func setupScrollView() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
        ])
    }
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(heyLabel)
        view.addSubview(label)
        contentView.addSubview(emailAddressTextField)
        contentView.addSubview(emailValidationLabel)
        contentView.addSubview(sendEmailButton)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = backgroundSystemColor
        
        NSLayoutConstraint.activate([
            heyLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            heyLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 25),
            
            label.topAnchor.constraint(equalTo: heyLabel.bottomAnchor, constant: 30),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 25),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -25),
            
            emailAddressTextField.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
            emailAddressTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 25),
            emailAddressTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -25),
            emailAddressTextField.heightAnchor.constraint(equalToConstant: textFieldHeight),
            
            emailValidationLabel.topAnchor.constraint(equalTo: emailAddressTextField.bottomAnchor, constant: 2),
            emailValidationLabel.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 25),
            
            sendEmailButton.topAnchor.constraint(equalTo: emailValidationLabel.bottomAnchor, constant: 50),
            sendEmailButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 25),
            sendEmailButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor , constant: -25),
            sendEmailButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            
            
        ])
    }
}
