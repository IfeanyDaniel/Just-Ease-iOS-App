//
//  ForgotPasswordController.swift
//  JustEaseApp
//
//  Created by iOSApp on 09/02/2023.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    let forgotPasswordViewModel = ForgotPasswordViewModel()
    var authViewModel = AuthViewModel()
    // MARK: - Scroll view
    lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.showsHorizontalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    // MARK: - content view
    lazy var contentView: UIView = {
        let content = UIView()
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    
    lazy var heyLabel: UILabel = {
        let label = UILabel()
        label.text = "Hey there!👋🏾"
        label.textColor = AppColors.green.color
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: AppFonts.silkabold.font, size: 25)
        label.textAlignment = .center
        return label
    }()
    
    //MARK: - Label below welcome back
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Enter your email and we’ll send you a link to reset your password"
        return label
    }()
    
    lazy var emailAddressTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Email address",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.addTarget(self, action: #selector(validateViews(_:)), for: .editingChanged)
        textField.keyboardType = .emailAddress
        textField.autocapitalizationType = .none
        textField.autocorrectionType = .no
        return textField
    }()
    lazy var emailValidationLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.numberOfLines = 1
        return label
    }()
    
    lazy var sendEmailButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Send link to email", for: .normal)
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(didTapSendEmaiButton), for: .touchUpInside)
        button.isEnabled = false
        button.backgroundColor = AppColors.grayBorderColor.color
        return button
    }()
    
    // MARK: - Login function
    @objc func didTapSendEmaiButton(){
        if InternetConnectionManager.isConnectedToNetwork(){
            Loader.shared.showLoader()
            let request = AuthRequest(email: LoginDetails().email, password: LoginDetails().password)
            authViewModel.authResponse(authRequest: request)
        } else {
            Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
        }
    }
    
    func showDoneButton(){
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dropKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.emailAddressTextField.inputAccessoryView = toolbar
    }
    @objc func dropKeyboard(){
        view.endEditing(true)
    }
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        showDoneButton()
        layoutViews()
        setupScrollView()
        self.hideKeyboardWhenTappedAround()
        authViewModel.delegate = self
        forgotPasswordViewModel.delegate = self
    }
    
    // MARK: - ... Validation of all textfield
    @objc func validateViews(_ textField: UITextField) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.validateEmail()
        }
    }
    // MARK: - ... Validation of email format

    func validateEmail() {
        guard let text = emailAddressTextField.text else { return }
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        let emailResult = emailPredicate.evaluate(with: text)
        if emailResult == true || emailAddressTextField.text == "" {
            emailValidationLabel.text = ""
            emailAddressTextField.layer.borderColor = UIColor.lightGray.cgColor
            emailAddressTextField.layer.borderWidth = 0
            sendEmailButton.isEnabled = true
            sendEmailButton.backgroundColor = AppColors.green.color
            
        } else {
            emailValidationLabel.text = "Please enter a valid email address"
            emailValidationLabel.textColor = AppColors.red.color
            emailAddressTextField.layer.borderColor = UIColor.red.cgColor
            emailAddressTextField.layer.borderWidth = 1
            sendEmailButton.isEnabled = false
            sendEmailButton.backgroundColor = AppColors.grayBorderColor.color
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    // MARK: - View did layout subviews
    override func viewDidLayoutSubviews() {
        scrollView.isScrollEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.contentSize = CGSize(width: 400, height: 300)
    }
}
