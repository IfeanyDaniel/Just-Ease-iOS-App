//
//  ForgotPasswordExtension.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/03/2023.
//

import Foundation
extension ForgotPasswordViewController: AuthViewModelDelegate,ForgotPasswordDelegate {
    func didReceiveForgotPasswordResponse(forgotPasswordResponse: ForgotPasswordResponse?) {
        if forgotPasswordResponse?.status == 200 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Sent successfully, Please check your mail", type: .success)
            let controller = ResetOtpViewController()
            Storage.savePassword(data: forgotPasswordResponse?.data?[0].password ?? "")
            Storage.saveUserEmail(data: forgotPasswordResponse?.data?[0].email ?? "")
            navigationController?.pushViewController(controller, animated: true)
        }
        if forgotPasswordResponse?.status  == 422 {
            Loader.shared.hideLoader()
            guard let errorMessage = forgotPasswordResponse?.message else {
                return
            }
            Toast.shared.showToastWithTItle(errorMessage, type: .error)
            
        }
    }
    
    func didReceiveAuthResponse(AuthResponse: AuthResponse?, _ statusCode: Int) {
        if statusCode == 200 {
            if InternetConnectionManager.isConnectedToNetwork(){
                guard let email = emailAddressTextField.text else {
                    return }
                Loader.shared.showLoader()
                forgotPasswordViewModel.getForgotPasswordResponse(email: email)
            }
            else {
                Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
            }
        }
        if statusCode == 400 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Something went wrong, Please try again", type: .error)
        }
    }
}
