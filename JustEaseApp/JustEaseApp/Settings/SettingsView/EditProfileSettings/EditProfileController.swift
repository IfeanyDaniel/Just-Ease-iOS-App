//
//  EditProfileController.swift
//  JustEaseApp
//
//  Created by iOSApp on 21/02/2023.
//


import UIKit
import Contacts
import FittedSheets
import GooglePlaces
import AVKit
import PhotosUI
typealias Parameters = [String: String]
struct Media {
    let key: String
    let filename: String
    let data: Data
    let mimeType: String // MimeType
    
    init?(withImage image: UIImage, forKey key: String) {
        self.key = key
        self.mimeType = "image/jpeg"
        self.filename = "photo\(arc4random()).jpeg"
        
        guard let data = image.jpegData(compressionQuality: 0.1) else {
            return nil
        }
        
        self.data = data
    }
}
class EditProfileController: UIViewController , UIGestureRecognizerDelegate, NextScreenDelegate,  UIImagePickerControllerDelegate,  UINavigationControllerDelegate {
    func showCamera() {
        proceedWithCameraAccess()
    }
    
    func showLibrary() {
        //
        getPhotoFromLibrary()
    }
    
    // MARK: - Scroll view
    var editProfileviewModel = EditProfileViewModel()
    var userProfileViewModel = UserProfileViewModel()
    var doneButton = UIBarButtonItem()
    var imagePickerForPhoto = UIImagePickerController()
    private var autocompleteViewController: GMSAutocompleteResultsViewController?
    func getPhoto() {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true)
    }
    func getPhotoFromLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            imagePickerForPhoto.delegate = self
            imagePickerForPhoto.sourceType = .photoLibrary
            imagePickerForPhoto.allowsEditing = false
            present(imagePickerForPhoto, animated: true, completion: nil)
        }
    }
    func proceedWithCameraAccess(){
        // handler in .requestAccess is needed to process user's answer to our request
        AVCaptureDevice.requestAccess(for: .video) { success in
            if success { // if request is granted (success is true)
                DispatchQueue.main.async {
                    self.getPhoto()
                }
            } else { // if request is denied (success is false)
                // Create Alert
                let alert = UIAlertController(title: "Camera", message: "Camera access is absolutely necessary to use this app", preferredStyle: .alert)
                
                // Add "OK" Button to alert, pressing it will bring you to the settings app
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                }))
                // Show the alert with animation
                self.present(alert, animated: true)
            }
        }
    }
    
    lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.showsHorizontalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    // MARK: - content view
    lazy var contentView: UIView = {
        let content = UIView()
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    
    lazy var editProfileLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Edit Profile"
        label.font = UIFont(name: AppFonts.silkabold.font, size: 25)
        label.textAlignment = .left
        return label
    }()
    lazy var profileImage:  UIImageView  = {
        let profileImageView =  UIImageView.customImage()
        profileImageView.image = UIImage(named: "dan")
        profileImageView.layer.cornerRadius = 7
        profileImageView.isHidden = true
        profileImageView.layer.cornerRadius = 40
        return profileImageView
    }()
    //MARK: - GESTURE TO PROFILE IMAGE
    func addGestureRecognizerOnProfileImage() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.didTapOnProfile))
        tapGesture.delegate = self
        view.isUserInteractionEnabled = true
        profileImage.addGestureRecognizer(tapGesture)
    }
    lazy var profileIcon: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 40
        button.isHidden = true
        button.setTitle(getUserInitial(), for: .normal)
        button.backgroundColor = AppColors.red.color
        button.addTarget(self, action: #selector(didTapOnProfile), for: .touchUpInside)
        return button
    }()
    @objc func didTapOnProfile() {
        popUploadSource()
    }
    func popUploadSource(){
        let controller = UploadSourceController()
        controller.delegate = self
        controller.label.text = "Select Option"
        let sheetController = SheetViewController(controller: controller, sizes: [.fixed(250), .fixed(250)])
        sheetController.blurBottomSafeArea = false
        sheetController.handleView.isHidden = true
        sheetController.topCornersRadius = 30
        present(sheetController, animated: false, completion: nil)
    }
    lazy var firstLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.textColor = placeholderSystemGrayColor
        label.text = "First Name"
        label.numberOfLines = 1
        return label
    }()
    lazy var firstNameTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.text = "John"
        textField.addTarget(self, action: #selector(validateViews(_:)), for: .editingChanged)
        return textField
    }()
    //MARK: - AlL VALIDATION LABEL
    lazy var firstNameValidationLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        return label
    }()
    
    lazy var lastNameLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.textColor = placeholderSystemGrayColor
        label.text = "Last Name"
        label.numberOfLines = 1
        return label
    }()
    lazy var lastNameTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.addTarget(self, action: #selector(validateViews(_:)), for: .editingChanged)
        textField.text = "Obi"
        return textField
    }()
    lazy var lastNameValidationLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        return label
    }()
    
    lazy var emailLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.textColor = placeholderSystemGrayColor
        label.text = "Email Address"
        label.numberOfLines = 1
        return label
    }()
    lazy var emailAddressTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.text = "ifeanyimbata3@gmail.com"
        textField.keyboardType = .emailAddress
        textField.addTarget(self, action: #selector(validateViews(_:)), for: .editingChanged)
        textField.autocapitalizationType = .none
        textField.autocorrectionType = .no
        return textField
    }()
    lazy var emailValidationLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.numberOfLines = 1
        return label
    }()
    
    lazy var phoneLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.textColor = placeholderSystemGrayColor
        label.text = "Phone Number"
        label.numberOfLines = 1
        return label
    }()
    lazy var phoneTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.keyboardType = .numberPad
        textField.text = "09039270990"
        return textField
    }()
    
    lazy var locationLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.textColor = placeholderSystemGrayColor
        label.text = "Location"
        label.numberOfLines = 1
        return label
    }()
    
    lazy var locationTextField: UIButton = {
        let button = UIButton.selectButtonDesign()
        button.setTitle("  Location", for: .normal)
        button.addTarget(self, action: #selector(didtapOnLocationButton), for: .touchUpInside)
        button.layer.borderColor = AppColors.lightGray.color.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 10
        button.backgroundColor = textFieldColor
        button.setTitleColor(textFieldTextColor, for: .normal)
        return button
    }()
    @objc func didtapOnLocationButton(_ sender: UIButton) {
        let autocompleteViewController = GMSAutocompleteResultsViewController()
        autocompleteViewController.delegate = self
        let searchController = UISearchController(searchResultsController: autocompleteViewController)
        searchController.searchResultsUpdater = autocompleteViewController
        searchController.searchBar.placeholder = "Search your location"
        present(searchController, animated: true, completion: nil)
    }
    lazy var setupButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Set up your panic recipient", for: .normal)
        button.layer.masksToBounds = true
        button.backgroundColor = backgroundSystemColor
        button.setTitleColor(AppColors.green.color, for: .normal)
        button.titleLabel?.font = UIFont(name: AppFonts.silka.font, size: 14)
        button.addTarget(self, action: #selector(didTapSetUpButton), for: .touchUpInside)
        return button
    }()
    @objc func didTapSetUpButton() {
        let controller = PanicSetUpController()
        let sheetController = SheetViewController(controller: controller, sizes: [.fixed(600), .fullScreen])
        sheetController.blurBottomSafeArea = false
        sheetController.handleView.isHidden = true
        sheetController.topCornersRadius = 30
        present(sheetController, animated: false, completion: nil)
    }
    // MARK: - Login function
    @objc func didTapForgetPasswordButton(){
        navigationController?.pushViewController(ResetOtpViewController(), animated: true)
    }
    
    func showDoneButton(){
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dropKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.firstNameTextField.inputAccessoryView = toolbar
        self.lastNameTextField.inputAccessoryView = toolbar
        self.emailAddressTextField.inputAccessoryView = toolbar
        self.phoneTextField.inputAccessoryView = toolbar
    }
    @objc func dropKeyboard(){
        view.endEditing(true)
    }
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        showDoneButton()
        layoutViews()
        setupScrollView()
        self.hideKeyboardWhenTappedAround()
        getUserDetails()
        addGestureRecognizerOnProfileImage()
        
    }
    func getUserDetails() {
        if InternetConnectionManager.isConnectedToNetwork(){
            Loader.shared.showLoader()
            userProfileViewModel.getUserProfileData(userID: Storage.getUserId())
            userProfileViewModel.delegate = self
        } else {
            Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
        }
    }
    func saveUserEditInput() {
        //        editProfileviewModel.delegate = self
        //        if InternetConnectionManager.isConnectedToNetwork(){
        //            guard let firstName = firstNameTextField.text, let lastName = lastNameTextField.text, let phoneNumber = phoneTextField.text , let email = emailAddressTextField.text else {
        //                return }
        //            Loader.shared.showLoader()
        //            print("Place name:\(Storage.getUserLocation())")
        //            let request = EditProfileRequest(first_name: firstName, last_name: lastName, email: email, phone_number: phoneNumber, address: Storage.getUserLocation())
        //            editProfileviewModel.getEditProfileResponse(userID: Storage.getUserId(), editProfileRequest: request)
        //        }
        //        else {
        //            Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
        //        }
        
        Loader.shared.showLoader()
        let parameters = [
            "first_name": firstNameTextField.text ?? "",
            "last_name": lastNameTextField.text ?? "",
            "email": emailAddressTextField.text ?? "",
            "phone_number": phoneTextField.text ?? "",
            "address": Storage.getUserLocation(),
            "image_url": "testImage000001"
        ]
        
        let image = profileImage.image
        
        guard let media = Media(withImage: (image ?? UIImage(named: ""))!, forKey: "image_url") else {
            return
        }
        
        guard let url = URL(string: ApiEndpoints.editProfileEndPoint + "/\(Storage.getUserId())") else {
            return
        }
        
        var request = URLRequest(url: url)
        let boundary = generateBoundary()
        
        request.httpMethod = "POST"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.addValue(getauthorizationKey(), forHTTPHeaderField: "Authorization")
        
        let dataBody = createDataBody(withParameters: parameters, media: [media], boundary: boundary)
        request.httpBody = dataBody
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print("?????\(response)")
            }
            
            if let data = data {
                do {
                    let jsonResponse = try JSONSerialization.jsonObject(with: data, options: [])
                    print("++++++++\(jsonResponse)")
                    DispatchQueue.main.async {
                        Loader.shared.hideLoader()
                        self.pop(numberOfTimes: 2)
                        Toast.shared.showToastWithTItle("Updated successfully", type: .success)
                    }
                    
                } catch {
                    print(error)
                }
            }
        }.resume()
    }
    // MARK: - Factories
    
    func generateBoundary() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func createDataBody(withParameters parameters: Parameters?, media: [Media]?, boundary: String) -> Data {
        
        let lineBreak = "\r\n"
        var body = Data()
        
        if let parameters = parameters {
            for (key, value) in parameters {
                body.append("--\(boundary + lineBreak)")
                body.append("Content-Disposition: form-data; name=\"\(key)\"\(lineBreak + lineBreak)")
                body.append("\(value + lineBreak)")
            }
        }
        
        if let media = media {
            for photo in media {
                body.append("--\(boundary + lineBreak)")
                body.append("Content-Disposition: form-data; name=\"\(photo.key)\"; filename=\"\(photo.filename)\"\(lineBreak)")
                body.append("Content-Type: \(photo.mimeType + lineBreak + lineBreak)")
                body.append(photo.data)
                body.append(lineBreak)
            }
        }
        
        body.append("--\(boundary)--\(lineBreak)")
        
        return body
    }
    @objc func goBack() {
        saveUserEditInput()
    }
    @objc  func didTapOnCancel() {
        navigationController?.popViewController(animated: true)
    }
    @objc func validateViews(_ textField: UITextField) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.validateFirstNameField()
            self.validateLastNameField()
            self.validateEmail()
        }
    }
    func pop(numberOfTimes: Int) {
        guard let navigationController = navigationController else {
            print("No navigation controller")
            return
        }
        let viewControllers = navigationController.viewControllers
        let index = numberOfTimes + 1
        if viewControllers.count >= index {
            navigationController.popToViewController(viewControllers[viewControllers.count - index], animated: true)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(didTapOnCancel))
        
        doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.rightBarButtonItem = doneButton
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
        navigationItem.rightBarButtonItem?.tintColor = textSystemColor
        requestAccess { accessGranted in
            print(accessGranted)
        }
    }
    
    func requestAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            completionHandler(true)
        case .denied:
            showSettingsAlert(completionHandler)
        case .restricted, .notDetermined:
            CNContactStore().requestAccess(for: .contacts) { granted, error in
                if granted {
                    completionHandler(true)
                } else {
                    DispatchQueue.main.async {
                        self.showSettingsAlert(completionHandler)
                    }
                }
            }
        @unknown default:
            fatalError()
        }
    }
    private func showSettingsAlert(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let alert = UIAlertController(title: nil, message: "This app requires access to Contacts to proceed. Go to Settings to grant access.", preferredStyle: .alert)
        if
            let settings = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(settings) {
            alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { action in
                completionHandler(false)
                UIApplication.shared.open(settings)
            })
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
            completionHandler(false)
        })
        present(alert, animated: true)
    }
    // MARK: - View did layout subviews
    override func viewDidLayoutSubviews() {
        scrollView.isScrollEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.contentSize = CGSize(width: 400, height: 750)
    }
    func getauthorizationKey() -> String {
        let token = "\(Storage.getUserToken())"
        let authorizationKey = "\(Storage.getTokenType()) ".appending(token)
        return authorizationKey
    }
    
}

extension EditProfileController : GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    //  GMSAutocompleteViewControllerDelegate {
    // Handle the user's selection.
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didAutocompleteWith place: GMSPlace) {
        guard let place = place.formattedAddress else {
            return
        }
        Storage.saveUserEditLocation(data: place)
        
        locationTextField.setTitle("  \(place)", for: .normal)
        locationTextField.setTitleColor(textFieldTextColor, for: .normal)
        print("Place name: \(place) \(Storage.getUserLocation())")
        //        print("Place ID: \(String(describing: place.placeID))")
        //        print("Place attributions: \(String(describing: place.attributions))")
        dismiss(animated: true, completion: nil)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    func convertImageToData(imageFilePath: String) {
        guard let imagefilePath = URL(string: imageFilePath) else {
            return
        }
        do {
            let imageData = try Data(contentsOf: imagefilePath)
            print(">>>>\(imageData)")
        }
        catch {
            // Handle error reading image data from file
            print("Error reading image data: \(error)")
        }
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            profileImage.isHidden = false
            profileImage.image = image
            profileIcon.isHidden = true
            // MARK: - getting and checking the image size
            //            let imgData = NSData(data: image.jpegData(compressionQuality: 1)!)
            //            let imageSize: Int = imgData.count
            //            let actualImageSize =  Double(imageSize) / 1024.0 / 1024.0 /  1024.0
            
            //            // MARK: - getting and checking the extension of the image
            //            guard let assetPath = info[UIImagePickerController.InfoKey.imageURL] as? NSURL else {
            //               return
            //            }
            //            print("???\(assetPath)")
            //            print("library-------\(convertImageToData(imageFilePath: "\(assetPath)"))")
            //            let jpegfileExtension = (assetPath.absoluteString?.hasSuffix("jpeg"))!
            //            let jpgfileExtension = (assetPath.absoluteString?.hasSuffix("jpg"))!
            //            let pngExtension =  (assetPath.absoluteString?.hasSuffix("png"))!
            //            let pdfExtension =  (assetPath.absoluteString?.hasSuffix("pdf"))!
            //
            //            print("actual size of image in kb ", actualImageSize )
            //
            //            if  actualImageSize > 3.0 || (jpgfileExtension && jpegfileExtension && pngExtension && pdfExtension) {
            //                Toast.shared.showToastWithTItle("Please select a jpeg or png image not more than 3mb image", type: .error)
            //            } else {
            //                profileImage.isHidden = false
            //                profileImage.image = image
            //                profileIcon.isHidden = true
            //
            //            }
        }
    }
    
}
extension Data {
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}
