//
//  EditProfileConstriant.swift
//  JustEaseApp
//
//  Created by iOSApp on 21/02/2023.
//

import UIKit
extension EditProfileController {
    //MARK: - Layout subviews for the scroll views
    func setupScrollView() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: profileIcon.bottomAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
        ])
    }
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(editProfileLabel)
        view.addSubview(profileIcon)
        view.addSubview(profileImage)
    
        contentView.addSubview(firstNameTextField)
        firstNameTextField.addSubview(firstLabel)
        contentView.addSubview(firstNameValidationLabel)
        
        contentView.addSubview(lastNameTextField)
        lastNameTextField.addSubview(lastNameLabel)
        contentView.addSubview(lastNameValidationLabel)
        
        contentView.addSubview(emailAddressTextField)
        emailAddressTextField.addSubview(emailLabel)
        contentView.addSubview(emailValidationLabel)
        
        contentView.addSubview(phoneTextField)
        phoneTextField.addSubview(phoneLabel)
        
        contentView.addSubview(locationTextField)
        locationTextField.addSubview(locationLabel)
        
        contentView.addSubview(setupButton)
       
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = .systemBackground
        
        NSLayoutConstraint.activate([
            editProfileLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            editProfileLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber),
    
            profileIcon.topAnchor.constraint(equalTo: editProfileLabel.bottomAnchor, constant: 30),
            profileIcon.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            profileIcon.heightAnchor.constraint(equalToConstant: 80),
            profileIcon.widthAnchor.constraint(equalToConstant: 80),
            
            profileImage.topAnchor.constraint(equalTo: editProfileLabel.bottomAnchor, constant: 30),
            profileImage.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            profileImage.heightAnchor.constraint(equalToConstant: 80),
            profileImage.widthAnchor.constraint(equalToConstant: 80),
            
            firstNameTextField.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
            firstNameTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: leadingNumber),
            firstNameTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: trailingNumber),
            firstNameTextField.heightAnchor.constraint(equalToConstant: textFieldHeight),

            firstLabel.topAnchor.constraint(equalTo: firstNameTextField.topAnchor, constant: 5),
            firstLabel.leadingAnchor.constraint(equalTo:  firstNameTextField.leadingAnchor, constant: 10),
            
            firstNameValidationLabel.topAnchor.constraint(equalTo: firstNameTextField.bottomAnchor, constant: 2),
            firstNameValidationLabel.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: leadingNumber),
            
            lastNameTextField.topAnchor.constraint(equalTo: firstNameTextField.bottomAnchor, constant: 20),
            lastNameTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: leadingNumber),
            lastNameTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: trailingNumber),
            lastNameTextField.heightAnchor.constraint(equalToConstant: textFieldHeight),

            lastNameLabel.topAnchor.constraint(equalTo: lastNameTextField.topAnchor, constant: 5),
            lastNameLabel.leadingAnchor.constraint(equalTo:  lastNameTextField.leadingAnchor, constant: 10),
            
            lastNameValidationLabel.topAnchor.constraint(equalTo: lastNameTextField.bottomAnchor, constant: 2),
            lastNameValidationLabel.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: leadingNumber),
            
            emailAddressTextField.topAnchor.constraint(equalTo: lastNameTextField.bottomAnchor, constant: 20),
            emailAddressTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: leadingNumber),
            emailAddressTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: trailingNumber),
            emailAddressTextField.heightAnchor.constraint(equalToConstant: textFieldHeight),
            
            emailValidationLabel.topAnchor.constraint(equalTo: emailAddressTextField.bottomAnchor, constant: 2),
            emailValidationLabel.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: leadingNumber),

            emailLabel.topAnchor.constraint(equalTo: emailAddressTextField.topAnchor, constant: 5),
            emailLabel.leadingAnchor.constraint(equalTo: emailAddressTextField.leadingAnchor, constant: 10),
            
            phoneTextField.topAnchor.constraint(equalTo: emailAddressTextField.bottomAnchor, constant: 20),
            phoneTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: leadingNumber),
            phoneTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: trailingNumber),
            phoneTextField.heightAnchor.constraint(equalToConstant: textFieldHeight),

            phoneLabel.topAnchor.constraint(equalTo: phoneTextField.topAnchor, constant: 5),
            phoneLabel.leadingAnchor.constraint(equalTo: phoneTextField.leadingAnchor, constant: 10),
            
            locationTextField.topAnchor.constraint(equalTo: phoneTextField.bottomAnchor, constant: 20),
            locationTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: leadingNumber),
            locationTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: trailingNumber),
            locationTextField.heightAnchor.constraint(equalToConstant: textFieldHeight),

            locationLabel.topAnchor.constraint(equalTo: locationTextField.topAnchor, constant: 5),
            locationLabel.leadingAnchor.constraint(equalTo: locationTextField.leadingAnchor, constant: 10),
        
            setupButton.topAnchor.constraint(equalTo: locationTextField.bottomAnchor, constant: 40),
            setupButton.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: leadingNumber),
            setupButton.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: trailingNumber),
            setupButton.heightAnchor.constraint(equalToConstant: buttonHeight),

            
        ])
    }
}
