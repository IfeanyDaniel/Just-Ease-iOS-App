//
//  EditProfileExtension.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/3/23.
//

import Foundation
import Kingfisher
extension EditProfileController:  UserProfileDelegate , EditProfileViewModelDelegate {
    func didReceiveUserProfileResponse(userProileResponse: LoginResponse?) {
        if userProileResponse?.status ==  200 {
            Loader.shared.hideLoader()
            firstNameTextField.text = userProileResponse?.data?.first_name
            lastNameTextField.text = userProileResponse?.data?.last_name
            emailAddressTextField.text = userProileResponse?.data?.email
            locationTextField.setTitle("  \(userProileResponse?.data?.address ?? "")", for: .normal)
            phoneTextField.text = userProileResponse?.data?.phone_number
            let img = userProileResponse?.data?.image_url ?? ""
            print("+++++\(userProileResponse?.data?.image_url ?? "")")
            if img  == "" || img  == "https://kmr-staging.lawpavilion.com/storage/images/apiusers/" {
                profileImage.isHidden = true
                profileIcon.isHidden = false
            } else {
                profileImage.isHidden = false
                profileImage.kf.setImage(with: URL(string: "\(img)"))
            }
        }
        if userProileResponse?.status ==  422 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Something went wrong", type: .error)
        }
    }
    
    func didReceiveEditProfileResponse(editProfileResponse: LoginResponse?, _ statusCode: Int) {
        if editProfileResponse?.status ==  200 {
            Loader.shared.hideLoader()
            pop(numberOfTimes: 2)
            Toast.shared.showToastWithTItle("Updated successfully", type: .success)
        }
        if editProfileResponse?.status == 422 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Something went wrong, Please try again", type: .error)
        }
        if editProfileResponse?.status == 400 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Something went wrong, Please try again", type: .error)
        }
    }
    
    
}
