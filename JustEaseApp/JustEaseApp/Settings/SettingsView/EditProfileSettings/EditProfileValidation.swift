//
//  EditProfileValidation.swift
//  JustEaseApp
//
//  Created by iOSApp on 21/02/2023.
//


import UIKit
extension EditProfileController {
    // MARK: - ... Validation of first name text field
    func validateFirstNameField() {
        let text = firstNameTextField.text
        let nameRegEx  = "^(?=.{2,100}$)[A-Za-zÀ-ú][A-Za-zÀ-ú'-]+(?: [A-Za-zÀ-ú'-]+)* *$"
        let textTest = NSPredicate(format: "SELF MATCHES %@", nameRegEx)
        let firstnameResult = textTest.evaluate(with: text)
        if (firstnameResult == true && text?.contains(" ") == false) || firstNameTextField.text == "" {
            firstNameValidationLabel.text = ""
            firstNameTextField.layer.borderColor = AppColors.lightGray.color.cgColor
            firstNameTextField.layer.borderWidth = 1
            doneButton.isEnabled = true
        } else {
            firstNameValidationLabel.text = "Name must be more than 2 character with no space "
            firstNameValidationLabel.textColor = AppColors.red.color
            firstNameTextField.layer.borderColor = UIColor.red.cgColor
            firstNameTextField.layer.borderWidth = 1
            doneButton.isEnabled = false
        }
    }
    // MARK: - ... Validation of last name text field
    func validateLastNameField() {
        let text = lastNameTextField.text
        let nameRegEx  = "^(?=.{2,100}$)[A-Za-zÀ-ú][A-Za-zÀ-ú'-]+(?: [A-Za-zÀ-ú'-]+)* *$"
        let textTest = NSPredicate(format: "SELF MATCHES %@",nameRegEx)
        let lastnameResult = textTest.evaluate(with: text)
        if (lastnameResult == true && text?.contains(" ") == false) || lastNameTextField.text == ""{
            lastNameValidationLabel.text = ""
            lastNameTextField.layer.borderColor = AppColors.lightGray.color.cgColor
            lastNameTextField.layer.borderWidth = 1
            doneButton.isEnabled = true
        } else {
            lastNameValidationLabel.text = "Name must be more than 2 character with no space "
            lastNameValidationLabel.textColor = AppColors.red.color
            lastNameTextField.layer.borderColor = UIColor.red.cgColor
            lastNameTextField.layer.borderWidth = 1
            doneButton.isEnabled = false
        }
    }
    // MARK: - ... Validation of email format

    func validateEmail() {
        guard let text = emailAddressTextField.text else { return }
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        let emailResult = emailPredicate.evaluate(with: text)
        if emailResult == true || emailAddressTextField.text == "" {
            emailValidationLabel.text = ""
            emailAddressTextField.layer.borderColor = AppColors.lightGray.color.cgColor
            emailAddressTextField.layer.borderWidth = 1
            doneButton.isEnabled = true
        } else {
            emailValidationLabel.text = "Please enter a valid email address"
            emailValidationLabel.textColor = AppColors.red.color
            emailAddressTextField.layer.borderColor = UIColor.red.cgColor
            emailAddressTextField.layer.borderWidth = 1
            doneButton.isEnabled = false
        }
    }
    
}

