//
//  FetchPanicContactResponse.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/14/23.
//

import Foundation
struct PanicContactsResponse: Decodable {
    let status: Int
    let message: String
    let data: [PanicContactInfo]
    
}
struct PanicContactInfo: Decodable {
    let id: Int
    let user_id: Int
    let name: String
    let phone_number:  String?
    let email:  String?
    
}
