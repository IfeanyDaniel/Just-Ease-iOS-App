//
//  AddContactModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/14/23.
//

import Foundation
struct AddContactRequest: Encodable {
   let user_id: Int
   let name: String
   let phone_number: String
   let email: String
}


