//
//  AddContactResponse.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/14/23.
//

import Foundation
struct AddContactResponse: Decodable {
    let status: Int
    let message: String
    let data: ContactInfo?
}
struct ContactInfo: Decodable {
    let name: String
}
