//
//  PanicSetupConstraints.swift
//  JustEaseApp
//
//  Created by iOSApp on 09/03/2023.
//

import UIKit
extension PanicSetUpController  {
    //MARK: - Layout subviews
    func layoutViews() {
        view.backgroundColor =  viewStackColor
        view.addSubview(panicLabel)
        view.addSubview(nameTextField)
        view.addSubview(emailAddressTextField)
        view.addSubview(phoneTextField)
        view.addSubview(emailValidationLabel)
        view.addSubview(chooseContactButton)
        view.addSubview(inviteButton)
        view.addSubview(contactCollectionView)
       
        NSLayoutConstraint.activate([
            panicLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 30),
            panicLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber),
    
            nameTextField.topAnchor.constraint(equalTo: panicLabel.bottomAnchor, constant: 15),
            nameTextField.leadingAnchor.constraint(equalTo:  view.leadingAnchor, constant: leadingNumber),
            nameTextField.trailingAnchor.constraint(equalTo:  view.trailingAnchor, constant: trailingNumber),
            nameTextField.heightAnchor.constraint(equalToConstant: textFieldHeight),
            
            phoneTextField.topAnchor.constraint(equalTo: nameTextField.bottomAnchor, constant: 15),
            phoneTextField.leadingAnchor.constraint(equalTo:  view.leadingAnchor, constant: leadingNumber),
            phoneTextField.trailingAnchor.constraint(equalTo:  view.trailingAnchor, constant: trailingNumber),
            phoneTextField.heightAnchor.constraint(equalToConstant: textFieldHeight),
            
            emailAddressTextField.topAnchor.constraint(equalTo: phoneTextField.bottomAnchor, constant: 15),
            emailAddressTextField.leadingAnchor.constraint(equalTo:  view.leadingAnchor, constant: leadingNumber),
            emailAddressTextField.trailingAnchor.constraint(equalTo:  view.trailingAnchor, constant: trailingNumber),
            emailAddressTextField.heightAnchor.constraint(equalToConstant: textFieldHeight),
            
            chooseContactButton.topAnchor.constraint(equalTo: emailAddressTextField.bottomAnchor, constant: 0),
            chooseContactButton.trailingAnchor.constraint(equalTo:  view.trailingAnchor, constant: trailingNumber),
            chooseContactButton.heightAnchor.constraint(equalToConstant: textFieldHeight),
            chooseContactButton.widthAnchor.constraint(equalToConstant: 150),
            
            emailValidationLabel.topAnchor.constraint(equalTo: emailAddressTextField.bottomAnchor, constant: 2),
            emailValidationLabel.leadingAnchor.constraint(equalTo:  view.leadingAnchor, constant: leadingNumber),

            inviteButton.topAnchor.constraint(equalTo: view.bottomAnchor, constant: -80),
            inviteButton.leadingAnchor.constraint(equalTo:  view.leadingAnchor, constant: leadingNumber),
            inviteButton.trailingAnchor.constraint(equalTo:  view.trailingAnchor, constant: trailingNumber),
            inviteButton.heightAnchor.constraint(equalToConstant: buttonHeight),

            
        ])
        contactCollectionView.anchorWithConstantsToTop(chooseContactButton.bottomAnchor,
                                                       left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 100, rightConstant: 20)
    }
}
