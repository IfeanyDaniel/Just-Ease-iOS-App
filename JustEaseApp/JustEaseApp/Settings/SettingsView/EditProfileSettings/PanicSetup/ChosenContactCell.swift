//
//  ChosenContactCell.swift
//  JustEaseApp
//
//  Created by iOSApp on 09/03/2023.
//

import UIKit
protocol ChosenContactDelegate: AnyObject {
    func onClickDeleteButton(index: IndexPath)
}
class ChosenContactCell: UICollectionViewCell {
    static var identifier: String = "settingsCell"
    var chosenContactDelegate: ChosenContactDelegate?
    var index : IndexPath?

    lazy var deleteIcon: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 10
        button.setImage(UIImage(systemName: "xmark.circle.fill"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = AppColors.black.color
        button.addTarget(self, action: #selector(clickContactForwardButton(_:)), for: .touchUpInside)
        return button

    }()
    lazy var cardLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Ifeanyi"
        label.textAlignment = .left
        label.textColor = AppColors.black.color
        label.font = UIFont(name: AppFonts.silkabold.font ,size: 12)
        return label
    }()
 
    @objc func clickContactForwardButton(_ sender: Any){
        chosenContactDelegate?.onClickDeleteButton(index: index!)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpViews() {
        addSubview(deleteIcon)
        addSubview(cardLabel)
        NSLayoutConstraint.activate([
            deleteIcon.topAnchor.constraint(equalTo: topAnchor, constant: 4),
            deleteIcon.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            deleteIcon.heightAnchor.constraint(equalToConstant: 20),
            deleteIcon.widthAnchor.constraint(equalToConstant: 20),
            
            cardLabel.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            cardLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),

            
        ])
    }
}
