//
//  ChosenContactCollectionView.swift
//  JustEaseApp
//
//  Created by iOSApp on 09/03/2023.
//

import UIKit
extension PanicSetUpController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  panicContactViewModel.numberOfRowsInSection(section: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell =  contactCollectionView.dequeueReusableCell(withReuseIdentifier: ChosenContactCell.identifier, for: indexPath) as? ChosenContactCell else { return UICollectionViewCell() }
        let panicContact = panicContactViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
        cell.chosenContactDelegate = self
        cell.backgroundColor = AppColors.lightPurple.color
        cell.cardLabel.text = panicContact.name
        cell.layer.cornerRadius = 10
        cell.index = indexPath
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.size.width / 2 - 40, height: 30)
    }
}
