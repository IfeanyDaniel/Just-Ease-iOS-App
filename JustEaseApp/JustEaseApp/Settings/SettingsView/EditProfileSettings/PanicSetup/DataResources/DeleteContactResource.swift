//
//  DeleteContactResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/15/23.
//

import Foundation
struct DeletePanicContactResource {
    func gteDeletetPanicContact(contactId: Int, completionHandler: @escaping (_ result: DeleteContactResponse?, _ statusCode: Int) -> Void) {
        let httpUtility = HTTPUtility()
        let panicContactUrl = ApiEndpoints.fetchPanicContactEndPoint + "/\(contactId)"
        print(">>>\(panicContactUrl)")
        let url = URL(string: panicContactUrl)!
        do {
            httpUtility.deleteAPIData(requestURL: url, resultType: DeleteContactResponse.self) { result, statusCode in
                completionHandler(result, statusCode)
            }
        }
    }
}
