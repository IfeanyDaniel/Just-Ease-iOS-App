//
//  AddContactResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/14/23.
//

import Foundation
struct AddContactResource {
    func getResponse(addContactRequest: AddContactRequest, completionHandler: @escaping (_ result:  AddContactResponse?,_ statusCode: Int) -> Void) {
        let url = ApiEndpoints.addContactEndPoint
        let addContactUrl = URL(string: url)!
        let httpUtility = HTTPUtility()
        do {
            let addContactPostBody = try JSONEncoder().encode(addContactRequest)
            httpUtility.postAuthorizationResponse(requestUrl: addContactUrl , requestBody: addContactPostBody, resultType: AddContactResponse.self) { addContactApiResponse, statusCode in
                completionHandler(addContactApiResponse,statusCode)
                print(">>>\(addContactApiResponse)")
            }
        } catch let error {
            debugPrint(error)
        }
    }
}
