//
//  FetchPanicContactResponse.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/14/23.
//

import Foundation
struct PanicContactResource {
    func getPanicContact(userId: Int, completionHandler: @escaping (_ result: PanicContactsResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let panicContactUrl = ApiEndpoints.fetchPanicContactEndPoint + "?user_id=\(userId)"
        print(">>>\(panicContactUrl)")
        let url = URL(string: panicContactUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType:  PanicContactsResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}
