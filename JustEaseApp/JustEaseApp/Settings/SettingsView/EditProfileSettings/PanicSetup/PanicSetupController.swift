//
//  PanicSetupController.swift
//  JustEaseApp
//
//  Created by iOSApp on 09/03/2023.
//

import UIKit
import ContactsUI
//protocol MoveToNextScreenDelegate: AnyObject {
//    func goToLawyerScreen()
//    func goToPoliceScreen()
//    func goToReportCrimeScreen()
//    func goToReportViolationScreen()
//}
class PanicSetUpController: UIViewController {
    //var delegate : MoveToNextScreenDelegate?
    var panicContactViewModel = PanicContactViewModel()
    var deleteContactViewModel = DeleteContactViewModel()
    var addContactViewModel = AddContactViewModel()
    lazy var panicLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Panic Setup"
        label.font = UIFont(name: AppFonts.silkabold.font, size: 20)
        label.textAlignment = .left
        return label
    }()
    
    lazy var nameTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Name",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        return textField
    }()
    
    lazy var emailAddressTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Email address",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.keyboardType = .emailAddress
        textField.addTarget(self, action: #selector(validateViews(_:)), for: .editingChanged)
        textField.autocapitalizationType = .none
        textField.autocorrectionType = .no
        return textField
    }()
    lazy var phoneTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Phone number",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.keyboardType = .numberPad
        return textField
    }()
    
    lazy var emailValidationLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.numberOfLines = 1
        return label
    }()
    @objc func validateViews(_ textField: UITextField) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.validateEmail()
        }
    }
    
    lazy var chooseContactButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Choose Contact", for: .normal)
        button.layer.masksToBounds = true
        button.backgroundColor = viewStackColor
        button.setTitleColor(AppColors.green.color, for: .normal)
        button.titleLabel?.font = UIFont(name: AppFonts.silka.font, size: 12)
        button.addTarget(self, action: #selector(didTapChooseContactButton), for: .touchUpInside)
        return button
    }()
    lazy var contactCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        cv.backgroundColor = viewStackColor
        cv.dataSource = self
        cv.delegate = self
        cv.showsVerticalScrollIndicator = false
        cv.isPagingEnabled = true
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    lazy var inviteButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Invite", for: .normal)
        button.layer.masksToBounds = true
        button.titleLabel?.font = UIFont(name: AppFonts.silka.font, size: 18)
        button.addTarget(self, action: #selector(didTapInviteButton), for: .touchUpInside)
        return button
    }()
   
    @objc func didTapChooseContactButton() {
        let contactPicker = CNContactPickerViewController()
                contactPicker.delegate = self
                contactPicker.displayedPropertyKeys = [CNContactPhoneNumbersKey]
                contactPicker.predicateForEnablingContact = NSPredicate(format: "phoneNumbers.@count > 0")
                contactPicker.predicateForSelectionOfContact = NSPredicate(format: "phoneNumbers.@count == 1")
                contactPicker.predicateForSelectionOfProperty = NSPredicate(format: "key == 'phoneNumbers'")
                self.present(contactPicker, animated: true, completion: nil)
    }
    @objc func didTapInviteButton() {
        if InternetConnectionManager.isConnectedToNetwork(){
            guard let fullname = nameTextField.text, let phone = phoneTextField.text , let email = emailAddressTextField.text else {
                return }
            if email != "" {
                Loader.shared.showLoader()
                let request = AddContactRequest(user_id: Storage.getUserId(), name: fullname, phone_number: phone, email: email)
                addContactViewModel.getAddContactResponse(addContactRequest: request)
            } else {
                Toast.shared.showToastWithTItle("Email address is required", type: .error)
            }
           
        }
        else {
            Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
        }
    }
    func validateEmail() {
        guard let text = emailAddressTextField.text else { return }
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        let emailResult = emailPredicate.evaluate(with: text)
        if emailResult == true || emailAddressTextField.text == "" {
            emailValidationLabel.text = ""
            emailAddressTextField.layer.borderColor = AppColors.lightGray.color.cgColor
            emailAddressTextField.layer.borderWidth = 1
            inviteButton.isEnabled = true
        } else {
            emailValidationLabel.text = "Please enter a valid email address"
            emailValidationLabel.textColor = AppColors.red.color
            emailAddressTextField.layer.borderColor = UIColor.red.cgColor
            emailAddressTextField.layer.borderWidth = 1
            inviteButton.isEnabled = false
        }
    }
 
    func showDoneButton(){
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dropKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.nameTextField.inputAccessoryView = toolbar
        self.phoneTextField.inputAccessoryView = toolbar
        self.emailAddressTextField.inputAccessoryView = toolbar
    }
    @objc func dropKeyboard(){
        view.endEditing(true)
    }
    override func viewDidLoad() {
        layoutViews()
        showDoneButton()
        hideKeyboardWhenTappedAround()
        registercCell()
        getAllPanicContact()
        deleteContactViewModel.delegate = self
        addContactViewModel.delegate = self
    }
    func getAllPanicContact() {
        Loader.shared.showLoader()
        panicContactViewModel.getPanicContact(userId: Storage.getUserId()) {
            Loader.shared.hideLoader()
            self.contactCollectionView.reloadData()
        }
    }
 
    func registercCell() {
        contactCollectionView.register(ChosenContactCell.self, forCellWithReuseIdentifier: ChosenContactCell.identifier)
    }
}
extension PanicSetUpController: CNContactPickerDelegate , ChosenContactDelegate{
    func onClickDeleteButton(index: IndexPath) {
        let panicContact = panicContactViewModel.cellForRowsAt(indexPath: index)[index.row]
        print(panicContact.id)
        Loader.shared.showLoader()
        deleteContactViewModel.getDeleteContactResponse(contactId: panicContact.id)
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        let firstName = contact.givenName
        let lastName = contact.familyName
        let phone = contact.phoneNumbers.first?.value.stringValue
            phoneTextField.text = phone
            nameTextField.text = "\(firstName) \(lastName)"

    }
}
extension PanicSetUpController: DeleteContactViewModelDelegate, AddContactViewModelDelegate {
    func didReceiveAddContactResponse(addContactResponse: AddContactResponse?, _ statusCode: Int) {
        if statusCode == 200 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("\(addContactResponse?.data?.name ?? "") has been successfully added to your panic contact", type: .success)
            getAllPanicContact()
        }
        if statusCode == 400 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle(addContactResponse?.message ?? "", type: .error)
        }
        if statusCode == 422 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle(addContactResponse?.message ?? "", type: .error)
        }
    }
    
    func didReceiveDeleteContactResponse(addContactResponse: DeleteContactResponse?, _ statusCode: Int) {
        if statusCode == 200 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Deleted successfully", type: .success)
            getAllPanicContact()
        }
        if statusCode == 400 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("An Error occured", type: .error)
        }
    }
}
