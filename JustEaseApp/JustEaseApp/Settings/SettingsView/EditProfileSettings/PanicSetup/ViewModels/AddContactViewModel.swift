//
//  AddContactViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/14/23.
//

import Foundation
protocol AddContactViewModelDelegate {
    func didReceiveAddContactResponse(addContactResponse: AddContactResponse?,_ statusCode: Int)
}
class AddContactViewModel {
    var delegate: AddContactViewModelDelegate?
    func getAddContactResponse(addContactRequest: AddContactRequest) {
        let addContactResource = AddContactResource()
        addContactResource.getResponse(addContactRequest: addContactRequest) { addContactApiResponse, statusCode in
            if statusCode ==  200 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveAddContactResponse(addContactResponse: addContactApiResponse, statusCode)
                }
            }
            if statusCode ==  400 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveAddContactResponse(addContactResponse: addContactApiResponse, statusCode)
                }
            }
            if statusCode ==  500 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveAddContactResponse(addContactResponse: addContactApiResponse, statusCode)
                }
            }
            if statusCode ==  422 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveAddContactResponse(addContactResponse: addContactApiResponse, statusCode)
                }
            }
        }
    }
}
