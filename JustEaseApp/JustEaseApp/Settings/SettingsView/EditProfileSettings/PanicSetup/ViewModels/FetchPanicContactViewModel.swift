//
//  FetchPanicContactViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/14/23.
//
import Foundation
struct PanicContact: Codable {
    var id: Int
    var name: String
}

class PanicContactViewModel {
    var panicContact: [PanicContact] = []
    func getPanicContact(userId: Int, completion: @escaping () -> Void) {
        let panicContactResource = PanicContactResource()
        panicContactResource.getPanicContact(userId: userId) {fetchPanicContactApiResponse in
            DispatchQueue.main.async { [self] in
                 let panicContactData = fetchPanicContactApiResponse?.data
                panicContact.removeAll()
                for index in 0..<(panicContactData?.count ?? 0) {
                    let contactId = panicContactData?[index].id ?? 0
                    let contactName = panicContactData?[index].name  ?? ""
                    panicContact.append(PanicContact(id: contactId, name: contactName))
                }
                completion()
            }
        }
    }
    func numberOfRowsInSection(section: Int) -> Int {
        return  panicContact.count
    }
    func cellForRowsAt(indexPath: IndexPath) -> [PanicContact] {
        return panicContact
    }
}
