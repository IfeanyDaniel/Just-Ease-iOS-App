//
//  DeleteContactViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/15/23.
//

import Foundation
protocol DeleteContactViewModelDelegate {
    func didReceiveDeleteContactResponse(addContactResponse: DeleteContactResponse?,_ statusCode: Int)
}
class DeleteContactViewModel {
    var delegate:  DeleteContactViewModelDelegate?
    func getDeleteContactResponse(contactId: Int) {
        let deleteContactResource = DeletePanicContactResource()
        deleteContactResource.gteDeletetPanicContact(contactId: contactId) { deleteConactApiResponse, statucCode  in
            if deleteConactApiResponse?.status == 200 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveDeleteContactResponse(addContactResponse: deleteConactApiResponse, statucCode)
                }
            }
            if deleteConactApiResponse?.status ?? 0 > 399 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveDeleteContactResponse(addContactResponse: deleteConactApiResponse, statucCode)
                }
            }
        }
    }
}
