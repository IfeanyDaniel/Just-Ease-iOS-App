//
//  SettingsController.swift
//  JustEaseApp
//
//  Created by iOSApp on 15/02/2023.
//

import UIKit
import SafariServices
import Kingfisher
class SettingsController: UIViewController, UserProfileDelegate, AuthViewModelDelegate   {
    var authViewModel = AuthViewModel()
    var userProfileViewModel = UserProfileViewModel()
    lazy var settingsLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Settings"
        label.textColor = AppColors.green.color
        label.font = UIFont(name: AppFonts.silkabold.font, size: 25)
        label.textAlignment = .left
        return label
    }()
    lazy var accountLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Account"
        label.font = UIFont(name: AppFonts.silkabold.font, size: 18)
        label.textAlignment = .left
        return label
    }()
    // MARK: - Personal Info
    lazy var personalView: UIView = {
        let content = UIView.viewHolderWhiteDesign()
        return content
    }()
    
    lazy var personalIcon: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 25
        button.isHidden = true
        button.setTitle(getUserInitial(), for: .normal)
        button.backgroundColor = AppColors.red.color
        return button
    }()
    lazy var profileImage:  UIImageView  = {
        let profileImageView =  UIImageView.customImage()
        profileImageView.layer.cornerRadius = 7
        profileImageView.isHidden = true
        profileImageView.layer.cornerRadius = 25
        return profileImageView
    }()
    lazy var personalNameLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = Storage.getFirstName() + " " + Storage.getLasttName()
        label.textAlignment = .left
        return label
    }()
    lazy var personalLabel: UILabel = {
        let label = UILabel()
        label.text = "Personal Info"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: AppFonts.silkabold.font, size: 10)
        label.textColor = AppColors.lightNavyBlue.color
        return label
    }()
    
    lazy var forwardButton: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.forwardButton.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(didTapOnPersonalButton), for: .touchUpInside)
        return button
    }()
    
    @objc func didTapOnPersonalButton() {
        navigationController?.pushViewController(ProfileViewController(), animated: true)
    }
    
    lazy var settingsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        cv.backgroundColor = backgroundSystemColor
        cv.dataSource = self
        cv.delegate = self
        cv.showsVerticalScrollIndicator = false
        cv.isPagingEnabled = true
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    let cards: [Cards] = {
        let termAndConditionCard = Cards(title: "Terms and Condition", ImageIcon: AppButtonImages.termAndCondition.image, forwardImage: AppButtonImages.forwardButton.image)
        
        let privacyCard  = Cards(title: "Privacy", ImageIcon: AppButtonImages.privacy.image, forwardImage: AppButtonImages.forwardButton.image)
        
        let aboutCard  = Cards(title: "About", ImageIcon: AppButtonImages.about.image, forwardImage: AppButtonImages.forwardButton.image)
        
        let logoutCard  = Cards(title: "Logout", ImageIcon: AppButtonImages.logout.image, forwardImage: AppButtonImages.forwardButton.image)
        
        return [termAndConditionCard, privacyCard, aboutCard, logoutCard]
    }()
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        registercCell()
        // showProfileImage()
    }
    //
    //    func showProfileImage() {
    //        if Storage.getImageUrl() == "" {
    //            profileImage.isHidden = true
    //            personalIcon.isHidden = false
    //        } else {
    //            profileImage.isHidden = false
    //            profileImage.kf.setImage(with: URL(string: "\(Storage.getImageUrl())"))
    //            personalIcon.isHidden = true
    //        }
    //    }
    func authenticate(){
        if InternetConnectionManager.isConnectedToNetwork(){
            Loader.shared.showLoader()
            let request = AuthRequest(email: LoginDetails().email, password: LoginDetails().password)
            authViewModel.authResponse(authRequest: request)
        } else {
            Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        authViewModel.delegate = self
        authenticate()
    }
    func getUserDetails() {
        if InternetConnectionManager.isConnectedToNetwork(){
            Loader.shared.showLoader()
            userProfileViewModel.getUserProfileData(userID: Storage.getUserId())
            userProfileViewModel.delegate = self
        } else {
            Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
        }
    }
    func registercCell() {
        settingsCollectionView.register(SettingsollectionViewCell.self, forCellWithReuseIdentifier: SettingsollectionViewCell.identifier)
    }
    
    func goToPrivacyPolicy() {
        if let url = URL(string: "https://lawrights.lawpavilion.com/privacy-policy") {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
    }
    func goToTermsAndCondition() {
        if let url = URL(string: "https://lawrights.lawpavilion.com/terms-and-conditions") {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
    }
    func goToAboutUs() {
        if let url = URL(string: "https://lawrights.lawpavilion.com/about-us") {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
    }
    func gotToSignin(){
        // present(LoginViewController(), animated: true)
        // MARK: - LOGGING OUT AND CLEARING THE USER DEFAULT AND KEYCHAIN
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Logging Out", message: "Are you sure you want to logout?", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { (action: UIAlertAction!) in
                //                    UserDefaultUtility.removeUserToken()
                //                    FloatUtility.resetDefaults()
                Storage.removeUserToken()
                UserDefaults.standard.setValue("notLoggedIn", forKey: "userID")
                let navigationController = UINavigationController.init(rootViewController: LoginViewController())
                navigationController.modalPresentationStyle = .fullScreen
                self.present(navigationController, animated: false, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func didReceiveAuthResponse(AuthResponse: AuthResponse?, _ statusCode: Int) {
        if statusCode == 200 {
            if InternetConnectionManager.isConnectedToNetwork(){
                getUserDetails()
            }
            else {
                Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
            }
        }
        if statusCode == 400 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Something went wrong, Please try again", type: .error)
        }
    }
}
extension SettingsController : SettingsCellDelegate {
    func onClickContactForwardButton(index: Int) {
        let card = cards[index]
        if card.title == "Terms and Condition" {
            goToTermsAndCondition()
        }
        if card.title == "Privacy" {
            goToPrivacyPolicy()
        }
        if card.title == "About" {
            goToAboutUs()
        }
        if card.title == "Logout" {
            gotToSignin()
        }
    }
    
    func didReceiveUserProfileResponse(userProileResponse: LoginResponse?) {
        if userProileResponse?.status ==  200 {
            Loader.shared.hideLoader()
            let firstname = userProileResponse?.data?.first_name
            Storage.saveFirstName(data: firstname ?? "")
            let lastname = userProileResponse?.data?.last_name
            Storage.saveLastName(data: lastname ?? "")
            personalNameLabel.text = Storage.getFirstName() + " " + Storage.getLasttName()
            let img = userProileResponse?.data?.image_url ?? ""
            print("+++++\(userProileResponse?.data?.image_url ?? "")")
            Storage.saveImage(url: "\(userProileResponse?.data?.image_url ?? "")")
            if img  == "" || img  == "https://kmr-staging.lawpavilion.com/storage/images/apiusers/" {
                profileImage.isHidden = true
                personalIcon.isHidden = false
            } else {
                profileImage.isHidden = false
                profileImage.kf.setImage(with: URL(string: "\(img)"))
            }
            
        }
        if userProileResponse?.status ==  422 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Something went wrong", type: .error)
        }
    }
}
