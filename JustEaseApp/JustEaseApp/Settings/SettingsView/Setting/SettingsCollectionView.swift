//
//  SettingsCollectionView.swift
//  JustEaseApp
//
//  Created by iOSApp on 15/02/2023.
//

import UIKit
extension SettingsController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  cards.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell =  settingsCollectionView.dequeueReusableCell(withReuseIdentifier: SettingsollectionViewCell.identifier, for: indexPath) as? SettingsollectionViewCell else { return UICollectionViewCell() }
        let cardDetails = cards[indexPath.row]
        cell.settingsCellDelegate = self
        cell.index = indexPath
        cell.imageIcon.setImage(cardDetails.ImageIcon, for: .normal)
        cell.cardLabel.text = cardDetails.title
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let card = cards[indexPath.item]
        if card.title == "Terms and Condition" {
            goToTermsAndCondition()
        }
        if card.title == "Privacy" {
           goToPrivacyPolicy()
        }
        if card.title == "About" {
            goToAboutUs()
        }
        if card.title == "Logout" {
            gotToSignin()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 80)
    }
}
