//
//  SettingsConstriants.swift
//  JustEaseApp
//
//  Created by iOSApp on 15/02/2023.
//

import UIKit
extension SettingsController {
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(settingsLabel)
        view.addSubview(accountLabel)
        view.addSubview(personalView)
        
        personalView.addSubview(personalIcon)
        personalView.addSubview(profileImage)
        personalView.addSubview(personalLabel)
        personalView.addSubview(personalNameLabel)
        personalView.addSubview(forwardButton)
        
        view.addSubview(settingsCollectionView)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = backgroundSystemColor
        
        NSLayoutConstraint.activate([
            settingsLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 80),
            settingsLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            
            accountLabel.topAnchor.constraint(equalTo: settingsLabel.bottomAnchor, constant: 20),
            accountLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            
            // MARK: -  Personal view
            personalView.topAnchor.constraint(equalTo: accountLabel.bottomAnchor, constant: 20),
            personalView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            personalView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            personalView.heightAnchor.constraint(equalToConstant: 80),
            
            personalIcon.topAnchor.constraint(equalTo: personalView.topAnchor, constant: 15),
            personalIcon.leadingAnchor.constraint(equalTo: personalView.leadingAnchor, constant: 0),
            personalIcon.heightAnchor.constraint(equalToConstant: 50),
            personalIcon.widthAnchor.constraint(equalToConstant: 50),
            
            profileImage.topAnchor.constraint(equalTo: personalView.topAnchor, constant: 15),
            profileImage.leadingAnchor.constraint(equalTo: personalView.leadingAnchor, constant: 0),
            profileImage.heightAnchor.constraint(equalToConstant: 50),
            profileImage.widthAnchor.constraint(equalToConstant: 50),
            
            personalNameLabel.topAnchor.constraint(equalTo: personalView.topAnchor, constant: 25),
            personalNameLabel.leadingAnchor.constraint(equalTo: personalIcon.trailingAnchor, constant: 10),
            
            personalLabel.topAnchor.constraint(equalTo: personalNameLabel.bottomAnchor, constant: 5),
            personalLabel.leadingAnchor.constraint(equalTo: personalIcon.trailingAnchor, constant: 10),
            
            forwardButton.topAnchor.constraint(equalTo: personalView.topAnchor, constant: 25),
            forwardButton.trailingAnchor.constraint(equalTo: personalView.trailingAnchor, constant: -1),
            
        ])
        settingsCollectionView.anchorWithConstantsToTop(personalView.bottomAnchor,
                                                       left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 100, rightConstant: 20)
    }
}
