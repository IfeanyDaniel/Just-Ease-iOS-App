//
//  SettingsCollectionViewCell.swift
//  JustEaseApp
//
//  Created by iOSApp on 15/02/2023.
//

import UIKit
protocol SettingsCellDelegate: AnyObject {
    func onClickContactForwardButton(index: Int)
}
class SettingsollectionViewCell: UICollectionViewCell {
    static var identifier: String = "settingsCell"
    var settingsCellDelegate: SettingsCellDelegate?
    var index : IndexPath?
    var card: Cards? {
        didSet {
            guard let card = card else {
                return
            }
            
            let imageName = card.ImageIcon
            imageIcon.setImage(imageName, for: .normal)
            
            let forwardImage = card.forwardImage
            forwardButton.setImage(forwardImage, for: .normal)
            
            let cardName = card.title
            cardLabel.text = cardName
        }
    }
    lazy var imageIcon: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.right.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var cardLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Privacy"
        label.textAlignment = .left
        label.font = UIFont(name: AppFonts.silkabold.font ,size: 16)
        return label
    }()
 
    lazy var forwardButton: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.forwardButton.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(clickContactForwardButton(_:)), for: .touchUpInside)
        return button
    }()
    @objc func clickContactForwardButton(_ sender: Any){
        guard let indexRow = index?.row else {
            return
        }
        settingsCellDelegate?.onClickContactForwardButton(index: indexRow)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpViews() {
        addSubview(imageIcon)
        addSubview(cardLabel)
        addSubview(forwardButton)
        NSLayoutConstraint.activate([
            imageIcon.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            imageIcon.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            imageIcon.heightAnchor.constraint(equalToConstant: 50),
            imageIcon.widthAnchor.constraint(equalToConstant: 50),
            
            cardLabel.topAnchor.constraint(equalTo: topAnchor, constant: 30),
            cardLabel.leadingAnchor.constraint(equalTo: imageIcon.trailingAnchor, constant: 10),
            
            forwardButton.topAnchor.constraint(equalTo: topAnchor, constant: 25),
            forwardButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            
        ])
    }
}
