//
//  ProfileConstriants.swift
//  JustEaseApp
//
//  Created by iOSApp on 21/02/2023.
//

import UIKit
extension ProfileViewController {
    //MARK: - Layout subviews for the scroll views
    func setupScrollView() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: profileIcon.bottomAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
        ])
    }
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(accountLabel)
        view.addSubview(profileIcon)
        view.addSubview(profileImage)
        //contentView.addSubview(emailValidationLabel)
        view.addSubview(editProfileButton)
        contentView.addSubview(firstNameTextField)
        firstNameTextField.addSubview(firstLabel)
        
        contentView.addSubview(lastNameTextField)
        lastNameTextField.addSubview(lastNameLabel)
        
        contentView.addSubview(emailAddressTextField)
        emailAddressTextField.addSubview(emailLabel)
        
        contentView.addSubview(phoneTextField)
        phoneTextField.addSubview(phoneLabel)
        
        contentView.addSubview(locationTextField)
        locationTextField.addSubview(locationLabel)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = .systemBackground
        
        NSLayoutConstraint.activate([
            accountLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            accountLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            
            editProfileButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 105),
            editProfileButton.trailingAnchor.constraint(equalTo: view.trailingAnchor , constant: -20),
            editProfileButton.heightAnchor.constraint(equalToConstant: 50),
            editProfileButton.widthAnchor.constraint(equalToConstant: 100),
            
            profileIcon.topAnchor.constraint(equalTo: editProfileButton.bottomAnchor, constant: 30),
            profileIcon.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            profileIcon.heightAnchor.constraint(equalToConstant: 80),
            profileIcon.widthAnchor.constraint(equalToConstant: 80),
            
            profileImage.topAnchor.constraint(equalTo: editProfileButton.bottomAnchor, constant: 30),
            profileImage.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            profileImage.heightAnchor.constraint(equalToConstant: 80),
            profileImage.widthAnchor.constraint(equalToConstant: 80),
            
            firstNameTextField.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
            firstNameTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 20),
            firstNameTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -20),
            firstNameTextField.heightAnchor.constraint(equalToConstant: 65),

            firstLabel.topAnchor.constraint(equalTo: firstNameTextField.topAnchor, constant: 5),
            firstLabel.leadingAnchor.constraint(equalTo:  firstNameTextField.leadingAnchor, constant: 10),
            
            lastNameTextField.topAnchor.constraint(equalTo: firstNameTextField.bottomAnchor, constant: 15),
            lastNameTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 20),
            lastNameTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -20),
            lastNameTextField.heightAnchor.constraint(equalToConstant: 65),

            lastNameLabel.topAnchor.constraint(equalTo: lastNameTextField.topAnchor, constant: 5),
            lastNameLabel.leadingAnchor.constraint(equalTo:  lastNameTextField.leadingAnchor, constant: 10),
            
            emailAddressTextField.topAnchor.constraint(equalTo: lastNameTextField.bottomAnchor, constant: 15),
            emailAddressTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 20),
            emailAddressTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -20),
            emailAddressTextField.heightAnchor.constraint(equalToConstant: 65),

            emailLabel.topAnchor.constraint(equalTo: emailAddressTextField.topAnchor, constant: 5),
            emailLabel.leadingAnchor.constraint(equalTo: emailAddressTextField.leadingAnchor, constant: 10),
            
            phoneTextField.topAnchor.constraint(equalTo: emailAddressTextField.bottomAnchor, constant: 15),
            phoneTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 20),
            phoneTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -20),
            phoneTextField.heightAnchor.constraint(equalToConstant: 65),

            phoneLabel.topAnchor.constraint(equalTo: phoneTextField.topAnchor, constant: 5),
            phoneLabel.leadingAnchor.constraint(equalTo: phoneTextField.leadingAnchor, constant: 10),
            
            locationTextField.topAnchor.constraint(equalTo: phoneTextField.bottomAnchor, constant: 15),
            locationTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 20),
            locationTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -20),
            locationTextField.heightAnchor.constraint(equalToConstant: 65),

            locationLabel.topAnchor.constraint(equalTo: locationTextField.topAnchor, constant: 5),
            locationLabel.leadingAnchor.constraint(equalTo: locationTextField.leadingAnchor, constant: 10),
        
            
        ])
    }
}
