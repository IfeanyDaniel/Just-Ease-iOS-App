//
//  ProfileExtension.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/2/23.
//

import Foundation
extension ProfileViewController : UserProfileDelegate {
    func didReceiveUserProfileResponse(userProileResponse: LoginResponse?) {
        if userProileResponse?.status ==  200 {
            Loader.shared.hideLoader()
            firstNameTextField.text = userProileResponse?.data?.first_name
            lastNameTextField.text = userProileResponse?.data?.last_name
            emailAddressTextField.text = userProileResponse?.data?.email
            locationTextField.text = userProileResponse?.data?.address
            phoneTextField.text = userProileResponse?.data?.phone_number
            let img = userProileResponse?.data?.image_url ?? ""
            if img  == "" || img  == "https://kmr-staging.lawpavilion.com/storage/images/apiusers/" {
                profileImage.isHidden = true
                profileIcon.isHidden = false
            } else {
                profileImage.isHidden = false
                profileImage.kf.setImage(with: URL(string: "\(img)"))
            }
        }
        if userProileResponse?.status ==  422 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Something went wrong", type: .error)
        }
    }
}
