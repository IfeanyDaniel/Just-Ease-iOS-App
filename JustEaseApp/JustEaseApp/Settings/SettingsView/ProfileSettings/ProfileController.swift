//
//  ProfileController.swift
//  JustEaseApp
//
//  Created by iOSApp on 21/02/2023.
//

import UIKit
class ProfileViewController: UIViewController {
    // MARK: - Scroll view
    var userProfileViewModel = UserProfileViewModel()
    lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.showsHorizontalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    // MARK: - content view
    lazy var contentView: UIView = {
        let content = UIView()
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    
    lazy var accountLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Account"
        label.font = UIFont(name: AppFonts.silkabold.font, size: 25)
        label.textAlignment = .left
        return label
    }()
    lazy var editProfileButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Edit Profile", for: .normal)
        button.layer.masksToBounds = true
        button.backgroundColor = backgroundSystemColor
        button.layer.cornerRadius = 25
        button.layer.borderWidth = 1
        button.layer.borderColor = AppColors.green.color.cgColor
        button.setTitleColor(AppColors.green.color, for: .normal)
        button.titleLabel?.font = UIFont(name: AppFonts.silkLight.font, size: 14)
        button.addTarget(self, action: #selector(didTapEditProfileButton), for: .touchUpInside)
        return button
    }()
   
    lazy var profileIcon: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 40
        button.isHidden = true
        button.setTitle(getUserInitial() , for: .normal)
        button.backgroundColor = AppColors.red.color
        return button
    }()
    lazy var profileImage:  UIImageView  = {
        let profileImageView =  UIImageView.customImage()
        profileImageView.layer.cornerRadius = 7
        profileImageView.isHidden = true
        profileImageView.layer.cornerRadius = 40
        return profileImageView
    }()
    lazy var firstLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.textColor = placeholderSystemGrayColor
        label.text = "First Name"
        label.numberOfLines = 1
        return label
    }()
    lazy var firstNameTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.isUserInteractionEnabled = false
        textField.text = "John"
        return textField
    }()
    
    lazy var lastNameLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.textColor = placeholderSystemGrayColor
        label.text = "Last Name"
        label.numberOfLines = 1
        return label
    }()
    lazy var lastNameTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.isUserInteractionEnabled = false
        textField.text = "Obi"
        return textField
    }()
    
    lazy var emailLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.textColor = placeholderSystemGrayColor
        label.text = "Email Address"
        label.numberOfLines = 1
        return label
    }()
    lazy var emailAddressTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.isUserInteractionEnabled = false
        textField.text = "ifeanyimbata3@gmail.com"
        textField.keyboardType = .emailAddress
        textField.autocapitalizationType = .none
        textField.isUserInteractionEnabled = false
        textField.autocorrectionType = .no
        return textField
    }()
    
    lazy var phoneLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.textColor = placeholderSystemGrayColor
        label.text = "Phone Number"
        label.numberOfLines = 1
        return label
    }()
    lazy var phoneTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.isUserInteractionEnabled = false
        textField.text = "09039270990"
        return textField
    }()
    
    lazy var locationLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.textColor = placeholderSystemGrayColor
        label.text = "Location"
        label.numberOfLines = 1
        return label
    }()
    lazy var locationTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.text = "2464 Royal Ln. Mesa, New Jersey 45463"
        textField.isUserInteractionEnabled = false
        return textField
    }()
    
    lazy var emailValidationLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.numberOfLines = 1
        return label
    }()
    
    
    // MARK: - Login function
    @objc func didTapEditProfileButton(){
        navigationController?.pushViewController(EditProfileController(), animated: true)
    }
  
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        setupScrollView()
        self.hideKeyboardWhenTappedAround()
        getUserDetails()
        
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    func getUserDetails() {
        if InternetConnectionManager.isConnectedToNetwork(){
            Loader.shared.showLoader()
            userProfileViewModel.getUserProfileData(userID: Storage.getUserId())
            userProfileViewModel.delegate = self
        } else {
            Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
        }
    }
 
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
    }
    // MARK: - View did layout subviews
    override func viewDidLayoutSubviews() {
        scrollView.isScrollEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.contentSize = CGSize(width: 400, height: 600)
    }
}
