//
//  UserProfileResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/1/23.
//

import Foundation
struct UserProfileResource {
    func getUserProfileResponse(userID:Int, completionHandler: @escaping (_ result: LoginResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let UserProfileUrl = ApiEndpoints.userProfileEndPoint + "/\(userID)"
        
        let url = URL(string: UserProfileUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType: LoginResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}
