//
//  EditProfileResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/2/23.
//

import Foundation
struct EditProfileResource {
    func getResponse(userID: Int, editProfileRequest: EditProfileRequest, completionHandler: @escaping (_ result: LoginResponse?,_ statusCode: Int) -> Void) {
        let url = ApiEndpoints.editProfileEndPoint + "/\(userID)"
        let editProfileUrl = URL(string: url)!
        let httpUtility = HTTPUtility()
        do {
            let editProfilePostBody = try JSONEncoder().encode(editProfileRequest)
            httpUtility.postWithImageAuthorizationResponse(requestUrl: editProfileUrl , requestBody: editProfilePostBody, resultType: LoginResponse.self) { loginApiResponse, statusCode in
                completionHandler(loginApiResponse,statusCode)
                print(">>>\(loginApiResponse)")
            }
        } catch let error {
            debugPrint(error)
        }
    }
}
