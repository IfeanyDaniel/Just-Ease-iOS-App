//
//  EditProfileRequest.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/2/23.
//

import Foundation
struct EditProfileRequest: Encodable {
   let first_name: String?
   let last_name: String?
   let email: String?
   let phone_number: String?
   let address: String?
}
