//
//  logoutResponse.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 6/7/23.
//

import Foundation
struct LogoutResponse: Decodable {
    let message: String
}
