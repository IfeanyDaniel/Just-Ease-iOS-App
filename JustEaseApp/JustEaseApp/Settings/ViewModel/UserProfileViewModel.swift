//
//  UserProfileViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/1/23.
//

import Foundation
protocol UserProfileDelegate {
    func didReceiveUserProfileResponse(userProileResponse: LoginResponse?)
}
class UserProfileViewModel {
    var delegate: UserProfileDelegate?
    func getUserProfileData(userID: Int) {
        let userProfileResource = UserProfileResource()
        userProfileResource.getUserProfileResponse(userID: userID) { getUserProfileResponse in
            if getUserProfileResponse?.status == 200 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveUserProfileResponse(userProileResponse: getUserProfileResponse)
                }
            }
            if getUserProfileResponse?.status == 422 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveUserProfileResponse(userProileResponse: getUserProfileResponse)
                }
            }
        }
    }
}
