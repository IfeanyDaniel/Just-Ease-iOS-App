//
//  EditProfileViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/2/23.
//

import Foundation
protocol EditProfileViewModelDelegate {
    func didReceiveEditProfileResponse(editProfileResponse: LoginResponse?,_ statusCode: Int)
}
class EditProfileViewModel {
    var delegate: EditProfileViewModelDelegate?
    func getEditProfileResponse(userID: Int, editProfileRequest: EditProfileRequest) {
        let editProfileResource = EditProfileResource()
        editProfileResource.getResponse(userID: userID, editProfileRequest: editProfileRequest) { editProfileApiResponse, statusCode in
            if statusCode ==  200 {
                let firstname = editProfileApiResponse?.data?.first_name
                Storage.saveFirstName(data: firstname ?? "")
                let lastname = editProfileApiResponse?.data?.last_name
                Storage.saveLastName(data: lastname ?? "")
                DispatchQueue.main.async {
                    self.delegate?.didReceiveEditProfileResponse(editProfileResponse: editProfileApiResponse, statusCode)
                }
            }
            if statusCode ==  400 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveEditProfileResponse(editProfileResponse: editProfileApiResponse, statusCode)
                }
            }
            if statusCode ==  500 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveEditProfileResponse(editProfileResponse: editProfileApiResponse, statusCode)
                }
            }
            if statusCode ==  422 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveEditProfileResponse(editProfileResponse: editProfileApiResponse, statusCode)
                }
            }
        }
    }
}
