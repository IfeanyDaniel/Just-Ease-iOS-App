//
//  ImageEnums.swift
//  JustEaseApp
//
//  Created by iOSApp on 08/02/2023.
//

import UIKit
// MARK: - ... Enum for holding images
enum AppImages {
    case onboarding1
    case onboarding2
    case onboarding3
    case focus
    case noFocus
    
    var image: String {
        switch self {
        case .onboarding1:
            return  "onboarding1"
        case .onboarding2:
            return  "onboarding2"
        case .onboarding3:
            return  "onboarding3"
        case .focus:
            return  "Focus-Icon"
        case .noFocus:
            return  "No-Focus-Icon"
            
        }
    }
}

// MARK: - ... Enum for holding images
enum AppButtonImages {
    case focus
    case noFocus
    case backArrow
    case checkSuccess
    case dropDown
    case justEaseIcon
    case notification
    case right
    case duties
    case others
    case constitution
    case messageIcon
    case noBookmarkIcon
    case bookmarkIconSelected
    case termAndCondition
    case privacy
    case about
    case logout
    case forwardButton
    case reportIcon
    case reportImage
    case findIcon
    case findImage
    case securityIcon
    case securityImage
    case knowRepIcon
    case knowRepImage
    case forwardGreenButton
    case shareIcon
    case bookmarkUnselected
    case deFaultForwardButton
    case location
    case camera
    case picIcon
    case gifIcon
    case practiceIcon
    case filterBy
    @available(iOS 13.0, *)
    var image: UIImage {
        switch self {
        case .focus:
            return  UIImage(named: "Focus-Icon")!
        case .noFocus:
            return  UIImage(named: "No-Focus-Icon")!
        case .backArrow:
            return UIImage(systemName: "arrow.left")!
        case .checkSuccess:
            return UIImage(named: "checkSuccess")!
        case .dropDown:
            return UIImage(named: "dropDownArrow")!
        case .justEaseIcon:
            return UIImage(named: "justEaseIcon2")!
        case .notification:
            return UIImage(named: "notification")!
        case .right:
            return UIImage(named: "rightIcon")!
        case .duties:
            return UIImage(named: "dutiesIcon")!
        case .others:
            return UIImage(named: "othersIcon")!
        case .constitution:
            return UIImage(named: "constitutionIcon")!
        case .messageIcon:
            return UIImage(named: "messageIcon")!
        case .noBookmarkIcon:
            return UIImage(named: "noBookmarkIcon")!
        case .termAndCondition:
            return UIImage(named: "TandC")!
        case .privacy:
            return UIImage(named: "privacyIcon")!
        case .about:
            return UIImage(named: "aboutIcon")!
        case .logout:
            return UIImage(named: "logoutIcon")!
        case .forwardButton:
            return UIImage(named: "forwardButton")!
        case .reportIcon:
            return UIImage(named: "reportIcon")!
        case .reportImage:
            return UIImage(named: "reportImage")!
        case .findIcon:
            return UIImage(named: "findIcon")!
        case .findImage:
            return UIImage(named: "findImage")!
        case .knowRepIcon:
            return UIImage(named: "knowRepIcon")!
        case .knowRepImage:
            return UIImage(named: "knowRepImage")!
        case .securityIcon:
            return UIImage(named: "securityIcon")!
        case .securityImage:
            return UIImage(named: "securityImage")!
        case .forwardGreenButton:
            return UIImage(named: "forwardGreenButton")!
        case .shareIcon:
            return UIImage(named: "shareIcon")!
        case .bookmarkIconSelected:
            return UIImage(named: "bookmarkIconSelected")!
        case .bookmarkUnselected:
            return UIImage(named: "bookmarkButtonUnSelect")!
        case .deFaultForwardButton:
            return UIImage(systemName: "chevron.right")!
        case .camera:
            return UIImage(systemName: "camera")!
        case .location:
            return UIImage(named: "Location")!
        case .picIcon:
            return UIImage(named: "picIcon")!
        case .gifIcon:
            return UIImage(named: "gifIcon")!
        case .practiceIcon:
            return UIImage(named: "discovery")!
        case .filterBy:
            return UIImage(named: "filterBy")!
        }
    }
}


// MARK: - ... Enum for holding fonts
enum AppFonts {
    case silka
    case silkabold
    case silkLight
    case silkaItalic
    case silkaMedium
    var font: String {
        switch self {
        case .silka:
            return "Silka-SemiBold"
        case .silkabold:
            return "Silka-Bold"
        case .silkLight:
            return "HelveticaNeue-Light"
        case .silkaMedium:
            return "Silka-Medium"
        case .silkaItalic:
            return "HelveticaNeue-LightItalic"
        }
    }
}
