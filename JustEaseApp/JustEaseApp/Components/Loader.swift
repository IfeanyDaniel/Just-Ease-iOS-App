//
//  Loader.swift
//  JustEaseApp
//
//  Created by iOSApp on 21/03/2023.
//


import UIKit
import Lottie

public class Loader: UIView{
    var animation: AnimationView!
    var animationFile = "loader"
    var blurView: UIVisualEffectView = {
        let effect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let effectView = UIVisualEffectView(effect: effect)
        effectView.translatesAutoresizingMaskIntoConstraints = false
        return effectView
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        insertSubview(blurView, at: 0)
        blurView.fillSuperview()
        animation = AnimationView(name: animationFile)
        animation.loopMode = .loop
        animation.translatesAutoresizingMaskIntoConstraints = false
        addSubview(animation)
        animation.centerInSuperview(size: .init(width: 400, height: 300))
        
    }
    
    public static var shared = Loader()
    public func showLoader(){
        
        animation.play()

        let window = UIApplication.shared.keyWindow!
        self.translatesAutoresizingMaskIntoConstraints = false
        self.tag = 91202
        
        window.addSubview(self)
        NSLayoutConstraint.activate([
            self.centerYAnchor.constraint(equalTo: window.centerYAnchor),
            self.centerXAnchor.constraint(equalTo: window.centerXAnchor),
            self.heightAnchor.constraint(equalTo: window.heightAnchor),
            self.widthAnchor.constraint(equalTo: window.widthAnchor)
        ])
    }
    
    public func hideLoader(){
        DispatchQueue.main.async {
            let view = self.viewWithTag(91202)
            view?.removeFromSuperview()
        }
    }
   
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}

