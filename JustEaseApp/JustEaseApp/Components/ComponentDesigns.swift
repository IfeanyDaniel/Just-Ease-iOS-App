//
//  ComponentDesigns.swift
//  JustEaseApp
//
//  Created by iOSApp on 08/02/2023.
//

import UIKit
let buttonHeight = CGFloat(55)
let textFieldHeight = CGFloat(65)
let leadingNumber = CGFloat(25)
let trailingNumber = -CGFloat(25)
let noInternetMessage = "No Internet Connection ,Make sure your device is connected to the internet."
extension UIButton {
    class func actionButtonDesign() -> UIButton {
        let button = UIButton()
        button.backgroundColor = AppColors.green.color
        button.setTitleColor(.white, for: .normal)
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 10
        button.titleLabel?.font = UIFont(name:  AppFonts.silka.font, size: 14)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
    class func showLocationButtonDesign() -> UIButton {
        let button = UIButton()
        button.setImage(UIImage(named: "Location"), for: .normal)
        button.setTitleColor(textSystemColor, for: .normal)
        button.titleLabel?.font = UIFont(name:  AppFonts.silka.font, size: 12)
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 15
        button.isHidden = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = textFieldColor
        return button
    }
    class func checkButtonDesign() -> UIButton {
        let button = UIButton()
        button.layer.masksToBounds = true
        button.backgroundColor = viewStackColor
        button.translatesAutoresizingMaskIntoConstraints = false
        button.contentHorizontalAlignment = .left
        button.setTitleColor(textSystemColor, for: .normal)
        button.titleLabel?.font = UIFont(name: AppFonts.silkaMedium.font, size: 14)
        return button
    }
    class func stackButtonDesign() -> UIButton {
        let button = UIButton()
        button.setTitleColor(AppColors.black.color, for: .normal)
        button.frame.size.width = 150
        button.frame.size.height = buttonHeight
        button.titleLabel?.font = UIFont(name:  AppFonts.silkaMedium.font, size: 12)
        button.layer.cornerRadius = 10
        return button
    }
    class func selectButtonDesign() -> UIButton {
        let button = UIButton()
        button.layer.borderColor = UIColor.lightGray.cgColor
        button.layer.borderWidth = 0
        button.layer.cornerRadius = 5
        button.contentHorizontalAlignment = .left
        button.setTitleColor(placeholderSystemGrayColor, for: .normal)
        button.titleLabel?.font = UIFont(name: AppFonts.silkaMedium.font, size: 12)
        button.backgroundColor = textFieldColor
        button.layer.cornerRadius = 10
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
    class func defaultImageButton() -> UIButton {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 40
        button.isHidden = true
        button.backgroundColor = AppColors.green.color
        return button
    }
   
}
extension paddedTextField {
    class func textFieldDesign() -> paddedTextField {
        let textField = paddedTextField()
        textField.layer.borderColor = AppColors.lightGray.color.cgColor
        textField.layer.borderWidth = 1
        textField.layer.cornerRadius = 10
        textField.backgroundColor = textFieldColor
        textField.textColor = textFieldTextColor
        textField.autocorrectionType = .no
        textField.font = UIFont(name:  AppFonts.silkaMedium.font, size: 16)
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }
}
extension paddedOtpTextField {
    class func textFieldOtpDesign() -> paddedOtpTextField {
        let textField = paddedOtpTextField()
        textField.layer.borderColor = AppColors.lightGray.color.cgColor
        textField.layer.borderWidth = 1
        textField.layer.cornerRadius = 10
        textField.backgroundColor = textFieldColor
        textField.textColor = textFieldTextColor
        textField.autocorrectionType = .no
        textField.keyboardType = .numberPad
        textField.frame.size.height = 45
        textField.frame.size.width = 45
        textField.font = UIFont(name:  AppFonts.silka.font, size: 16)
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }
}
extension UILabel {
    class func labelTextDesign() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 6
        label.font = UIFont(name: AppFonts.silka.font ,size: 13)
        label.textColor = textSystemColor
        return label
    }
    class func validationLabelDesign() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: AppFonts.silkLight.font ,size: 11)
        return label
    }
    class func boldFirstText(firstText: String, secondText: String, thirdText:String) -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        let firstAttribute = [
            NSAttributedString.Key.font: UIFont(name: AppFonts.silkabold.font, size: 14.0)!
         ]
         let secondAttribute = [
            NSAttributedString.Key.font: UIFont(name: AppFonts.silkLight.font, size: 14.0)!
         ]
        let thirdAttribute = [
           NSAttributedString.Key.font: UIFont(name: AppFonts.silkaMedium.font, size: 14.0)!
        ]
         let firstTextString = NSAttributedString(string: firstText, attributes: firstAttribute)
         let secondTextString  = NSAttributedString(string: secondText, attributes: secondAttribute)
         let thirdTextString = NSAttributedString(string: thirdText, attributes: thirdAttribute)
         let newString = NSMutableAttributedString()
         newString.append(firstTextString)
         newString.append(secondTextString)
         newString.append(thirdTextString)
         label.attributedText = newString
         label.textColor = textSystemColor
        label.numberOfLines = 3
        return label
    }
}
extension UIView {
    class func viewHolderWhiteDesign() -> UIView {
        let content = UIView()
        content.backgroundColor = backgroundSystemColor
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }
    class func viewHolderDesign() -> UIView {
        let content = UIView()
        content.heightAnchor.constraint(equalToConstant: 130.0).isActive = true
        content.sizeToFit()
        content.layer.cornerRadius = 10
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }
    class func stackViewHolderDesign() -> UIView {
        let content = UIView()
        content.backgroundColor = viewStackColor
        content.layer.cornerRadius = 10
        content.translatesAutoresizingMaskIntoConstraints = false
        content.layer.shadowOpacity = 0.30
        content.layer.shadowColor = AppColors.darkerGray.color.cgColor
        content.layer.shadowOffset = CGSize(width: 3, height: 3)
        content.layer.shadowRadius = 5.0
        return content
    }
     func addDashBorder() {
        let color = UIColor.white.cgColor
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.name = "DashBorder"
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 1.5
        shapeLayer.lineJoin = .round
        shapeLayer.lineDashPattern = [2,4]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 10).cgPath
        
        self.layer.masksToBounds = false
        
         self.layer.addSublayer(shapeLayer)
    }
}
extension UITextView {
    class func textViewDesign() -> UITextView {
        let tv = UITextView()
        tv.isEditable = false
        tv.layer.borderColor = AppColors.lightGray.color.cgColor
        tv.layer.borderWidth = 0
        tv.layer.cornerRadius = 10
        tv.backgroundColor = backgroundSystemColor
        tv.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.textColor = textSystemColor
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 7
        style.alignment = .justified
        let attributes = [NSAttributedString.Key.paragraphStyle: style,.foregroundColor: textSystemColor,.font: UIFont(name: AppFonts.silkaMedium.font, size: 15)]
        tv.attributedText = NSAttributedString(string: tv.text, attributes: attributes as [NSAttributedString.Key : Any])
        return tv
    }
    class func writeAbletextViewDesign() -> UITextView {
        let tv = UITextView()
        tv.isEditable = true
        tv.backgroundColor = textFieldColor
        tv.layer.cornerRadius = 10
        tv.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.textColor = placeholderSystemGrayColor
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 2
        style.alignment = .justified
        let attributes = [NSAttributedString.Key.paragraphStyle: style,.foregroundColor:placeholderSystemGrayColor,.font: UIFont(name: AppFonts.silkaMedium.font, size: 14)]
        tv.attributedText = NSAttributedString(string: tv.text, attributes: attributes as [NSAttributedString.Key : Any])
        return tv
    }
}
extension UIImageView {
    class func customImage() -> UIImageView {
        let profileImageView =  UIImageView()
        profileImageView.image = AppButtonImages.notification.image
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.isUserInteractionEnabled = true
        profileImageView.layer.masksToBounds = true
        profileImageView.layer.cornerRadius = 20
        return profileImageView
    }
}
class paddedTextField: UITextField {
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width - 10, height: bounds.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width - 10, height: bounds.height)
    }
    
}
class paddedOtpTextField: UITextField {
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 22, y: bounds.origin.y, width: bounds.width - 22, height: bounds.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 22, y: bounds.origin.y, width: bounds.width - 22, height: bounds.height)
    }
    
}
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
class PaddingLabel: UILabel {
    
    var topInset: CGFloat
    var bottomInset: CGFloat
    var leftInset: CGFloat
    var rightInset: CGFloat
    
    required init(withInsets top: CGFloat, _ bottom: CGFloat,_ left: CGFloat,_ right: CGFloat) {
        self.topInset = top
        self.bottomInset = bottom
        self.leftInset = left
        self.rightInset = right
        super.init(frame: CGRect.zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }
}
public extension UILabel{
    convenience init(text: String, font: UIFont, numberOfLines: Int = 1, color: UIColor, alignment: NSTextAlignment){
        self.init(frame: .zero)
        self.text = text
        self.textColor = color
        self.font = font
        self.numberOfLines = numberOfLines
        self.textAlignment = alignment
        self.adjustsFontSizeToFitWidth = true
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    func setLineHeight(spacing: CGFloat){
        let attributedString = NSMutableAttributedString(string: self.text ?? "")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = spacing
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributedString.length))
        attributedText = attributedString
    }
    func setText(_ text: String){
        self.text = text
    }

}
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
public extension String{
    func toDate(format: String = "yyyy-MM-dd HH:mm:ss") -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale.init(identifier: "en_UK")
        let date = dateFormatter.date(from: self)
        return date ?? Date()
    }
}
public extension Date {
    func timeAgoDisplay() -> String {
        let secondsAgo = Int(Date().timeIntervalSince(self))
     
        let minute = 60 
        let hour = 60 * minute
        let day = 24 * hour
        let week = 7 * day
        if secondsAgo < minute {
            return "\(secondsAgo) seconds ago"
        }
        else if secondsAgo < hour {
            let ago = secondsAgo / minute
            return ago > 1 ? "\(ago) minutes ago" : "\(ago) minute ago"
        }
        else if secondsAgo < day {
            if (secondsAgo / hour == 1 ){
                return "\(secondsAgo / hour) hour ago"
            }else{
                return "\(secondsAgo / hour) hours ago"
            }
            
        }
        else if secondsAgo < week {
            return "\(secondsAgo / day) days ago"
        }
        let ago = secondsAgo / week
        return ago > 1 ? "\(ago) weeks ago" : "\(ago) week ago"
    }
}
