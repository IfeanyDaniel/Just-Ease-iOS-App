//
//  ColorSwitch.swift
//  JustEaseApp
//
//  Created by iOSApp on 08/02/2023.
//

import UIKit
// MARK: - ... Enum for holding colors to be used throughout the app
var backgroundSystemGrayColor : UIColor {
    return UIColor { (trait) -> UIColor in
        switch trait.userInterfaceStyle {
        case .dark:
            return AppColors.black.color
        default:
            return  AppColors.lightGray.color
        }
    }
}
var viewStackColor : UIColor {
    return UIColor { (trait) -> UIColor in
        switch trait.userInterfaceStyle {
        case .dark:
            return AppColors.darkBrown.color
        default:
            return  AppColors.white.color
        }
    }
}
var lightGraySystemColor : UIColor {
    return UIColor { (trait) -> UIColor in
        switch trait.userInterfaceStyle {
        case .dark:
            return AppColors.darkBrown.color
        default:
            return  AppColors.lightGray.color
        }
    }
}
var textSystemColor : UIColor {
    return UIColor { (trait) -> UIColor in
        switch trait.userInterfaceStyle {
        case .dark:
            return  AppColors.white.color
        default:
            return  AppColors.navyBlue.color
        }
    }
}
var greenSystemColor : UIColor {
    return UIColor { (trait) -> UIColor in
        switch trait.userInterfaceStyle {
        case .dark:
            return  AppColors.darkBrown.color
        default:
            return  AppColors.lightGreen.color
        }
    }
}
var borderTextColor : UIColor {
    return UIColor { (trait) -> UIColor in
        switch trait.userInterfaceStyle {
        case .dark:
            return  AppColors.white.color
        default:
            return  AppColors.white.color
        }
    }
}

var backgroundSystemColor : UIColor {
    return UIColor { (trait) -> UIColor in
        switch trait.userInterfaceStyle {
        case .dark:
            return AppColors.black.color
        default:
            return  AppColors.white.color
        }
    }
}

var lightBackgroundColor : UIColor {
    return UIColor { (trait) -> UIColor in
        switch trait.userInterfaceStyle {
        case .dark:
            return UIColor(white: 0.3, alpha: 0.5)
        default:
            return  UIColor(white: 0, alpha: 0.5)
        }
    }
}

var placeholderSystemGrayColor : UIColor {
    return UIColor { (trait) -> UIColor in
        switch trait.userInterfaceStyle {
        case .dark:
            return AppColors.placeholderColor.color
        default:
            return  AppColors.placeholderColor.color
        }
    }
}

var cardSystemColor : UIColor {
    return UIColor { (trait) -> UIColor in
        switch trait.userInterfaceStyle {
        case .dark:
            return AppColors.lightBlack.color
        default:
            return  AppColors.white.color
        }
    }
}
var textFieldColor : UIColor {
    return UIColor { (trait) -> UIColor in
        switch trait.userInterfaceStyle {
        case .dark:
            return AppColors.darkBrown.color
        default:
            return  AppColors.lightGray.color
        }
    }
}
var commentButtonColor : UIColor {
    return UIColor { (trait) -> UIColor in
        switch trait.userInterfaceStyle {
        case .dark:
            return AppColors.black.color
        default:
            return  AppColors.white.color
        }
    }
}
var textFieldTextColor : UIColor {
    return UIColor { (trait) -> UIColor in
        switch trait.userInterfaceStyle {
        case .dark:
            return AppColors.white.color
        default:
            return  AppColors.black.color
        }
    }
}

