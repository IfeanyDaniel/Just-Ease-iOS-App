//
//  BaseCell.swift
//  JustEaseApp
//
//  Created by iOSApp on 14/02/2023.
//

import Foundation
import UIKit

class BaseCell : UICollectionViewCell {
   
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupView()
  }
  
  func setupView() {
    
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
