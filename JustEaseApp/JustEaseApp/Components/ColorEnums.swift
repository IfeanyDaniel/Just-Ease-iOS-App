//
//  ColorEnums.swift
//  JustEaseApp
//
//  Created by iOSApp on 08/02/2023.
//

import UIKit
enum AppColors {
    case white
    case gray
    case lightGray
    case black
    case red
    case lightNavyBlue
    case navyBlue
    case lighterGray
    case darkGray
    case placeholderColor
    case borderGrayColor
    case darkerGray
    case shadowColor
    case grayBorderColor
    case lightBlack
    case lighterBlack
    case green
    case darkBrown
    case lightRed
    case lightGreen
    case fairBrown
    case lightBrown
    case lightPurple
    case lightMilky
    case lightOrange
    case lightPink
    case lightBlue
    var color: UIColor {
        switch self {
        case .white:
            return UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 1.00)
        case .gray:
            return UIColor(red: 0.98, green: 0.98, blue: 0.98, alpha: 1.00)
        case .lightGray:
            return UIColor(red: 0.91, green: 0.91, blue: 0.92, alpha: 1.00)
        case .black:
            return UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 1.00)
        case .red:
            return UIColor(red: 0.86, green: 0.20, blue: 0.21, alpha: 1.00)
        case .navyBlue:
            return  UIColor(red: 0.10, green: 0.16, blue: 0.29, alpha: 1.00)
        case .lightNavyBlue:
            return UIColor(red: 0.47, green: 0.49, blue: 0.57, alpha: 1.00)
        case .lighterGray:
            return  UIColor(red: 0.64, green: 0.65, blue: 0.65, alpha: 1.00)
        case .darkGray:
            return UIColor(red: 0.957, green: 0.957, blue: 0.957, alpha: 1)
        case .borderGrayColor:
            return UIColor(red: 0.77, green: 0.77, blue: 0.77, alpha: 1.00)
        case .placeholderColor:
            return  UIColor(red: 0.65, green: 0.67, blue: 0.72, alpha: 1.00)
        case .darkerGray:
            return  UIColor(red: 0.59, green: 0.59, blue: 0.59, alpha: 1.00)
        case .grayBorderColor:
            return  UIColor(red: 0.88, green: 0.88, blue: 0.88, alpha: 1.00)
        case .shadowColor:
            return  UIColor(red: 0.917, green: 0.926, blue: 0.955, alpha: 0.7)
        case .lightBlack:
            return UIColor(red: 0.25, green: 0.25, blue: 0.25, alpha: 1.00)
        case .lighterBlack:
            return UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 0.50)
        case .darkBrown:
            return UIColor(red: 0.20, green: 0.20, blue: 0.20, alpha: 1.00)
        case .green:
            return UIColor(red: 0.29, green: 0.65, blue: 0.50, alpha: 1.00)
        case .lightRed:
            return UIColor(red: 0.98, green: 0.92, blue: 0.92, alpha: 1.00)
        case .lightGreen:
            return UIColor(red: 0.91, green: 0.96, blue: 0.95, alpha: 1.00)
        case .fairBrown:
            return UIColor(red: 0.49, green: 0.27, blue: 0.01, alpha: 1.00)
        case .lightBrown:
            return UIColor(red: 1.00, green: 0.73, blue: 0.40, alpha: 1.00)
        case .lightPurple:
            return UIColor(red: 0.93, green: 0.92, blue: 1.00, alpha: 1.00)
        case .lightMilky:
            return UIColor(red: 0.98, green: 0.85, blue: 0.82, alpha: 1.00)
        case .lightOrange:
            return UIColor(red: 0.99, green: 0.95, blue: 0.91, alpha: 1.00)
        case .lightPink:
            return UIColor(red: 0.98, green: 0.91, blue: 0.93, alpha: 1.00)
        case .lightBlue:
            return UIColor(red: 0.91, green: 0.97, blue: 1.00, alpha: 1.00)
        }
    }
}
