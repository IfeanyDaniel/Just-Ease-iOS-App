//
//  CreateCommentResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/12/23.
//

import Foundation
struct CreateCommentResource {
    func getResponse(feedId: Int, createFeedCommentRequest: CreateCommentRequest, completionHandler: @escaping (_ result: CreateCommentResponse?,_ statusCode: Int) -> Void) {
        let url = ApiEndpoints.createFeedCommentEndPoint + "/\(feedId)/create_comment"
        print(">>>\(url)")
        let createFeedCommentUrl = URL(string: url)!
        let httpUtility = HTTPUtility()
        do {
            let createFeedCommentPostBody = try JSONEncoder().encode(createFeedCommentRequest)
            httpUtility.postAuthorizationResponse(requestUrl: createFeedCommentUrl, requestBody: createFeedCommentPostBody, resultType: CreateCommentResponse.self) { createFeedCommentApiResponse, statusCode in
                completionHandler(createFeedCommentApiResponse,statusCode)
                print(">>>\(createFeedCommentApiResponse)")
            }
        } catch let error {
            debugPrint(error)
        }
    }
}


