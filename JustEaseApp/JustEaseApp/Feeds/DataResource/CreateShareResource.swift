//
//  CreateShareResource.swift
//  JustEaseApp
//
//  Created by iOSApp on 12/05/2023.
//

import Foundation
struct CreateShareResource {
    func getResponse(feedId: Int, createFeedShareRequest: CreateShareRequest, completionHandler: @escaping (_ result: CreateShareResponse?,_ statusCode: Int) -> Void) {
        let url = ApiEndpoints.createFeedShareEndPoint + "/\(feedId)/create_feed_share"
        print(">>>\(url)")
        let createFeedShareUrl = URL(string: url)!
        let httpUtility = HTTPUtility()
        do {
            let createFeedSharePostBody = try JSONEncoder().encode(createFeedShareRequest)
            httpUtility.postAuthorizationResponse(requestUrl: createFeedShareUrl, requestBody: createFeedSharePostBody, resultType: CreateShareResponse.self) { createFeedShareApiResponse, statusCode in
                completionHandler(createFeedShareApiResponse,statusCode)
                print(">>>\(createFeedShareApiResponse)")
            }
        } catch let error {
            debugPrint(error)
        }
    }
}
