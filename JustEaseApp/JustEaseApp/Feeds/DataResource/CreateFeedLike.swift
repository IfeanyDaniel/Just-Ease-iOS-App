//
//  CreateFeedLike.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/11/23.
//

import Foundation
struct CreateFeedLikeResource {
    func getResponse(feedId: Int, creatFeedLikeRequest: CreateFeedLikeRequest, completionHandler: @escaping (_ result: CreateFeedLikeResponse?,_ statusCode: Int) -> Void) {
        let url = ApiEndpoints.createFeedLikeEndPoint + "/\(feedId)/create_feed_like"
        let creatFeedLikeUrl = URL(string: url)!
        let httpUtility = HTTPUtility()
        do {
            let creatFeedLikePostBody = try JSONEncoder().encode(creatFeedLikeRequest)
            httpUtility.postAuthorizationResponse(requestUrl: creatFeedLikeUrl, requestBody: creatFeedLikePostBody, resultType: CreateFeedLikeResponse.self) { createFeedLikeApiResponse, statusCode in
                completionHandler(createFeedLikeApiResponse,statusCode)
                print(">>>\(createFeedLikeApiResponse)")
            }
        } catch let error {
            debugPrint(error)
        }
    }
}
