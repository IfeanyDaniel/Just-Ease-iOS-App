//
//  FeedResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/11/23.
//


import Foundation
struct FeedsResource {
    func getFeedsResponse(completionHandler: @escaping (_ result: FeedsResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let  feedsUrl = ApiEndpoints.feedEndPoint
        
        let url = URL(string: feedsUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType: FeedsResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}
