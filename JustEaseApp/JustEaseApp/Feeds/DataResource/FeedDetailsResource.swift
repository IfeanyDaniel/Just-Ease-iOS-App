//
//  FeedDetailsResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/11/23.
//

import Foundation
struct FeedDetailsResource {
    func getFeedDetailsResponse(feedId: Int, userId: Int, completionHandler: @escaping (_ result: FeedDetailsResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let  feedsUrl = ApiEndpoints.feedDetailEndPoint + "/\(feedId)?user_id=\(userId)"
        let url = URL(string: feedsUrl)!
        print(feedsUrl)
        do {
            httpUtility.getAPIData(requestURL: url, resultType: FeedDetailsResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}
