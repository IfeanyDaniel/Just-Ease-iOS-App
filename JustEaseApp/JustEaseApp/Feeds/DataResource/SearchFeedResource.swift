//
//  SearchFeedResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/12/23.
//

import Foundation
struct SearchFeedsResource {
    func getSearchFeedsResponse(keyword:String, completionHandler: @escaping (_ result: SearchFeedsResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let  feedsUrl = ApiEndpoints.feedSearchEndPoint + "?search_query=\(keyword)"
        if let url = URL(string: feedsUrl) {
            // Use the valid URL
            // ...
            do {
                httpUtility.getAPIData(requestURL: url, resultType: SearchFeedsResponse.self) { result in
                    completionHandler(result)
                }
            }
        } else {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Not Found", type: .error)
            // Handle the case where feedsUrl is not a valid URL
            // You can provide a default URL or display an error message
        }
        
    }
}
