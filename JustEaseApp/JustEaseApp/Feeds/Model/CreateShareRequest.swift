//
//  CreateShareRequest.swift
//  JustEaseApp
//
//  Created by iOSApp on 12/05/2023.
//


struct CreateShareRequest: Encodable {
   let user_id: Int
}
