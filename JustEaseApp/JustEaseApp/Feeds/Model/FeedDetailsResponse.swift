//
//  CreateFeedLikeViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/11/23.
//
import Foundation

// MARK: - Welcome
struct FeedDetailsResponse: Codable {
    let status: Int
    let message: String
    let data: [FeedDetailsData]
}

// MARK: - Datum
struct  FeedDetailsData: Codable {
    let id, authorID, feedCategoryID: Int
    let title, content: String
    let imageURL: String?
    let commentsCount, feedViewCount, feedLikesCount, feedShareCount: Int
    let isLiked: Bool
    let feedCategory: FeedCategoryData?
    let comments: [Comment]

    enum CodingKeys: String, CodingKey {
        case id
        case authorID = "author_id"
        case feedCategoryID = "feed_category_id"
        case title, content
        case imageURL = "image_url"
        case commentsCount = "comments_count"
        case feedViewCount = "feed_view_count"
        case feedLikesCount = "feed_likes_count"
        case feedShareCount = "feed_share_count"
        case isLiked = "is_liked"
        case feedCategory = "feed_category"
        case comments
    }
}
// MARK: - FeedCategory
struct FeedCategoryData: Codable {
    let id: Int
    let name: String?
    let imageURL: String?
    let image: String?

    enum CodingKeys: String, CodingKey {
        case id, name
        case imageURL = "image_url"
        case image
    }
}

// MARK: - Comment
struct Comment: Codable {
    let id, userID: Int
    let commentableType: String?
    let commentableID: Int
    let content, createdAt, updatedAt: String?
    let commentLikesCount: Int
    let isLiked: Bool
    let user: FeedUserDetails?

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case commentableType = "commentable_type"
        case commentableID = "commentable_id"
        case content
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case commentLikesCount = "comment_likes_count"
        case isLiked = "is_liked"
       // case commentReplies = "comment_replies"
        case user
    }
}

// MARK: - User
struct FeedUserDetails: Codable {
    let firstName, lastName, phoneNumber, email: String?
    let address, longitude, latitude: String?
    let imageURL, emailVerifiedAt: String?

    enum CodingKeys: String, CodingKey {
        case firstName = "first_name"
        case lastName = "last_name"
        case phoneNumber = "phone_number"
        case email, address, longitude, latitude
        case imageURL = "image_url"
        case emailVerifiedAt = "email_verified_at"
    }
}

