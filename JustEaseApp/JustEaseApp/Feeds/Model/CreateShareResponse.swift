//
//  CreateShareResponse.swift
//  JustEaseApp
//
//  Created by iOSApp on 12/05/2023.
//

import Foundation
struct CreateShareResponse: Decodable {
    let status: Int
    let message: String
//    let data: CreateFeedLikeData
}
//struct CreateFeedLikeData: Decodable {
//    let id: Int
//    let author_id: Int
//    let feed_category_id: Int
//    let title: String
//    let content: String
//}
