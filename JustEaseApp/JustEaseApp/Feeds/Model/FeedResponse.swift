//
//  FeedResponse.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/11/23.
//

import Foundation
struct FeedsResponse: Decodable {
    let status: Int
    let message: String
    let data: FeedsData
}
struct FeedsData: Decodable {
    let current_page: Int
    let data: [FeedsInfo]
    let total : Int
}
struct FeedsInfo: Decodable {
    let id: Int
    let author_id: Int
    let feed_category_id: Int
    let title:  String?
    let content: String?
    let image_url: String?
    let comments_count: Int
    let feed_likes_count:Int
    let feed_view_count: Int
    let feed_share_count:Int
    let is_liked: Bool
    let feed_category: FeedCategoryInfo?
}
struct FeedCategoryInfo: Decodable {
    let id: Int
    let name: String
    let image_url: String?
    let image: String?
}
