//
//  FeedSearchModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/11/23.
//

import Foundation
struct SearchFeedsResponse: Decodable {
    let status: Int
    let data: [SearchFeedsData]
}
struct SearchFeedsData: Decodable {
    let id: Int
    let author_id: Int
    let feed_category_id: Int
    let title:  String?
    let content: String?
    let image_url: String?
    let is_liked: Bool
    let feed_category: SearchFeedCategoryInfo?
}

struct SearchFeedCategoryInfo: Decodable {
    let id: Int
    let name: String
    let image_url: String?
    let image: String?
}
