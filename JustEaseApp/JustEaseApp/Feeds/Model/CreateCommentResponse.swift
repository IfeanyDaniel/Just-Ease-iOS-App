//
//  CreateCommentResponse.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/11/23.
//

import Foundation
struct CreateCommentResponse: Decodable {
    let status: Int
    let message: String
//    let data: CreateFeedLikeData
}
//struct CreateFeedLikeData: Decodable {
//    let id: Int
//    let author_id: Int
//    let feed_category_id: Int
//    let title: String
//    let content: String
//}
