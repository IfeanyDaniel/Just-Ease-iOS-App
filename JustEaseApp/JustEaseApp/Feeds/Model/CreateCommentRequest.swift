//
//  CreateFeedComment.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/11/23.
//

import Foundation
struct CreateCommentRequest: Encodable {
   let user_id: Int
   let content : String
}
