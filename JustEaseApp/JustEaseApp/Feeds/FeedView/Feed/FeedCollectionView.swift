//
//  FeedCollectionView.swift
//  JustEaseApp
//
//  Created by iOSApp on 14/02/2023.
//


import UIKit
extension FeedsViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  feedsViewModel.numberOfRowsInSection(section: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell =  feedsCollectionView.dequeueReusableCell(withReuseIdentifier: FeedsCellData.identifier, for: indexPath) as? FeedsCellData else { return UICollectionViewCell() }
        let feeds = feedsViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
        if feeds.feedCategoryName == "" {
            cell.label.text = "Others"
        } else {
            cell.label.text = feeds.feedCategoryName
        }
        cell.detailslabel.text = feeds.title
        if feeds.feedCategoryImage != "" {
            cell.FeedsImageView.kf.setImage(with: URL(string: feeds.feedCategoryImage))
        } else {
            cell.FeedsImageView.image = UIImage(named: "justEaseIcon2")
        }
        cell.layer.borderWidth = 1
        cell.layer.borderColor = AppColors.green.color.cgColor
        cell.layer.cornerRadius = 10
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 40, height: 130)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let feeds = feedsViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
        let controller = FeedDetailsController()
        controller.feedTitle = feeds.title
        controller.content = feeds.content
        controller.feedImage = feeds.feedCategoryImage
        controller.feedIsLiked = feeds.islike
        controller.feedId = feeds.id
        controller.imageUrl = feeds.imageUrl
        print(">>>>>>>>>>>>>>>>>\(feeds.imageUrl)")
        navigationController?.pushViewController(controller, animated: true)
    }
}
