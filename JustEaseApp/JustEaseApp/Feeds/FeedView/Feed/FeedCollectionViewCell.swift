//
//  FeedCollectionViewCell.swift
//  JustEaseApp
//
//  Created by iOSApp on 14/02/2023.
//

import UIKit
class FeedsCellData: BaseCell  {
    static var identifier: String = "FeedsCellCellId"
    
    lazy var FeedsImageView :  UIImageView  = {
        let profileImageView =  UIImageView()
        profileImageView.image = UIImage(named: "justEaseIcon2")
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.layer.masksToBounds =  true
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.layer.cornerRadius = 7
        profileImageView.layer.borderWidth = 1
        profileImageView.layer.borderColor = AppColors.lightGray.color.cgColor
        return profileImageView
    }()
    
    lazy var firstlabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "DID YOU KNOW?"
        label.font = UIFont(name: AppFonts.silkLight.font, size: 12)
        label.numberOfLines = 1
        label.textColor = AppColors.green.color
        label.textAlignment = .left
        return label
    }()
    
    lazy var detailslabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Do you know that arbitral proceedings should be condu"
        label.font = UIFont(name: AppFonts.silkLight.font, size: 12)
        label.numberOfLines = 4
        label.textAlignment = .left
        label.textColor = textSystemColor
        return label
    }()
    
//    lazy var messageButton: UIButton = {
//        let button = UIButton()
//        button.translatesAutoresizingMaskIntoConstraints = false
//        button.setTitleColor(textSystemColor, for: .normal)
//        button.setTitle(" 11", for: .normal)
//        button.setImage(AppButtonImages.messageIcon.image, for: .normal)
//        button.titleLabel?.font = UIFont(name: AppFonts.silkLight.font, size: 10)
//        return button
//    }()
    lazy var label: UILabel = {
        let label = PaddingLabel(withInsets: 5, 5, 15, 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Others"
        label.textColor = AppColors.fairBrown.color
        label.textAlignment = .left
        label.backgroundColor = AppColors.lightBrown.color
        label.layer.cornerRadius = 10
        label.font = UIFont(name: AppFonts.silkLight.font ,size: 12)
        label.layer.masksToBounds = true
        return label
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
        
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        FeedsImageView.image = nil
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpViews() {
        addSubview(FeedsImageView)
        addSubview(firstlabel)
        addSubview(detailslabel)
        //addSubview(messageButton)
        addSubview(label)
        NSLayoutConstraint.activate([
            FeedsImageView.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            FeedsImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 5),
            FeedsImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5),
            FeedsImageView.widthAnchor.constraint(equalToConstant: 125),
        
            firstlabel.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            firstlabel.leadingAnchor.constraint(equalTo: FeedsImageView.trailingAnchor, constant: 5),
            firstlabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            
            detailslabel.topAnchor.constraint(equalTo: firstlabel.bottomAnchor, constant: 7),
            detailslabel.leadingAnchor.constraint(equalTo: FeedsImageView.trailingAnchor, constant: 5),
            detailslabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            
//            messageButton.topAnchor.constraint(equalTo: detailslabel.bottomAnchor, constant: 10),
//            messageButton.leadingAnchor.constraint(equalTo: FeedsImageView.trailingAnchor, constant: 5),
//            messageButton.widthAnchor.constraint(equalToConstant: 40),
            
            label.topAnchor.constraint(equalTo: detailslabel.bottomAnchor, constant: 8),
            label.leadingAnchor.constraint(equalTo: FeedsImageView.trailingAnchor, constant: 5),

        ])
        
    }
    
}

  
  



