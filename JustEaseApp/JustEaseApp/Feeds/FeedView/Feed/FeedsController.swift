//
//  Feeds.swift
//  JustEaseApp
//
//  Created by iOSApp on 14/02/2023.
//

import UIKit
class FeedsViewController: UIViewController, AuthViewModelDelegate, UITextFieldDelegate   {
    var timer:Timer?
    var feedsViewModel = FeedsViewModel()
    var authViewModel = AuthViewModel()
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Feeds"
        label.textColor = AppColors.green.color
        label.font = UIFont(name: AppFonts.silkabold.font, size: 25)
        label.textAlignment = .left
        return label
    }()
   
    lazy var searchTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Search for everything about Rights",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.addTarget(self, action: #selector(textFieldValDidChange(_:)), for: .editingChanged)
        textField.autocapitalizationType = .none
        return textField
    }()
    // Implement the UITextFieldDelegate method to handle text changes
     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         // Check if the text field already contains text
         if let currentText = textField.text {
             // Find the index of the first letter in the current text
             if let firstLetterIndex = currentText.firstIndex(where: { !$0.isWhitespace }) {
                 // Check if the replacement string starts with spaces
                 if string.first == " " && string.prefix(upTo: firstLetterIndex).contains(" ") {
                     // If the replacement string starts with spaces after the first letter, ignore those spaces
                     textField.text = String(currentText.prefix(upTo: firstLetterIndex)) + string.trimmingCharacters(in: .whitespacesAndNewlines)
                     return false
                 }
             }
         }

         // Allow other characters to be entered
         return true
     }
    // MARK: - ... Validation of all search field
    @objc func textFieldValDidChange(_ textField: UITextField) {
        timer?.invalidate()
        //setup timer
        timer = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.searchText), userInfo: nil, repeats: false)
        
    }
    @objc func searchText() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.6) { [weak self] in
            if self?.searchTextField.text != "" {
                Loader.shared.showLoader()
                self?.feedsViewModel.getSearchFeeds(keyword: self?.searchTextField.text ?? "") {
                    Loader.shared.hideLoader()
                    DispatchQueue.main.async { [self] in
                        self?.feedsCollectionView.reloadData()
                    }
                }
            } else {
                self?.getAllFeeds()
            }
        }
    }
    lazy var feedsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 15
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        cv.backgroundColor = backgroundSystemColor
        cv.dataSource = self
        cv.delegate = self
        cv.showsVerticalScrollIndicator = true
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    
    func showDoneButton(){
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dropKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.searchTextField.inputAccessoryView = toolbar
    }
    @objc func dropKeyboard(){
        view.endEditing(true)
    }

    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        showDoneButton()
        layoutViews()
        self.hideKeyboardWhenTappedAround()
        registercCell()
        searchTextField.delegate = self
        
    }
    func getAllFeeds() {
        Loader.shared.showLoader()
        feedsViewModel.getFeeds {
            Loader.shared.hideLoader()
            DispatchQueue.main.async { [self] in
                feedsCollectionView.reloadData()
            }
        }

    }
    override func viewWillAppear(_ animated: Bool) {
        authViewModel.delegate = self
        authenticate()
    }
    func authenticate(){
        if InternetConnectionManager.isConnectedToNetwork(){
            Loader.shared.showLoader()
            let request = AuthRequest(email: LoginDetails().email, password: LoginDetails().password)
            authViewModel.authResponse(authRequest: request)
        } else {
            Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
        }
    }
    func registercCell() {
        feedsCollectionView.register(FeedsCellData.self, forCellWithReuseIdentifier: FeedsCellData.identifier)
    }
    
    func didReceiveAuthResponse(AuthResponse: AuthResponse?, _ statusCode: Int) {
        if statusCode == 200 {
            if InternetConnectionManager.isConnectedToNetwork(){
                getAllFeeds()
            }
            else {
                Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
            }
        }
        if statusCode == 400 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Something went wrong, Please try again", type: .error)
        }
    }
}
