//
//  FeedsDetailsConstraint.swift
//  JustEaseApp
//
//  Created by iOSApp on 26/02/2023.
//

import UIKit
extension FeedDetailsController {
    //MARK: - Layout subviews for the scroll views
    func setupScrollView() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: didYouKnowLabel.bottomAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
        ])
    }
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(didYouKnowLabel)
        contentView.addSubview(label)
        contentView.addSubview(image)
        contentView.addSubview(note)                                                                                                                          
        contentView.addSubview(viewAndSharesLabel)
        contentView.addSubview(loveButton)
        contentView.addSubview(noLoveButton)
        contentView.addSubview(messageButton)
        contentView.addSubview(shareButton)
        
        contentView.addSubview(commentView)
        commentView.addSubview(profileIcon)
        commentView.addSubview(defaultProfileIcon)
        commentView.addSubview(commentButton)
        
        contentView.addSubview(commentCollectionView)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = .systemBackground
        
        NSLayoutConstraint.activate([
            didYouKnowLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            didYouKnowLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            
            label.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15),
            label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leadingNumber),
            label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: trailingNumber),
            
            image.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 15),
            image.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leadingNumber),
            image.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: trailingNumber),
            image.heightAnchor.constraint(equalToConstant: 200),
            
            note.topAnchor.constraint(equalTo: image.bottomAnchor, constant: 20),
            note.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leadingNumber),
            note.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: trailingNumber),
            note.heightAnchor.constraint(equalToConstant: 150),
            
            viewAndSharesLabel.topAnchor.constraint(equalTo: note.bottomAnchor, constant: 10),
            viewAndSharesLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leadingNumber),
            
            loveButton.topAnchor.constraint(equalTo: viewAndSharesLabel.bottomAnchor, constant: 10),
            loveButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leadingNumber),
            loveButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            loveButton.widthAnchor.constraint(equalToConstant: 60),
            
            noLoveButton.topAnchor.constraint(equalTo: viewAndSharesLabel.bottomAnchor, constant: 10),
            noLoveButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leadingNumber),
            noLoveButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            noLoveButton.widthAnchor.constraint(equalToConstant: 60),
            
            messageButton.topAnchor.constraint(equalTo: viewAndSharesLabel.bottomAnchor, constant: 10),
            messageButton.leadingAnchor.constraint(equalTo: loveButton.trailingAnchor, constant: leadingNumber),
            messageButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            messageButton.widthAnchor.constraint(equalToConstant: 60),
            
            shareButton.topAnchor.constraint(equalTo: viewAndSharesLabel.bottomAnchor, constant: 10),
            shareButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: trailingNumber),
            shareButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            shareButton.widthAnchor.constraint(equalToConstant: 60),
            
            commentView.topAnchor.constraint(equalTo: shareButton.bottomAnchor, constant: 10),
            commentView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0),
            commentView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0),
            commentView.heightAnchor.constraint(equalToConstant: 70),
            
            defaultProfileIcon.topAnchor.constraint(equalTo: commentView.topAnchor, constant: 12),
            defaultProfileIcon.leadingAnchor.constraint(equalTo: commentView.leadingAnchor, constant: leadingNumber),
            defaultProfileIcon.heightAnchor.constraint(equalToConstant: 40),
            defaultProfileIcon.widthAnchor.constraint(equalToConstant: 40),
            
            profileIcon.topAnchor.constraint(equalTo: commentView.topAnchor, constant: 12),
            profileIcon.leadingAnchor.constraint(equalTo: commentView.leadingAnchor, constant: leadingNumber),
            profileIcon.heightAnchor.constraint(equalToConstant: 40),
            profileIcon.widthAnchor.constraint(equalToConstant: 40),
            
            commentButton.topAnchor.constraint(equalTo: commentView.topAnchor, constant: 12),
            commentButton.leadingAnchor.constraint(equalTo: profileIcon.trailingAnchor, constant: 15),
            commentButton.trailingAnchor.constraint(equalTo: commentView.trailingAnchor, constant: trailingNumber),
            commentButton.heightAnchor.constraint(equalToConstant: 40),
            
        ])
        commentCollectionView.anchorWithConstantsToTop(commentView.bottomAnchor,
                                                       left: contentView.leftAnchor, bottom:  contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 0, leftConstant:0, bottomConstant: 100, rightConstant: 0)
    }
}
