//
//  FeedDetailsCollectionView.swift
//  JustEaseApp
//
//  Created by iOSApp on 27/02/2023.
//

import UIKit
import Kingfisher
extension Date {
    func adding(minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
}

extension FeedDetailsController {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        feedDetailsViewModel.numberOfComments()
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = commentCollectionView.dequeueReusableCell(withReuseIdentifier: CommentCell.identifier, for: indexPath) as? CommentCell else { return UICollectionViewCell() }
    //    let commentDetails = feedDetailsViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
        let comment = feedDetailsViewModel.cellForRowsComment(indexPath: indexPath)[indexPath.row]
        cell.index = indexPath
        
        if comment.imageUrl != "" || comment.imageUrl != "https://kmr-staging.lawpavilion.com/storage/images/apiusers/" {
            cell.profileImage.kf.setImage(with: URL(string: "\(comment.imageUrl ?? "")"))
            cell.profileImage.isHidden = false
            cell.defaultImage.isHidden =  true
        } else {
            cell.defaultImage.setTitle("\(comment.first_name.first!) \(comment.last_name.first!)", for: .normal)
            cell.defaultImage.isHidden = false
            cell.profileImage.isHidden = true
        }
        cell.nameLabel.text = "\(comment.first_name) \(comment.last_name)"
        cell.commentLabel.text = "\(comment.content)"
        if comment.is_liked == true {
            cell.loveButton.isHidden = false
            cell.noLoveButton.isHidden = true
        } else {
            cell.loveButton.isHidden = true
            cell.noLoveButton.isHidden = false
        }
       
        let timeAgo = comment.createdAt
        cell.timeLabel.text = timeAgo.timeAgoDisplay()
        cell.commentDelegate = self
        cell.backgroundColor = textFieldColor
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.size.width, height: 130)
    }

}
