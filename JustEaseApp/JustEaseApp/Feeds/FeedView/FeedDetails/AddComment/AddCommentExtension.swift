//
//  AddCommentExtension.swift
//  JustEaseApp
//
//  Created by iOSApp on 12/05/2023.
//

import Foundation
extension AddCommentViewController : CreateCommentViewModelDelegate, CreateCrimeCommentDelegate {
    func didReceiveCreateCrimeCommentResponse(createCrimeResponse: CreateCrimeCommentResponse?, _ statusCode: Int) {
        if statusCode == 200 {
            Toast.shared.showToastWithTItle("Sent successfully", type: .success)
            dismiss(animated: false) {
                self.delegate?.reloadCommentSection()
            }
        }
        if statusCode == 400 {
            Toast.shared.showToastWithTItle(createCrimeResponse?.message ?? "", type: .error)
        }
    }
    
    func didReceiveCreateCommentResponse(createFeedLikeResponse: CreateCommentResponse?, _ statusCode: Int) {
        if statusCode == 200 {
            Toast.shared.showToastWithTItle("Sent successfully", type: .success)
            dismiss(animated: false) {
                self.delegate?.reloadCommentSection()
            }
        }
        if statusCode == 400 {
            Toast.shared.showToastWithTItle(createFeedLikeResponse?.message ?? "", type: .error)
        }
    }
}
