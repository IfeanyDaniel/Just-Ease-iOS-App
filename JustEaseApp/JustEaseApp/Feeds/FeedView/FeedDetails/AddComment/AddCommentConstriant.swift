//
//  AddCommentConstriant.swift
//  JustEaseApp
//
//  Created by iOSApp on 14/03/2023.
//

import UIKit
extension  AddCommentViewController {
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(commentTextField)
        view.addSubview(line)
        view.addSubview(replyButton)
        view.addSubview(replyCrimeButton)
        view.addSubview(buttonStack)
        view.backgroundColor =  viewStackColor
        NSLayoutConstraint.activate([
            commentTextField.topAnchor.constraint(equalTo: view.topAnchor, constant: 30),
            commentTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber),
            commentTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            commentTextField.heightAnchor.constraint(equalToConstant: textFieldHeight),
            
            line.topAnchor.constraint(equalTo: commentTextField.bottomAnchor, constant: 2),
            line.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            line.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            line.heightAnchor.constraint(equalToConstant: 0.5),
            
            replyButton.topAnchor.constraint(equalTo: line.bottomAnchor, constant: 10),
            replyButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            replyButton.heightAnchor.constraint(equalToConstant: textFieldHeight - 10),
            replyButton.widthAnchor.constraint(equalToConstant: 40),
            
            replyCrimeButton.topAnchor.constraint(equalTo: line.bottomAnchor, constant: 10),
            replyCrimeButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            replyCrimeButton.heightAnchor.constraint(equalToConstant: textFieldHeight - 10),
            replyCrimeButton.widthAnchor.constraint(equalToConstant: 40),
            
            buttonStack.topAnchor.constraint(equalTo: line.bottomAnchor, constant: 25),
            buttonStack.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber),
            buttonStack.heightAnchor.constraint(equalToConstant: 22),
            buttonStack.widthAnchor.constraint(equalToConstant: 50),
        ])
       
    }
}
