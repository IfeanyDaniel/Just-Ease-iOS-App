//
//  CommentViewController.swift
//  JustEaseApp
//
//  Created by iOSApp on 14/03/2023.
//

import UIKit
protocol ReloadCommentDelegate: AnyObject {
    func reloadCommentSection()
}
class AddCommentViewController: UIViewController {
    var delegate : ReloadCommentDelegate?
    var createCommentViewModel = CreateCommentViewModel()
    var createCrimeCommentViewModel = CreateCrimeCommentViewModel()
    var feedId = 0
    var reportCrimeId = 0
    lazy var commentTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Add a comment...",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.layer.borderColor = viewStackColor.cgColor
        textField.layer.borderWidth = 0
        textField.backgroundColor =  viewStackColor
        textField.addTarget(self, action: #selector(validateViews(_:)), for: .editingChanged)
        return textField
    }()
    // MARK: - content view
    lazy var line: UIView = {
        let content = UIView()
        content.translatesAutoresizingMaskIntoConstraints = false
        content.backgroundColor = AppColors.green.color
        return content
    }()
    lazy var replyButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Reply", for: .normal)
        button.layer.masksToBounds = true
        button.backgroundColor = viewStackColor
        button.setTitleColor(AppColors.green.color, for: .normal)
        button.titleLabel?.font = UIFont(name: AppFonts.silka.font, size: 12)
        button.addTarget(self, action: #selector(didTapOnReply), for: .touchUpInside)
        return button
    }()
    lazy var replyCrimeButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Reply", for: .normal)
        button.layer.masksToBounds = true
        button.backgroundColor = viewStackColor
        button.setTitleColor(AppColors.green.color, for: .normal)
        button.titleLabel?.font = UIFont(name: AppFonts.silka.font, size: 12)
        button.isHidden = true
        button.addTarget(self, action: #selector(didTapOnReplyCrimeButton), for: .touchUpInside)
        return button
    }()
    lazy var picIcon: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.picIcon.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var gifIcon: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.gifIcon.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var buttonStack: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(picIcon)
        stackView.addArrangedSubview(gifIcon)
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 10
        return stackView
    }()
    @objc func didTapOnReply() {
        if commentTextField.text != "" {
            if InternetConnectionManager.isConnectedToNetwork(){
                let request = CreateCommentRequest(user_id: Storage.getUserId(), content: commentTextField.text ?? "")
                createCommentViewModel.getCreateCommentResponse(feedId: feedId, createCommentRequest: request)
            } else {
                Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
            }
        } else {
            Toast.shared.showToastWithTItle("Please enter a comment", type: .error)
        }
    }
    @objc func didTapOnReplyCrimeButton() {
        if commentTextField.text != "" {
            if InternetConnectionManager.isConnectedToNetwork(){
                let request = CreateCrimeCommentRequest(user_id: Storage.getUserId(), content: commentTextField.text ?? "")
                createCrimeCommentViewModel.getCreateCrimeCommentResponse(reportCrimeId: reportCrimeId, createCrimeCommentRequest: request)
            } else {
                Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
            }
        } else {
            Toast.shared.showToastWithTItle("Please enter a comment", type: .error)
        }
    }
    func showDoneButton(){
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dropKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.commentTextField.inputAccessoryView = toolbar
    }
    @objc func dropKeyboard(){
        view.endEditing(true)
    }
    // MARK: - ... Validation of all textfield
    @objc func validateViews(_ textField: UITextField) {
       
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        showDoneButton()
        createCommentViewModel.delegate = self
        createCrimeCommentViewModel.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        commentTextField.becomeFirstResponder()
    }
}
