//
//  FeedDetailsCell.swift
//  JustEaseApp
//
//  Created by iOSApp on 27/02/2023.
//

import UIKit
protocol CommentDelegate: AnyObject {
    func onClickLoveButton(index: Int)
    func onClickNoLoveButton(index: Int)
}
class CommentCell: UICollectionViewCell {
    static var identifier: String = "FeedCommentCell"
    var commentDelegate: CommentDelegate?
    var index : IndexPath?
    var card: Cards? {
        didSet {
            guard let card = card else {
                return
            }
            let forwardImage = card.forwardImage
            defaultImage.setImage(forwardImage, for: .normal)
            
            let cardName = card.title
            nameLabel.text = cardName
        }
    }
    
    lazy var defaultImage: UIButton = {
        let button = UIButton.defaultImageButton()
        button.isHidden = true
        button.layer.cornerRadius = 20
        return button
    }()
    lazy var profileImage:  UIImageView  = {
        let profileImageView =  UIImageView.customImage()
        profileImageView.isHidden = true
        profileImageView.layer.cornerRadius = 20
        return profileImageView
    }()
    lazy var nameLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textAlignment = .left
        label.text = "IFEANYI MBATA"
        label.textColor = textSystemColor
        label.font = UIFont(name: AppFonts.silkabold.font, size: 12)
        return label
    }()
    lazy var timeLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textAlignment = .left
        label.text = "1 day ago"
        label.textColor = textSystemColor
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 10)
        return label
    }()
    lazy var commentLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textAlignment = .left
        label.textColor = textSystemColor
        label.text = "I love playing footbal with boot in the field"
        label.font = UIFont(name: AppFonts.silkLight.font, size: 12)
        return label
    }()
    lazy var loveButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.backgroundColor = textFieldColor
        button.titleLabel?.font = UIFont(name:  AppFonts.silkLight.font, size: 12)
        button.setTitleColor(AppColors.green.color, for: .normal)
        button.setImage(UIImage(named: "love"), for: .normal)
        button.setTitle("  1", for: .normal)
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(didTapOnLoveButton(_:)), for: .touchUpInside)
        return button
    }()
    lazy var noLoveButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.backgroundColor = .clear
        button.isHidden = true
        button.titleLabel?.font = UIFont(name:  AppFonts.silkLight.font, size: 12)
        button.setTitleColor(AppColors.green.color, for: .normal)
        button.setImage(UIImage(named: "noLove"), for: .normal)
        button.setTitle("  0", for: .normal)
        button.addTarget(self, action: #selector(didTapOnNoLoveButton(_:)), for: .touchUpInside)
        return button
    }()
    @objc func didTapOnLoveButton(_ sender: Any){
        guard let indexRow = index?.row else {
            return
        }
        commentDelegate?.onClickLoveButton(index: indexRow)
    }
    @objc func didTapOnNoLoveButton(_ sender: Any){
        guard let indexRow = index?.row else {
            return
        }
        commentDelegate?.onClickNoLoveButton(index: indexRow)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    func setUpViews() {
        addSubview(defaultImage)
        addSubview(nameLabel)
        addSubview(timeLabel)
        addSubview(commentLabel)
        addSubview(loveButton)
        addSubview(noLoveButton)
        addSubview(profileImage)
        
        NSLayoutConstraint.activate([
            defaultImage.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            defaultImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            defaultImage.heightAnchor.constraint(equalToConstant: 40),
            defaultImage.widthAnchor.constraint(equalToConstant: 40),
            
            profileImage.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            profileImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            profileImage.heightAnchor.constraint(equalToConstant: 40),
            profileImage.widthAnchor.constraint(equalToConstant: 40),
            
            nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            nameLabel.leadingAnchor.constraint(equalTo:defaultImage.trailingAnchor, constant: 10),
            nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            
            timeLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 5),
            timeLabel.leadingAnchor.constraint(equalTo: defaultImage.trailingAnchor, constant: 10),
            timeLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            
            commentLabel.topAnchor.constraint(equalTo: timeLabel.bottomAnchor, constant: 5),
            commentLabel.leadingAnchor.constraint(equalTo:defaultImage.trailingAnchor, constant: 10),
            commentLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            
            noLoveButton.topAnchor.constraint(equalTo: commentLabel.bottomAnchor, constant: 15),
            noLoveButton.leadingAnchor.constraint(equalTo: defaultImage.trailingAnchor, constant: 5),
            noLoveButton.heightAnchor.constraint(equalToConstant: 40),
            noLoveButton.widthAnchor.constraint(equalToConstant: 60),
            
            loveButton.topAnchor.constraint(equalTo: commentLabel.bottomAnchor, constant: 15),
            loveButton.leadingAnchor.constraint(equalTo: defaultImage.trailingAnchor, constant: 5),
            loveButton.heightAnchor.constraint(equalToConstant: 40),
            loveButton.widthAnchor.constraint(equalToConstant: 60),
            
        ])
    }
}
