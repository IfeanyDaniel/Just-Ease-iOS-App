//
//  FeedsDetailsController.swift
//  JustEaseApp
//
//  Created by iOSApp on 26/02/2023.
//

import UIKit
import FittedSheets
import Kingfisher
class FeedDetailsController: UIViewController,UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout  {
    // MARK: - Scroll view
    var feedTitle = ""
    var content = ""
    var feedImage = ""
    var feedIsLiked = false
    var imageUrl = ""
    var feedDetailsViewModel = FeedDetailsViewModel()
    var createFeedLikeViewModel = CreateFeedLikeViewModel()
   
    var likes: Int = 0
    var feedId = 0
    var numberOfLikes = 0
    lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.showsHorizontalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    // MARK: - content view
    lazy var contentView: UIView = {
        let content = UIView()
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    
    lazy var didYouKnowLabel: UILabel = {
        let label = UILabel()
        label.text = "DID YOU KNOW?"
        label.textColor = AppColors.green.color
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 25)
        label.textAlignment = .center
        return label
    }()
    
    //MARK: - Label below welcome back
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.font = UIFont(name: AppFonts.silkaMedium.font ,size: 16)
        label.text = "Digital assault can put you into jail for 15 years?"
        return label
    }()
    lazy var image :  UIImageView  = {
        let profileImageView =  UIImageView()
        profileImageView.image = UIImage(named: "justEaseIcon2")
        profileImageView.contentMode = .scaleAspectFit
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.layer.cornerRadius = 7
        profileImageView.isUserInteractionEnabled = true
        return profileImageView
    }()
    lazy var note: UITextView = {
        let tv = UITextView.textViewDesign()
        tv.text = ""
        return tv
    }()

    lazy var viewAndSharesLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textColor = placeholderSystemGrayColor
        label.font = UIFont(name: AppFonts.silkLight.font ,size: 14)
        label.text = "15 Views ● 15 Shares"
        return label
    }()
    lazy var loveButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.backgroundColor = textFieldColor
        button.layer.cornerRadius = 25
        button.setTitleColor(textSystemColor, for: .normal)
        button.setImage(UIImage(named: "love"), for: .normal)
        button.addTarget(self, action: #selector(didTapLoveButton), for: .touchUpInside)
        return button
    }()
    lazy var noLoveButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.backgroundColor = textFieldColor
        button.layer.cornerRadius = 25
        button.isHidden = true
        button.setTitleColor(textSystemColor, for: .normal)
        button.setImage(UIImage(named: "noLove"), for: .normal)
        button.addTarget(self, action: #selector(didTapNoLoveButton), for: .touchUpInside)
        return button
    }()
    @objc func didTapLoveButton() {
        if InternetConnectionManager.isConnectedToNetwork(){
            let request = CreateFeedLikeRequest(user_id: Storage.getUserId())
            createFeedLikeViewModel.getCreateFeedLikeResponse(feedId: feedId, createFeedLikeRequest: request)
        } else {
            Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
        }
    showUnlikeButton()
        if feedIsLiked == true {
            noLoveButton.setTitle(" \(numberOfLikes - 1)", for: .normal)
        } else {
            noLoveButton.setTitle(" \(numberOfLikes)", for: .normal)
        }
        
    }
    @objc func didTapNoLoveButton() {
        if InternetConnectionManager.isConnectedToNetwork(){
            let request = CreateFeedLikeRequest(user_id: Storage.getUserId())
            createFeedLikeViewModel.getCreateFeedLikeResponse(feedId: feedId, createFeedLikeRequest: request)
        } else {
            Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
        }
       showlikeButton()
        if feedIsLiked == true {
            loveButton.setTitle(" \(numberOfLikes)", for: .normal)
        } else {
            loveButton.setTitle(" \(numberOfLikes + 1)", for: .normal)
        }
    }
    
    func showlikeButton() {
        noLoveButton.isHidden = true
        loveButton.isHidden = false
        loveButton.setTitle(" \(numberOfLikes)", for: .normal)
        
    }
    func showUnlikeButton() {
        noLoveButton.isHidden = false
        loveButton.isHidden = true
        noLoveButton.setTitle(" \(numberOfLikes)", for: .normal)
    }
    lazy var messageButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.backgroundColor = backgroundSystemColor
        button.layer.cornerRadius = 0
        button.setTitleColor(textSystemColor, for: .normal)
        button.setImage(AppButtonImages.messageIcon.image, for: .normal)
        button.setTitle("  8", for: .normal)
        button.addTarget(self, action: #selector(didTapNoLoveButton), for: .touchUpInside)
        return button
    }()
    
    lazy var shareButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.backgroundColor = backgroundSystemColor
        button.layer.cornerRadius = 0
        button.frame.size.width = 55
        button.setImage(UIImage(named: "share"), for: .normal)
        button.addTarget(self, action: #selector(didTapShareButton), for: .touchUpInside)
        return button
    }()
    lazy var commentView: UIView = {
        let content = UIView()
        content.backgroundColor = textFieldColor
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
     lazy var defaultProfileIcon: UIButton = {
         let button = UIButton.defaultImageButton()
         button.layer.cornerRadius = 20
         button.isHidden = true
         button.setTitle(getUserInitial() , for: .normal)
         button.backgroundColor = AppColors.red.color
         return button
     }()
    lazy var profileIcon :  UIImageView  = {
        let profileImageView =  UIImageView.customImage()
        profileImageView.isUserInteractionEnabled = true
        profileImageView.layer.masksToBounds = true
        profileImageView.isHidden = true
        profileImageView.layer.cornerRadius = 20
        return profileImageView
    }()
    lazy var commentButton: UIButton = {
        let button = UIButton.selectButtonDesign()
        button.backgroundColor = commentButtonColor
        button.layer.cornerRadius = 15
        button.setTitle(" Add a comment", for: .normal)
        button.setTitleColor(placeholderSystemGrayColor, for: .normal)
        button.addTarget(self, action: #selector(didTapCommentButton), for: .touchUpInside)
        return button
    }()
    @objc func didTapCommentButton() {
        let controller = AddCommentViewController()
        controller.delegate = self
        controller.feedId = feedId
        controller.replyButton.isHidden = false
        controller.replyCrimeButton.isHidden = true
        let sheetController = SheetViewController(controller: controller, sizes: [.fixed(200), .fullScreen])
        sheetController.blurBottomSafeArea = false
        sheetController.handleView.isHidden = true
        sheetController.topCornersRadius = 30
        present(sheetController, animated: false, completion: nil)
    }
    lazy var commentCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        cv.backgroundColor = textFieldColor
        cv.dataSource = self
        cv.delegate = self
        cv.showsVerticalScrollIndicator = false
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    @objc func didTapShareButton() {
        let shareSheetVC = UIActivityViewController(
            activityItems: [label.text as Any, note.text as Any ], applicationActivities: nil
        )
        self.present(shareSheetVC, animated: true)
    }
    func registercCell() {
        commentCollectionView.register(CommentCell.self, forCellWithReuseIdentifier: CommentCell.identifier)
    }
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        setupScrollView()
        self.hideKeyboardWhenTappedAround()
        registercCell()
        getFeedDetails()
        createFeedLikeViewModel.delegate = self
    }
    func getFeedDetails() {
        Loader.shared.showLoader()
        feedDetailsViewModel.getFeeds(feedId: feedId, userId: Storage.getUserId()) {
            Loader.shared.hideLoader()
            if self.feedDetailsViewModel.numberOfComments() > 0 {
                self.commentCollectionView.reloadData()
            } else {
                self.commentCollectionView.isHidden = true
            }
            
            self.feedIsLiked = self.feedDetailsViewModel.getSomeFeedDetails()[0].islike
            self.messageButton.setTitle(" \(self.feedDetailsViewModel.getSomeFeedDetails()[0].commentsCount)", for: .normal)
            self.viewAndSharesLabel.text = " \(self.feedDetailsViewModel.getSomeFeedDetails()[0].feedViewCount) Views ● \(self.feedDetailsViewModel.getSomeFeedDetails()[0].feedShareCount) Shares"
            self.numberOfLikes = self.feedDetailsViewModel.getSomeFeedDetails()[0].feedLikesCount
            print("???\(self.numberOfLikes)")
            self.getAllFeedDetails()
        }
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
       // getAllFeedDetails()
    }
    func getAllFeedDetails() {
        label.text = feedTitle
        image.kf.setImage(with: URL(string: feedImage))
        note.text =  content
        if Storage.getImageUrl() != ""  {
            profileIcon.kf.setImage(with: URL(string: "\(Storage.getImageUrl())"))
            profileIcon.isHidden = false
            defaultProfileIcon.isHidden = true
        } else {
            defaultProfileIcon.setTitle("\(getUserInitial())", for: .normal)
            profileIcon.isHidden = true
            defaultProfileIcon.isHidden = false
        }
        if feedImage != "" {
            image.kf.setImage(with: URL(string: feedImage))
        } else {
            image.image = UIImage(named: "justEaseIcon2")
        }
        if feedIsLiked == true {
            showlikeButton()
            
        } else {
            showUnlikeButton()
        }
    }
    // MARK: - View did layout subviews
    override func viewDidLayoutSubviews() {
        scrollView.isScrollEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.contentSize = CGSize(width: 400, height: 1100)
    }
}
extension  FeedDetailsController: CommentDelegate, CreateFeedLikeViewModelDelegate , ReloadCommentDelegate{
    func reloadCommentSection() {
        getFeedDetails()
    }
    
    func didReceiveCreateFeedLikeResponse(createFeedLikeResponse: CreateFeedLikeResponse?, _ statusCode: Int) {
        if statusCode == 200 {
            
        }
        if statusCode == 400 {
            Toast.shared.showToastWithTItle(createFeedLikeResponse?.message ?? "", type: .error)
        }
    }
    
    func onClickLoveButton(index: Int) {
        
    }
    
    func onClickNoLoveButton(index: Int) {
        
    }
    
    
}
