//
//  FeedViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/11/23.
//

import Foundation
struct FeedsDetails: Codable {
    var id: Int
    var feedCategoryId: Int
    var title: String
    var content: String
    var imageUrl: String
    var islike: Bool
    var feedCategoryImage: String
    var feedCategoryName: String
}
class FeedsViewModel {
    var feedsDetails:[FeedsDetails] = []
    var homePageFeed: [FeedsDetails] = []
    func getFeeds(completion: @escaping () -> Void) {
        let feedsResource = FeedsResource()
        feedsResource.getFeedsResponse { getFeedsApiResponse in
            DispatchQueue.main.async { [self] in
                let feedData = getFeedsApiResponse?.data.data
                feedsDetails.removeAll()
                homePageFeed.removeAll()
                for index in 0..<(feedData?.count ?? 0) {
                    let feedId = feedData?[index].id ?? 0
                    let feedCategoryId = feedData?[index].feed_category?.id ?? 0
                    let feedTitle = feedData?[index].title ?? ""
                    let feedContent = feedData?[index].content ?? ""
                    let feedImageUrl = feedData?[index].image_url ?? ""
                    let feedLiked = feedData?[index].is_liked ?? false
                    let feedCategoryImageUrl = feedData?[index].feed_category?.image_url ?? ""
                    let feedName = feedData?[index].feed_category?.name ?? ""
                    
                    feedsDetails.append(FeedsDetails(id: feedId, feedCategoryId: feedCategoryId, title: feedTitle, content: feedContent, imageUrl: feedImageUrl,islike: feedLiked, feedCategoryImage: feedCategoryImageUrl, feedCategoryName: feedName))
                }
                for index in 0..<5 {
                    let feedId = feedData?[index].id ?? 0
                    let feedCategoryId = feedData?[index].feed_category?.id ?? 0
                    let feedTitle = feedData?[index].title ?? ""
                    let feedContent = feedData?[index].content ?? ""
                    let feedImageUrl = feedData?[index].image_url ?? ""
                    let feedLiked = feedData?[index].is_liked ?? false
                    let feedCategoryImageUrl = feedData?[index].feed_category?.image_url ?? ""
                    let feedName = feedData?[index].feed_category?.name ?? ""
                    
                    homePageFeed.append(FeedsDetails(id: feedId, feedCategoryId: feedCategoryId, title: feedTitle, content: feedContent, imageUrl: feedImageUrl, islike: feedLiked, feedCategoryImage: feedCategoryImageUrl, feedCategoryName: feedName))
                }
                completion()
            }
        }
    }
    
    func getSearchFeeds(keyword:String, completion: @escaping () -> Void) {
        let feedsResource = SearchFeedsResource()
        feedsResource.getSearchFeedsResponse(keyword: keyword) { getFeedsApiResponse in
            DispatchQueue.main.async { [self] in
                let feedData = getFeedsApiResponse?.data
                feedsDetails.removeAll()
                for index in 0..<(feedData?.count ?? 0) {
                    let feedId = feedData?[index].id ?? 0
                    let feedCategoryId = feedData?[index].feed_category?.id ?? 0
                    let feedTitle = feedData?[index].title ?? ""
                    let feedContent = feedData?[index].content ?? ""
                    let feedImageUrl = feedData?[index].image_url ?? ""
                    let feedLiked = feedData?[index].is_liked ?? false
                    let feedCategoryImageUrl = feedData?[index].feed_category?.image_url ?? ""
                    let feedName = feedData?[index].feed_category?.name ?? ""
                    
                    feedsDetails.append(FeedsDetails(id: feedId, feedCategoryId: feedCategoryId, title: feedTitle, content: feedContent, imageUrl: feedImageUrl, islike: feedLiked, feedCategoryImage: feedCategoryImageUrl, feedCategoryName: feedName))
                }
                completion()
            }
        }
    }
    func numberOfRowsInSection(section: Int) -> Int {
        return  feedsDetails.count
    }
    func cellForRowsAt(indexPath: IndexPath) -> [FeedsDetails] {
        return feedsDetails
    }
    func numberOfRowsInSectionHome(section: Int) -> Int {
        return  homePageFeed.count
    }
    func cellForRowsAtHome(indexPath: IndexPath) -> [FeedsDetails] {
        return homePageFeed
    }
}
