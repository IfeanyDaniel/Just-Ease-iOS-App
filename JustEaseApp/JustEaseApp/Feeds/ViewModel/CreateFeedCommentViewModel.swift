//
//  CreateFeedCommentViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/12/23.
//

import Foundation
protocol CreateCommentViewModelDelegate {
    func didReceiveCreateCommentResponse(createFeedLikeResponse: CreateCommentResponse?,_ statusCode: Int)
}
class CreateCommentViewModel {
    var delegate: CreateCommentViewModelDelegate?
    func getCreateCommentResponse(feedId: Int, createCommentRequest: CreateCommentRequest) {
        let createCommentResource = CreateCommentResource()
        createCommentResource.getResponse(feedId: feedId, createFeedCommentRequest: createCommentRequest) { createCommentApiResponse, statusCode in
            if statusCode ==  200 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateCommentResponse(createFeedLikeResponse: createCommentApiResponse, statusCode)
                }
            }
            if statusCode ==  400 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateCommentResponse(createFeedLikeResponse: createCommentApiResponse, statusCode)
                }
            }
            if statusCode ==  422 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateCommentResponse(createFeedLikeResponse: createCommentApiResponse, statusCode)
                }
            }
            if statusCode ==  500 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateCommentResponse(createFeedLikeResponse: createCommentApiResponse, statusCode)
                }
            }
            if statusCode ==  200 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateCommentResponse(createFeedLikeResponse: createCommentApiResponse, statusCode)
                }
            }
        }
    }
}
