//
//  CreateShareViewModel.swift
//  JustEaseApp
//
//  Created by iOSApp on 12/05/2023.
//

import Foundation
protocol CreateShareViewModelDelegate {
    func didReceiveCreateShareResponse(createFeedShareResponse: CreateShareResponse?,_ statusCode: Int)
}
class CreateShareViewModel {
    var delegate: CreateShareViewModelDelegate?
    func getCreateShareResponse(feedId: Int, createShareRequest: CreateShareRequest) {
        let createShareResource = CreateShareResource()
        createShareResource.getResponse(feedId: feedId, createFeedShareRequest: createShareRequest) { createShareApiResponse, statusCode in
            if statusCode ==  200 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateShareResponse(createFeedShareResponse: createShareApiResponse, statusCode)
                }
            }
            if statusCode ==  400 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateShareResponse(createFeedShareResponse: createShareApiResponse, statusCode)
                }
            }
            if statusCode ==  422 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateShareResponse(createFeedShareResponse: createShareApiResponse, statusCode)
                }
            }
            if statusCode ==  500 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateShareResponse(createFeedShareResponse: createShareApiResponse, statusCode)
                }
            }
            if statusCode ==  200 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateShareResponse(createFeedShareResponse: createShareApiResponse, statusCode)
                }
            }
        }
    }
}
