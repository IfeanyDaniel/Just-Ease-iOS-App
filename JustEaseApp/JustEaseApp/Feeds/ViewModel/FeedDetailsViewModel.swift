//
//  FeedDetailsViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/11/23.
import Foundation
struct AllFeedsDetails: Codable {
    var id: Int
    var feedCategoryId: Int
    var commentsCount: Int
    var feedViewCount: Int
    var feedLikesCount: Int
    var feedShareCount: Int
    var islike: Bool
    var feedComment: [FeedComment]
}
struct FeedComment: Codable {
    var id: Int
    var imageUrl: String?
    var createdAt: Date
    var content : String
    var first_name: String
    var last_name: String
    var comment_likes_count: Int
    var is_liked: Bool
    
}
struct UserDetails: Codable {
    var first_name: String
    var last_name: String
}
class FeedDetailsViewModel {
    var feedsDetails:[AllFeedsDetails] = []
    var feedComment: [FeedComment] = []
    func getFeeds(feedId: Int, userId: Int,completion: @escaping () -> Void) {
        let feedsDetailsResource = FeedDetailsResource()
        feedsDetailsResource.getFeedDetailsResponse(feedId: feedId, userId: userId) { feedDetailsApiResponse in
            DispatchQueue.main.async { [self] in
                let feedData = feedDetailsApiResponse?.data
                feedsDetails.removeAll()
                for index in 0..<(feedData?.count ?? 0) {
                    let feedId = feedData?[index].id ?? 0
                    let feedCategoryId = feedData?[index].feedCategoryID ?? 0
                    let countOfcomment = feedData?[index].commentsCount ?? 0
                    let feedCountOfViews = feedData?[index].feedViewCount ?? 0
                    let feedCountOfLikes = feedData?[index].feedLikesCount ?? 0
                    let isliked = feedData?[index].isLiked
                    let feedCommentData =  feedData?[index].comments
                    let feedShareCount = feedData?[index].feedShareCount ?? 0
                    print(">>\(feedCountOfLikes)")
                    print(">>\(feedShareCount)")
                    feedComment.removeAll()
                    for index in 0..<(feedCommentData?.count ?? 0)  {
                        let commentId = feedCommentData?[index].id ?? 0
                        let commentCreatedAt = feedCommentData?[index].createdAt ?? ""
                        let commentContent = feedCommentData?[index].content ?? ""
                        let userFirstName = feedCommentData?[index].user?.firstName ?? ""
                        let userLastName = feedCommentData?[index].user?.lastName ?? ""
                        let userCommentLikes = feedCommentData?[index].commentLikesCount ?? 0
                        let userIsLiked = feedCommentData?[index].isLiked
                        let userImageUrl = feedCommentData?[index].user?.imageURL ?? ""
                        
                        feedComment.append(FeedComment(id: commentId,imageUrl: userImageUrl, createdAt: commentCreatedAt.toDate(format: "yyyy-MM-dd HH:mm:ss").adding(minutes: 60), content: commentContent, first_name: userFirstName, last_name: userLastName,comment_likes_count: userCommentLikes, is_liked: userIsLiked ?? false))
                        
                        feedComment = feedComment.sorted(by: {$0.createdAt > $1.createdAt})
                       
                        
                    }
                    feedsDetails.append(AllFeedsDetails(id: feedId, feedCategoryId: feedCategoryId, commentsCount: countOfcomment, feedViewCount: feedCountOfViews, feedLikesCount: feedCountOfLikes, feedShareCount: feedShareCount, islike: isliked ?? false, feedComment: feedComment.compactMap({return $0})))
                }
                completion()
            }
        }
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return  feedsDetails.count
    }
    func cellForRowsAt(indexPath: IndexPath) -> [AllFeedsDetails] {
        return feedsDetails
    }
    func getSomeFeedDetails() -> [AllFeedsDetails] {
        return feedsDetails
    }
    func numberOfComments() -> Int {
        return  feedComment.count
    }
    func cellForRowsComment(indexPath: IndexPath) -> [FeedComment] {
        return feedComment
    }
}
