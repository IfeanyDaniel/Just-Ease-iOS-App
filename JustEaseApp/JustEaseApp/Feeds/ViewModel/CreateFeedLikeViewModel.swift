//
//  CreateFeedLikeViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/11/23.
//
import Foundation
protocol CreateFeedLikeViewModelDelegate {
    func didReceiveCreateFeedLikeResponse(createFeedLikeResponse: CreateFeedLikeResponse?,_ statusCode: Int)
}
class CreateFeedLikeViewModel {
    var delegate: CreateFeedLikeViewModelDelegate?
    func getCreateFeedLikeResponse(feedId: Int, createFeedLikeRequest: CreateFeedLikeRequest) {
        let createFeedLikeResource = CreateFeedLikeResource()
        createFeedLikeResource.getResponse(feedId: feedId, creatFeedLikeRequest: createFeedLikeRequest) { createFeedLikeApiResponse, statusCode in
            if statusCode == 200 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateFeedLikeResponse(createFeedLikeResponse: createFeedLikeApiResponse, statusCode)
                }
            }
            if statusCode == 400 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateFeedLikeResponse(createFeedLikeResponse: createFeedLikeApiResponse, statusCode)
                }
            }
            if statusCode == 422 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateFeedLikeResponse(createFeedLikeResponse: createFeedLikeApiResponse, statusCode)
                }
            }
            if statusCode == 500 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateFeedLikeResponse(createFeedLikeResponse: createFeedLikeApiResponse, statusCode)
                }
            }
        }
    }
}
