//
//  RightInfoController.swift
//  JustEaseApp
//
//  Created by iOSApp on 23/02/2023.
//

import UIKit
import RealmSwift
class InfoController: UIViewController {
    // MARK: - Variables And Properties
    var list = [DetailList]()
    var detailsList: DetailList? = nil
    let realm = try! Realm()  // getting an instance of the database
    lazy var lists: Results<DetailList> = {realm.objects(DetailList.self) }()
    var subTitle = ""
    var subId = 0
    var rightQuestionViewModel = QuestionViewModel()
    var names = ""
    var isBookmark = ""
    var listIndex = 0
    lazy var headerlabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Do couples who co-habit but haven't legally married have the right to adopt?"
        label.textColor = AppColors.green.color
        label.numberOfLines = 4
        label.font = UIFont(name: AppFonts.silkabold.font, size: 20)
        label.textAlignment = .left
        return label
    }()
    lazy var note: UITextView = {
        let tv = UITextView.textViewDesign()
        tv.text = ""
        return tv
    }()
    lazy var bookMarkButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.backgroundColor = backgroundSystemColor
        button.layer.cornerRadius = 0
        button.frame.size.height = 55
        button.isHidden = true
        button.setImage(UIImage(named: "bookmarkButtonSelect"), for: .normal)
        button.addTarget(self, action: #selector(didTapBookmarkButton), for: .touchUpInside)
        return button
    }()
    lazy var unSelectedbookMarkButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.backgroundColor = backgroundSystemColor
        button.layer.cornerRadius = 0
        button.frame.size.height = 55
        button.setImage(UIImage(named: "bookmarkButtonUnSelect"), for: .normal)
        button.addTarget(self, action: #selector(didTapUnSelectedBookmarkButton), for: .touchUpInside)
        return button
    }()
    @objc func didTapBookmarkButton() {
        unSelectedbookMarkButton.isHidden = false
        bookMarkButton.isHidden = true
        // which item to remove
        print("item")
            let item = self.lists[listIndex]
            print(item)
            realm.beginWrite()
            realm.delete(item)
            do {
               try realm.commitWrite() // save data
             }
        catch {
        }
    }
    @objc func didTapUnSelectedBookmarkButton() {
        unSelectedbookMarkButton.isHidden = true
        bookMarkButton.isHidden = false

        let items = DetailList()
        items.note = note.text
        items.title = headerlabel.text
        items.isBookMarked = "bookmarked"
        realm.beginWrite() // about to write transaction to the database
        realm.add(items)
        do{
           try realm.commitWrite()
        }
        catch{
           print(error.localizedDescription)
        }
    }
    lazy var shareButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.backgroundColor = backgroundSystemColor
        button.layer.cornerRadius = 0
        button.frame.size.width = 55
        button.setImage(AppButtonImages.shareIcon.image, for: .normal)
        button.addTarget(self, action: #selector(didTapShareButton), for: .touchUpInside)
        return button
    }()
    lazy var reportViolationButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.frame.size.width = 150
        button.setTitle("REPORT A VIOLATION", for: .normal)
        button.addTarget(self, action: #selector(didTapReportViolationButton), for: .touchUpInside)
        return button
    }()
    @objc func didTapReportViolationButton() {
        navigationController?.pushViewController(ReportViolationController(), animated: true)
    }
    @objc func didTapShareButton() {
            let shareSheetVC = UIActivityViewController(
                activityItems: [headerlabel.text as Any, note.text as Any ], applicationActivities: nil
            )
            self.present(shareSheetVC, animated: true)
    }
    lazy var stackViewHolder : UIStackView = {
        let stackView = UIStackView()
        stackView.backgroundColor = backgroundSystemColor
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.isUserInteractionEnabled = true
        stackView.addArrangedSubview(reportViolationButton)
        stackView.addArrangedSubview(bookMarkButton)
        stackView.addArrangedSubview(unSelectedbookMarkButton)
        stackView.addArrangedSubview(shareButton)
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.spacing = 0
        return stackView
    }()
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        self.hideKeyboardWhenTappedAround()
        if subTitle != "" {
            getRightQuestionAndAnswer()
        }
    }
    func getRightQuestionAndAnswer(){
        Loader.shared.showLoader()
        rightQuestionViewModel.getQuestions(questionId: subId) { [self] in
            Loader.shared.hideLoader()
            headerlabel.text = subTitle
            let lawData = rightQuestionViewModel.displayData()[0].law
          
            for index in 0..<lawData.count {
                names = names + "\n" + lawData[index].name
            }
            note.text = "\(rightQuestionViewModel.displayData()[0].answer)\n\n \(names)"
        }
    }
    @objc func done() {
       dismiss(animated: true)
    }
    
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    
    func showBookMarkStatus() {
        if isBookmark == "bookmarked" {
            bookMarkButton.isHidden = false
            unSelectedbookMarkButton.isHidden = true
        } else {
            bookMarkButton.isHidden = true
            unSelectedbookMarkButton.isHidden = false
        }
    }
    func showBookMarkSelectedState() {
        for index in 0..<lists.count {
            if subTitle == lists[index].title {
                bookMarkButton.isHidden = false
                unSelectedbookMarkButton.isHidden = true
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        showBookMarkStatus()
        showBookMarkSelectedState()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
        
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.setHidesBackButton(true, animated: true)
       
    }
}

