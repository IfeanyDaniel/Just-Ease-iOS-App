//
//  RightInfoConstriants.swift
//  JustEaseApp
//
//  Created by iOSApp on 23/02/2023.
//

import UIKit
extension InfoController {
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(headerlabel)
        view.addSubview(note)
        view.addSubview(stackViewHolder)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        view.backgroundColor = backgroundSystemColor
        
        NSLayoutConstraint.activate([
            headerlabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            headerlabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            headerlabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            
            note.topAnchor.constraint(equalTo: headerlabel.bottomAnchor, constant: 10),
            note.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            note.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            note.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -150),
            
            stackViewHolder.topAnchor.constraint(equalTo: view.bottomAnchor, constant: -100),
            stackViewHolder.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            stackViewHolder.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            stackViewHolder.heightAnchor.constraint(equalToConstant: 55),
                                            
            ])
        
    }
}
