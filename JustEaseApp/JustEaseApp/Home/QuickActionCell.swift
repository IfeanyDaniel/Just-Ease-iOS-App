//
//  HomeCollectionViewCell.swift
//  JustEaseApp
//
//  Created by iOSApp on 14/02/2023.
//

import UIKit
protocol CollectionViewCellDelegate: AnyObject {
    func onClickCell(index: Int)
}
class QuickActionCollectionViewCell: UICollectionViewCell {
    static var identifier: String = "qucikActionCollectionCell"
    var cardCellDelegate: CollectionViewCellDelegate?
    var index : IndexPath?
   
    lazy var icon: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.right.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(clickDeleteCardButton(_:)), for: .touchUpInside)
        return button
    }()
    @objc func clickDeleteCardButton(_ sender: Any){
        guard let indexRow = index?.row else {
            return
        }
        cardCellDelegate?.onClickCell(index: indexRow)
    }
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Right"
        label.textAlignment = .center
        label.font = UIFont(name: AppFonts.silkLight.font ,size: 12)
        return label
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpViews() {
      addSubview(icon)
      addSubview(label)
        NSLayoutConstraint.activate([
           icon.topAnchor.constraint(equalTo: topAnchor, constant: 10),
           icon.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
           icon.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
           icon.heightAnchor.constraint(equalToConstant: 65),
           
           label.topAnchor.constraint(equalTo: icon.topAnchor, constant: 70),
           label.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
           label.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
    
        ])
        
    }
}
