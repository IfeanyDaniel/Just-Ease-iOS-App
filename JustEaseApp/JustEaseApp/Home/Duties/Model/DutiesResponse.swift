//
//  DutiesResponseModel.swift
//  JustEaseApp
//
//  Created by iOSApp on 29/03/2023.
//


import Foundation
struct DutiesResponse: Decodable {
    let status: Int
    let message: String
    let data: DutiesData
}
struct DutiesData: Decodable {
    let current_page: Int
    let data: [DutiesInfo]
    let total : Int
}
struct DutiesInfo: Decodable {
    let id: Int
    let title: String
}
