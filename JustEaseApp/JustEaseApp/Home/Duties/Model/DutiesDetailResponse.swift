//
//  DutiesDetailResponse.swift
//  JustEaseApp
//
//  Created by iOSApp on 29/03/2023.
//

import Foundation
struct DutiesDetailsResponse: Decodable {
    let status: Int
    let message: String
    let data: DutiesDetailsData
}
struct DutiesDetailsData: Decodable {
    let current_page: Int
    let data: [DutiesDataInfo]
    let total : Int
}
struct DutiesDataInfo: Decodable {
    let id: Int
    let title: String
    let questions: [DutiesQuestions]
}
struct DutiesQuestions: Decodable {
    let id: Int
    let title: String
}
