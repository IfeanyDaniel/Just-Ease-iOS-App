//
//  DutiesViewModel.swift
//  JustEaseApp
//
//  Created by iOSApp on 29/03/2023.
//

import Foundation
struct Duties: Codable {
    var id: Int
    var title: String
}

class DutiesViewModel {
    var duties :[Duties] = []
    func getDuties(numberPerPage: Int, completion: @escaping () -> Void) {
        let dutiesResource = DutiesResource()
        dutiesResource.getDuties(recordPerPage: numberPerPage) { getDutiesApiResponse in
            DispatchQueue.main.async { [self] in
                let totalNumber = getDutiesApiResponse?.data.total
                let dutiesData = getDutiesApiResponse?.data.data
                Storage.saveDutiesTotalRecord(number: totalNumber ?? 0)
                duties.removeAll()
                for index in 0..<(dutiesData?.count ?? 0) {
                    let dutiesId = dutiesData?[index].id ?? 0
                    let dutiesTitle = dutiesData?[index].title ?? ""
                    duties.append(Duties(id: dutiesId,title: dutiesTitle))
                    Storage.saveDuties(textInfo: duties)
                }
                completion()
            }
        }
    }
    func numberOfRowsInSection(section: Int) -> Int {
        return  duties.count
    }
    func cellForRowsAt(indexPath: IndexPath) -> [Duties] {
        return duties
    }
}
