//
//  DutiesDetailsResource.swift
//  JustEaseApp
//
//  Created by iOSApp on 29/03/2023.
//

import Foundation
class DutiesDetailsViewModel {
    var dutiesDetails: [Section] = []
    var options: [Option] = []
    var questionId = 0
    var questionTitle = ""
    func getDutiesRightDetails(dutiesId: Int,totalDutiesRecord: Int, completion: @escaping () -> Void) {
        let dutiesDetailsResource = DutiesDetailsResource()
        UserDefaults.standard.removeObject(forKey: Storage.allDutiesDetailsKey)
        dutiesDetailsResource.getDutiesDetails(id: dutiesId, totalDutiesRecord: totalDutiesRecord) { getDutiesDetailsApiResponse in
            DispatchQueue.main.async { [self] in
                
                let totalNumber =  getDutiesDetailsApiResponse?.data.total ?? 0
                let dutiesDetailsData = getDutiesDetailsApiResponse?.data.data
                Storage.saveDutiesDetailsTotalRecord(number: totalNumber)
                dutiesDetails.removeAll()
                for index in 0..<(dutiesDetailsData?.count ?? 0)  {
                    let title = dutiesDetailsData?[index].title ?? ""
                    let questionData = dutiesDetailsData?[index].questions
                    let countOfQuestions = questionData?.count ?? 0
                    options.removeAll()
                    for index in 0..<(questionData?.count ?? 0)  {
                        questionId = questionData?[index].id ?? 0
                        questionTitle = questionData?[index].title ?? ""
                        options.append(Option(id: questionId, title: questionTitle))
                    }
                    dutiesDetails.append(Section(title: title, numberOfQuestions: countOfQuestions, option: options.compactMap({ return $0
                    })))
                    Storage.saveDutiesDetails(textInfo: dutiesDetails)
                }
                completion()
            }
        }
       
    }
    func numberOfRowsInSection() -> [Section]  {
        return dutiesDetails
    }
    func cellForRowsAt(indexPath: IndexPath) -> [Section] {
        return dutiesDetails
    }
}
