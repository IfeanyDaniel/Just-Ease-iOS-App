//
//  DutiesDetailsContoller.swift
//  JustEaseApp
//
//  Created by iOSApp on 24/02/2023.
//

import UIKit
class DutiesDetailsController: UIViewController, UITableViewDataSource, UITableViewDelegate , UITextFieldDelegate{
    var dutiesId = 0
    var dutiesTitle = ""
    var dutiesDetailsViewModel = DutiesDetailsViewModel()
    var dutiesDetailsFilterData = [Section]()
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "DUTIES"
        label.textColor = AppColors.green.color
        label.font = UIFont(name: AppFonts.silkabold.font, size: 20)
        label.textAlignment = .left
        return label
    }()
   
    lazy var searchTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Search",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.addTarget(self, action: #selector(textFieldValDidChange(_:)), for: .editingChanged)
        textField.autocapitalizationType = .none
        return textField
    }()
    @objc func textFieldValDidChange(_ textField: UITextField) {
        dutiesDetailsFilterData = []
        let wordToSearchLowercased = searchTextField.text?.lowercased()

        if wordToSearchLowercased == "" {
            dutiesDetailsFilterData = Storage.getAllDutiesDetails()
        }
        for word in Storage.getAllDutiesDetails(){
            if word.title.lowercased().contains(wordToSearchLowercased ?? "") {
                dutiesDetailsFilterData.append(Section(title: word.title, numberOfQuestions: word.numberOfQuestions, option: word.option.compactMap({ return $0
                })))
            }
        }
        self.dutiesTableView.reloadData()
    }
   
    lazy var dutiesTableView : UITableView = {
        let table = UITableView()
        table.backgroundColor = backgroundSystemColor
        table.rowHeight = 80
        table.sizeToFit()
        table.delegate = self
        table.dataSource = self
        table.showsVerticalScrollIndicator = false
        table.separatorColor = backgroundSystemColor
        return table
    }()
    

    func showDoneButton(){
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dropKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.searchTextField.inputAccessoryView = toolbar
    }
    @objc func dropKeyboard(){
        view.endEditing(true)
    }

    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        showDoneButton()
        layoutViews()
        self.hideKeyboardWhenTappedAround()
        registercCell()
        getDutiesDetails()
        searchTextField.delegate = self
    }
    // Implement the UITextFieldDelegate method to handle text changes
     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         // Check if the text field already contains text
         if let currentText = textField.text {
             // Find the index of the first letter in the current text
             if let firstLetterIndex = currentText.firstIndex(where: { !$0.isWhitespace }) {
                 // Check if the replacement string starts with spaces
                 if string.first == " " && string.prefix(upTo: firstLetterIndex).contains(" ") {
                     // If the replacement string starts with spaces after the first letter, ignore those spaces
                     textField.text = String(currentText.prefix(upTo: firstLetterIndex)) + string.trimmingCharacters(in: .whitespacesAndNewlines)
                     return false
                 }
             }
         }

         // Allow other characters to be entered
         return true
     }
    func getDutiesDetails() {
        Loader.shared.showLoader()
        label.text = dutiesTitle
        dutiesDetailsViewModel.getDutiesRightDetails(dutiesId: dutiesId, totalDutiesRecord: Storage.getDutiesDetailsTotalRecord()) {
            Loader.shared.hideLoader()
            self.dutiesDetailsFilterData = Storage.getAllDutiesDetails()
            self.dutiesTableView.reloadData()
        }
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
        
    }
   
    func registercCell() {
        dutiesTableView.register(DetailsCell.self, forCellReuseIdentifier: DetailsCell.identifier)
         dutiesTableView.register(SubCell.self, forCellReuseIdentifier: SubCell.identifier)
    }
}
extension DutiesDetailsController: DetailsCellDelegate, SubCellDelegate {
    func onClickForwardButton(index: Int) {
    }
    
    func onClickDetailsForwardButton(index: Int) {
    }
}
