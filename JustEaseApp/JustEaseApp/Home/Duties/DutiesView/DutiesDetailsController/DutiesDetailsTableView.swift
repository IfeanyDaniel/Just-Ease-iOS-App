//
//  DutiesDetailsCollectionView.swift
//  JustEaseApp
//
//  Created by iOSApp on 24/02/2023.
//

import UIKit
extension DutiesDetailsController{
    func numberOfSections(in tableView: UITableView) -> Int {
        dutiesDetailsFilterData.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  dutiesDetailsFilterData[section].isOpened {
            return  dutiesDetailsFilterData[section].option.count + 1
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: DetailsCell.identifier, for: indexPath) as? DetailsCell else { return UITableViewCell() }
            let duties = dutiesDetailsFilterData[indexPath.section]
            cell.cardLabel.text = duties.title
            cell.numberOfItemsLabel.text = "\(duties.numberOfQuestions) Subitems"
            cell.selectionStyle = .none
            cell.index = indexPath
            cell.detailsCellDelegate = self
            cell.backgroundColor = backgroundSystemColor
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: SubCell.identifier, for: indexPath) as? SubCell else { return UITableViewCell() }
            cell.cardLabel.text = dutiesDetailsFilterData[indexPath.section].title
            cell.selectionStyle = .none
            cell.index = indexPath
            cell.subCellDelegate = self
            cell.backgroundColor = backgroundSystemColor
            cell.cardLabel.text = dutiesDetailsFilterData[indexPath.section].option[indexPath.row - 1].title
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 {
            dutiesDetailsFilterData[indexPath.section].isOpened = !dutiesDetailsFilterData[indexPath.section].isOpened
            tableView.reloadSections([indexPath.section], with: .none)
        } else {
            let dutiesSub = dutiesDetailsFilterData[indexPath.section].option[indexPath.row - 1]
            print("sub cell tapped")
            let controller = InfoController()
            controller.subTitle = dutiesSub.title
            controller.subId = dutiesSub.id
            navigationController?.pushViewController(controller, animated: true)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        50
    }
    
}


