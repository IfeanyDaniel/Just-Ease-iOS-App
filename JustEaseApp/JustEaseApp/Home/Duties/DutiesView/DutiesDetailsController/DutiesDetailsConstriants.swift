//
//  DutiesDetailsConstriants.swift
//  JustEaseApp
//
//  Created by iOSApp on 24/02/2023.
//

import UIKit
extension DutiesDetailsController {
 
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(label)
        view.addSubview(searchTextField)
        view.addSubview(dutiesTableView)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = backgroundSystemColor
        
        NSLayoutConstraint.activate([
    
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            
            searchTextField.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 20),
            searchTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            searchTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            searchTextField.heightAnchor.constraint(equalToConstant: 65),
            
        ])
        dutiesTableView.anchorWithConstantsToTop(searchTextField.bottomAnchor,
                                                     left: view.leftAnchor, bottom: view.bottomAnchor, right:  view.rightAnchor, topConstant: 20, leftConstant: 20, bottomConstant: 50, rightConstant: 20)
    }
}
