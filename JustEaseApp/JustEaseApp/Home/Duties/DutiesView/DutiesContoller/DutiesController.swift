//
//  DutiesController.swift
//  JustEaseApp
//
//  Created by iOSApp on 23/02/2023.
//


import UIKit
class DutiesViewController: UIViewController,UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout, UITextFieldDelegate  {
    var dutiesViewmodel = DutiesViewModel()
    var dutiesFilterData = [Duties]()
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "My Duties"
        label.textColor = AppColors.green.color
        label.font = UIFont(name: AppFonts.silkabold.font, size: 25)
        label.textAlignment = .left
        return label
    }()
   
    lazy var searchTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Search",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.addTarget(self, action: #selector(textFieldValDidChange(_:)), for: .editingChanged)
        textField.autocapitalizationType = .none
        return textField
    }()
    @objc func textFieldValDidChange(_ textField: UITextField) {
        dutiesFilterData = []
        let wordToSearchLowercased = searchTextField.text?.lowercased()

        if wordToSearchLowercased == "" {
            dutiesFilterData = Storage.getAllDuties()
        }
        for word in Storage.getAllDuties(){
            if word.title.lowercased().contains(wordToSearchLowercased ?? "") {
                dutiesFilterData.append(Duties(id: word.id, title: word.title))
            }
        }
        self.dutiesCollectionView.reloadData()
    }
    // Implement the UITextFieldDelegate method to handle text changes
     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         // Check if the text field already contains text
         if let currentText = textField.text {
             // Find the index of the first letter in the current text
             if let firstLetterIndex = currentText.firstIndex(where: { !$0.isWhitespace }) {
                 // Check if the replacement string starts with spaces
                 if string.first == " " && string.prefix(upTo: firstLetterIndex).contains(" ") {
                     // If the replacement string starts with spaces after the first letter, ignore those spaces
                     textField.text = String(currentText.prefix(upTo: firstLetterIndex)) + string.trimmingCharacters(in: .whitespacesAndNewlines)
                     return false
                 }
             }
         }

         // Allow other characters to be entered
         return true
     }
    lazy var dutiesCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 15
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        cv.backgroundColor = backgroundSystemColor
        cv.dataSource = self
        cv.delegate = self
        cv.showsVerticalScrollIndicator = true
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    
    func showDoneButton(){
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dropKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.searchTextField.inputAccessoryView = toolbar
    }
    @objc func dropKeyboard(){
        view.endEditing(true)
    }
    func getAllDuties() {
        Loader.shared.showLoader()
        dutiesViewmodel.getDuties(numberPerPage: Storage.getDutiesTotalRecord()) { [weak self] in
            guard let self = self else { return }
            Loader.shared.hideLoader()
            DispatchQueue.main.async { [self] in
                self.dutiesCollectionView.reloadData()
            }
        }
    }
    // MA
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        showDoneButton()
        layoutViews()
        self.hideKeyboardWhenTappedAround()
        registercCell()
        getAllDuties()
        dutiesFilterData = Storage.getAllDuties()
        searchTextField.delegate = self
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
    }
   
    func registercCell() {
        dutiesCollectionView.register(CardCollectionViewCell.self, forCellWithReuseIdentifier: CardCollectionViewCell.identifier)
    }
}
extension DutiesViewController: CardCellDelegate {
    func onClickCardForwardButton(index: Int) {
        navigationController?.pushViewController(DutiesDetailsController(), animated: true)
    }
}
