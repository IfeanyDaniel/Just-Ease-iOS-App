//
//  DutiesCollectionView.swift
//  JustEaseApp
//
//  Created by iOSApp on 24/02/2023.
//

import UIKit
extension DutiesViewController{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //dutiesViewmodel.numberOfRowsInSection(section: section)
        dutiesFilterData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = dutiesCollectionView.dequeueReusableCell(withReuseIdentifier: CardCollectionViewCell.identifier, for: indexPath) as? CardCollectionViewCell else { return UICollectionViewCell() }
       // let duties = dutiesViewmodel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
        let duties = dutiesFilterData[indexPath.row]
        cell.index = indexPath
        cell.cardCellDelegate = self
        cell.cardLabel.text = duties.title
        cell.backgroundColor = backgroundSystemColor
        cell.layer.borderColor = AppColors.green.color.cgColor
        cell.layer.borderWidth = 0.5
        cell.layer.cornerRadius = 5
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = .zero
        cell.layer.shadowRadius = 0.2
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 50, height: 60)
    }
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let duties = dutiesFilterData[indexPath.row]
        let controller = DutiesDetailsController()
        controller.dutiesId = duties.id
        controller.dutiesTitle = duties.title
        navigationController?.pushViewController(controller, animated: true)
    }
}
