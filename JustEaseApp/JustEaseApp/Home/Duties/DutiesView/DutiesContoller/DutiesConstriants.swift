//
//  DutiesConstriants.swift
//  JustEaseApp
//
//  Created by iOSApp on 23/02/2023.
//

import UIKit
extension DutiesViewController {
 
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(label)
        view.addSubview(searchTextField)
        view.addSubview(dutiesCollectionView)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = backgroundSystemColor
        
        NSLayoutConstraint.activate([
    
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            
            searchTextField.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 20),
            searchTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber),
            searchTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            searchTextField.heightAnchor.constraint(equalToConstant: 65),
            
        ])
        dutiesCollectionView.anchorWithConstantsToTop(searchTextField.bottomAnchor,
                                                     left: view.leftAnchor, bottom: view.bottomAnchor, right:  view.rightAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 50, rightConstant: 0)
    }
}
