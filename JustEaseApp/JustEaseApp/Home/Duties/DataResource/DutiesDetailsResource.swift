//
//  DutiesDetailsResource.swift
//  JustEaseApp
//
//  Created by iOSApp on 29/03/2023.
//

import Foundation
struct DutiesDetailsResource {
    func getDutiesDetails(id: Int,totalDutiesRecord: Int, completionHandler: @escaping (_ result: DutiesDetailsResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let dutiesDetailsUrl = ApiEndpoints.dutiesDetailsEndPoint + "/\(id)/topics?records_per_page=\(totalDutiesRecord)"
        let url = URL(string: dutiesDetailsUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType:  DutiesDetailsResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}
