//
//  DutiesResource.swift
//  JustEaseApp
//
//  Created by iOSApp on 29/03/2023.
//

import Foundation
struct DutiesResource {
    func getDuties(recordPerPage: Int, completionHandler: @escaping (_ result: DutiesResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let dutiesUrl = ApiEndpoints.dutiesEndPoint + "?records_per_page=\(recordPerPage)"
        
        let url = URL(string: dutiesUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType:  DutiesResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}
