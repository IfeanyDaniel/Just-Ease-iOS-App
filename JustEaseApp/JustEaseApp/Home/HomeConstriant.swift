//
//  HomeConstriant.swift
//  JustEaseApp
//
//  Created by iOSApp on 13/02/2023.
//

import UIKit
extension HomeViewController {
    //MARK: - Layout subviews for the scroll views
    func setupScrollView() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 10),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
        ])
    }
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(justEaseLabel)
        view.addSubview(notification)
        view.addSubview(label)
        guard let window = UIApplication.shared.keyWindow else { return }
        
        window.addSubview(panicButtton)
        
       window.bringSubviewToFront(panicButtton)
        
        contentView.addSubview(pageControl)
        contentView.addSubview(quickActionViewHolder)
        contentView.addSubview(quickNewsViewHolder)
        quickActionViewHolder.addSubview(quickActionCollectionView)
        quickNewsViewHolder.addSubview(quickNewsCollectionView)
        contentView.addSubview(quickLinkLabel)
        contentView.addSubview(stackViewHolder)
        
        // MARK: - Report
        firstView.addSubview(reportImage)
        firstView.addSubview(reportLabel)
        firstView.addSubview(reportHeader)
        firstView.addSubview(reportIconButton)
        
        // MARK: - Find
        secondView.addSubview(findImage)
        secondView.addSubview(findLabel)
        secondView.addSubview(findHeader)
        secondView.addSubview(findIconButton)
        
        // MARK: - Security
        thirdView.addSubview(securityImage)
        thirdView.addSubview(securityLabel)
        thirdView.addSubview(securityHeader)
        thirdView.addSubview(securityIconButton)
        
        // MARK: - Know your representatives
        fourthView.addSubview(repImage)
        fourthView.addSubview(repLabel)
        fourthView.addSubview(repHeader)
        fourthView.addSubview(repIconButton)
        
        navigationItem.setHidesBackButton(true, animated: true)
        navigationController?.isNavigationBarHidden = true
        view.backgroundColor = backgroundSystemColor
        
        
        
        NSLayoutConstraint.activate([
            panicButtton.bottomAnchor.constraint(equalTo: window.bottomAnchor, constant: -100),
            panicButtton.trailingAnchor.constraint(equalTo: window.trailingAnchor, constant: trailingNumber),
            panicButtton.heightAnchor.constraint(equalToConstant: 60),
            panicButtton.widthAnchor.constraint(equalToConstant: 60),
            
            justEaseLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 80),
            justEaseLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            
            notification.topAnchor.constraint(equalTo: view.topAnchor, constant: 80),
            notification.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            
            label.topAnchor.constraint(equalTo: justEaseLabel.bottomAnchor, constant: 10),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50),
            
            quickActionViewHolder.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            quickActionViewHolder.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0),
            quickActionViewHolder.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0),
            quickActionViewHolder.heightAnchor.constraint(equalToConstant: 100),
            
            quickNewsViewHolder.topAnchor.constraint(equalTo: quickActionViewHolder.bottomAnchor, constant: 0),
            quickNewsViewHolder.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            quickNewsViewHolder.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            quickNewsViewHolder.heightAnchor.constraint(equalToConstant: 260),
            
            quickLinkLabel.topAnchor.constraint(equalTo: quickNewsViewHolder.bottomAnchor, constant: 25),
            quickLinkLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            
            stackViewHolder.topAnchor.constraint(equalTo: quickLinkLabel.bottomAnchor, constant: 10),
            stackViewHolder.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            stackViewHolder.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            
            // MARK: - Report
            reportImage.topAnchor.constraint(equalTo: firstView.topAnchor, constant: 0),
            reportImage.trailingAnchor.constraint(equalTo: firstView.trailingAnchor, constant: -10),
            reportImage.bottomAnchor.constraint(equalTo: firstView.bottomAnchor, constant: 0),
            reportImage.widthAnchor.constraint(equalToConstant: 150),
            
            reportIconButton.topAnchor.constraint(equalTo: firstView.topAnchor, constant: 10),
            reportIconButton.leadingAnchor.constraint(equalTo: firstView.leadingAnchor, constant: 20),
            reportIconButton.widthAnchor.constraint(equalToConstant: 45),
            
            
            reportHeader.topAnchor.constraint(equalTo: reportIconButton.bottomAnchor, constant: 15),
            reportHeader.leadingAnchor.constraint(equalTo: firstView.leadingAnchor, constant: 20),
            
            reportLabel.topAnchor.constraint(equalTo: reportHeader.bottomAnchor, constant: 5),
            reportLabel.leadingAnchor.constraint(equalTo: firstView.leadingAnchor, constant: 20),
            
            // MARK: - Make a find
            findImage.topAnchor.constraint(equalTo: secondView.topAnchor, constant: 0),
            findImage.trailingAnchor.constraint(equalTo: secondView.trailingAnchor, constant: -10),
            findImage.bottomAnchor.constraint(equalTo: secondView.bottomAnchor, constant: 0),
            findImage.widthAnchor.constraint(equalToConstant: 150),
            
            findIconButton.topAnchor.constraint(equalTo: secondView.topAnchor, constant: 10),
            findIconButton.leadingAnchor.constraint(equalTo: secondView.leadingAnchor, constant: 20),
            findIconButton.widthAnchor.constraint(equalToConstant: 45),
            
            
            findHeader.topAnchor.constraint(equalTo: findIconButton.bottomAnchor, constant: 15),
            findHeader.leadingAnchor.constraint(equalTo: secondView.leadingAnchor, constant: 20),
            
            findLabel.topAnchor.constraint(equalTo: findHeader.bottomAnchor, constant: 5),
            findLabel.leadingAnchor.constraint(equalTo: secondView.leadingAnchor, constant: 20),
            
            
            // MARK: - Security
            securityImage.topAnchor.constraint(equalTo: thirdView.topAnchor, constant: 0),
            securityImage.trailingAnchor.constraint(equalTo: thirdView.trailingAnchor, constant: -10),
            securityImage.bottomAnchor.constraint(equalTo: thirdView.bottomAnchor, constant: 0),
            securityImage.widthAnchor.constraint(equalToConstant: 150),
            
            securityIconButton.topAnchor.constraint(equalTo: thirdView.topAnchor, constant: 10),
            securityIconButton.leadingAnchor.constraint(equalTo: thirdView.leadingAnchor, constant: 20),
            securityIconButton.widthAnchor.constraint(equalToConstant: 45),
            
            
            securityHeader.topAnchor.constraint(equalTo: securityIconButton.bottomAnchor, constant: 15),
            securityHeader.leadingAnchor.constraint(equalTo: thirdView.leadingAnchor, constant: 20),
            
            securityLabel.topAnchor.constraint(equalTo: securityHeader.bottomAnchor, constant: 5),
            securityLabel.leadingAnchor.constraint(equalTo: thirdView.leadingAnchor, constant: 20),
            
            // MARK: - Know your representatives
            repImage.topAnchor.constraint(equalTo: fourthView.topAnchor, constant: 0),
            repImage.trailingAnchor.constraint(equalTo: fourthView.trailingAnchor, constant: -10),
            repImage.bottomAnchor.constraint(equalTo: fourthView.bottomAnchor, constant: 0),
            repImage.widthAnchor.constraint(equalToConstant: 150),
            
            repIconButton.topAnchor.constraint(equalTo: fourthView.topAnchor, constant: 10),
            repIconButton.leadingAnchor.constraint(equalTo: fourthView.leadingAnchor, constant: 20),
            repIconButton.widthAnchor.constraint(equalToConstant: 45),
            
            
            repHeader.topAnchor.constraint(equalTo: repIconButton.bottomAnchor, constant: 15),
            repHeader.leadingAnchor.constraint(equalTo: fourthView.leadingAnchor, constant: 20),
            
            repLabel.topAnchor.constraint(equalTo: repHeader.bottomAnchor, constant: 5),
            repLabel.leadingAnchor.constraint(equalTo: fourthView.leadingAnchor, constant: 20),
            
            
            quickLinkLabel.topAnchor.constraint(equalTo: quickNewsViewHolder.bottomAnchor, constant: 25),
            quickLinkLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            
        ])
        
       quickActionCollectionView.anchorWithConstantsToTop(quickActionViewHolder.topAnchor,
                                                         left: quickActionViewHolder.leftAnchor, bottom: quickActionViewHolder.bottomAnchor, right: quickActionViewHolder.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
       quickNewsCollectionView.anchorWithConstantsToTop(quickNewsViewHolder.topAnchor,
                                                          left: quickNewsViewHolder.leftAnchor, bottom: quickNewsViewHolder.bottomAnchor, right: quickNewsViewHolder.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
    }
}
