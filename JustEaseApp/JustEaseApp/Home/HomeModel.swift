//
//  HomeModel.swift
//  JustEaseApp
//
//  Created by iOSApp on 14/02/2023.
//

import UIKit
struct QuickActionPage {
    let title: String
    let imageName: String
}
struct QuickActionNews {
    let image: String
    let firstLabel: String
    let secondLabel: String
    let thirdLabel: String
}
struct Card {
    let image: String
    let title: String
    let color: UIColor
}
