//
//  LgaAndTownCell.swift
//  JustEaseApp
//
//  Created by iOSApp on 01/03/2023.
//

import UIKit
class TownCell: UITableViewCell {
    static var identifier = "townCellId"
    lazy var icon: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "Location"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var townNameLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Ikeja Local Government"
        label.textColor = textSystemColor
        return label
    }()
    lazy var stateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Lagos, Nigeria"
        label.numberOfLines = 1
        label.textAlignment = .left
        label.font = UIFont(name: AppFonts.silkLight.font, size: 12)
        label.textColor = AppColors.lighterGray.color
        return label
    }()
   
    // MARK: - ... Adding subviews and constraints in the cell
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(icon)
        addSubview(townNameLabel)
        addSubview(stateLabel)
       
        NSLayoutConstraint.activate([
            icon.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            icon.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            icon.heightAnchor.constraint(equalToConstant: 50),
            icon.widthAnchor.constraint(equalToConstant: 25),
            
            townNameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 25),
            townNameLabel.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: 10),
            
            stateLabel.topAnchor.constraint(equalTo: townNameLabel.bottomAnchor, constant: 4),
            stateLabel.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: 10),
            
        ])
    }
    required init?(coder: NSCoder) {
       fatalError("init(code:) has not been implemented")
    }
}
