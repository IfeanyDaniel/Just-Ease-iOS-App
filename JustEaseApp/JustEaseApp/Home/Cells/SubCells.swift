//
//  SubCells.swift
//  JustEaseApp
//
//  Created by iOSApp on 19/03/2023.
//

import UIKit
protocol SubCellDelegate: AnyObject {
    func onClickForwardButton(index: Int)
}
class SubCell: UITableViewCell {
    static var identifier: String = "subTableCell"
    var subCellDelegate: SubCellDelegate?
    var index : IndexPath?
    
    // MARK: - content view
    lazy var view: UIView = {
        let content = UIView()
        content.translatesAutoresizingMaskIntoConstraints = false
        content.backgroundColor = backgroundSystemColor
        return content
    }()
    
    lazy var cardLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.numberOfLines = 5
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 12)
        return label
    }()
   
    lazy var forwardButton: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.forwardGreenButton.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(clickContactForwardButton(_:)), for: .touchUpInside)
        return button
    }()
    @objc func clickContactForwardButton(_ sender: Any){
        guard let indexRow = index?.row else {
            return
        }
        subCellDelegate?.onClickForwardButton(index: indexRow)
    }
  
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // MARK: - ... Adding subviews and constraints in the cell
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
    }
    func setUpViews() {
        addSubview(view)
        addSubview(cardLabel)
        addSubview(forwardButton)
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            view.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            view.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            view.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            
            cardLabel.topAnchor.constraint(equalTo:  view.topAnchor, constant: 16),
            cardLabel.leadingAnchor.constraint(equalTo:  view.leadingAnchor, constant: 20),
            cardLabel.trailingAnchor.constraint(equalTo:  view.trailingAnchor, constant: -20),
        
            forwardButton.topAnchor.constraint(equalTo:  view.topAnchor, constant: 20),
            forwardButton.trailingAnchor.constraint(equalTo:  view.trailingAnchor, constant: -16),
            
        ])
    }
}
