//
//  RightDetailsCell.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/02/2023.
//

import UIKit
protocol DetailsCellDelegate: AnyObject {
    func onClickDetailsForwardButton(index: Int)
}
class DetailsCell: UITableViewCell {
    static var identifier: String = "tableCell"
    var detailsCellDelegate: DetailsCellDelegate?
    var index : IndexPath?
    
    // MARK: - content view
    lazy var view: UIView = {
        let content = UIView()
        content.translatesAutoresizingMaskIntoConstraints = false
        content.backgroundColor = backgroundSystemColor
        content.layer.borderColor = AppColors.green.color.cgColor
        content.layer.borderWidth = 0.5
        content.layer.cornerRadius = 5
        content.layer.shadowOpacity = 0.5
        content.layer.shadowOffset = .zero
        content.layer.shadowRadius = 0.2
        return content
    }()
    
    lazy var cardLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Adoption"
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 12)
        return label
    }()
    lazy var numberOfItemsLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "7 Subitems"
        label.textColor = placeholderSystemGrayColor
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 10)
        return label
    }()
    lazy var forwardButton: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.forwardGreenButton.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(clickContactForwardButton(_:)), for: .touchUpInside)
        return button
    }()
    @objc func clickContactForwardButton(_ sender: Any){
        guard let indexRow = index?.row else {
            return
        }
        detailsCellDelegate?.onClickDetailsForwardButton(index: indexRow)
    }
  
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // MARK: - ... Adding subviews and constraints in the cell
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
    }
    func setUpViews() {
        addSubview(view)
        addSubview(cardLabel)
        addSubview(numberOfItemsLabel)
        addSubview(forwardButton)
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            view.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            view.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            view.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            
            cardLabel.topAnchor.constraint(equalTo:  view.topAnchor, constant: 14),
            cardLabel.leadingAnchor.constraint(equalTo:  view.leadingAnchor, constant: 20),
            cardLabel.trailingAnchor.constraint(equalTo:  view.trailingAnchor, constant: -20),
            
            numberOfItemsLabel.topAnchor.constraint(equalTo: cardLabel.bottomAnchor, constant: 2),
            numberOfItemsLabel.leadingAnchor.constraint(equalTo:  view.leadingAnchor, constant: 20),
           
            forwardButton.topAnchor.constraint(equalTo:  view.topAnchor, constant: 20),
            forwardButton.trailingAnchor.constraint(equalTo:  view.trailingAnchor, constant: -16),
            
        ])
    }
}
