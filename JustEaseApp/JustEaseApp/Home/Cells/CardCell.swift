//
//  RightCell.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/02/2023.
//

import UIKit
protocol CardCellDelegate: AnyObject {
    func onClickCardForwardButton(index: Int)
}
class CardCollectionViewCell: UICollectionViewCell {
    static var identifier: String = "cardCollectionCell"
    var cardCellDelegate: CardCellDelegate?
    var index : IndexPath?
    var card: Cards? {
        didSet {
            guard let card = card else {
                return
            }
            let forwardImage = card.forwardImage
            forwardButton.setImage(forwardImage, for: .normal)
            
            let cardName = card.title
            cardLabel.text = cardName
        }
    }
    
    lazy var cardLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text =  "ADOPTION"
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 12)
        return label
    }()
    
    lazy var forwardButton: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.forwardGreenButton.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(clickContactForwardButton(_:)), for: .touchUpInside)
        return button
    }()
    @objc func clickContactForwardButton(_ sender: Any){
        guard let indexRow = index?.row else {
            return
        }
        cardCellDelegate?.onClickCardForwardButton(index: indexRow)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func configure(with viewModel: DetailsListViewModel){
      cardLabel.text = viewModel.title
    }
    
    func setUpViews() {
        addSubview(cardLabel)
        addSubview(forwardButton)
        NSLayoutConstraint.activate([
            cardLabel.topAnchor.constraint(equalTo: topAnchor, constant: 17),
            cardLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            cardLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
           
            forwardButton.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            forwardButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            
        ])
    }
}
