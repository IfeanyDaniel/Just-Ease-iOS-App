//
//  LoadingScreen.swift
//  JustEaseApp
//
//  Created by iOSApp on 23/03/2023.
//

import UIKit
class Loading : UIViewController , AuthViewModelDelegate {
    var authViewModel = AuthViewModel()
    lazy var image: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "JustEaseIcon"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "JustEase"
        label.textColor = AppColors.white.color
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 40)
        label.textAlignment = .center
        return label
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        authViewModel.delegate = self
        view.backgroundColor = AppColors.green.color
        view.addSubview(label)
        view.addSubview(image)
        NSLayoutConstraint.activate([
            image.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -65),
            image.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40),
            image.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40),
            image.widthAnchor.constraint(equalToConstant: 100),
            image.heightAnchor.constraint(equalToConstant: 100),
            
            label.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40)
        ])
        if InternetConnectionManager.isConnectedToNetwork(){
            //Loader.shared.showLoader()
            let request = AuthRequest(email: LoginDetails().email, password: LoginDetails().password)
            authViewModel.authResponse(authRequest: request)
        } else {
            Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
        }
    }
    func didReceiveAuthResponse(AuthResponse: AuthResponse?, _ statusCode: Int) {
        if statusCode == 200 {
            if InternetConnectionManager.isConnectedToNetwork(){
                    navigationController?.pushViewController(TabBarViewController(), animated: true)
                    Loader.shared.hideLoader()
            }
            else {
                Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
            }
        }
        if statusCode == 400 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Something went wrong, Please try again", type: .error)
        }
    }
}
