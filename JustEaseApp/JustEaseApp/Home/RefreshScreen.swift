//
//  Refresh.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/2/23.
//

import Foundation
import UIKit
class Refresh : UIViewController , AuthViewModelDelegate {
    var authViewModel = AuthViewModel()
    lazy var image: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "JustEaseIcon"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "JustEase"
        label.textColor = AppColors.white.color
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 40)
        label.textAlignment = .center
        return label
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(label)
        view.addSubview(image)
        NSLayoutConstraint.activate([
            image.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -65),
            image.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40),
            image.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40),
            image.widthAnchor.constraint(equalToConstant: 100),
            image.heightAnchor.constraint(equalToConstant: 100),
            
            label.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40)
        ])
        authViewModel.delegate = self
        view.backgroundColor = AppColors.green.color
       // Loader.shared.showLoader()
        if InternetConnectionManager.isConnectedToNetwork(){
            Toast.shared.showToastWithTItle("Refreshing content...Please wait", type: .success)
            let request = AuthRequest(email: LoginDetails().email, password: LoginDetails().password)
            authViewModel.authResponse(authRequest: request)
        } else {
            dismiss(animated: true)
            Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
        }
    }
    func didReceiveAuthResponse(AuthResponse: AuthResponse?, _ statusCode: Int) {
        if statusCode == 200 {
            if InternetConnectionManager.isConnectedToNetwork(){
                Toast.shared.showToastWithTItle("Successfully Refreshed", type: .success)
                    dismiss(animated: false)
                    Loader.shared.hideLoader()
            }
            else {
                Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
            }
        }
        if statusCode == 400 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Something went wrong, Please try again", type: .error)
        }
    }
}
