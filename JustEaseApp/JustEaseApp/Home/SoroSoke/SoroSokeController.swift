//
//  SoroSokeController.swift
//  JustEaseApp
//
//  Created by iOSApp on 05/03/2023.
//

import UIKit
class SoroSokeController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var delegate : MoveToNextScreenDelegate?
    
    lazy var tableView : UITableView = {
        let table = UITableView()
        table.backgroundColor = viewStackColor
        table.rowHeight = 60
        table.sizeToFit()
        table.delegate = self
        table.dataSource = self
        table.isScrollEnabled = false
        table.separatorColor = AppColors.lighterGray.color
        return table
    }()
    let page: [String] = {
        let lawyer = "Report a Crime"
        let police = "Report a Violation"
        return [lawyer,police]
    }()
    func registercCell() {
        tableView.register(TableViewCell.self, forCellReuseIdentifier: TableViewCell.identifier)

    }

    override func viewDidLoad() {
         layoutViews()
         registercCell()
    }
    
}
