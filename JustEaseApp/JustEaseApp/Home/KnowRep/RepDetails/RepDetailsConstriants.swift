//
//  RepDetailsConstriants.swift
//  JustEaseApp
//
//  Created by iOSApp on 02/03/2023.
//

import UIKit
extension  RepsDetailsController {
    //MARK: - Layout subviews for the scroll views
    func setupScrollView() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: buttonStack.bottomAnchor, constant: 10),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
        ])
    }
    
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(nameLabel)
        view.addSubview(profileIcon)
        view.addSubview(buttonStack)
        contentView.addSubview(stackViewHolder)
        
        positionView.addSubview(positionLabel)
        positionView.addSubview(repPositionLabel)
    
        aboutView.addSubview(aboutLabel)
        aboutView.addSubview(repAboutLabel)
        
        practiceaAreaView.addSubview(practiceaAreaLabel)
        practiceaAreaView.addSubview(practiceCollectionView)
        
        senetorialView.addSubview(senetorialLabel)
        senetorialView.addSubview(repAreaCoveredLabel)
        
        originView.addSubview(originLabel)
        originView.addSubview(repsStateLabel)
        
        contactView.addSubview(contactLabel)
        contactView.addSubview(contactStack)
        
        websiteView.addSubview(websiteStack)
        addressView.addSubview(addressStack)
       
    NSLayoutConstraint.activate([
        profileIcon.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
        profileIcon.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        profileIcon.heightAnchor.constraint(equalToConstant: 80),
        profileIcon.widthAnchor.constraint(equalToConstant: 80),
        
        nameLabel.topAnchor.constraint(equalTo: profileIcon.bottomAnchor, constant: 10),
        nameLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
        nameLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
        
        buttonStack.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 20),
        buttonStack.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40),
        buttonStack.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40),
        buttonStack.heightAnchor.constraint(equalToConstant: buttonHeight),
        
        stackViewHolder.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15),
        stackViewHolder.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
        stackViewHolder.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
        stackViewHolder.bottomAnchor.constraint(equalTo: contactView.bottomAnchor, constant: -50),
        
        positionLabel.topAnchor.constraint(equalTo: positionView.topAnchor, constant: 15),
        positionLabel.leadingAnchor.constraint(equalTo: positionView.leadingAnchor, constant: 15),
        
        repPositionLabel.topAnchor.constraint(equalTo: positionLabel.bottomAnchor, constant: 15),
        repPositionLabel.leadingAnchor.constraint(equalTo: positionView.leadingAnchor, constant: 15),
        
        aboutLabel.topAnchor.constraint(equalTo: aboutView.topAnchor, constant: 15),
        aboutLabel.leadingAnchor.constraint(equalTo: aboutView.leadingAnchor, constant: 15),
        
        repAboutLabel.topAnchor.constraint(equalTo:aboutLabel.bottomAnchor, constant: 15),
        repAboutLabel.leadingAnchor.constraint(equalTo: aboutView.leadingAnchor, constant: 15),
        repAboutLabel.trailingAnchor.constraint(equalTo: aboutView.trailingAnchor, constant: -15),
        
        practiceaAreaLabel.topAnchor.constraint(equalTo: practiceaAreaView.topAnchor, constant: 15),
        practiceaAreaLabel.leadingAnchor.constraint(equalTo:practiceaAreaView.leadingAnchor, constant: 15),
        
        senetorialLabel.topAnchor.constraint(equalTo: senetorialView.topAnchor, constant: 15),
        senetorialLabel.leadingAnchor.constraint(equalTo:senetorialView.leadingAnchor, constant: 15),
        
        repAreaCoveredLabel.topAnchor.constraint(equalTo: senetorialLabel.bottomAnchor, constant: 15),
        repAreaCoveredLabel.leadingAnchor.constraint(equalTo:senetorialView.leadingAnchor, constant: 15),
        repAreaCoveredLabel.trailingAnchor.constraint(equalTo:senetorialView.trailingAnchor, constant: -15),
        
        originLabel.topAnchor.constraint(equalTo: originView.topAnchor, constant: 15),
        originLabel.leadingAnchor.constraint(equalTo: originView.leadingAnchor, constant: 15),
        
        repsStateLabel.topAnchor.constraint(equalTo: originLabel.bottomAnchor, constant: 15),
        repsStateLabel.leadingAnchor.constraint(equalTo: originView.leadingAnchor, constant: 15),
        repsStateLabel.trailingAnchor.constraint(equalTo: originView.trailingAnchor, constant: -15),
        
        contactLabel.topAnchor.constraint(equalTo: contactView.topAnchor, constant: 15),
        contactLabel.leadingAnchor.constraint(equalTo: contactView.leadingAnchor, constant: 15),
        
        contactStack.topAnchor.constraint(equalTo: contactLabel.bottomAnchor, constant: 15),
        contactStack.trailingAnchor.constraint(equalTo: contactView.trailingAnchor, constant: -15),
        contactStack.leadingAnchor.constraint(equalTo: contactView.leadingAnchor, constant: 15),
        
        websiteStack.topAnchor.constraint(equalTo: websiteView.topAnchor, constant: 5),
        websiteStack.trailingAnchor.constraint(equalTo: websiteView.trailingAnchor, constant: -15),
        websiteStack.leadingAnchor.constraint(equalTo: websiteView.leadingAnchor, constant: 15),
        
        addressStack.topAnchor.constraint(equalTo: addressView.topAnchor, constant: 5),
        addressStack.trailingAnchor.constraint(equalTo: addressView.trailingAnchor, constant: -15),
        addressStack.leadingAnchor.constraint(equalTo: addressView.leadingAnchor, constant: 15),
        
        ])
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = .systemBackground
        practiceCollectionView.anchorWithConstantsToTop(practiceaAreaLabel.topAnchor,
                                                     left: practiceaAreaView.leftAnchor, bottom: practiceaAreaView.bottomAnchor, right:  practiceaAreaView.rightAnchor, topConstant: 20, leftConstant: leadingNumber, bottomConstant: 10, rightConstant: leadingNumber )
    }
   
}
