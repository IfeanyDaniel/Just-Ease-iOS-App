//
//  PracticeAreaCell.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/16/23.
//

import UIKit

class PracticeAreaViewCell: UICollectionViewCell {
    static var identifier: String = "PracticeAreanCell"

    lazy var practiceAreaLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textAlignment = .center
        label.layer.cornerRadius = 15
        label.clipsToBounds = true
        label.font = UIFont(name: AppFonts.silkabold.font, size: 12)
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    func setUpViews() {
        addSubview(practiceAreaLabel)
        NSLayoutConstraint.activate([
            practiceAreaLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            practiceAreaLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 25),
            practiceAreaLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -25),
            practiceAreaLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 10)
            
        ])
    }
}
