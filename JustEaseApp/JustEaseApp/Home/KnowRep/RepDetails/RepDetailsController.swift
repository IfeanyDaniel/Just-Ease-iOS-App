//
//  RepDetailsController.swift
//  JustEaseApp
//
//  Created by iOSApp on 02/03/2023.
//
import UIKit
class RepsDetailsController: UIViewController, UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    // MARK: - Scroll view
    var repsImage = "-"
    var repsName = "-"
    var repsPosition = "-"
    var aboutReps = "-"
    var repsPhoneNumber = "-"
    var repsState = "-"
    var repsAreaCovered = "-"
    var repsEmail = "-"
    var repsAddress = "-"
    var repsWebsite = "-"
    var email = "-"
    var phoneNumber = "-"
    var practiceAreas : [PracticeAreas] = []

    var doneButton = UIBarButtonItem()
    
    lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.showsHorizontalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    // MARK: - content view
    lazy var contentView: UIView = {
        let content = UIView()
        content.translatesAutoresizingMaskIntoConstraints = false
        content.backgroundColor = backgroundSystemColor
        return content
    }()
    
    lazy var nameLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "HON. ISRAEL SUNNY GOLI"
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 20)
        label.textAlignment = .center
        return label
    }()

    lazy var profileIcon :  UIImageView  = {
        let profileImageView =  UIImageView()
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.layer.masksToBounds =  true
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.layer.cornerRadius = 40
        profileImageView.frame.size.width = 150
        profileImageView.frame.size.height = buttonHeight
        return profileImageView
    }()
    lazy var emailButton: UIButton = {
        let button = UIButton.stackButtonDesign()
        button.setTitle(" Email", for: .normal)
        button.setImage(UIImage(named: "email"), for: .normal)
        button.backgroundColor = AppColors.lightBlue.color
        button.addTarget(self, action: #selector(didTapOnEmail), for: .touchUpInside)
        return button
    }()
    @objc func didTapOnEmail() {
        if let url = URL(string: "mailto:\(repsEmail)") {
            UIApplication.shared.open(url)
        }
    }
    lazy var callButton: UIButton = {
        let button = UIButton.stackButtonDesign()
        button.setTitle(" Call", for: .normal)
        button.setImage(UIImage(named: "call"), for: .normal)
        button.backgroundColor = AppColors.lightOrange.color
        button.addTarget(self, action: #selector(didTapOnCall), for: .touchUpInside)
        return button
    }()
    
    @objc func didTapOnCall() {
        guard let url = URL(string: "tel://+234\(phoneNumber)") else {return}
        UIApplication.shared.open(url)
    }
    lazy var buttonStack: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(emailButton)
        stackView.addArrangedSubview(callButton)
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 10
        return stackView
    }()
    // MARK: -  Position
    lazy var positionView: UIView = {
        let content = UIView.stackViewHolderDesign()
        content.heightAnchor.constraint(equalToConstant: 100.0).isActive = true
        content.layer.cornerRadius = 10
        return content
    }()
    lazy var positionLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Position"
        label.font = UIFont(name: AppFonts.silkabold.font, size: 16)
        label.textAlignment = .left
        return label
    }()
    lazy var repPositionLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = ""
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 12)
        label.textAlignment = .left
        return label
    }()
    // MARK: -  About me
    lazy var aboutView: UIView = {
        let content = UIView.stackViewHolderDesign()
        content.heightAnchor.constraint(equalToConstant: 150.0).isActive = true
        content.layer.cornerRadius = 10
        content.sizeToFit()
        return content
    }()
    lazy var aboutLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "About Me"
        label.sizeToFit()
        label.font = UIFont(name: AppFonts.silkabold.font, size: 16)
        label.textAlignment = .left
        return label
    }()
    lazy var repAboutLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = ""
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 12)
        label.textAlignment = .left
        return label
    }()
    // MARK: -  Prcatice area
    lazy var practiceaAreaView: UIView = {
        let content = UIView.stackViewHolderDesign()
        content.heightAnchor.constraint(equalToConstant: 150.0).isActive = true
        content.layer.cornerRadius = 10
        content.isHidden = true
        return content
    }()
    lazy var practiceaAreaLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Practice Areas"
        label.font = UIFont(name: AppFonts.silkabold.font, size: 16)
        label.textAlignment = .left
        return label
    }()
    lazy var practiceCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 4
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        cv.backgroundColor = viewStackColor
        cv.dataSource = self
        cv.delegate = self
        cv.showsVerticalScrollIndicator = true
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    // MARK: -  Senetaorial District
    lazy var senetorialView: UIView = {
        let content = UIView.stackViewHolderDesign()
        content.heightAnchor.constraint(equalToConstant: 150.0).isActive = true
        content.layer.cornerRadius = 10
        return content
    }()
    lazy var senetorialLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Senatorial District"
        label.font = UIFont(name: AppFonts.silkabold.font, size: 16)
        label.textAlignment = .left
        return label
    }()
    lazy var repAreaCoveredLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = ""
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 12)
        label.textAlignment = .left
        return label
    }()
    // MARK: -  State of Origin
    lazy var originView: UIView = {
        let content = UIView.stackViewHolderDesign()
        content.heightAnchor.constraint(equalToConstant: 100.0).isActive = true
        content.layer.cornerRadius = 10
        return content
    }()
    lazy var originLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "State of Origin"
        label.font = UIFont(name: AppFonts.silkabold.font, size: 16)
        label.textAlignment = .left
        return label
    }()
    lazy var repsStateLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = ""
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 12)
        label.textAlignment = .left
        return label
    }()
    // MARK: -  Contact
    lazy var contactView: UIView = {
        let content = UIView.stackViewHolderDesign()
        content.heightAnchor.constraint(equalToConstant: 300.0).isActive = true
        content.layer.cornerRadius = 10
        return content
    }()
    lazy var contactLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Contact"
        label.font = UIFont(name: AppFonts.silkabold.font, size: 16)
        label.textAlignment = .left
        return label
    }()
    lazy var websiteView: UIView = {
        let content = UIView.stackViewHolderDesign()
        content.heightAnchor.constraint(equalToConstant: 100.0).isActive = true
        content.layer.cornerRadius = 10
        return content
    }()
    lazy var websiteLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Website"
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 16)
        label.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        label.textAlignment = .left
        return label
    }()
    lazy var repsWebsiteLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.font = UIFont(name: AppFonts.silka.font, size: 12)
        label.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        label.textAlignment = .left
        return label
    }()
    lazy var websiteStack : UIStackView = {
        let stackView = UIStackView()
        stackView.backgroundColor = viewStackColor
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.isUserInteractionEnabled = true
        stackView.addArrangedSubview(websiteLabel)
        stackView.addArrangedSubview(repsWebsiteLabel)
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 10
        return stackView
    }()
    lazy var addressView: UIView = {
        let content = UIView.stackViewHolderDesign()
        content.heightAnchor.constraint(equalToConstant: 100.0).isActive = true
        content.layer.cornerRadius = 10
        return content
    }()
    lazy var addressLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Address"
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 16)
        label.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        label.textAlignment = .left
        return label
    }()
    lazy var repsAddressLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.font = UIFont(name: AppFonts.silka.font, size: 12)
        label.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        label.textAlignment = .left
        return label
    }()
    lazy var addressStack : UIStackView = {
        let stackView = UIStackView()
        stackView.backgroundColor = viewStackColor
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.isUserInteractionEnabled = true
        stackView.addArrangedSubview(addressLabel)
        stackView.addArrangedSubview(repsAddressLabel)
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 10
        return stackView
    }()
    lazy var contactStack : UIStackView = {
        let stackView = UIStackView()
        stackView.backgroundColor = viewStackColor
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.isUserInteractionEnabled = true
        stackView.addArrangedSubview(websiteView)
        stackView.addArrangedSubview(addressView)
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 10
        return stackView
    }()
    lazy var stackViewHolder : UIStackView = {
        let stackView = UIStackView()
        stackView.backgroundColor = backgroundSystemColor
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.isUserInteractionEnabled = true
        stackView.addArrangedSubview(positionView)
        stackView.addArrangedSubview(aboutView)
        stackView.addArrangedSubview(practiceaAreaView)
        stackView.addArrangedSubview(senetorialView)
        stackView.addArrangedSubview(originView)
        stackView.addArrangedSubview(contactView)
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 10
        return stackView
    }()
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        setupScrollView()
        getAllRepsDetails()
        practiceCollectionView.register(PracticeAreaViewCell.self, forCellWithReuseIdentifier: PracticeAreaViewCell.identifier)
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    func getAllRepsDetails() {
        if practiceaAreaView.isHidden ==  true && originView.isHidden == true {
            profileIcon.image =  UIImage(named: "locationIcon")
        } else {
            profileIcon.kf.setImage(with: URL(string: repsImage))
        }
       
        nameLabel.text = repsName
        repPositionLabel.text = repsPosition
        repAboutLabel.text = aboutReps
        repAreaCoveredLabel.text = repsAreaCovered
        repsStateLabel.text = repsState
        repsWebsiteLabel.text = repsWebsite
        repsAddressLabel.text = repsAddress
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
        
    }
    // MARK: - View did layout subviews
    override func viewDidLayoutSubviews() {
        scrollView.isScrollEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.contentSize = CGSize(width: 400, height: 900)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        practiceAreas.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = practiceCollectionView.dequeueReusableCell(withReuseIdentifier: PracticeAreaViewCell.identifier, for: indexPath) as? PracticeAreaViewCell else { return UICollectionViewCell() }
        cell.practiceAreaLabel.text =  practiceAreas[indexPath.row].title
        cell.practiceAreaLabel.textColor = AppColors.green.color
        cell.practiceAreaLabel.backgroundColor =  AppColors.lightGreen.color
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: practiceaAreaView.frame.size.width - 50, height: 40)
    }
   
}
