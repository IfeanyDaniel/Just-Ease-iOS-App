//
//  StateCollectionView.swift
//  JustEaseApp
//
//  Created by iOSApp on 28/02/2023.
//

import UIKit
extension StateController {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == StatesCollectionView {
           return  stateViewModel.numberOfRowsInSection(section: section)
        } else {
            return searchStateViewModel.numberOfRowsInSection(section: section)
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == StatesCollectionView {
            guard let cell = StatesCollectionView.dequeueReusableCell(withReuseIdentifier: CardCollectionViewCell.identifier, for: indexPath) as? CardCollectionViewCell else { return UICollectionViewCell() }
            let state = stateViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
            cell.index = indexPath
            cell.cardCellDelegate = self
            cell.cardLabel.text = state.title
            cell.backgroundColor = backgroundSystemColor
            cell.layer.borderColor = AppColors.green.color.cgColor
            cell.layer.borderWidth = 0.5
            cell.layer.cornerRadius = 5
            cell.layer.shadowOpacity = 0.5
            cell.layer.shadowOffset = .zero
            cell.layer.shadowRadius = 0.2
            return cell
        } else {
            guard let cell = SearchStateCollectionView.dequeueReusableCell(withReuseIdentifier: CardCollectionViewCell.identifier, for: indexPath) as? CardCollectionViewCell else { return UICollectionViewCell() }
            let state = searchStateViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
            cell.index = indexPath
            cell.cardCellDelegate = self
            cell.cardLabel.text = state.title
            cell.backgroundColor = backgroundSystemColor
            cell.layer.borderColor = AppColors.green.color.cgColor
            cell.layer.borderWidth = 0.5
            cell.layer.cornerRadius = 5
            cell.layer.shadowOpacity = 0.5
            cell.layer.shadowOffset = .zero
            cell.layer.shadowRadius = 0.2
            return cell
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 50, height: 60)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == StatesCollectionView {
            let state = stateViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
            let controller = TownController()
            controller.state = state.title
            controller.stateId = state.id
            navigationController?.pushViewController(controller, animated: true)
        } else {
            let state = searchStateViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
            let controller = TownController()
            controller.state = state.title
            controller.stateId = state.id
            navigationController?.pushViewController(controller, animated: true)
        }
       
    }
}
