//
//  StateConstraints.swift
//  JustEaseApp
//
//  Created by iOSApp on 28/02/2023.
//

import UIKit
extension StateController {
 
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(label)
        view.addSubview(searchTextField)
        view.addSubview(StatesCollectionView)
        view.addSubview(SearchStateCollectionView)
        view.addSubview(noContentView)
        noContentView.addSubview(noRecordIcon)
        noContentView.addSubview(noContentLabel)
        noContentView.addSubview(instructionLabel)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = backgroundSystemColor
        
        NSLayoutConstraint.activate([
    
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            
            searchTextField.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 20),
            searchTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            searchTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            searchTextField.heightAnchor.constraint(equalToConstant: 65),
            
            noContentView.topAnchor.constraint(equalTo: searchTextField.bottomAnchor, constant: 30),
            noContentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            noContentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            noContentView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
        
            noRecordIcon.centerXAnchor.constraint(equalTo: noContentView.centerXAnchor),
            noRecordIcon.topAnchor.constraint(equalTo: noContentView.topAnchor, constant: 150),
            noRecordIcon.widthAnchor.constraint(equalToConstant: 200),
            noRecordIcon.heightAnchor.constraint(equalToConstant: 200),
            
            noContentLabel.topAnchor.constraint(equalTo: noRecordIcon.bottomAnchor, constant: 20),
            noContentLabel.leadingAnchor.constraint(equalTo: noContentView.leadingAnchor, constant: 20),
            noContentLabel.trailingAnchor.constraint(equalTo:noContentView.trailingAnchor, constant: -20),
            
            instructionLabel.topAnchor.constraint(equalTo: noContentLabel.bottomAnchor, constant: 20),
            instructionLabel.leadingAnchor.constraint(equalTo: noContentView.leadingAnchor, constant: 20),
            instructionLabel.trailingAnchor.constraint(equalTo: noContentView.trailingAnchor, constant: -20),
            
        ])
        StatesCollectionView.anchorWithConstantsToTop(searchTextField.bottomAnchor,
                                                     left: view.leftAnchor, bottom: view.bottomAnchor, right:  view.rightAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 50, rightConstant: 0)
        SearchStateCollectionView.anchorWithConstantsToTop(searchTextField.bottomAnchor,
                                                     left: view.leftAnchor, bottom: view.bottomAnchor, right:  view.rightAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 50, rightConstant: 0)
    }
}
