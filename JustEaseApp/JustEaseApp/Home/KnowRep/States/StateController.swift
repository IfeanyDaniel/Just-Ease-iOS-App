//
//  StateController.swift
//  JustEaseApp
//
//  Created by iOSApp on 28/02/2023.
//

import UIKit
class StateController: UIViewController,UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout, UITextFieldDelegate  {
    var stateViewModel =  StateViewModel()
    var timer:Timer?
    var searchStateViewModel = SearchStateViewModel()
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "States"
        label.textColor = AppColors.green.color
        label.font = UIFont(name: AppFonts.silkabold.font, size: 25)
        label.textAlignment = .left
        return label
    }()
    lazy var noContentView : UIView = {
        let content = UIView()
        content.backgroundColor = backgroundSystemColor
        content.isHidden = true
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    lazy var noRecordIcon: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.noBookmarkIcon.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var noContentLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Content not available"
        label.textAlignment = .center
        label.font = UIFont(name: AppFonts.silkabold.font, size: 14)
        return label
    }()
    lazy var instructionLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textColor = AppColors.lightNavyBlue.color
        label.text = "Please check back for content."
        label.textAlignment = .center
        return label
    }()
    lazy var searchTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Search",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.addTarget(self, action: #selector(textFieldValDidChange(_:)), for: .editingChanged)
        textField.autocapitalizationType = .none
        return textField
    }()
    // MARK: - ... Validation of all search field
    @objc func textFieldValDidChange(_ textField: UITextField) {
        timer?.invalidate()
        //setup timer
        timer = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.searchText), userInfo: nil, repeats: false)
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // Check if the text field already contains text
        if let currentText = textField.text {
            // Find the index of the first letter in the current text
            if let firstLetterIndex = currentText.firstIndex(where: { !$0.isWhitespace }) {
                // Check if the replacement string starts with spaces
                if string.first == " " && string.prefix(upTo: firstLetterIndex).contains(" ") {
                    // If the replacement string starts with spaces after the first letter, ignore those spaces
                    textField.text = String(currentText.prefix(upTo: firstLetterIndex)) + string.trimmingCharacters(in: .whitespacesAndNewlines)
                    return false
                }
            }
        }

        // Allow other characters to be entered
        return true
    }
    @objc func searchText() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.6) { [weak self] in
            if self?.searchTextField.text != "" {
                Loader.shared.showLoader()
                self?.searchStateViewModel.getSearchState(userKeyword: self?.searchTextField.text ?? "", completion: {
                    Loader.shared.hideLoader()
                    if (self?.searchStateViewModel.searchState.count)! > 0 {
                        self?.SearchStateCollectionView.reloadData()
                        self?.SearchStateCollectionView.isHidden = false
                        self?.StatesCollectionView.isHidden = true
                        self?.noContentView.isHidden = true
                    } else {
                        self?.SearchStateCollectionView.isHidden = true
                        self?.StatesCollectionView.isHidden = true
                        self?.noContentView.isHidden = false
                    }
                })
            } else {
                self?.getAllState()
                self?.SearchStateCollectionView.isHidden = true
                self?.noContentView.isHidden = true
            }
        }
    }
    lazy var StatesCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 15
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        cv.backgroundColor = backgroundSystemColor
        cv.dataSource = self
        cv.delegate = self
        cv.showsVerticalScrollIndicator = true
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    lazy var SearchStateCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 15
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        cv.backgroundColor = backgroundSystemColor
        cv.isHidden = true
        cv.dataSource = self
        cv.delegate = self
        cv.showsVerticalScrollIndicator = true
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    func showDoneButton(){
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dropKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.searchTextField.inputAccessoryView = toolbar
    }
    @objc func dropKeyboard(){
        view.endEditing(true)
    }

    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        showDoneButton()
        layoutViews()
        searchTextField.delegate = self
        self.hideKeyboardWhenTappedAround()
        registercCell()
        getAllState()
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    func getAllState() {
        Loader.shared.showLoader()
        stateViewModel.getState {
            Loader.shared.hideLoader()
            DispatchQueue.main.async { [self] in
                
                self.StatesCollectionView.isHidden = false
                self.StatesCollectionView.reloadData()
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
    }
   
    func registercCell() {
        StatesCollectionView.register(CardCollectionViewCell.self, forCellWithReuseIdentifier: CardCollectionViewCell.identifier)
        SearchStateCollectionView.register(CardCollectionViewCell.self, forCellWithReuseIdentifier: CardCollectionViewCell.identifier)
    }
}
extension StateController: CardCellDelegate {
    func onClickCardForwardButton(index: Int) {
        navigationController?.pushViewController(RightDetailsController(), animated: true)
    }
}
