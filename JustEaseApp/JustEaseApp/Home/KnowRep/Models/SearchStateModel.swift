//
//  SearchStateModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/6/23.
//

import Foundation
struct SearchStateResponse: Decodable {
    let status: Int
    let message: String
    let data: [SearchStateData]
}
struct SearchStateData: Decodable {
    let id: Int
    let title: String
}
