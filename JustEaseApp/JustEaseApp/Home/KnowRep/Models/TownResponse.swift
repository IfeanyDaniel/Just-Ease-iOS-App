//
//  TownResponse.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/4/23.
//

import Foundation
struct TownResponse: Decodable {
    let status: Int
    let message: String
    let data: [TownData]
}
struct TownData: Decodable {
    let id: Int
    let title: String
   // let updated_at: String
    let local_government_id: Int
    let local_government : LocalGovernment
}
struct LocalGovernment: Decodable {
    let id: Int
    let state_id: Int
}

