//
//  LgAreasResponse.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/4/23.
//

import Foundation
struct LgAreaResponse: Decodable {
    let status: Int
    let message: String
    let data: [LgAreaData]
}
struct LgAreaData: Decodable {
    let id: Int
    let title: String
    
}
