//
//  StateResponse.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/4/23.
//

import Foundation
struct StateResponse: Decodable {
    let status: Int
    let message: String
    let data: [StateData]
}
struct StateData: Decodable {
    let id: Int
    let title: String
    let updated_at: String
    let is_deleted: Int
}
