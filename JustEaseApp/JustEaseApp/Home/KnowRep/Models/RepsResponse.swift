//
//  RepsResponse.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/4/23.
//

import Foundation

struct RepsResponse: Decodable {
    let status: Int
    let message: String
    let data: [RepsData]
}
struct RepsData: Decodable {
    let id: Int
    let name: String
    let email: String?
    let website: String?
    let position: String
    let image: RepsImageData?
    let about_rep: String
    let name_of_area_covered: String
    let address: String
    let state: String
    let phone_number: String?
    
}
struct RepsImageData: Decodable {
    let id : Int
    let file_path: String
}

