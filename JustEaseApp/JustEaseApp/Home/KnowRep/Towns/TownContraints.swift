//
//  TownContraints.swift
//  JustEaseApp
//
//  Created by iOSApp on 28/02/2023.
//

import UIKit
extension TownController  {
 
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(label)
        view.addSubview(stateLabel)
        view.addSubview(searchTextField)
        view.addSubview(townsTableView)
        view.addSubview(searchTownsTableView)
        view.addSubview(noContentView)
        noContentView.addSubview(noRecordIcon)
        noContentView.addSubview(noContentLabel)
        noContentView.addSubview(instructionLabel)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = backgroundSystemColor
        
        NSLayoutConstraint.activate([
    
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            
            stateLabel.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 10),
            stateLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
        
            searchTextField.topAnchor.constraint(equalTo: stateLabel.bottomAnchor, constant: 20),
            searchTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber),
            searchTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            searchTextField.heightAnchor.constraint(equalToConstant: textFieldHeight),
            
            noContentView.topAnchor.constraint(equalTo: searchTextField.bottomAnchor, constant: 30),
            noContentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            noContentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            noContentView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
            noRecordIcon.centerXAnchor.constraint(equalTo: noContentView.centerXAnchor),
            noRecordIcon.topAnchor.constraint(equalTo: noContentView.topAnchor, constant: 150),
            noRecordIcon.widthAnchor.constraint(equalToConstant: 200),
            noRecordIcon.heightAnchor.constraint(equalToConstant: 200),
            
            noContentLabel.topAnchor.constraint(equalTo: noRecordIcon.bottomAnchor, constant: 20),
            noContentLabel.leadingAnchor.constraint(equalTo: noContentView.leadingAnchor, constant:  leadingNumber),
            noContentLabel.trailingAnchor.constraint(equalTo:noContentView.trailingAnchor, constant: trailingNumber),
            
            instructionLabel.topAnchor.constraint(equalTo: noContentLabel.bottomAnchor, constant: 20),
            instructionLabel.leadingAnchor.constraint(equalTo: noContentView.leadingAnchor, constant: leadingNumber),
            instructionLabel.trailingAnchor.constraint(equalTo: noContentView.trailingAnchor, constant: trailingNumber),
            
        ])
        townsTableView.anchorWithConstantsToTop(searchTextField.bottomAnchor,
                                                     left: view.leftAnchor, bottom: view.bottomAnchor, right:  view.rightAnchor, topConstant: 20, leftConstant: 25, bottomConstant: 50, rightConstant: 25)
        searchTownsTableView.anchorWithConstantsToTop(searchTextField.bottomAnchor,
                                                     left: view.leftAnchor, bottom: view.bottomAnchor, right:  view.rightAnchor, topConstant: 20, leftConstant: 25, bottomConstant: 50, rightConstant: 25)
    }
}
