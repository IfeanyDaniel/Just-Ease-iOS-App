//
//  TownCollectionView.swift
//  JustEaseApp
//
//  Created by iOSApp on 28/02/2023.
//
import UIKit
extension TownController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView ==  townsTableView {
            return townViewModel.numberOfRowsInSection(section: section)
        } else {
            return searchTownViewModel.numberOfRowsInSection(section: section)
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView ==  townsTableView {
            guard let cell = townsTableView.dequeueReusableCell(withIdentifier: TownCell.identifier, for: indexPath) as? TownCell else { return UITableViewCell() }
            let towns = townViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
            cell.selectionStyle = .none
            cell.stateLabel.text = "\(state), Nigeria"
            cell.townNameLabel.text = towns.title
            return cell
        } else {
            guard let cell = searchTownsTableView.dequeueReusableCell(withIdentifier: TownCell.identifier, for: indexPath) as? TownCell else { return UITableViewCell() }
            let towns = searchTownViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
            cell.selectionStyle = .none
            cell.stateLabel.text = "\(state), Nigeria"
            cell.townNameLabel.text = towns.title
            return cell
        }
       
    }
    
    func tableView(_ tableView: UITableView,
                   viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView ==  townsTableView {
            let towns = townViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
            let controller = LgaAreasController()
            controller.state = state
            controller.townId = towns.id
            controller.townTitle = towns.title
            navigationController?.pushViewController(controller, animated: true)
        } else {
            let towns = searchTownViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
            let controller = LgaAreasController()
            controller.state = state
            controller.townId = towns.id
            controller.townTitle = towns.title
            navigationController?.pushViewController(controller, animated: true)
        }
    }
}
