//
//  LgaAreaController.swift
//  JustEaseApp
//
//  Created by iOSApp on 01/03/2023.
//

import UIKit
class LgaAreasController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    var lgAreaViewModel = LgAreaViewModel()
    var townId = 0
    var townTitle = ""
    var state = ""
    var timer:Timer?
    var searchLgAreaViewModel = SearchLgAreaViewModel()
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Local Government Areas"
        label.textColor = AppColors.green.color
        label.font = UIFont(name: AppFonts.silkabold.font, size: 25)
        label.textAlignment = .left
        return label
    }()
    lazy var stateLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "In Osusu"
        label.numberOfLines = 1
        label.textAlignment = .left
        label.font = UIFont(name: AppFonts.silkLight.font, size: 14)
        label.textColor = AppColors.lighterGray.color
        return label
    }()
    
    lazy var searchTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Search for Local Government",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.autocapitalizationType = .none
        textField.addTarget(self, action: #selector(textFieldValDidChange(_:)), for: .editingChanged)
        return textField
    }()
    // MARK: - ... Validation of all search field
    @objc func textFieldValDidChange(_ textField: UITextField) {
        timer?.invalidate()
        //setup timer
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.searchText), userInfo: nil, repeats: false)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // Check if the text field already contains text
        if let currentText = textField.text {
            // Find the index of the first letter in the current text
            if let firstLetterIndex = currentText.firstIndex(where: { !$0.isWhitespace }) {
                // Check if the replacement string starts with spaces
                if string.first == " " && string.prefix(upTo: firstLetterIndex).contains(" ") {
                    // If the replacement string starts with spaces after the first letter, ignore those spaces
                    textField.text = String(currentText.prefix(upTo: firstLetterIndex)) + string.trimmingCharacters(in: .whitespacesAndNewlines)
                    return false
                }
            }
        }

        // Allow other characters to be entered
        return true
    }
    @objc func searchText() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.8) { [weak self] in
            if self?.searchTextField.text != "" {
                Loader.shared.showLoader()
                self?.searchLgAreaViewModel.getSearchLgArea(userKeyword: self?.searchTextField.text ?? "", completion: {
                    Loader.shared.hideLoader()
                    if self?.searchLgAreaViewModel.searchLgArea.count ?? 0 > 0 {
                        self?.searchLgaAreasTableView.reloadData()
                        self?.searchLgaAreasTableView.isHidden = false
                        self?.lgaAreasTableView.isHidden = true
                        self?.noContentView.isHidden = true
                    } else {
                        self?.searchLgaAreasTableView.isHidden = true
                        self?.lgaAreasTableView.isHidden = true
                        self?.noContentView.isHidden = false
                    }
                })
            } else {
                self?.getLgArea()
                self?.searchLgaAreasTableView.isHidden = true
                self?.noContentView.isHidden = true
            }
        }
    }
    lazy var noContentView : UIView = {
        let content = UIView()
        content.backgroundColor = backgroundSystemColor
        content.isHidden = true
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    lazy var noRecordIcon: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.noBookmarkIcon.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var noContentLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Content not available"
        label.textAlignment = .center
        label.font = UIFont(name: AppFonts.silkabold.font, size: 14)
        return label
    }()
    lazy var instructionLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textColor = AppColors.lightNavyBlue.color
        label.text = "Please check back for content."
        label.textAlignment = .center
        return label
    }()
    lazy var lgaAreasTableView : UITableView = {
        let table = UITableView()
        table.backgroundColor = backgroundSystemColor
        table.rowHeight = 80
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.sizeToFit()
        table.delegate = self
        table.dataSource = self
        table.separatorColor = AppColors.green.color
        return table
    }()
    lazy var searchLgaAreasTableView : UITableView = {
        let table = UITableView()
        table.backgroundColor = backgroundSystemColor
        table.rowHeight = 80
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.sizeToFit()
        table.delegate = self
        table.dataSource = self
        table.isHidden = true
        table.separatorColor = AppColors.green.color
        return table
    }()
    func showDoneButton(){
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dropKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.searchTextField.inputAccessoryView = toolbar
    }
    @objc func dropKeyboard(){
        view.endEditing(true)
    }
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        showDoneButton()
        layoutViews()
        self.hideKeyboardWhenTappedAround()
        registercCell()
        getLgArea()
        searchTextField.delegate = self
    }
    func getLgArea() {
        Loader.shared.showLoader()
        lgAreaViewModel.getLgArea(id: townId) {
            Loader.shared.hideLoader()
            self.stateLabel.text = "in \(self.townTitle)"
            DispatchQueue.main.async { [self] in
                if lgAreaViewModel.lgArea.count == 0 {
                    noContentView.isHidden = false
                    lgaAreasTableView.isHidden = true
                    
                } else {
                    self.lgaAreasTableView.reloadData()
                   
                    noContentView.isHidden = true
                    lgaAreasTableView.isHidden = false
                }
                
            }
        }
        
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
    }
    
    func registercCell() {
        lgaAreasTableView.register(TownCell.self, forCellReuseIdentifier: TownCell.identifier)
        searchLgaAreasTableView.register(TownCell.self, forCellReuseIdentifier: TownCell.identifier)
        
    }
}

