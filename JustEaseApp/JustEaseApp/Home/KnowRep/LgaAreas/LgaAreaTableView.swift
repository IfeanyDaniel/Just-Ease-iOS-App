//
//  LgaAreaTableView.swift
//  JustEaseApp
//
//  Created by iOSApp on 01/03/2023.
//

import UIKit
extension LgaAreasController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == lgaAreasTableView {
            return lgAreaViewModel.numberOfRowsInSection(section: section)
        }else {
            return searchLgAreaViewModel.numberOfRowsInSection(section: section)
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == lgaAreasTableView {
            guard let cell = lgaAreasTableView.dequeueReusableCell(withIdentifier: TownCell.identifier, for: indexPath) as? TownCell else { return UITableViewCell() }
            let lgArea = lgAreaViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
            cell.selectionStyle = .none
            cell.townNameLabel.text = lgArea.title
            cell.stateLabel.text = "\(state), Nigeria"
            return cell
        }else {
            guard let cell = searchLgaAreasTableView.dequeueReusableCell(withIdentifier: TownCell.identifier, for: indexPath) as? TownCell else { return UITableViewCell() }
            let lgArea =  searchLgAreaViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
            cell.selectionStyle = .none
            cell.townNameLabel.text = lgArea.title
            cell.stateLabel.text = "\(state), Nigeria"
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView,
                   viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == lgaAreasTableView {
            let lgArea = lgAreaViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
            let controller = RepsController()
            controller.id = lgArea.id
            controller.lgAreaTitle = lgArea.title
            navigationController?.pushViewController(controller, animated: true)
        } else {
            let lgArea = searchLgAreaViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
            let controller = RepsController()
            controller.id = lgArea.id
            controller.lgAreaTitle = lgArea.title
            navigationController?.pushViewController(controller, animated: true)
        }
        
    }
}
