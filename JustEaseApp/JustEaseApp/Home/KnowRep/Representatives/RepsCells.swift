//
//  RepsCells.swift
//  JustEaseApp
//
//  Created by iOSApp on 02/03/2023.
//

import UIKit
class RepsCell: UITableViewCell {
    static var identifier = "repsCellId"
    lazy var icon :  UIImageView  = {
        let profileImageView =  UIImageView()
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.layer.masksToBounds =  true
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.layer.cornerRadius = 40
        return profileImageView
    }()
    lazy var repNameLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "SEN. THEODORE ORJI"
        label.textColor = textSystemColor
        return label
    }()
    lazy var repPositionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "State House of Assembly"
        label.numberOfLines = 1
        label.textAlignment = .left
        label.font = UIFont(name: AppFonts.silkLight.font, size: 12)
        label.textColor = AppColors.lighterGray.color
        return label
    }()
    lazy var forwardButton: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.forwardGreenButton.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
       // button.addTarget(self, action: #selector(clickContactForwardButton(_:)), for: .touchUpInside)
        return button
    }()
    // MARK: - ... Adding subviews and constraints in the cell
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(icon)
        addSubview(repNameLabel)
        addSubview(repPositionLabel)
        addSubview(forwardButton)
        
        NSLayoutConstraint.activate([
            icon.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            icon.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            icon.heightAnchor.constraint(equalToConstant: 80),
            icon.widthAnchor.constraint(equalToConstant: 80),
            
            repNameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 35),
            repNameLabel.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: 10),
            
            repPositionLabel.topAnchor.constraint(equalTo: repNameLabel.bottomAnchor, constant: 15),
            repPositionLabel.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: 10),
            
            forwardButton.topAnchor.constraint(equalTo: topAnchor, constant: 50),
            forwardButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            
        ])
    }
    required init?(coder: NSCoder) {
        fatalError("init(code:) has not been implemented")
    }
}
