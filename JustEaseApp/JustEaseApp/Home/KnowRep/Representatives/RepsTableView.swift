//
//  RepsTableView.swift
//  JustEaseApp
//
//  Created by iOSApp on 02/03/2023.
//

import UIKit
import Kingfisher
extension RepsController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repsViewModel.numberOfRowsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: RepsCell.identifier, for: indexPath) as? RepsCell else { return UITableViewCell() }
        let repDetails = repsViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
        cell.repNameLabel.text = repDetails.name
        cell.repPositionLabel.text = repDetails.position
        cell.icon.kf.setImage(with: URL(string: repDetails.image_path))
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView,
                   viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let repDetails = repsViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
        let controller = RepsDetailsController()
        controller.repsImage = repDetails.image_path
        controller.repsName = repDetails.name
        controller.repsPosition = repDetails.position
        controller.aboutReps = repDetails.aboutRep
        controller.repsAreaCovered = repDetails.name_of_area_covered
        controller.repsState = repDetails.state
        controller.repsWebsite = repDetails.website
        controller.repsAddress = repDetails.address
        controller.repsEmail = repDetails.email
        controller.phoneNumber = repDetails.phonenumber
        navigationController?.pushViewController(controller, animated: true)
    }
}
