//
//  RepsContraints.swift
//  JustEaseApp
//
//  Created by iOSApp on 02/03/2023.
//

import UIKit
extension RepsController  {
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(label)
        view.addSubview(stateLabel)
        view.addSubview(repsTableView)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = backgroundSystemColor
        
        NSLayoutConstraint.activate([
    
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            
            stateLabel.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 10),
            stateLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
        
            
        ])
        repsTableView.anchorWithConstantsToTop(stateLabel.bottomAnchor,
                                                     left: view.leftAnchor, bottom: view.bottomAnchor, right:  view.rightAnchor, topConstant: 30, leftConstant: 25, bottomConstant: 50, rightConstant: 25)
    }
}
