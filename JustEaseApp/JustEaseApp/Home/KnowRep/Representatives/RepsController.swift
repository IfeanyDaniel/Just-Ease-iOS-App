//
//  RepsController.swift
//  JustEaseApp
//
//  Created by iOSApp on 02/03/2023.
//

import UIKit
class RepsController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var repsViewModel = RepsViewModel()
    var id = 0
    var lgAreaTitle = ""
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Representatives"
        label.textColor = AppColors.green.color
        label.font = UIFont(name: AppFonts.silkabold.font, size: 25)
        label.textAlignment = .left
        return label
    }()
    lazy var stateLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = ""
        label.numberOfLines = 1
        label.textAlignment = .left
        label.font = UIFont(name: AppFonts.silkLight.font, size: 14)
        label.textColor = AppColors.lighterGray.color
        return label
    }()
   
    lazy var repsTableView : UITableView = {
        let table = UITableView()
        table.backgroundColor = backgroundSystemColor
        table.rowHeight = 120
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.sizeToFit()
        table.delegate = self
        table.dataSource = self
        table.separatorColor = AppColors.green.color
        return table
    }()
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        registercCell()
        getRepsDetails()
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
    }
    func getRepsDetails() {
        Loader.shared.showLoader()
        repsViewModel.getRepsDetails(lgAreaId: id) {
            Loader.shared.hideLoader()
            DispatchQueue.main.async { [self] in
                self.repsTableView.reloadData()
                stateLabel.text = "In \(lgAreaTitle) Local Government "
            }
        }
    }
    func registercCell() {
        repsTableView.register(RepsCell.self, forCellReuseIdentifier: RepsCell.identifier)

    }
}

