//
//  StateResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/4/23.
//
import Foundation
struct StateResource {
    func getState(completionHandler: @escaping (_ result: StateResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let  stateUrl = ApiEndpoints.stateEndPoint
        
        let url = URL(string:  stateUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType:  StateResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}
