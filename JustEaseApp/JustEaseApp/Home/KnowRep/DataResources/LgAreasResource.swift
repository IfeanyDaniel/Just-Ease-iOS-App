//
//  LgAreasResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/4/23.
//

import Foundation
struct LgAreaResource {
    func getLgArea(townId: Int, completionHandler: @escaping (_ result: LgAreaResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let lgAreaUrl = ApiEndpoints.lgaEndPoint + "/\(townId)/local-government"
        
        let url = URL(string: lgAreaUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType: LgAreaResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}
