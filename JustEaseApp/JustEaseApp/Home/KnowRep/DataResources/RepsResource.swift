//
//  RepsResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/4/23.
//

import Foundation
struct RepsResource {
    func getReps(id: Int, completionHandler: @escaping (_ result: RepsResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let repsUrl = ApiEndpoints.repEndPoint + "/\(id)"
        let url = URL(string: repsUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType: RepsResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}
