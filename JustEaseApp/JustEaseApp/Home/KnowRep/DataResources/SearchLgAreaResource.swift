//
//  SearchLgAreaResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/7/23.
//

import Foundation
struct SearchLgAreaResource {
    func getLgAreaSearch(keyword: String, completionHandler: @escaping (_ result: LgAreaResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let lgAreaSearchUrl = ApiEndpoints.lgSearchEndPoint + "=\(keyword)"
        if let url = URL(string: lgAreaSearchUrl) {
            // Use the valid URL
            do {
                httpUtility.getAPIData(requestURL: url, resultType: LgAreaResponse.self) { result in
                    completionHandler(result)
                }
            }
        } else {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Not Found", type: .error)
        }
    }
}
