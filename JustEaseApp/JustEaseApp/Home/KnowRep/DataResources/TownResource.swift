//
//  TownResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/4/23.
//

import Foundation
struct TownResource {
    func getTown(stateId: Int, completionHandler: @escaping (_ result: TownResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let townUrl = ApiEndpoints.townEndPoint + "?state_id=\(stateId)"
        
        let url = URL(string: townUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType: TownResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}

