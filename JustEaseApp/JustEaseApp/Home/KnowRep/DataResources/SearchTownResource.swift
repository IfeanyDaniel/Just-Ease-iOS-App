//
//  SearchTownResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/7/23.
//

import Foundation
struct SearchTownResource {
    func getTownSearch(keyword: String, stateId: Int,completionHandler: @escaping (_ result: TownResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let townSearchUrl = ApiEndpoints.townSearchEndPoint + "=\(keyword)&state_id=\(stateId)"
        if let url = URL(string: townSearchUrl) {
            // Use the valid URL
            do {
                httpUtility.getAPIData(requestURL: url, resultType: TownResponse.self) { result in
                    completionHandler(result)
                }
            }
        } else {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Not Found", type: .error)
        }
    }
}
