//
//  SearchStateResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/6/23.
//

import Foundation
struct SearchStateResource {
    func getStateSearch(keyword: String, completionHandler: @escaping (_ result: SearchStateResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let stateSearchUrl = ApiEndpoints.stateSearchEndPoint + "=\(keyword)"
        print(">>>\(stateSearchUrl)")
        if let url = URL(string: stateSearchUrl) {
            // Use the valid URL
            do {
                httpUtility.getAPIData(requestURL: url, resultType: SearchStateResponse.self) { result in
                    completionHandler(result)
                }
            }
        } else {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Not Found", type: .error)
        }
      
    }
}
