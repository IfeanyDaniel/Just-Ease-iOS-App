//
//  RepsResponse.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/4/23.
//

import Foundation
struct RepsDetails: Codable {
    var id: Int
    var name: String
    var email: String
    var website: String
    var position: String
    var image_path: String
    var aboutRep: String
    var name_of_area_covered: String
    var address: String
    var state: String
    var phonenumber: String
}
class RepsViewModel {
    var repsDetails: [RepsDetails] = []
    func getRepsDetails(lgAreaId: Int, completion: @escaping () -> Void) {
        let repResource = RepsResource()
        repResource.getReps(id: lgAreaId) { getRepsApiResponse in
            DispatchQueue.main.async { [self] in
               let repsData = getRepsApiResponse?.data
                repsDetails.removeAll()
                for index in 0..<(repsData?.count ?? 0) {
                    let repsId = repsData?[index].id ?? 0
                    let repsName = repsData?[index].name ?? ""
                    let repsEmail = repsData?[index].email ?? ""
                    let repsWebsite = repsData?[index].website ?? ""
                    let repsPosition = repsData?[index].position ?? ""
                    let aboutRep = repsData?[index].about_rep ?? ""
                    let repsAreaCovered = repsData?[index].name_of_area_covered ?? ""
                    let repsAddress = repsData?[index].address ?? ""
                    let repsState = repsData?[index].state ?? ""
                    let repsPhoneNumber = repsData?[index].phone_number ?? ""
                    let repsImage = repsData?[index].image?.file_path ?? ""
                    repsDetails.append(RepsDetails(id: 7, name: repsName, email: repsEmail, website: repsWebsite, position: repsPosition, image_path: repsImage, aboutRep: aboutRep, name_of_area_covered: repsAreaCovered, address: repsAddress, state: repsState, phonenumber: repsPhoneNumber))
                }
                completion()
            }
        }
    }
    func numberOfRowsInSection(section: Int) -> Int {
        return  repsDetails.count
    }
    func cellForRowsAt(indexPath: IndexPath) -> [RepsDetails] {
        return repsDetails
    }
}
