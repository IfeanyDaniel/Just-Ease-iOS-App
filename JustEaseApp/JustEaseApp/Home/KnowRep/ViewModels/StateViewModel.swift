//
//  StateViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/4/23.
//

import Foundation
struct State : Codable {
    var id: Int
    var title: String
}
class StateViewModel {
    var state :[State] = []
    func getState(completion: @escaping () -> Void) {
        let stateResource = StateResource()
        stateResource.getState() { getStateApiResponse in
            DispatchQueue.main.async { [self] in
                let stateData = getStateApiResponse?.data
                state.removeAll()
                for index in 0..<(stateData?.count ?? 0) {
                    let stateId = stateData?[index].id ?? 0
                    let stateTitle = stateData?[index].title ?? ""
                    state.append(State(id: stateId,title: stateTitle))
                }
                completion()
            }
        }
    }
    func numberOfRowsInSection(section: Int) -> Int {
        return  state.count
    }
    func cellForRowsAt(indexPath: IndexPath) -> [State] {
        return state
    }
}
