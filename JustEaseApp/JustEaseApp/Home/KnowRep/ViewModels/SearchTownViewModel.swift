//
//  SearchTownViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/7/23.
//

import Foundation
struct SearchTown: Codable {
    var id: Int
    var title: String
}

class SearchTownViewModel {
    var searchTown: [SearchTown] = []
    func getSearchTown(userKeyword: String, stateId: Int, completion: @escaping () -> Void) {
        let searchTownResource = SearchTownResource()
        searchTownResource.getTownSearch(keyword: userKeyword, stateId: stateId) { getSearchTownApiResponse  in
            DispatchQueue.main.async { [self] in
                let searchTownData = getSearchTownApiResponse?.data
                searchTown.removeAll()
                for index in 0..<(searchTownData?.count ?? 0) {
                    let searchTownId = searchTownData?[index].id ?? 0
                    let searchTownitle = searchTownData?[index].title ?? ""
                    searchTown.append(SearchTown(id: searchTownId,title:searchTownitle))
                }
                completion()
            }
        }
        
    }
    func numberOfRowsInSection(section: Int) -> Int {
        return  searchTown.count
    }
    func cellForRowsAt(indexPath: IndexPath) -> [SearchTown] {
        return searchTown
    }
}
