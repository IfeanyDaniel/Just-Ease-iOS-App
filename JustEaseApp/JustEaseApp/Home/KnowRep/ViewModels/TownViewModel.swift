//
//  TownViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/4/23.
//


import Foundation
struct Town: Codable {
    var id: Int
    var title: String
}

class TownViewModel {
    var town: [Town] = []
    func getTown(id: Int, completion: @escaping () -> Void) {
        let townResource = TownResource()
        townResource.getTown(stateId: id) { getTownApiResponse in
            DispatchQueue.main.async { [self] in
                let townData = getTownApiResponse?.data
                town.removeAll()
                for index in 0..<(townData?.count ?? 0) {
                    let townId = townData?[index].id ?? 0
                    let townTitle = townData?[index].title ?? ""
                    town.append(Town(id: townId,title: townTitle))
                }
                completion()
            }
        }
    }
    func numberOfRowsInSection(section: Int) -> Int {
        return  town.count
    }
    func cellForRowsAt(indexPath: IndexPath) -> [Town] {
        return town
    }
}
