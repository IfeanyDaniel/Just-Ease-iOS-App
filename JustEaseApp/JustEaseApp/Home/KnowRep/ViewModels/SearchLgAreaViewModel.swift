//
//  SearchLgAreaViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/7/23.
//


import Foundation
struct SearchLgArea: Codable {
    var id: Int
    var title: String
}

class SearchLgAreaViewModel {
    var searchLgArea: [SearchLgArea] = []
    func getSearchLgArea(userKeyword: String, completion: @escaping () -> Void) {
        let searchLgAreaResource = SearchLgAreaResource()
        searchLgAreaResource.getLgAreaSearch(keyword: userKeyword) { getSearchLgAreaApiResponse in
            DispatchQueue.main.async { [self] in
                let searchLgAreaData = getSearchLgAreaApiResponse?.data
                searchLgArea.removeAll()
                for index in 0..<(searchLgAreaData?.count ?? 0) {
                    let searchLgAreaId = searchLgAreaData?[index].id ?? 0
                    let searchLgAreaTitle = searchLgAreaData?[index].title ?? ""
                    searchLgArea.append(SearchLgArea(id: searchLgAreaId ,title: searchLgAreaTitle ))
                }
                completion()
            }
        }
    }
    func numberOfRowsInSection(section: Int) -> Int {
        return  searchLgArea.count
    }
    func cellForRowsAt(indexPath: IndexPath) -> [SearchLgArea] {
        return searchLgArea
    }
}
