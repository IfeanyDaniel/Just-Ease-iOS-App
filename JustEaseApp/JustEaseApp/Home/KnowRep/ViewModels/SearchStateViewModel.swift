//
//  SearchViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/7/23.
//


import Foundation
struct SearchState : Codable {
    var id: Int
    var title: String
}
class SearchStateViewModel {
    var searchState :[SearchState ] = []
    func getSearchState(userKeyword:String, completion: @escaping () -> Void) {
        let searchStateResource = SearchStateResource()
        searchStateResource.getStateSearch(keyword: userKeyword) { getSearchStateApiResponse in
            DispatchQueue.main.async { [self] in
                let searchStateData = getSearchStateApiResponse?.data
                searchState.removeAll()
                for index in 0..<(searchStateData?.count ?? 0) {
                    let searchStateId = searchStateData?[index].id ?? 0
                    let searchStateTitle = searchStateData?[index].title ?? ""
                    searchState.append(SearchState(id: searchStateId,title:searchStateTitle))
                }
                completion()
            }
        }
    }
    func numberOfRowsInSection(section: Int) -> Int {
        return  searchState.count
    }
    func cellForRowsAt(indexPath: IndexPath) -> [SearchState] {
        return searchState
    }
}
