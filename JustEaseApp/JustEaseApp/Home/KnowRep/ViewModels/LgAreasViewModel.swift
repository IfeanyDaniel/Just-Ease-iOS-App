//
//  LgAreasViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/4/23.
//

import Foundation
struct LgArea: Codable {
    var id: Int
    var title: String
}

class LgAreaViewModel {
    var lgArea: [LgArea] = []
    func getLgArea(id: Int, completion: @escaping () -> Void) {
        let lgAreaResource = LgAreaResource()
        lgAreaResource.getLgArea(townId: id) { getLgAreaApiResponse in
            DispatchQueue.main.async { [self] in
                let lgAreaData = getLgAreaApiResponse?.data
                lgArea.removeAll()
                for index in 0..<(lgAreaData?.count ?? 0) {
                    let lgAreaId = lgAreaData?[index].id ?? 0
                    let lgAreaTitle = lgAreaData?[index].title ?? ""
                    lgArea.append(LgArea(id: lgAreaId,title: lgAreaTitle))
                }
                completion()
            }
        }
    }
    func numberOfRowsInSection(section: Int) -> Int {
        return  lgArea.count
    }
    func cellForRowsAt(indexPath: IndexPath) -> [LgArea] {
        return lgArea
    }
}
