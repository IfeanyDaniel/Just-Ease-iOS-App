//
//  QuickActionNewsCell.swift
//  JustEaseApp
//
//  Created by iOSApp on 14/02/2023.
//

import UIKit
protocol QuickNewCellDelegate: AnyObject {
    func onClickCell(index: IndexPath)
}
class QuickNewsCollectionViewCell: UICollectionViewCell {
    static var identifier: String = "QuickNewCollectionCell"
    var quickNewDelegate: QuickNewCellDelegate?
    var index : IndexPath?
    
    @objc func clickDeleteCardButton(_ sender: Any){
        quickNewDelegate?.onClickCell(index: index!)
    }
    lazy var imageView :  UIImageView  = {
        let profileImageView =  UIImageView()
        profileImageView.image = UIImage(named: "image")
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.layer.masksToBounds =  true
        profileImageView.layer.cornerRadius = 10
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.isUserInteractionEnabled = true
        return profileImageView
    }()
    lazy var firstLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Did you Know?"
        label.textAlignment = .left
        label.textColor = AppColors.green.color
        label.font = UIFont(name: AppFonts.silkLight.font ,size: 12)
        return label
    }()
    lazy var secondLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Do you know the grounds a doctor can be held liable for negligence?"
        label.textAlignment = .left
        label.numberOfLines = 3
        label.textColor = AppColors.white.color
        label.font = UIFont(name: AppFonts.silkLight.font ,size: 12)
        return label
    }()
    lazy var thirdLabel: UILabel = {
        let label = PaddingLabel(withInsets: 5, 5, 15, 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Health/Medical"
        label.textColor = AppColors.fairBrown.color
        label.textAlignment = .left
        label.backgroundColor = AppColors.lightBrown.color
        label.layer.cornerRadius = 10
        label.font = UIFont(name: AppFonts.silkLight.font ,size: 12)
        label.layer.masksToBounds = true
        return label
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpViews() {
        addSubview(imageView)
        imageView.addSubview(firstLabel)
        imageView.addSubview(secondLabel)
        imageView.addSubview(thirdLabel)
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            imageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            
            firstLabel.bottomAnchor.constraint(equalTo: imageView.bottomAnchor, constant: -95),
            firstLabel.leadingAnchor.constraint(equalTo: imageView.leadingAnchor, constant: 20),
            
            secondLabel.bottomAnchor.constraint(equalTo: imageView.bottomAnchor, constant: -50),
            secondLabel.leadingAnchor.constraint(equalTo: imageView.leadingAnchor, constant: 20),
            secondLabel.trailingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: -20),
            
            thirdLabel.bottomAnchor.constraint(equalTo: imageView.bottomAnchor, constant: -20),
            thirdLabel.leadingAnchor.constraint(equalTo: imageView.leadingAnchor, constant: 20),
            
        ])
        
    }
}
