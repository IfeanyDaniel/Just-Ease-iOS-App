//
//  RightQuestionResource.swift
//  JustEaseApp
//
//  Created by iOSApp on 28/03/2023.
//

import Foundation
struct QuestionResource {
    func getRightQuestion(questionId: Int,completionHandler: @escaping (_ result: QuestionResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let questionUrl = ApiEndpoints.questionEndPoint + "/\(questionId)"
        let url = URL(string: questionUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType:  QuestionResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}
