//
//  RightResource.swift
//  JustEaseApp
//
//  Created by iOSApp on 27/03/2023.
//

import Foundation
struct RightResource {
    func getRight(recordPerPage: Int, completionHandler: @escaping (_ result: RightResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let rightUrl = ApiEndpoints.rightEndPoint + "?records_per_page=\(recordPerPage)"
        
        let url = URL(string: rightUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType:  RightResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}
