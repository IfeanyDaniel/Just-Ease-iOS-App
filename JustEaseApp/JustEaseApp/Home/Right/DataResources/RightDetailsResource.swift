//
//  RightDetailsResource.swift
//  JustEaseApp
//
//  Created by iOSApp on 27/03/2023.
//

import Foundation
struct RightDetailsResource {
    func getRightDetails(rightId: Int,totalRightRecord: Int, completionHandler: @escaping (_ result: RightDetailsResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let rightDetailsUrl = ApiEndpoints.rightDetailsEndPoint + "/\(rightId)/topics?records_per_page=\(totalRightRecord)"
        let url = URL(string: rightDetailsUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType:  RightDetailsResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}
