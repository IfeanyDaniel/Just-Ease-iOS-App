//
//  RightQuestionViewmodel.swift
//  JustEaseApp
//
//  Created by iOSApp on 28/03/2023.
//
import Foundation
struct Question : Codable {
    var title: String
    var answer: String
    var law: [Laws]
}
struct Laws: Codable {
    var name: String
}
class QuestionViewModel {
    var questions :[Question] = []
    var questionLaws :[Laws] = []
    var questionId = 0
    var lawName = ""
    func getQuestions(questionId: Int, completion: @escaping () -> Void) {
        let questionResource = QuestionResource()
        questionResource.getRightQuestion(questionId: questionId) { getQuestionApiResponse in
            DispatchQueue.main.async { [self] in
                let questionData = getQuestionApiResponse?.data
                questions.removeAll()
                for index in 0..<(questionData?.count ?? 0) {
                    let title = questionData?[index].title ?? ""
                    let answer  = questionData?[index].answer ?? ""
                    let laws  = questionData?[index].laws
                    for index in 0..<(laws?.count ?? 0)  {
                        lawName = laws?[index].name ?? ""
                        questionLaws.append(Laws(name: lawName))
                    }
                    questions.append(Question(title: title, answer: answer, law: questionLaws.compactMap({ return $0
                    })))
                }
                completion()
            }
        }
    }
    func numberOCount() -> Int {
        return  questions.count
    }
    func displayData() -> [Question] {
        return questions
    }
}
