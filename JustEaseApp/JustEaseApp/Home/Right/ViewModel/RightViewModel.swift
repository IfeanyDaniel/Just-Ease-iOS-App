//
//  RightViewModel.swift
//  JustEaseApp
//
//  Created by iOSApp on 27/03/2023.
//

import Foundation
struct Right : Codable {
    var id: Int
    var title: String
}

class RightViewModel {
    var right :[Right] = []
    func getRight (numberPerPage: Int, completion: @escaping () -> Void) {
        let rightResource = RightResource()
        rightResource.getRight(recordPerPage: numberPerPage) { getRightApiResponse in
            DispatchQueue.main.async { [self] in
                let totalNumber = getRightApiResponse?.data.total
                let rightData = getRightApiResponse?.data.data
                Storage.saveRightTotalRecord(number: totalNumber ?? 0)
                right.removeAll()
                for index in 0..<(rightData?.count ?? 0) {
                    let rightId = rightData?[index].id ?? 0
                    let rightTitle = rightData?[index].title ?? ""
                    right.append(Right(id: rightId,title: rightTitle))
                    Storage.saveRight(textInfo: right)
                }
              
                completion()
            }
        }
    }
    func numberOfRowsInSection(section: Int) -> Int {
        return  right.count
    }
    func cellForRowsAt(indexPath: IndexPath) -> [Right] {
        return right
    }
}
