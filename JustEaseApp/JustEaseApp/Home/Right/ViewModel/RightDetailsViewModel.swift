//
//  RightDetailsViewModel.swift
//  JustEaseApp
//
//  Created by iOSApp on 27/03/2023.
//

import Foundation
struct Option : Codable {
    let id : Int
    let title: String
}
class Section : Codable {
    let title : String
    let numberOfQuestions: Int
    let option : [Option]
    var isOpened: Bool
    
    init(title: String, numberOfQuestions: Int, option: [Option], isOpened: Bool = false) {
        self.title = title
        self.numberOfQuestions = numberOfQuestions
        self.option = option
        self.isOpened = isOpened
    }
}
class RightDetailsViewModel {
    var rightDetails :[Section] = []
    var options :[Option] = []
    var questionId = 0
    var questionTitle = ""
    func getRightRightDetails(rightId: Int,totalRightRecord: Int, completion: @escaping () -> Void) {
        let rightDetailsResource = RightDetailsResource()
        UserDefaults.standard.removeObject(forKey: Storage.allRightDetailsKey)
        rightDetailsResource.getRightDetails(rightId: rightId, totalRightRecord: totalRightRecord) { getRightDetailsApiResponse in
            DispatchQueue.main.async { [self] in
                let totalNumber =  getRightDetailsApiResponse?.data.total ?? 0
                let rightDetailsData = getRightDetailsApiResponse?.data.data
                Storage.saveRightDetailsTotalRecord(number: totalNumber)
                rightDetails.removeAll()
                for index in 0..<(rightDetailsData?.count ?? 0)  {
                    let title = rightDetailsData?[index].title ?? ""
                    let questionData = rightDetailsData?[index].questions
                    let countOfQuestions = questionData?.count ?? 0
                    options.removeAll()
                    for index in 0..<(questionData?.count ?? 0)  {
                        questionId = questionData?[index].id ?? 0
                        questionTitle = questionData?[index].title ?? ""
                        options.append(Option(id: questionId, title: questionTitle))
                    }
                    rightDetails.append(Section(title: title, numberOfQuestions: countOfQuestions, option: options.compactMap({ return $0
                    })))
                    Storage.saveRightDetails(textInfo: rightDetails)
                }
                completion()
               
            }
        }
       
    }
    func numberOfRowsInSection() -> [Section]  {
        return rightDetails
    }
    func cellForRowsAt(indexPath: IndexPath) -> [Section] {
        return rightDetails
    }
}
