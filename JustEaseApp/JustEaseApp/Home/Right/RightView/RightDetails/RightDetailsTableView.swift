//
//  RightDetailsCollectionView.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/02/2023.
//

import UIKit
import UIKit
extension RightDetailsController{
    func numberOfSections(in tableView: UITableView) -> Int {
       // rightDetailsViewModel.numberOfRowsInSection().count
        rightDetailsFilterData.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // rightDetailsFilterData rightDetailsViewModel.numberOfRowsInSection()
        if rightDetailsFilterData[section].isOpened {
            return rightDetailsFilterData[section].option.count + 1
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: DetailsCell.identifier, for: indexPath) as? DetailsCell else { return UITableViewCell() }
           // let right = rightDetailsViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.section]
             let right = rightDetailsFilterData[indexPath.section]
            cell.cardLabel.text = right.title
            cell.numberOfItemsLabel.text = "\(right.numberOfQuestions) Subitems"
            cell.selectionStyle = .none
            cell.index = indexPath
            cell.detailsCellDelegate = self
            cell.backgroundColor = backgroundSystemColor
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: SubCell.identifier, for: indexPath) as? SubCell else { return UITableViewCell() }
           // cell.cardLabel.text = rightDetailsViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.section].title
             cell.cardLabel.text = rightDetailsFilterData[indexPath.section].title
            cell.selectionStyle = .none
            cell.index = indexPath
            cell.subCellDelegate = self
            cell.backgroundColor = backgroundSystemColor
           // cell.cardLabel.text = rightDetailsViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.section].option[indexPath.row - 1].title
            cell.cardLabel.text = rightDetailsFilterData[indexPath.section].option[indexPath.row - 1].title
            return cell
        }
       
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 {
           // rightDetailsViewModel.rightDetails[indexPath.section].isOpened = !rightDetailsViewModel.rightDetails[indexPath.section].isOpened
            rightDetailsFilterData[indexPath.section].isOpened = !rightDetailsFilterData[indexPath.section].isOpened
            tableView.reloadSections([indexPath.section], with: .none)
        } else {
          //  let rightSub = rightDetailsViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.section].option[indexPath.row - 1]
            let rightSub = rightDetailsFilterData[indexPath.section].option[indexPath.row - 1]
            print("sub cell tapped")
            let controller = InfoController()
            controller.subTitle = rightSub.title
            controller.subId = rightSub.id
            navigationController?.pushViewController(controller, animated: true)
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        50
    }
  
}


