//
//  RightCollectionView.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/02/2023.
//

import UIKit
extension RightViewController {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //rightViewModel.numberOfRowsInSection(section: section)
        rightFilterData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = rightCollectionView.dequeueReusableCell(withReuseIdentifier: CardCollectionViewCell.identifier, for: indexPath) as? CardCollectionViewCell else { return UICollectionViewCell() }
       // let right = rightViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
        let right = rightFilterData[indexPath.row]
        cell.index = indexPath
        cell.cardCellDelegate = self
        cell.cardLabel.text = right.title
        cell.backgroundColor = backgroundSystemColor
        cell.layer.borderColor = AppColors.green.color.cgColor
        cell.layer.borderWidth = 0.5
        cell.layer.cornerRadius = 5
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = .zero
        cell.layer.shadowRadius = 0.2
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 50, height: 60)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       // let right = rightViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
        let right =  rightFilterData[indexPath.row]
        let controller = RightDetailsController()
        controller.rightID = right.id
        print("&&&&&&&&&&&&&&&\(right.id)")
        controller.rightTitle = right.title
        navigationController?.pushViewController(controller, animated: true)
    }
}
