//
//  RightQuestionInfoResponse.swift
//  JustEaseApp
//
//  Created by iOSApp on 28/03/2023.
//

import Foundation
struct QuestionResponse: Decodable {
    let status: Int
    let message: String
    let data: [QuestionData]
}
struct QuestionData: Decodable {
    let id: Int
    let title: String
    let answer: String
    let topic_id: Int
    let laws: [LawInfo]
}
struct LawInfo: Decodable {
    let id: Int
    let question_id: Int
    let name: String
}

