//
//  File.swift
//  JustEaseApp
//
//  Created by iOSApp on 27/03/2023.
//

import Foundation
struct RightDetailsResponse: Decodable {
    let status: Int
    let message: String
    let data: RightDetailsData
}
struct RightDetailsData: Decodable {
    let current_page: Int
    let data: [RightDataInfo]
    let total : Int
}
struct RightDataInfo: Decodable {
    let id: Int
    let title: String
    let questions: [RightQuestions]
}
struct RightQuestions: Decodable {
    let id: Int
    let title: String
}
