//
//  RightModel.swift
//  JustEaseApp
//
//  Created by iOSApp on 27/03/2023.
//

import Foundation
struct RightResponse: Decodable {
    let status: Int
    let message: String
    let data: RightData
}
struct RightData: Decodable {
    let current_page: Int
    let data: [RightInfo]
    let total : Int
}
struct RightInfo: Decodable {
    let id: Int
    let title: String
}
