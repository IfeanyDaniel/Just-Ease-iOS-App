//
//  PanicViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/30/23.
//
import Foundation
protocol PanicViewModelDelegate {
    func didReceivePanicResponse(panicResponse: PanicResponse?,_ statusCode: Int)
}
class PanicViewModel {
    var delegate: PanicViewModelDelegate?
    func getPanicResponse(panicRequest: PanicRequest, videoUrl: URL) {
        let panicResource = PanicResource()
        panicResource.getResponse(createPanicRequest: panicRequest, videoUrl: videoUrl) { panicApiResponse, statusCode in
            if statusCode == 200 {
                DispatchQueue.main.async {
                    self.delegate?.didReceivePanicResponse(panicResponse: panicApiResponse, statusCode)
                }
            }
            if statusCode == 400 {
                DispatchQueue.main.async {
                    self.delegate?.didReceivePanicResponse(panicResponse: panicApiResponse, statusCode)
                }
            }
            if statusCode == 500 {
                DispatchQueue.main.async {
                    self.delegate?.didReceivePanicResponse(panicResponse: panicApiResponse, statusCode)
                }
            }
            if statusCode == 422 {
                DispatchQueue.main.async {
                    self.delegate?.didReceivePanicResponse(panicResponse: panicApiResponse, statusCode)
                }
            }
        }
    }
}
