//
//  DangerController.swift
//  JustEaseApp
//
//  Created by iOSApp on 09/03/2023.
//

import UIKit
import AVFoundation
protocol VideoRecordingDelegate: AnyObject {
    func moveToVideoRecordingScreen()
}
class DangerController: UIViewController {
    var delegate : VideoRecordingDelegate?
    lazy var dangerIcon: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.masksToBounds = true
        button.setTitle("!", for: .normal)
        button.layer.cornerRadius = 20
        button.backgroundColor = AppColors.red.color
        return button
    }()
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textAlignment = .center
        label.font = UIFont(name: AppFonts.silkaMedium.font ,size: 12)
        label.text = "You just clicked on the panic button, \n are you sure you are in danger?"
        label.textColor = textSystemColor
        return label
    }()
    lazy var dangerButton: UIButton = {
        let button = UIButton()
        button.setTitle("Yes, I'm in danger", for: .normal)
        button.backgroundColor = AppColors.red.color
        button.frame.size.height = 45
        button.layer.cornerRadius = 5
        button.setTitleColor(AppColors.white.color, for: .normal)
        button.addTarget(self, action: #selector(didTapOndangerButton), for: .touchUpInside)
        button.titleLabel?.font = UIFont(name: AppFonts.silkaMedium.font, size: 14)
        return button
    }()
    @objc func didTapOndangerButton() {
        dismiss(animated: false){
            self.delegate?.moveToVideoRecordingScreen()
        }
    }
    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.setTitle("No, just kidding", for: .normal)
        button.addTarget(self, action: #selector(didTapOnCancelButton), for: .touchUpInside)
        button.titleLabel?.font = UIFont(name: AppFonts.silkaMedium.font, size: 14)
        button.layer.cornerRadius = 5
        button.frame.size.height = 45
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = backgroundSystemColor
        button.setTitleColor(AppColors.red.color, for: .normal)
        button.layer.borderWidth = 1.5
        button.layer.borderColor = AppColors.red.color.cgColor
        return button
    }()
    @objc func didTapOnCancelButton() {
        dismiss(animated: true)
    }
    lazy var buttonStack: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(dangerButton)
        stackView.addArrangedSubview(cancelButton)
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = 10
        return stackView
    }()
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
    }

}

