//
//  DangerConstraint.swift
//  JustEaseApp
//
//  Created by iOSApp on 09/03/2023.
//

import UIKit
extension DangerController  {
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(label)
        view.addSubview(dangerIcon)
        view.addSubview(buttonStack)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = viewStackColor
        
        NSLayoutConstraint.activate([
            dangerIcon.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            dangerIcon.topAnchor.constraint(equalTo: view.topAnchor, constant: 50),
            dangerIcon.widthAnchor.constraint(equalToConstant: 40),
            dangerIcon.heightAnchor.constraint(equalToConstant: 40),
            
            label.topAnchor.constraint(equalTo: dangerIcon.bottomAnchor, constant: 40),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber + 10),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber + 10),
            
            buttonStack.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -80),
            buttonStack.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber) ,
            buttonStack.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            buttonStack.heightAnchor.constraint(equalToConstant: 100)
        ])
    }
}
