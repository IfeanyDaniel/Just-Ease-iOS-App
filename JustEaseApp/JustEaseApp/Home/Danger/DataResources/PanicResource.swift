//
//  PanicResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/30/23.
//

import Foundation
struct PanicResource {
    func getResponse(createPanicRequest: PanicRequest, videoUrl: URL ,completionHandler: @escaping (_ result: PanicResponse?,_ statusCode: Int) -> Void) {
        let createPanicURL = URL(string: ApiEndpoints.createPanicEndPoint)!
        let httpUtility = HTTPUtility()
        do {
            let createPanicPostBody = try JSONEncoder().encode(createPanicRequest)
            httpUtility.postVideoResponse(requestUrl: createPanicURL, videoUrl: videoUrl, requestBody: createPanicPostBody, resultType: PanicResponse.self) { createPanicApiResponse, statusCode in
                completionHandler(createPanicApiResponse,statusCode)
            }
        } catch let error {
            debugPrint(error)
        }
    }
}
