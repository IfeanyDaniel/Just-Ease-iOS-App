//
//  PanicRequestModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/30/23.
//

import Foundation
struct PanicRequest: Encodable {
   let user_id: Int
   let file: String
}
