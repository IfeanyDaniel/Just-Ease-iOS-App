//
//  HomeCollectionViews.swift
//  JustEaseApp
//
//  Created by iOSApp on 14/02/2023.
//

import UIKit
extension HomeViewController{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == quickActionCollectionView {
            return  quickActionPage.count
        } else {
            return feedsViewModel.numberOfRowsInSectionHome(section: section)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == quickActionCollectionView {
            guard let cell =  quickActionCollectionView.dequeueReusableCell(withReuseIdentifier: QuickActionCollectionViewCell.identifier, for: indexPath) as? QuickActionCollectionViewCell else { return UICollectionViewCell() }
            let  quickAction =  quickActionPage[indexPath.row]
            cell.backgroundColor = backgroundSystemColor
            cell.label.text =  quickAction.title
            cell.index = indexPath
            cell.cardCellDelegate = self
            cell.icon.setImage(UIImage(named: quickAction.imageName), for: .normal)
            return cell
        } else {
            guard let cell =  quickNewsCollectionView.dequeueReusableCell(withReuseIdentifier: QuickNewsCollectionViewCell.identifier, for: indexPath) as? QuickNewsCollectionViewCell else { return UICollectionViewCell() }
            let feeds = feedsViewModel.cellForRowsAtHome(indexPath: indexPath)[indexPath.row]
            cell.secondLabel.text = feeds.title
            cell.thirdLabel.text = feeds.feedCategoryName
            if feeds.feedCategoryName == "" {
                cell.thirdLabel.text = "Others"
            }
            return cell
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == quickActionCollectionView {
            return CGSize(width: quickActionViewHolder.frame.width / 4, height: 100)
        } else {
            return CGSize(width: quickNewsViewHolder.frame.size.width, height: 220)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == quickActionCollectionView {
            let card = quickActionPage[indexPath.row]
            if card.title  == "Rights" {
                navigationController?.pushViewController(RightViewController(), animated: false)
            }
            if card.title  == "Duties" {
                navigationController?.pushViewController(DutiesViewController(), animated: false)
            }
            if card.title  == "Others" {
                navigationController?.pushViewController(OthersController(), animated: false)
            }
            if card.title  == "Constitution" {
                navigationController?.pushViewController(ConstitutionViewController(), animated: false)
            }
        } else {
            let feeds = feedsViewModel.cellForRowsAtHome(indexPath: indexPath)[indexPath.row]
            let controller = FeedDetailsController()
            controller.feedTitle = feeds.title
            controller.content = feeds.content
            controller.feedImage = feeds.feedCategoryImage
            controller.feedIsLiked = feeds.islike
            controller.feedId = feeds.id
            controller.imageUrl = feeds.imageUrl
            navigationController?.pushViewController(controller, animated: true)
        }
    }
}
