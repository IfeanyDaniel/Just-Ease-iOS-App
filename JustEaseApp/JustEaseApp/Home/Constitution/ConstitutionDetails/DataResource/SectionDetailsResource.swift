//
//  SectionDetailsResource.swift
//  JustEaseApp
//
//  Created by iOSApp on 27/03/2023.
//

import Foundation
struct SectionDetailsResource {
    func getSectionsDetails(sectionNumber: Int, completionHandler: @escaping (_ result: SectionDetailsResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let sectionsUrl = ApiEndpoints.constitutionDetailsEndPoint + "/\(sectionNumber)/content"
        
        let url = URL(string: sectionsUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType:SectionDetailsResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}
