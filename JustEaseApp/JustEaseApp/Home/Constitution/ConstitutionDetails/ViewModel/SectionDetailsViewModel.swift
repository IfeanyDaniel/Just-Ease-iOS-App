//
//  SectionViewModel.swift
//  JustEaseApp
//
//  Created by iOSApp on 27/03/2023.
//

import Foundation
protocol SectionsDetailDelegate {
    func didReceiveSectionsDetailResponse(sectionsDetailResponse: SectionDetailsResponse?)
}

class SectionsDetailsViewModel {
    var delegate : SectionsDetailDelegate?
    func getSectionDetails(sectionNumber: Int, completion: @escaping () -> Void) {
        let sectionDetailsResource = SectionDetailsResource()
        sectionDetailsResource.getSectionsDetails(sectionNumber: sectionNumber) { getSectionDetailsApiResponse in
            DispatchQueue.main.async { [self] in
                self.delegate?.didReceiveSectionsDetailResponse(sectionsDetailResponse: getSectionDetailsApiResponse)
            }
        }
    }
}
