//
//  ConstitutionDetailsController.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/02/2023.
//

import UIKit
class ConstitutionDetailsController: UIViewController, SectionsDetailDelegate {
    var doneButton = UIBarButtonItem()
    var sectionDetailsViewModel = SectionsDetailsViewModel()
    var sectionNum = 0
    var constitutionSectionNumber = ""
    var sectonTitle = ""
    lazy var headerlabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = ""
        label.textColor = AppColors.green.color
        label.font = UIFont(name: AppFonts.silkabold.font, size: 25)
        label.textAlignment = .left
        return label
    }()
    lazy var labelBelowHeader: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = ""
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 14)
        return label
    }()
    lazy var note: UITextView = {
        let tv = UITextView.textViewDesign()
        tv.text = ""
        return tv
    }()
    lazy var shareButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.backgroundColor = backgroundSystemColor
        button.layer.cornerRadius = 0
        button.setImage(AppButtonImages.shareIcon.image, for: .normal)
        button.addTarget(self, action: #selector(didTapShareButton), for: .touchUpInside)
        return button
    }()
    @objc func didTapShareButton() {
            let shareSheetVC = UIActivityViewController(
                activityItems: [headerlabel.text as Any, labelBelowHeader.text as Any, note.text as Any ], applicationActivities: nil
            )
            self.present(shareSheetVC, animated: true)
    }
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        self.hideKeyboardWhenTappedAround()
        Loader.shared.showLoader()
        self.sectionDetailsViewModel.delegate = self
        sectionDetailsViewModel.getSectionDetails(sectionNumber: sectionNum) {}
        
    }
    @objc func done() {
       dismiss(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.setHidesBackButton(true, animated: true)
       
        doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(done))
        self.navigationItem.rightBarButtonItem = doneButton
        navigationItem.rightBarButtonItem?.tintColor = textSystemColor
        
    }
    func didReceiveSectionsDetailResponse(sectionsDetailResponse: SectionDetailsResponse?) {
        if sectionsDetailResponse != nil {
            Loader.shared.hideLoader()
            note.attributedText = sectionsDetailResponse?.data[0].contents[0].body.htmlToAttributedString
            note.textColor = textSystemColor
            headerlabel.text = constitutionSectionNumber
            labelBelowHeader.text = sectonTitle
        } else {
            Toast.shared.showToastWithTItle("Could not load details", type: .error)
        }
    }
    
}

