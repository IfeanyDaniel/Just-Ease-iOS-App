//
//  ConstitutionDetailsConstraint.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/02/2023.
//

import UIKit
extension ConstitutionDetailsController {
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(headerlabel)
        view.addSubview(labelBelowHeader)
        view.addSubview(note)
        view.addSubview(shareButton)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        view.backgroundColor = backgroundSystemColor
        
        NSLayoutConstraint.activate([
            headerlabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 50),
            headerlabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            
            labelBelowHeader.topAnchor.constraint(equalTo: headerlabel.bottomAnchor, constant: 10),
            labelBelowHeader.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            labelBelowHeader.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            
            note.topAnchor.constraint(equalTo: labelBelowHeader.bottomAnchor, constant: 10),
            note.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            note.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            note.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -100),
            
            shareButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            shareButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            shareButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50),
            shareButton.heightAnchor.constraint(equalToConstant: 100)
                                    
                                    
            ])
        
    }
}
