//
//  SectionDetailsResponse.swift
//  JustEaseApp
//
//  Created by iOSApp on 27/03/2023.
//

import Foundation
struct SectionDetailsResponse: Decodable {
    let status: Int
    let message: String
    let data: [SectionDetailsData]
}
struct SectionDetailsData: Decodable {
    let id: Int
    let section: String
    let title: String
    let contents: [SectionContent]
}
struct SectionContent: Decodable {
    let id: Int
    let section_id: Int
    let body: String
}
