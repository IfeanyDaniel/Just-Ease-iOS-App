//
//  SectionResponse.swift
//  JustEaseApp
//
//  Created by iOSApp on 27/03/2023.
//

import Foundation
struct SectionResponse: Decodable {
    let status: Int
    let message: String
    let data: SectionData
}
struct SectionData: Decodable {
    let current_page: Int
    let data: [SectionInfo]
    let total : Int
}
struct SectionInfo: Decodable {
    let id: Int
    let section: String
    let title: String
}
