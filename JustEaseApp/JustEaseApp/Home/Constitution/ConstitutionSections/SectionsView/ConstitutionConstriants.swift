//
//  ConstitutionConstriants.swift
//  JustEaseApp
//
//  Created by iOSApp on 21/02/2023.
//


import UIKit
extension ConstitutionViewController {
 
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(label)
        view.addSubview(constitutionCollectionView)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = backgroundSystemColor
        
        NSLayoutConstraint.activate([
    
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            
            
        ])
        constitutionCollectionView.anchorWithConstantsToTop(label.bottomAnchor,
                                                     left: view.leftAnchor, bottom: view.bottomAnchor, right:  view.rightAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 50, rightConstant: 0)
    }
}
