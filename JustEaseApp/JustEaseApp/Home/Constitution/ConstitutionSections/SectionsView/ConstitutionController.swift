//
//  ConstitutionController.swift
//  JustEaseApp
//
//  Created by iOSApp on 21/02/2023.
//

import UIKit
class ConstitutionViewController: UIViewController,UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout  {
    var sectionsViewModel = SectionsViewModel()
    var pageNumber = 10
    var fetchMore = false
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Full Constitution"
        label.textColor = AppColors.green.color
        label.font = UIFont(name: AppFonts.silkabold.font, size: 25)
        label.textAlignment = .left
        return label
    }()
    
    lazy var constitutionCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 15
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        cv.backgroundColor = backgroundSystemColor
        cv.dataSource = self
        cv.delegate = self
        cv.showsVerticalScrollIndicator = true
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    
    @objc func dropKeyboard(){
        view.endEditing(true)
    }
    func beginBatchFetch() {
        if fetchMore {
            pageNumber += 10
            sectionsViewModel.getSections(numberPerPage: pageNumber) { [self] in
                DispatchQueue.main.async { [self] in
                    fetchMore = true
                    self.constitutionCollectionView.reloadData()
                }
            }
        } else {
            fetchMore = true
        }
    }
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        self.hideKeyboardWhenTappedAround()
        registercCell()
        getConstititionSections()
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
    }
    func getConstititionSections() {
        Loader.shared.showLoader()
        sectionsViewModel.getSections(numberPerPage: Storage.getSectionTotalRecord()) { [self] in
            DispatchQueue.main.async { [self] in
                Loader.shared.hideLoader()
                self.constitutionCollectionView.reloadData()
            }
        }
    }
    func registercCell() {
        constitutionCollectionView.register(ConstitutionCell.self, forCellWithReuseIdentifier: ConstitutionCell.identifier)
    }
}
extension ConstitutionViewController: ConstitutionCellDelegate {
    func onClickCardForwardButton(index: Int) {
        
    }
}
