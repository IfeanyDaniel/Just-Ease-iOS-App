//
//  ConstitutionCardCell.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/02/2023.
//

import UIKit
protocol ConstitutionCellDelegate: AnyObject {
    func onClickCardForwardButton(index: Int)
}
class ConstitutionCell: UICollectionViewCell {
    static var identifier: String = "constitutionCardCell"
    var cardCellDelegate: ConstitutionCellDelegate?
    var index : IndexPath?
    var card: Cards? {
        didSet {
            guard let card = card else {
                return
            }
            let forwardImage = card.forwardImage
            forwardButton.setImage(forwardImage, for: .normal)
            
            let cardName = card.title
            cardLabel.text = cardName
        }
    }
    
    lazy var cardLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Section 1"
        label.font = UIFont(name: AppFonts.silkabold.font, size: 16)
        return label
    }()
    lazy var subTitleLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "State of the Federation and the Federal Capital Territory, Abuja."
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 12)
        label.numberOfLines = 5
        return label
    }()
    lazy var forwardButton: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.forwardGreenButton.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(clickContactForwardButton(_:)), for: .touchUpInside)
        return button
    }()
    @objc func clickContactForwardButton(_ sender: Any){
        guard let indexRow = index?.row else {
            return
        }
        cardCellDelegate?.onClickCardForwardButton(index: indexRow)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpViews() {
        addSubview(cardLabel)
        addSubview(subTitleLabel)
        addSubview(forwardButton)
        NSLayoutConstraint.activate([
            cardLabel.topAnchor.constraint(equalTo: topAnchor, constant: 12),
            cardLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
           
            subTitleLabel.topAnchor.constraint(equalTo: cardLabel.bottomAnchor, constant: 3),
            subTitleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            subTitleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -25),
            
            forwardButton.topAnchor.constraint(equalTo: topAnchor, constant: 30),
            forwardButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            
        ])
    }
}
