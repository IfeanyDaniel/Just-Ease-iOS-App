//
//  ConstitutionCollectionView.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/02/2023.
//


import UIKit
extension ConstitutionViewController {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        sectionsViewModel.numberOfRowsInSection(section: section)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = constitutionCollectionView.dequeueReusableCell(withReuseIdentifier: ConstitutionCell.identifier, for: indexPath) as? ConstitutionCell else { return UICollectionViewCell() }
        let section = sectionsViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
        cell.index = indexPath
        cell.cardCellDelegate = self
        cell.cardLabel.text = "Section \(section.sectionNumber)"
        cell.subTitleLabel.text = section.title
        cell.backgroundColor = backgroundSystemColor
        cell.layer.borderColor = AppColors.green.color.cgColor
        cell.layer.borderWidth = 0.5
        cell.layer.cornerRadius = 5
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = .zero
        cell.layer.shadowRadius = 0.2
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 50, height: 90)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let section = sectionsViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
        let controller = ConstitutionDetailsController()
        controller.sectionNum = section.id
        controller.sectonTitle = section.title
        controller.constitutionSectionNumber = "Section \(section.sectionNumber)"
        present(UINavigationController(rootViewController: controller), animated: false)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        DispatchQueue.main.async { [self] in
            if Storage.getSectionTotalRecord() >= pageNumber {
                        beginBatchFetch()
                    }
                }
    }
}
