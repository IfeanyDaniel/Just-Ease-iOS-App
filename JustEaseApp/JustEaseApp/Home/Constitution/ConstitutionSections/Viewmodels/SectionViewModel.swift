//
//  SectionViewModel.swift
//  JustEaseApp
//
//  Created by iOSApp on 27/03/2023.
//

import Foundation
struct Sections : Codable {
    var id: Int
    var sectionNumber: String
    var title: String
}

class SectionsViewModel {
    var sections:[Sections] = []
    func getSections(numberPerPage: Int, completion: @escaping () -> Void) {
        let sectionsResource = SectionsResource()
        sectionsResource.getSections(recordPerPage: numberPerPage) { getSectionsApiResponse in
            DispatchQueue.main.async { [self] in
                let perPageNumber = getSectionsApiResponse?.data.total
                let sectionData = getSectionsApiResponse?.data.data
                Storage.saveSectionTotalRecord(number: perPageNumber ?? 0)
                sections.removeAll()
                for index in 4..<(sectionData?.count ?? 0) {
                    let sectionId = sectionData?[index].id ?? 0
                    let sectionTitle = sectionData?[index].title ?? ""
                    let sectionNumber = sectionData?[index].section ?? ""
                    sections.append(Sections(id: sectionId, sectionNumber: sectionNumber, title: sectionTitle))
                }
                completion()
            }
        }
    }
    func numberOfRowsInSection(section: Int) -> Int {
        return  sections.count
    }
    func cellForRowsAt(indexPath: IndexPath) -> [Sections] {
        return sections
    }
}
