//
//  SectionsResources.swift
//  JustEaseApp
//
//  Created by iOSApp on 27/03/2023.
//

import Foundation
struct SectionsResource {
    func getSections(recordPerPage: Int, completionHandler: @escaping (_ result: SectionResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let sectionsUrl = ApiEndpoints.constitutionEndPoint + "?records_per_page=\(recordPerPage)"
        
        let url = URL(string: sectionsUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType:SectionResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}
