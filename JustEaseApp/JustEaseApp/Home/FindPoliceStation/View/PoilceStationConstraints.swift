//
//  PoilceStationConstraints.swift
//  JustEaseApp
//
//  Created by iOSApp on 07/03/2023.
//

import UIKit
extension PoliceStationController  {
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(label)
        view.addSubview(location)
        view.addSubview(changeLocationButton)
        view.addSubview(policeStationTableView)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = backgroundSystemColor
        view.addSubview(noContentView)
        noContentView.addSubview(noRecordIcon)
        noContentView.addSubview(noContentLabel)
        noContentView.addSubview(instructionLabel)
        
        NSLayoutConstraint.activate([
            
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -120),
            
            location.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            location.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            location.heightAnchor.constraint(equalToConstant: 30),
            location.widthAnchor.constraint(equalToConstant: 150),
            
            changeLocationButton.topAnchor.constraint(equalTo: location.bottomAnchor, constant: 5),
            changeLocationButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            changeLocationButton.heightAnchor.constraint(equalToConstant: 50),
            changeLocationButton.widthAnchor.constraint(equalToConstant: 100),
            
            noContentView.topAnchor.constraint(equalTo: changeLocationButton.bottomAnchor, constant: 30),
            noContentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            noContentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            noContentView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
        
            noRecordIcon.centerXAnchor.constraint(equalTo: noContentView.centerXAnchor),
            noRecordIcon.topAnchor.constraint(equalTo: noContentView.topAnchor, constant: 150),
            noRecordIcon.widthAnchor.constraint(equalToConstant: 200),
            noRecordIcon.heightAnchor.constraint(equalToConstant: 200),
            
            noContentLabel.topAnchor.constraint(equalTo: noRecordIcon.bottomAnchor, constant: 20),
            noContentLabel.leadingAnchor.constraint(equalTo: noContentView.leadingAnchor, constant: 20),
            noContentLabel.trailingAnchor.constraint(equalTo:noContentView.trailingAnchor, constant: -20),
            
            instructionLabel.topAnchor.constraint(equalTo: noContentLabel.bottomAnchor, constant: 20),
            instructionLabel.leadingAnchor.constraint(equalTo: noContentView.leadingAnchor, constant: 20),
            instructionLabel.trailingAnchor.constraint(equalTo: noContentView.trailingAnchor, constant: -20),
            
        ])
        
        policeStationTableView.anchorWithConstantsToTop(changeLocationButton.bottomAnchor,
                                                  left: view.leftAnchor, bottom: view.bottomAnchor, right:  view.rightAnchor, topConstant: 15, leftConstant: leadingNumber, bottomConstant: 50, rightConstant: leadingNumber)
    }
}
