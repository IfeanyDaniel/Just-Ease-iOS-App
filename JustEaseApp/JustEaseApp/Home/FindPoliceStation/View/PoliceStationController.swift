//
//  PoliceStationController.swift
//  JustEaseApp
//
//  Created by iOSApp on 07/03/2023.
//

import UIKit
import CoreLocation
class PoliceStationController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var policeStatioViewModel = PoliceStationViewModel()
    var locManager = CLLocationManager()
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Stations Near me"
        label.textColor = AppColors.green.color
        label.font = UIFont(name: AppFonts.silkabold.font, size: 20)
        label.textAlignment = .left
        return label
    }()
    
    lazy var location: UIButton = {
        let button = UIButton.showLocationButtonDesign()
        return button
    }()
    
    lazy var changeLocationButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Change Location", for: .normal)
        button.setTitleColor(textSystemColor, for: .normal)
        button.titleLabel?.font = UIFont(name:  AppFonts.silkLight.font, size: 13)
        button.addTarget(self, action: #selector(didTapOnlocation), for: .touchUpInside)
        button.backgroundColor = backgroundSystemColor
        return button
    }()
    @objc func didTapOnlocation() {
        present(UINavigationController(rootViewController: ChangeLocationController()), animated: true)
    }
    lazy var policeStationTableView : UITableView = {
        let table = UITableView()
        table.backgroundColor = backgroundSystemColor
        table.rowHeight = 120
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.sizeToFit()
        table.delegate = self
        table.dataSource = self
        table.showsVerticalScrollIndicator = false
        table.separatorColor = AppColors.green.color
        return table
    }()
    lazy var noContentView : UIView = {
        let content = UIView()
        content.backgroundColor = backgroundSystemColor
        content.isHidden = true
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    
    lazy var noRecordIcon: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.noBookmarkIcon.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var noContentLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Content not available"
        label.textAlignment = .center
        label.font = UIFont(name: AppFonts.silkabold.font, size: 14)
        return label
    }()
    lazy var instructionLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textColor = AppColors.lightNavyBlue.color
        label.text = "Please check back for content."
        label.textAlignment = .center
        return label
    }()
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        registercCell()
        getAllNearbyPoliceStation()
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    func showTableViewRecord() {
        noContentView.isHidden = true
        policeStationTableView.isHidden = false
    }
    func showNoContentView() {
        noContentView.isHidden = false
        policeStationTableView.isHidden = true
    }
    
    func getAllNearbyPoliceStation(){
        guard let currentLocation = locManager.location else {
            return
        }
        let locate = CLLocation(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
        Loader.shared.showLoader()
        policeStatioViewModel.getPoliceStation(long: "\(currentLocation.coordinate.longitude)", lat: "\(currentLocation.coordinate.latitude)") {
            Loader.shared.hideLoader()
            locate.fetchCityAndCountry { streetNumber, lg, street, city, country, error in
                self.location.isHidden = false
                self.location.setTitle("\(streetNumber) \(lg), \(city ?? "")", for: .normal)
            }
            DispatchQueue.main.async { [self] in
                if policeStatioViewModel.policeStation.count == 0 {
                    showNoContentView()
                } else {
                    policeStationTableView.reloadData()
                }
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
    }
    
    func registercCell() {
        policeStationTableView.register(LawyersCell.self, forCellReuseIdentifier: LawyersCell.identifier)
        
    }
}

