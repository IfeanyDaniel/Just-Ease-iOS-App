//
//  PoliceStationTablevIEW.swift
//  JustEaseApp
//
//  Created by iOSApp on 07/03/2023.
//

import UIKit
extension PoliceStationController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return policeStatioViewModel.numberOfRowsInSection().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: LawyersCell.identifier, for: indexPath) as?LawyersCell else { return UITableViewCell() }
        let policeStation = policeStatioViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
        cell.nameLabel.text = policeStation.name
        cell.addressLabel.text = "Location:\(policeStation.city)"
        cell.distanceLabel.text = "\(Int(policeStation.distance))km away from you"
        cell.selectionStyle = .none
        cell.forwardButton.isHidden = true
        cell.locationButton.isHidden = false
        cell.locationButton.setImage(UIImage(named: "Location"), for: .normal)
        cell.icon.image =  UIImage(named: "locationIcon")
        return cell
    }
    
    func tableView(_ tableView: UITableView,
                   viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let policeStation = policeStatioViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
        let controller = RepsDetailsController()
        controller.practiceaAreaView.isHidden = true
        controller.positionView.isHidden = true
        controller.senetorialView.isHidden = true
        controller.originView.isHidden = true
        controller.websiteView.isHidden = true
        controller.aboutView.isHidden = true
        controller.repsName = policeStation.name
        controller.repsEmail = policeStation.email
        controller.phoneNumber = policeStation.phoneNumber
        controller.profileIcon.image = UIImage(named: "locationIcon")
        if policeStation.streetAdress == "" {
            controller.addressView.isHidden = true
        } else {
            controller.addressView.isHidden = false
            controller.repsAddress = policeStation.streetAdress
        }
        navigationController?.pushViewController(controller, animated: true)
    }
}
