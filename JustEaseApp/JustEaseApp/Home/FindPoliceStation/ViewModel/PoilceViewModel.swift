//
//  PoilceViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/12/23.
//

import Foundation
struct PoliceStation {
    let id : Int
    let name: String
    let email: String
    let distance: Float
    let phoneNumber: String
    let streetAdress: String
    let city: String
    let state: String
}

class PoliceStationViewModel {
    var policeStation: [PoliceStation] = []
    func getPoliceStation(long: String, lat: String,completion: @escaping () -> Void ) {
        let policeNearbyResource = PoliceNearbyResource()
        policeNearbyResource.getPoliceNearbyResponse(latitude: lat, longitude: long) { getNearByPoliceStationApiResponse in
            DispatchQueue.main.async { [self] in
                let totalPoliceStation = getNearByPoliceStationApiResponse?.data.total ?? 0
                let policeStationData = getNearByPoliceStationApiResponse?.data.data
                Storage.saveStationTotalRecord(number: totalPoliceStation)
                policeStation.removeAll()
                for index in 0..<(policeStationData?.count ?? 0) {
                    let policeStationId = policeStationData?[index].id ?? 0
                    let policeStationName = policeStationData?[index].name ?? ""
                    let policeStationEmail = policeStationData?[index].email ?? ""
                    let policeStationDistance = policeStationData?[index].distance ?? 0.0
                    let policeStationNumber = policeStationData?[index].phone_number ?? ""
                    let policeStationAddress = policeStationData?[index].primary_location?.street_address ?? ""
                    let policeStationCity = policeStationData?[index].primary_location?.city ?? ""
                    let policeStationState = policeStationData?[index].primary_location?.state ?? ""
                    policeStation.append(PoliceStation(id: policeStationId, name: policeStationName, email: policeStationEmail, distance: policeStationDistance, phoneNumber: policeStationNumber, streetAdress: policeStationAddress, city: policeStationCity, state: policeStationState))
                }
                completion()
            }
        }
    }
    func numberOfRowsInSection() -> [PoliceStation]  {
        return policeStation
    }
    func cellForRowsAt(indexPath: IndexPath) -> [PoliceStation] {
        return policeStation
    }
}
