//
//  PoliceResourece.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/12/23.
//

import Foundation
struct PoliceNearbyResource {
    func getPoliceNearbyResponse(latitude: String, longitude: String, completionHandler: @escaping (_ result: PoliceResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let nearByLawyersUrl = ApiEndpoints.policeEndPoint + "?longitude=\(longitude)&latitude=\(latitude)"
        
        let url = URL(string: nearByLawyersUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType: PoliceResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}
