//
//  PoliceResponse.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/12/23.
//

import Foundation
import Foundation
struct PoliceResponse: Decodable {
    let status: Int
    let message: String
    let data: PoliceData
}
struct PoliceData: Decodable {
    let current_page: Int
    let data: [PoliceInfo]
    let total : Int
}
struct PoliceInfo: Decodable {
    let id: Int
    let name: String?
    let phone_number: String?
    let distance: Float?
    let email: String?
    let about: String?
    let area_command_id: Int
    let primary_location_id: Int
    let primary_location: PrimaryLocation?
    let area_command: AreaCommand?
}
struct PrimaryLocation: Decodable {
    let id: Int
    let police_station_id: Int
    let street_address: String
    let city: String
    let state: String
    let longitude: String
    let latitude: String
}
struct AreaCommand: Decodable {
    let id: Int
    let area_name: String
    let email: String
    let phone_number: String
    let street_address: String
    let city: String
    let state_id: Int
}
