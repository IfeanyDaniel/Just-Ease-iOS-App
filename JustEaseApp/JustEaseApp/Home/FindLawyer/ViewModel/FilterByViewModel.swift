//
//  FilterByViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/1/23.
//

import Foundation
class FilterByKeywordViewModel {
    var lawyers :[Lawyers] = []
    var practiceAreas :[PracticeAreas] = []
    func getByPracticeAres(filterByKeyword: String,lat: String, long: String, recordPerPage: Int, page: Int,completion: @escaping () -> Void ) {
        let filterByKeywordResource = FilterByKeywordResource()
        filterByKeywordResource.getNearbyLawyersResponse(filterByKeyword: filterByKeyword, latitude: lat, longitude: long, recordPerPage: recordPerPage, page: page) { getNearByLawyersApiResponse in
            DispatchQueue.main.async { [self] in
                let totalLawyers = getNearByLawyersApiResponse?.data.total ?? 0
                let lawyersData = getNearByLawyersApiResponse?.data.data
                Storage.saveLawyersTotalRecord(number: totalLawyers)
                lawyers.removeAll()
                for index in 0..<(lawyersData?.count ?? 0) {
                    let lawyerId = lawyersData?[index].id
                    let lawyerName = lawyersData?[index].name
                    let lawyerImage = lawyersData?[index].image?.file_path
                    let lawyerDistance = lawyersData?[index].distance
                    let lawyerWebsite = lawyersData?[index].website
                    let lawyerPhoneNumber = lawyersData?[index].phone_number
                    let lawyerStreetAddress = lawyersData?[index].primary_location.street_address
                    let aboutLawyer = lawyersData?[index].about_lawyer
                    let lawyerEmail = lawyersData?[index].email
                    let practiceArea = lawyersData?[index].practice_areas
                    for index in 0..<(practiceArea?.count ?? 0) {
                        let practiceAreaId = practiceArea?[index].id
                        let practiceAreaTitle = practiceArea?[index].title
                        let practiceAreaDetails = practiceArea?[index].detail
                        practiceAreas.append(PracticeAreas(id: practiceAreaId ?? 0, title: practiceAreaTitle ?? "", detail: practiceAreaDetails ?? ""))
                    }
                    lawyers.append(Lawyers(id:lawyerId ?? 0,name: lawyerName ?? "", image: lawyerImage ?? "", distance: lawyerDistance ?? 0.00, practiceAreas: practiceAreas.compactMap({return $0}), about_lawyer: aboutLawyer ?? "", website: lawyerWebsite ?? "", phoneNumber: lawyerPhoneNumber ?? "", email: lawyerEmail ?? "", location: lawyerStreetAddress ?? ""))
                }
                completion()
            }
        }
    }
    func numberOfRowsInSection() -> [Lawyers]  {
        return lawyers
    }
    func cellForRowsAt(indexPath: IndexPath) -> [Lawyers] {
        return lawyers
    }
}
