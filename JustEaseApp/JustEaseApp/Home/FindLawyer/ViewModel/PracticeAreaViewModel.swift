//
//  PracticeAreaViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/10/23.
//

import Foundation
struct PracticeArea: Codable {
    var id: Int
    var title: String
    var details: String
}
class PracticeAreaViewModel {
    var practiceArea :[PracticeArea] = []
    func getPracticeArea(completion: @escaping () -> Void) {
        let practiceAreaResource = PracticeAreaResource()
        practiceAreaResource.getPracticeAreaResponse() { getPracticeAreaApiResponse in
            DispatchQueue.main.async { [self] in
                let practiceAreaData = getPracticeAreaApiResponse?.data
                practiceArea.removeAll()
                for index in 0..<(practiceAreaData?.count ?? 0) {
                    let practiceAreaId = practiceAreaData?[index].id ?? 0
                    let practiceAreaTitle = practiceAreaData?[index].title ?? ""
                    let practiceAreaDetails = practiceAreaData?[index].detail ?? ""
                    practiceArea.append(PracticeArea(id: practiceAreaId,title: practiceAreaTitle,details: practiceAreaDetails))
                }
                completion()
            }
        }
    }
    func numberOfRowsInSection(section: Int) -> Int {
        return  practiceArea.count
    }
    func cellForRowsAt(indexPath: IndexPath) -> [PracticeArea] {
        return practiceArea
    }
}
