//
//  FindLawyerController.swift
//  JustEaseApp
//
//  Created by iOSApp on 05/03/2023.
//

import UIKit
protocol MoveToNextScreenDelegate: AnyObject {
    func goToLawyerScreen()
    func goToPoliceScreen()
    func goToReportCrimeScreen()
    func goToReportViolationScreen()
}
class FindLawyerController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var delegate : MoveToNextScreenDelegate?
    lazy var tableView : UITableView = {
        let table = UITableView()
        table.backgroundColor = viewStackColor
        table.rowHeight = 60
        table.sizeToFit()
        table.delegate = self
        table.dataSource = self
        table.isScrollEnabled = false
        table.separatorColor = AppColors.lightGray.color
        return table
    }()
    let page: [String] = {
        let lawyer = "Find a Lawyer"
        let police = "Find a Police station"
        return [lawyer,police]
    }()
    func registercCell() {
        tableView.register(TableViewCell.self, forCellReuseIdentifier: TableViewCell.identifier)

    }

    override func viewDidLoad() {
         layoutViews()
         registercCell()
    }
    
}
