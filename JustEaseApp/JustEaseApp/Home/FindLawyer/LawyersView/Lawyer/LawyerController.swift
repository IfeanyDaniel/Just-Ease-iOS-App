//
//  LawyerController.swift
//  JustEaseApp
//
//  Created by iOSApp on 07/03/2023.
//

import UIKit
import FittedSheets
import UIKit
import GoogleMaps
import CoreLocation
import MapKit
class LawyersController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // MARK: - content view
    var isOpen : Bool?
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var nearbyLawyersViewModel = NearbyLawyersViewModel()
    var filterByPracticeAreaViewModel = FilterByPracticeAreaViewModel()
    var filterByKeyword = FilterByKeywordViewModel()
    var routingFrom = ""
    var praticeAreaId = 0
 
    lazy var filterView: UIView = {
        let content = UIView()
        content.translatesAutoresizingMaskIntoConstraints = false
        content.backgroundColor = lightGraySystemColor
        content.layer.cornerRadius = 10
        content.isHidden = true
        return content
    }()
    lazy var filterHolderView: UIView = {
        let content = UIView()
        content.translatesAutoresizingMaskIntoConstraints = false
        content.backgroundColor = .clear
        content.layer.cornerRadius = 10
        content.isHidden = true
        return content
    }()
    lazy var practiceAreaButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("  View by practise Area", for: .normal)
        button.setImage(AppButtonImages.practiceIcon.image, for: .normal)
        button.setTitleColor(textSystemColor, for: .normal)
        button.contentHorizontalAlignment = .left
        button.titleLabel?.font = UIFont(name:  AppFonts.silkabold.font, size: 13)
        button.addTarget(self, action: #selector(didTapOnPracticeArea), for: .touchUpInside)
        button.backgroundColor = lightGraySystemColor
        return button
    }()
    @objc func didTapOnPracticeArea() {
        navigationController?.pushViewController(PracticeAreasController(), animated: true)
    }
    lazy var line: UIView = {
        let content = UIView()
        content.translatesAutoresizingMaskIntoConstraints = false
        content.backgroundColor = AppColors.green.color
        return content
    }()
    lazy var filterByButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("  Filter by", for: .normal)
        button.setImage(AppButtonImages.filterBy.image, for: .normal)
        button.contentHorizontalAlignment = .left
        button.setTitleColor(textSystemColor, for: .normal)
        button.titleLabel?.font = UIFont(name:  AppFonts.silkabold.font, size: 13)
        button.addTarget(self, action: #selector(didTapOnFilterBy), for: .touchUpInside)
        button.backgroundColor = lightGraySystemColor
        return button
    }()
    @objc func didTapOnFilterBy() {
        let controller = FilterByViewController()
        controller.delegate = self
        let sheetController = SheetViewController(controller: controller, sizes: [.fixed(350), .fixed(350)])
        sheetController.blurBottomSafeArea = false
        sheetController.handleView.isHidden = true
        sheetController.topCornersRadius = 30
        present(sheetController, animated: false, completion: nil)
        filterHolderView.isHidden = true
        filterView.isHidden = true
        isOpen = false
    }
    lazy var triButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setImage(UIImage(named: "tri"), for: .normal)
        button.addTarget(self, action: #selector(didTapOnlocation), for: .touchUpInside)
        button.backgroundColor = backgroundSystemColor
        return button
    }()
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Lawyers Near me"
        label.textColor = AppColors.green.color
        label.font = UIFont(name: AppFonts.silkabold.font, size: 20)
        label.textAlignment = .left
        return label
    }()
    
    lazy var location: UIButton = {
        let button = UIButton.showLocationButtonDesign()
        return button
    }()
    
    lazy var changeLocationButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Change Location", for: .normal)
        button.setTitleColor(textSystemColor, for: .normal)
        button.titleLabel?.font = UIFont(name:  AppFonts.silkLight.font, size: 13)
        button.addTarget(self, action: #selector(didTapOnlocation), for: .touchUpInside)
        button.backgroundColor = backgroundSystemColor
        return button
    }()
    
    @objc func didTapOnlocation() {
        present(UINavigationController(rootViewController: ChangeLocationController()), animated: true)
    }
    
    lazy var lawyersTableView : UITableView = {
        let table = UITableView()
        table.backgroundColor = backgroundSystemColor
        table.rowHeight = 120
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.sizeToFit()
        table.delegate = self
        table.dataSource = self
        table.showsVerticalScrollIndicator = false
        table.separatorColor = AppColors.green.color
        return table
    }()
    
    lazy var noContentView : UIView = {
        let content = UIView()
        content.backgroundColor = backgroundSystemColor
        content.isHidden = true
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    
    lazy var noRecordIcon: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.noBookmarkIcon.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var noContentLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Content not available"
        label.textAlignment = .center
        label.font = UIFont(name: AppFonts.silkabold.font, size: 14)
        return label
    }()
    lazy var instructionLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textColor = AppColors.lightNavyBlue.color
        label.text = "Please check back for content."
        label.textAlignment = .center
        return label
    }()
    lazy var lawyerDirButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("   View Lawyer's Directory", for: .normal)
        button.titleLabel?.font = UIFont(name: AppFonts.silkaMedium.font, size: 12)
        button.contentHorizontalAlignment = .left
        button.addTarget(self, action: #selector(didTapOnLawyerDirectory), for: .touchUpInside)
        return button
    }()
    
    lazy var forwardButton: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.deFaultForwardButton.image, for: .normal)
        button.tintColor = AppColors.white.color
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(didTapOnLawyerDirectory), for: .touchUpInside)
        return button
    }()
    @objc func didTapOnLawyerDirectory() {
        navigationController?.pushViewController(LawyersDirectoryController(), animated: true)
    }
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        registercCell()
        getAllNearByLawyers()
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        
        isOpen = false
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "slider.horizontal.3"), style: .plain, target: self, action: #selector(goToPracticeArea))
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationItem.rightBarButtonItem?.tintColor = textSystemColor
    }
    func showNoContentView() {
        noContentView.isHidden = false
        lawyersTableView.isHidden = true
    }
    
    func showTableViewRecord() {
        noContentView.isHidden = true
        lawyersTableView.isHidden = false
    }
    func getAllNearByLawyers() {
        Loader.shared.showLoader()
        guard let currentLocation = locManager.location else {
            return
        }
        let locate = CLLocation(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
        print("lat\(currentLocation.coordinate.latitude)")
        print("lon\(currentLocation.coordinate.longitude)")
        if Storage.getRoute() == "" {
            nearbyLawyersViewModel.getNearbyLawyers(long: "\(currentLocation.coordinate.longitude)", lat: "\(currentLocation.coordinate.latitude)") {
                Loader.shared.hideLoader()
                locate.fetchCityAndCountry {streetNumber ,lg, street, city, country, error in
                    self.location.isHidden = false
                    self.location.setTitle("\(streetNumber) \(lg), \(city ?? "")", for: .normal)
                }
                DispatchQueue.main.async { [self] in
                    if nearbyLawyersViewModel.practiceAreas.count == 0 {
                        showNoContentView()
                    } else {
                        showTableViewRecord()
                        self.lawyersTableView.reloadData()
                    }
                    
                }
            }
        } else if Storage.getRoute() == "filterBy" {
            print(">>\(Storage.getFilterByKeyword())")
            filterByKeyword.getByPracticeAres(filterByKeyword: Storage.getFilterByKeyword(), lat: "\(currentLocation.coordinate.latitude)", long: "\(currentLocation.coordinate.longitude)", recordPerPage: 20, page: 1) {
                Loader.shared.hideLoader()
                DispatchQueue.main.async { [self] in
                    if filterByKeyword.practiceAreas.count == 0 {
                        showNoContentView()
                    } else {
                        showTableViewRecord()
                        self.lawyersTableView.reloadData()
                    }
                }
            }
        }
        else {

            filterByPracticeAreaViewModel.getByPracticeAres(id: praticeAreaId, long: "\(currentLocation.coordinate.longitude)", lat: "\(currentLocation.coordinate.latitude)") {
                Loader.shared.hideLoader()
                DispatchQueue.main.async { [self] in
                    if filterByPracticeAreaViewModel.practiceAreas.count == 0 {
                        showNoContentView()
                    } else {
                        showTableViewRecord()
                        self.lawyersTableView.reloadData()
                    }
                    
                }
            }
        }
        
    }
    @objc func goToPracticeArea() {
        if isOpen == false {
            isOpen = true
            filterHolderView.isHidden = false
            filterView.isHidden = false
        } else {
            isOpen = false
            filterHolderView.isHidden = true
            filterView.isHidden = true
        }
        
    }
    
    func registercCell() {
        lawyersTableView.register(LawyersCell.self, forCellReuseIdentifier: LawyersCell.identifier)
        
    }
}
extension LawyersController : FilterByDelegate {
    func didFilterLaywers() {
        getAllNearByLawyers()
    }
    
}
