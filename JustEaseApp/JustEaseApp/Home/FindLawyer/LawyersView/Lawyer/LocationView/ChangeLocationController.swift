//
//  LocationController.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/10/23.
//

import UIKit
import GooglePlaces
class ChangeLocationController: UIViewController,UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout  {
    var timer:Timer?
    let token = GMSAutocompleteSessionToken.init()
    // Create a type filter.
    var locationAreas : [String] = []
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Enter Location"
        label.textColor = AppColors.green.color
        label.font = UIFont(name: AppFonts.silkabold.font, size: 25)
        label.textAlignment = .left
        return label
    }()
    lazy var noContentView : UIView = {
        let content = UIView()
        content.backgroundColor = backgroundSystemColor
        content.isHidden = true
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    lazy var noRecordIcon: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.noBookmarkIcon.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var noContentLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Content not available"
        label.textAlignment = .center
        label.font = UIFont(name: AppFonts.silkabold.font, size: 14)
        return label
    }()
    lazy var instructionLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textColor = AppColors.lightNavyBlue.color
        label.text = "Please check back for content."
        label.textAlignment = .center
        return label
    }()
    lazy var searchTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Search",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.addTarget(self, action: #selector(textFieldValDidChange(_:)), for: .editingChanged)
        textField.autocapitalizationType = .none
        return textField
    }()
    // MARK: - ... Validation of all search field
    @objc func textFieldValDidChange(_ textField: UITextField) {
        timer?.invalidate()
        //setup timer
        timer = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.searchText), userInfo: nil, repeats: false)
   
      
    }
    @objc func searchText() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.6) { [weak self] in
            let filter = GMSAutocompleteFilter()
            filter.countries = ["NG"]
            // Set bounds to inner-west Sydney Australia.
            //            let neBoundsCorner = CLLocationCoordinate2D(latitude: -33.843366,
            //                                                        longitude: 151.134002)
            //            let swBoundsCorner = CLLocationCoordinate2D(latitude: -33.875725,
            //                                                        longitude: 151.200349)
            //        filter.locationBias = GMSPlaceRectangularLocationOption(neBoundsCorner,
            //                                                                swBoundsCorner)
            GMSPlacesClient().findAutocompletePredictions(fromQuery: self?.searchTextField.text ?? "",
                                                          
                                                          filter: filter,
                                                          sessionToken: self?.token,
                                                          callback: { [self] (results, error) in
                if let error = error {
                    print("Autocomplete error: \(error)")
                    return
                }
                if let results = results {
                    self?.locationAreas.removeAll()
                    for result in results {
                        print("Result \(result.attributedFullText.string)")
                       
                        self?.locationAreas.append(result.attributedFullText.string)
                       
                    }
                }
                self?.locationCollectionView.reloadData()
            })
        }
    }
    lazy var locationCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 15
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        cv.backgroundColor = backgroundSystemColor
        cv.dataSource = self
        cv.delegate = self
        cv.showsVerticalScrollIndicator = true
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
   
    func showDoneButton(){
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dropKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.searchTextField.inputAccessoryView = toolbar
    }
    @objc func dropKeyboard(){
        view.endEditing(true)
    }

    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        showDoneButton()
        layoutViews()
        self.hideKeyboardWhenTappedAround()
        registercCell()
        //getAllState()
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    func getAllState() {
//        Loader.shared.showLoader()
//        stateViewModel.getState {
//            Loader.shared.hideLoader()
//            DispatchQueue.main.async { [self] in
//
//                self.StatesCollectionView.isHidden = false
//                self.StatesCollectionView.reloadData()
//            }
//        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = backgroundSystemColor
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(didTapOnCancel))
        navigationItem.rightBarButtonItem?.tintColor = textSystemColor
    }
    @objc func didTapOnCancel() {
        dismiss(animated: true)
    }
    func registercCell() {
        locationCollectionView.register(CardCollectionViewCell.self, forCellWithReuseIdentifier: CardCollectionViewCell.identifier)
    }
}
extension ChangeLocationController: CardCellDelegate {
    func onClickCardForwardButton(index: Int) {
        navigationController?.pushViewController(RightDetailsController(), animated: true)
    }
}
