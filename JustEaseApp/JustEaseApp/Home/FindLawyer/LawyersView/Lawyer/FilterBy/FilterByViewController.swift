//
//  FilterByViewController.swift
//  JustEaseApp
//
//  Created by iOSApp on 15/03/2023.
//

import UIKit
protocol FilterByDelegate: AnyObject {
    func didFilterLaywers()
}
class FilterByViewController : UIViewController {
    var delegate : FilterByDelegate?
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Filter by"
        label.textColor = AppColors.green.color
        label.font = UIFont(name: AppFonts.silkabold.font, size: 16)
        label.textAlignment = .left
        return label
    }()
    lazy var individualButton: UIButton = {
        let button = UIButton.checkButtonDesign()
        button.setTitle("Individual Lawyers", for: .normal)
        button.addTarget(self, action: #selector(didTapIndividualButton), for: .touchUpInside)
        button.frame.size.height = 55
        return button
    }()
    @objc func didTapIndividualButton() {
        dismiss(animated: true){
            self.delegate?.didFilterLaywers()
            Storage.saveFilterByKeyword(data: "single_lawyer")
        }
        Storage.saveRoutingNearByLawyerFrom(source: "filterBy")
    }
    lazy var lawFirmButton: UIButton = {
        let button = UIButton.checkButtonDesign()
        button.setTitle("Law Firms", for: .normal)
        button.addTarget(self, action: #selector(didTapLawFirmlButton), for: .touchUpInside)
        button.frame.size.height = 55
        return button
    }()
    @objc func didTapLawFirmlButton() {
        dismiss(animated: true){
            self.delegate?.didFilterLaywers()
            Storage.saveFilterByKeyword(data: "law_firm")
        }
        Storage.saveRoutingNearByLawyerFrom(source: "filterBy")
    }
    lazy var seeAllButton: UIButton = {
        let button = UIButton.checkButtonDesign()
        button.setTitle("See All", for: .normal)
        button.addTarget(self, action: #selector(didTapSeeAllButton), for: .touchUpInside)
        button.frame.size.height = 55
        return button
    }()
    @objc func didTapSeeAllButton() {
        dismiss(animated: true){
            self.delegate?.didFilterLaywers()
            Storage.saveFilterByKeyword(data: "")
        }
        Storage.saveRoutingNearByLawyerFrom(source: "filterBy")
    }
    lazy var buttonStack: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(individualButton)
        stackView.addArrangedSubview(lawFirmButton)
        stackView.addArrangedSubview(seeAllButton)
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = 10
        return stackView
    }()
    override func viewDidLoad() {
        layoutViews()
        
    }
    
}
