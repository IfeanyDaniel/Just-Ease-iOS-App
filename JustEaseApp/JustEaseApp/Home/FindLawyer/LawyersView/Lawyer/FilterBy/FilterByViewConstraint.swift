//
//  FilterByViewConstraint.swift
//  JustEaseApp
//
//  Created by iOSApp on 15/03/2023.
//

import UIKit
extension FilterByViewController {
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(label)
        view.addSubview(buttonStack)
        view.backgroundColor =  viewStackColor
        
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 30),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            
            buttonStack.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -70),
            buttonStack.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber) ,
            buttonStack.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            buttonStack.heightAnchor.constraint(equalToConstant: 185)
        ])
    }
}
