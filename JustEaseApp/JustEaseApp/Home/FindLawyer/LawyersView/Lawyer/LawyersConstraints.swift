//
//  LawyersConstraints.swift
//  JustEaseApp
//
//  Created by iOSApp on 07/03/2023.
//

import UIKit
extension LawyersController  {
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(label)
        view.addSubview(location)
        view.addSubview(changeLocationButton)
        view.addSubview(lawyersTableView)
        view.addSubview(lawyerDirButton)
        view.addSubview(filterHolderView)
        view.addSubview(filterView)
        filterView.addSubview(practiceAreaButton)
        filterView.addSubview(line)
        filterView.addSubview(filterByButton)
        lawyerDirButton.addSubview(forwardButton)
        view.addSubview(noContentView)
        noContentView.addSubview(noRecordIcon)
        noContentView.addSubview(noContentLabel)
        noContentView.addSubview(instructionLabel)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = backgroundSystemColor
        
        NSLayoutConstraint.activate([
    
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -120),
            
            filterHolderView.topAnchor.constraint(equalTo: view.topAnchor, constant: 80),
            filterHolderView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            filterHolderView.heightAnchor.constraint(equalToConstant: 150),
            filterHolderView.widthAnchor.constraint(equalToConstant: 330),
            
            filterView.topAnchor.constraint(equalTo: view.topAnchor, constant: 90),
            filterView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            filterView.heightAnchor.constraint(equalToConstant: 140),
            filterView.widthAnchor.constraint(equalToConstant: 330),
            
            practiceAreaButton.topAnchor.constraint(equalTo: filterView.topAnchor, constant: 8),
            practiceAreaButton.trailingAnchor.constraint(equalTo: filterView.trailingAnchor, constant: -15),
            practiceAreaButton.leadingAnchor.constraint(equalTo:filterView.leadingAnchor, constant: 15),
            practiceAreaButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            
            line.topAnchor.constraint(equalTo: practiceAreaButton.bottomAnchor, constant: 5),
            line.leadingAnchor.constraint(equalTo: filterView.leadingAnchor, constant: 0),
            line.trailingAnchor.constraint(equalTo: filterView.trailingAnchor, constant: 0),
            line.heightAnchor.constraint(equalToConstant: 0.5),
            
            filterByButton.topAnchor.constraint(equalTo: line.bottomAnchor, constant: 5),
            filterByButton.trailingAnchor.constraint(equalTo: filterView.trailingAnchor, constant: -15),
            filterByButton.leadingAnchor.constraint(equalTo:filterView.leadingAnchor, constant: 15),
            filterByButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            
            
            location.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            location.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            location.heightAnchor.constraint(equalToConstant: 30),
            location.widthAnchor.constraint(equalToConstant: 150),
            
            changeLocationButton.topAnchor.constraint(equalTo: location.bottomAnchor, constant: 5),
            changeLocationButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            changeLocationButton.heightAnchor.constraint(equalToConstant: 50),
            changeLocationButton.widthAnchor.constraint(equalToConstant: 100),
            
            noContentView.topAnchor.constraint(equalTo: changeLocationButton.bottomAnchor, constant: 30),
            noContentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            noContentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            noContentView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
        
            noRecordIcon.centerXAnchor.constraint(equalTo: noContentView.centerXAnchor),
            noRecordIcon.topAnchor.constraint(equalTo: noContentView.topAnchor, constant: 150),
            noRecordIcon.widthAnchor.constraint(equalToConstant: 200),
            noRecordIcon.heightAnchor.constraint(equalToConstant: 200),
            
            noContentLabel.topAnchor.constraint(equalTo: noRecordIcon.bottomAnchor, constant: 20),
            noContentLabel.leadingAnchor.constraint(equalTo: noContentView.leadingAnchor, constant: 20),
            noContentLabel.trailingAnchor.constraint(equalTo:noContentView.trailingAnchor, constant: -20),
            
            instructionLabel.topAnchor.constraint(equalTo: noContentLabel.bottomAnchor, constant: 20),
            instructionLabel.leadingAnchor.constraint(equalTo: noContentView.leadingAnchor, constant: 20),
            instructionLabel.trailingAnchor.constraint(equalTo: noContentView.trailingAnchor, constant: -20),
            
            lawyerDirButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50),
            lawyerDirButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            lawyerDirButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber),
            lawyerDirButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            
            forwardButton.topAnchor.constraint(equalTo: lawyerDirButton.topAnchor, constant: 2),
            forwardButton.trailingAnchor.constraint(equalTo: lawyerDirButton.trailingAnchor, constant: -15),
            forwardButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            
        ])
        
        lawyersTableView.anchorWithConstantsToTop(changeLocationButton.bottomAnchor,
                                                     left: view.leftAnchor, bottom: view.bottomAnchor, right:  view.rightAnchor, topConstant: 15, leftConstant: leadingNumber, bottomConstant: 130, rightConstant: leadingNumber)
    }
}
