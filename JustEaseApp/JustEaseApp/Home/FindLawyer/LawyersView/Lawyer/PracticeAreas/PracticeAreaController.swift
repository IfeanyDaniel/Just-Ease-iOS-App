//
//  PracticeAreaController.swift
//  JustEaseApp
//
//  Created by iOSApp on 13/03/2023.
//

import UIKit
import FittedSheets
class PracticeAreasController: UIViewController,UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout  {
    var practiceAreaViewModel = PracticeAreaViewModel()
    var id = 0
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Major Practice Areas"
        label.textColor = AppColors.green.color
        label.font = UIFont(name: AppFonts.silkabold.font, size: 25)
        label.textAlignment = .left
        return label
    }()
   
    lazy var practiceAreasCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 15
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        cv.backgroundColor = backgroundSystemColor
        cv.dataSource = self
        cv.delegate = self
        cv.showsVerticalScrollIndicator = true
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()

    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        self.hideKeyboardWhenTappedAround()
        registercCell()
        getAllPracticeAreas()
    }
    
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
    }
    func getAllPracticeAreas() {
        Loader.shared.showLoader()
        practiceAreaViewModel.getPracticeArea {
            Loader.shared.hideLoader()
            DispatchQueue.main.async { [self] in
                self.practiceAreasCollectionView.reloadData()
            }
        }
    }
    func registercCell() {
        practiceAreasCollectionView.register(CardCollectionViewCell.self, forCellWithReuseIdentifier: CardCollectionViewCell.identifier)
    }
}

extension PracticeAreasController : SeeLawyersDelegate, CardCellDelegate {
    func goToLawyersNearMe() {
        let controller = LawyersController()
        controller.praticeAreaId = id
        navigationController?.pushViewController(controller, animated: true)
    }
    func onClickCardForwardButton(index: Int) {
        navigationController?.pushViewController(LawyersController(), animated: true)
    }
}
