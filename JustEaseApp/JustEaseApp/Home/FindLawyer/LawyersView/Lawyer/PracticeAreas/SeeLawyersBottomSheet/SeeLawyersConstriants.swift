//
//  SeeLawyersConstriants.swift
//  JustEaseApp
//
//  Created by iOSApp on 13/03/2023.
//

import UIKit
extension SeeLawyersController {
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(label)
        view.addSubview(belowlabel)
        view.addSubview(seeLawyersButton)
        view.backgroundColor =  viewStackColor
        
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 30),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            
            belowlabel.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 15),
            belowlabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            
            seeLawyersButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -70),
            seeLawyersButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber) ,
            seeLawyersButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            seeLawyersButton.heightAnchor.constraint(equalToConstant: buttonHeight)
        ])
    }
}
