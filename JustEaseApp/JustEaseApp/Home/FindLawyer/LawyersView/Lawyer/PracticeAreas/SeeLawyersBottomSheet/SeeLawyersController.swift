//
//  SeeLawyersController.swift
//  JustEaseApp
//
//  Created by iOSApp on 13/03/2023.
//

import UIKit
protocol SeeLawyersDelegate: AnyObject {
    func goToLawyersNearMe()
    
}
class SeeLawyersController: UIViewController {
    var delegate : SeeLawyersDelegate?
    var practiceAreasTitle = ""
    var practiceAreasDetailsTitle = ""
    var practiceAreasId = 0
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textColor = AppColors.green.color
        label.font = UIFont(name: AppFonts.silkabold.font, size: 18)
        label.textAlignment = .left
        return label
    }()
    lazy var belowlabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textColor = textSystemColor
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 14)
        label.textAlignment = .left
        return label
    }()
    lazy var seeLawyersButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("See Lawyers", for: .normal)
        button.addTarget(self, action: #selector(didTapOnSeeLawyers), for: .touchUpInside)
        return button
    }()
    @objc func didTapOnSeeLawyers() {
        dismiss(animated: true)
        delegate?.goToLawyersNearMe()
        Storage.saveRoutingNearByLawyerFrom(source: "PracticeAreas")
    }
    override func viewDidLoad() {
         layoutViews()
        label.text = practiceAreasTitle
        belowlabel.text = practiceAreasDetailsTitle
    }
}

