//
//  PracticeAreaCollectionvIEW.swift
//  JustEaseApp
//
//  Created by iOSApp on 13/03/2023.
//

import UIKit
import FittedSheets
extension PracticeAreasController {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        practiceAreaViewModel.numberOfRowsInSection(section: section)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = practiceAreasCollectionView.dequeueReusableCell(withReuseIdentifier: CardCollectionViewCell.identifier, for: indexPath) as? CardCollectionViewCell else { return UICollectionViewCell() }
        let practiceAreas = practiceAreaViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
        cell.index = indexPath
        cell.cardLabel.text = practiceAreas.title
        cell.cardCellDelegate = self
        cell.backgroundColor = backgroundSystemColor
        cell.layer.borderColor = AppColors.green.color.cgColor
        cell.layer.borderWidth = 0.5
        cell.layer.cornerRadius = 5
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = .zero
        cell.layer.shadowRadius = 0.2
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 50, height: 60)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let practiceAreas = practiceAreaViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
        id = practiceAreas.id
        let controller = SeeLawyersController()
        controller.delegate = self
        controller.practiceAreasId = practiceAreas.id
        controller.practiceAreasTitle = practiceAreas.title
        controller.practiceAreasDetailsTitle = practiceAreas.details
        let sheetController = SheetViewController(controller: controller, sizes: [.fixed(350), .fixed(350)])
        sheetController.blurBottomSafeArea = false
        sheetController.handleView.isHidden = true
        sheetController.topCornersRadius = 30
        present(sheetController, animated: false, completion: nil)
    }
}
