//
//  PracticeAreaConstraint.swift
//  JustEaseApp
//
//  Created by iOSApp on 13/03/2023.
//

import UIKit
extension PracticeAreasController {
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(label)
        view.addSubview(practiceAreasCollectionView)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = backgroundSystemColor
        
        NSLayoutConstraint.activate([
    
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            
        ])
        practiceAreasCollectionView.anchorWithConstantsToTop(label.bottomAnchor,
                                                     left: view.leftAnchor, bottom: view.bottomAnchor, right:  view.rightAnchor, topConstant: 50, leftConstant: 0, bottomConstant: 50, rightConstant: 0)
    }
}
