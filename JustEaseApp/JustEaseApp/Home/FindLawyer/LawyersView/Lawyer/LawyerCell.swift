//
//  LawyerCell.swift
//  JustEaseApp
//
//  Created by iOSApp on 07/03/2023.
//
import UIKit
class LawyersCell: UITableViewCell {
    static var identifier = "lawyerCellId"
    lazy var icon :  UIImageView  = {
        let profileImageView =  UIImageView()
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.layer.masksToBounds =  true
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.layer.cornerRadius = 40
        return profileImageView
    }()
    lazy var nameLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "SEN. THEODORE ORJI"
        label.textColor = textSystemColor
        return label
    }()
    lazy var addressLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Location: K50 Road 5, Ikota Shopping Complex , VGC"
        label.numberOfLines = 5
        label.textAlignment = .left
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 12)
        label.textColor = textSystemColor
        return label
    }()
   lazy var distanceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "4km away from you"
        label.numberOfLines = 1
        label.textAlignment = .left
        label.font = UIFont(name: AppFonts.silkLight.font, size: 13)
        label.textColor = AppColors.lighterGray.color
        return label
    }()
    lazy var forwardButton: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.forwardGreenButton.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
       // button.addTarget(self, action: #selector(clickContactForwardButton(_:)), for: .touchUpInside)
        return button
    }()
    lazy var locationButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "Location"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isHidden = true
       // button.addTarget(self, action: #selector(clickContactForwardButton(_:)), for: .touchUpInside)
        return button
    }()
    // MARK: - ... Adding subviews and constraints in the cell
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(icon)
        addSubview(nameLabel)
        addSubview(addressLabel)
        addSubview(distanceLabel)
        addSubview(forwardButton)
        addSubview(locationButton)
        NSLayoutConstraint.activate([
            icon.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            icon.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            icon.heightAnchor.constraint(equalToConstant: 80),
            icon.widthAnchor.constraint(equalToConstant: 80),
            
            nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 25),
            nameLabel.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: 10),
            nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            
            addressLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 10),
            addressLabel.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: 10),
            addressLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -30),
            
            distanceLabel.topAnchor.constraint(equalTo: addressLabel.bottomAnchor, constant: 10),
            distanceLabel.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: 10),
            
            forwardButton.topAnchor.constraint(equalTo: topAnchor, constant: 50),
            forwardButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            
            locationButton.topAnchor.constraint(equalTo: topAnchor, constant: 50),
            locationButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            
        ])
    }
    required init?(coder: NSCoder) {
        fatalError("init(code:) has not been implemented")
    }
}
