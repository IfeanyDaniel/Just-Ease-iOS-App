//
//  FindLawyerTableView.swift
//  JustEaseApp
//
//  Created by iOSApp on 05/03/2023.
//


import UIKit
extension FindLawyerController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return page.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.identifier, for: indexPath) as? TableViewCell else { return UITableViewCell() }
        cell.nameLabel.text = page[indexPath.row]
        cell.backgroundColor = viewStackColor
        cell.selectionStyle = .none
        return cell
    }
       
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: false)
        if page[indexPath.row] == "Find a Lawyer" {
            delegate?.goToLawyerScreen()
        } else {
             delegate?.goToPoliceScreen()
                
        }
    }
}
