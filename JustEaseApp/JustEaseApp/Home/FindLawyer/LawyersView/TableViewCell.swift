//
//  TableViewCell.swift
//  JustEaseApp
//
//  Created by iOSApp on 05/03/2023.
//

import UIKit
class TableViewCell: UITableViewCell {
    static var identifier = "cell"
    lazy var nameLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.font = UIFont(name: AppFonts.silkaMedium.font ,size: 14)
        label.text = "Find a Lawyer"
        label.textColor = textSystemColor
        return label
    }()
    lazy var forwardButton: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.deFaultForwardButton.image, for: .normal)
        button.tintColor = textSystemColor
        button.translatesAutoresizingMaskIntoConstraints = false
       // button.addTarget(self, action: #selector(clickContactForwardButton(_:)), for: .touchUpInside)
        return button
    }()
    // MARK: - ... Adding subviews and constraints in the cell
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(nameLabel)
        addSubview(forwardButton)
        
        NSLayoutConstraint.activate([
            nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 17),
            nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -17),
            
            forwardButton.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            forwardButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
            
        ])
    }
    required init?(coder: NSCoder) {
        fatalError("init(code:) has not been implemented")
    }
}
