//
//  LawyersDirectoryTableView.swift
//  JustEaseApp
//
//  Created by iOSApp on 07/03/2023.
//

import UIKit
extension LawyersDirectoryController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lawyersDirectory.numberOfRowsInSection().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: LawyersCell.identifier, for: indexPath) as?LawyersCell else { return UITableViewCell() }
        let lawyersDirectory = lawyersDirectory.cellForRowsAt(indexPath: indexPath)[indexPath.row]
        cell.addressLabel.text = lawyersDirectory.location
        cell.nameLabel.text = lawyersDirectory.name
        cell.distanceLabel.text = "\(Int(lawyersDirectory.distance))km away from you"
        if lawyersDirectory.image != "" {
            cell.icon.kf.setImage(with: URL(string: lawyersDirectory.image))
        } else {
            cell.icon.backgroundColor =  AppColors.green.color
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView,
                   viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let lawyersDirectory = lawyersDirectory.cellForRowsAt(indexPath: indexPath)[indexPath.row]
        passInfo(details: lawyersDirectory)
    }
    func passInfo(details: Lawyers) {
        let controller = RepsDetailsController()
        controller.practiceaAreaView.isHidden = false
        controller.positionView.isHidden = true
        controller.senetorialView.isHidden = true
        controller.originView.isHidden = true
        controller.repsName = details.name
        controller.repsEmail = details.email
        controller.phoneNumber = details.phoneNumber
        controller.practiceAreas = details.practiceAreas
        if details.image != "" {
            controller.repsImage = details.image
        } else {
            controller.profileIcon.backgroundColor = AppColors.green.color
        }
        if details.website == "" {
            controller.websiteView.isHidden = true
        } else {
            controller.websiteStack.isHidden = false
            controller.repsWebsite = details.website
        }
        if details.location == "" {
            controller.addressView.isHidden = true
        } else {
            controller.addressView.isHidden = false
            controller.repsAddress = details.location
        }
        if details.about_lawyer == "" {
            controller.aboutView.isHidden = true
        } else {
            controller.aboutView.isHidden = false
            controller.aboutReps = details.about_lawyer
        }
        navigationController?.pushViewController(controller, animated: true)
    }
}
