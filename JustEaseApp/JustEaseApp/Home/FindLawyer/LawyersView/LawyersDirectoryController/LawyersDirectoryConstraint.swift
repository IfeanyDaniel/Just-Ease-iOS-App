//
//  LawyersDirectoryConstriant.swift
//  JustEaseApp
//
//  Created by iOSApp on 07/03/2023.
//

import UIKit
extension LawyersDirectoryController  {
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(lawyersTableView)
        view.addSubview(noContentView)
        noContentView.addSubview(noRecordIcon)
        noContentView.addSubview(noContentLabel)
        noContentView.addSubview(instructionLabel)
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = backgroundSystemColor
        NSLayoutConstraint.activate([
            
        noContentView.topAnchor.constraint(equalTo: view.topAnchor, constant: 30),
        noContentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
        noContentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
        noContentView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
    
        noRecordIcon.centerXAnchor.constraint(equalTo: noContentView.centerXAnchor),
        noRecordIcon.topAnchor.constraint(equalTo: noContentView.topAnchor, constant: 150),
        noRecordIcon.widthAnchor.constraint(equalToConstant: 200),
        noRecordIcon.heightAnchor.constraint(equalToConstant: 200),
        
        noContentLabel.topAnchor.constraint(equalTo: noRecordIcon.bottomAnchor, constant: 20),
        noContentLabel.leadingAnchor.constraint(equalTo: noContentView.leadingAnchor, constant: 20),
        noContentLabel.trailingAnchor.constraint(equalTo:noContentView.trailingAnchor, constant: -20),
       
        instructionLabel.topAnchor.constraint(equalTo: noContentLabel.bottomAnchor, constant: 20),
        instructionLabel.leadingAnchor.constraint(equalTo: noContentView.leadingAnchor, constant: 20),
        instructionLabel.trailingAnchor.constraint(equalTo: noContentView.trailingAnchor, constant: -20),
        ])
        
        lawyersTableView.anchorWithConstantsToTop(view.topAnchor,
                                                     left: view.leftAnchor, bottom: view.bottomAnchor, right:  view.rightAnchor, topConstant: 15, leftConstant: leadingNumber, bottomConstant: -50, rightConstant: leadingNumber)
    }
}
