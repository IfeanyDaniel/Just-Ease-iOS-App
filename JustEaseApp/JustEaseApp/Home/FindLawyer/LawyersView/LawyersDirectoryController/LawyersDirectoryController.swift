//
//  LawyersDirectoryConstriants.swift
//  JustEaseApp
//
//  Created by iOSApp on 07/03/2023.
//

import UIKit
import GoogleMaps
import CoreLocation
import MapKit
class LawyersDirectoryController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    var lawyersDirectory = LawyersDirectoryViewModel()
    lazy var lawyersTableView : UITableView = {
        let table = UITableView()
        table.backgroundColor = backgroundSystemColor
        table.rowHeight = 120
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.sizeToFit()
        table.delegate = self
        table.dataSource = self
        table.separatorColor = AppColors.green.color
        return table
    }()
    lazy var noContentView : UIView = {
        let content = UIView()
        content.backgroundColor = backgroundSystemColor
        content.isHidden = true
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    
    lazy var noRecordIcon: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.noBookmarkIcon.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var noContentLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Content not available"
        label.textAlignment = .center
        label.font = UIFont(name: AppFonts.silkabold.font, size: 14)
        return label
    }()
    lazy var instructionLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textColor = AppColors.lightNavyBlue.color
        label.text = "Please check back for content."
        label.textAlignment = .center
        return label
    }()
    @objc func didTapOnLawyerDirectory() {
        navigationController?.pushViewController(RepsDetailsController(), animated: true)
    }
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        registercCell()
        getLawyersDirectory()
    }
    func showNoContentView() {
        noContentView.isHidden = false
        lawyersTableView.isHidden = true
    }
    func getLawyersDirectory() {
        guard let currentLocation = locManager.location else {
            return
        }
        Loader.shared.showLoader()
        lawyersDirectory.getLawyersDirectory(long: "\(currentLocation.coordinate.longitude)", lat: "\(currentLocation.coordinate.latitude)", page: 1, recordPerPage: Storage.getLawyersDirectoryTotal()) {
            Loader.shared.hideLoader()
            if self.lawyersDirectory.lawyers.count == 0 {
                self.showNoContentView()
            } else {
                self.lawyersTableView.reloadData()
            }
        }
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
    }
   
    func registercCell() {
       lawyersTableView.register(LawyersCell.self, forCellReuseIdentifier: LawyersCell.identifier)

    }
}

