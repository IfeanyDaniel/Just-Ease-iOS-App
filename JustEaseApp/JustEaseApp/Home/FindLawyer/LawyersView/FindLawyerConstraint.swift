//
//  FindLawyerConstraint.swift
//  JustEaseApp
//
//  Created by iOSApp on 05/03/2023.
//

import UIKit
extension FindLawyerController  {
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(tableView)
        view.backgroundColor =  viewStackColor
        tableView.anchorWithConstantsToTop(view.topAnchor,
                                                     left: view.leftAnchor, bottom: view.bottomAnchor, right:  view.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
    }
}
