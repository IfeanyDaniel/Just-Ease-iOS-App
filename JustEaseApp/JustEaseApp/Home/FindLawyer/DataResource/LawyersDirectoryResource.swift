//
//  LawyersDirectoryResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/3/23.
//

import Foundation
struct LawyersDirectoryResource {
    func getLawyersDirectoryResponse(latitude: String, longitude: String,page: Int,recordPerPage: Int, completionHandler: @escaping (_ result: NearByLawyerResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let lawyersDirectoryUrl = ApiEndpoints.lawywersDirectoryEndPoint + "?longitude=\(longitude)&latitude=\(latitude)&page=\(page)&order_by=a-zrecords_per_page=\(recordPerPage)"
        let url = URL(string: lawyersDirectoryUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType: NearByLawyerResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}
