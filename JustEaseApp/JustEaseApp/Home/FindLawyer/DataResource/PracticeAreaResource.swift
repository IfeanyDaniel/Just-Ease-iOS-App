//
//  PracticeAreaResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/10/23.
//

import Foundation
struct PracticeAreaResource {
    func getPracticeAreaResponse(completionHandler: @escaping (_ result: PracticeAreaResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let  practiceAreaUrl = ApiEndpoints.practiceAreasEndPoint
        
        let url = URL(string: practiceAreaUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType: PracticeAreaResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}
