//
//  NearbyLawyersResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/9/23.
//

import Foundation
struct NearbyLawyersResource {
    func getNearbyLawyersResponse(latitude: String, longitude: String, completionHandler: @escaping (_ result: NearByLawyerResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let nearByLawyersUrl = ApiEndpoints.nearLawywersEndPoint + "?longitude=\(longitude)&latitude=\(latitude)"
        
        let url = URL(string: nearByLawyersUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType: NearByLawyerResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}
