//
//  FilterByResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/1/23.
//

import Foundation
struct FilterByKeywordResource {
    func getNearbyLawyersResponse(filterByKeyword: String,latitude: String, longitude: String, recordPerPage: Int, page: Int, completionHandler: @escaping (_ result: NearByLawyerResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let nearByLawyersUrl = ApiEndpoints.nearLawywersEndPoint + "?lawyer_type=\(filterByKeyword)&longitude=\(longitude)&latitude=\(latitude)&order_by=a-z&records_per_page=\(recordPerPage)&page=\(page)"
        
        let url = URL(string: nearByLawyersUrl)!
        print("??????\(url)")
        do {
            httpUtility.getAPIData(requestURL: url, resultType: NearByLawyerResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}
