//
//  FilterByPracticeAreaResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/10/23.
//
import Foundation
struct FilterByPracticeAreaResource {
    func getByPracticeAreaResponse(practiceId: Int, latitude: String, longitude: String, completionHandler: @escaping (_ result: NearByLawyerResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let filterByPracticeAreaUrl = ApiEndpoints.filterbypracticeAreasEndPoint + "?practice_area_id=\(practiceId)&longitude=\(longitude)&latitude=\(latitude)&order_by=a-z"
      
        let url = URL(string: filterByPracticeAreaUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType: NearByLawyerResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}

