//
//  PracticeAreaResponse.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/10/23.
//

import Foundation
struct PracticeAreaResponse: Decodable {
    let status: Int
    let message: String
    let data: [PracticeAreaData]
}
struct PracticeAreaData: Decodable {
    let id: Int
    let title: String
    let detail: String
}
