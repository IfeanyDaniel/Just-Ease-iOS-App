//
//  NearbyLawyersResponse.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/9/23.
//
import Foundation
struct NearByLawyerResponse: Decodable {
    let status: Int
    let message: String
    let data: LawyersNearByData
}
struct LawyersNearByData: Decodable {
    let current_page: Int
    let data: [LawyersNearByInfo]
    let total : Int
}
struct LawyersNearByInfo: Decodable {
    let id: Int
    let primary_location_id: Int
    let status:  String?
    let website: String?
    let email: String?
    let title_id:  Int?
    let other_title: String?
    let first_name: String?
    let last_name: String?
    let distance: Float?
    let phone_number: String?
    let about_lawyer: String?
    let updated_at: String?
    let name: String
    let image: ImageData?
    let practice_areas: [LawyersPracticeAreas]
    let primary_location: LawyerPrimaryLocation
}
struct ImageData: Decodable {
    let id : Int
    let file_path: String?
}
struct LawyersPracticeAreas: Decodable {
    let id: Int
    let title: String
    let detail: String
}
struct LawyerPrimaryLocation: Decodable {
    let id: Int
    let street_address: String?
    let city: String?
    let longitude: String
    let latitude: String
    let state: String?
}
