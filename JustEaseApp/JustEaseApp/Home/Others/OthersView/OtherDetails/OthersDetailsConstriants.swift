//
//  OthersDetailsConstriants.swift
//  JustEaseApp
//
//  Created by iOSApp on 24/02/2023.
//

import UIKit
extension OthersDetailsController {
 
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(label)
        view.addSubview(searchTextField)
        view.addSubview(othersTableView)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = backgroundSystemColor
        
        NSLayoutConstraint.activate([
    
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            
            searchTextField.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 20),
            searchTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber),
            searchTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            searchTextField.heightAnchor.constraint(equalToConstant: 65),
            
        ])
        othersTableView.anchorWithConstantsToTop(searchTextField.bottomAnchor,
                                                     left: view.leftAnchor, bottom: view.bottomAnchor, right:  view.rightAnchor, topConstant: 20, leftConstant: 20, bottomConstant: 50, rightConstant: 20)
    }
}
