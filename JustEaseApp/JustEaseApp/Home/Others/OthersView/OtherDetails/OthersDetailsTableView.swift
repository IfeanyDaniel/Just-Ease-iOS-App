//
//  OthersDetailsCollectionView.swift
//  JustEaseApp
//
//  Created by iOSApp on 24/02/2023.
//
//
import UIKit
extension OthersDetailsController{
    func numberOfSections(in tableView: UITableView) -> Int {
        othersDetailsFilterData.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if othersDetailsFilterData[section].isOpened {
            return othersDetailsFilterData[section].option.count + 1
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: DetailsCell.identifier, for: indexPath) as? DetailsCell else { return UITableViewCell() }
            let others = othersDetailsFilterData[indexPath.section]
            cell.cardLabel.text = others.title
            cell.numberOfItemsLabel.text = "\(others.numberOfQuestions) Subitems"
            cell.selectionStyle = .none
            cell.index = indexPath
            cell.detailsCellDelegate = self
            cell.backgroundColor = backgroundSystemColor
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: SubCell.identifier, for: indexPath) as? SubCell else { return UITableViewCell() }
            cell.cardLabel.text = othersDetailsFilterData[indexPath.section].title
            cell.selectionStyle = .none
            cell.index = indexPath
            cell.subCellDelegate = self
            cell.backgroundColor = backgroundSystemColor
            cell.cardLabel.text = othersDetailsViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.section].option[indexPath.row - 1].title
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 {
            othersDetailsFilterData[indexPath.section].isOpened = !othersDetailsFilterData[indexPath.section].isOpened
            tableView.reloadSections([indexPath.section], with: .none)
        } else {
            let othersSub = othersDetailsFilterData[indexPath.section].option[indexPath.row - 1]
            let controller = InfoController()
            controller.subTitle = othersSub.title
            controller.subId = othersSub.id
            navigationController?.pushViewController(controller, animated: true)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        50
    }
    
}

