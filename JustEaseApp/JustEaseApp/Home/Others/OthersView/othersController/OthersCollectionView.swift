//
//  OthersCollectionView.swift
//  JustEaseApp
//
//  Created by iOSApp on 24/02/2023.
//
import UIKit
extension OthersController {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        othersFilterData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = othersCollectionView.dequeueReusableCell(withReuseIdentifier: CardCollectionViewCell.identifier, for: indexPath) as? CardCollectionViewCell else { return UICollectionViewCell() }
        let others = othersFilterData[indexPath.row]
        cell.index = indexPath
        cell.cardCellDelegate = self
        cell.cardLabel.text = others.title
        cell.backgroundColor = backgroundSystemColor
        cell.layer.borderColor = AppColors.green.color.cgColor
        cell.layer.borderWidth = 0.5
        cell.layer.cornerRadius = 5
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = .zero
        cell.layer.shadowRadius = 0.2
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 50, height: 60)
    }
    
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         let others = othersFilterData[indexPath.row]
         let controller = OthersDetailsController()
         controller.othersId = others.id
         controller.othersTitle = others.title
         navigationController?.pushViewController(controller, animated: true)
     }
}
