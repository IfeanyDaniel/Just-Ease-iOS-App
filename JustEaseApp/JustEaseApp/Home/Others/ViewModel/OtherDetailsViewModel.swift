//
//  OtherDetailsViewmodel.swift
//  JustEaseApp
//
//  Created by iOSApp on 30/03/2023.
//
import Foundation
class OthersDetailsViewModel {
    var othersDetails: [Section] = []
    var options: [Option] = []
    var questionId = 0
    var questionTitle = ""
    func getOthersDetails(othersId: Int,totalOthersRecord: Int, completion: @escaping () -> Void) {
        let othersDetailsResource = OthersDetailsResource()
        UserDefaults.standard.removeObject(forKey: Storage.allOtherDetailsKey)
        othersDetailsResource.getOthersDetails(id: othersId, totalOthersRecord: totalOthersRecord) { getOthersDetailsApiResponse in
            DispatchQueue.main.async { [self] in
                let totalNumber =  getOthersDetailsApiResponse?.data.total ?? 0
                let othersDetailsData = getOthersDetailsApiResponse?.data.data
                Storage.saveOthersDetailsTotalRecord(number: totalNumber)
                othersDetails.removeAll()
                for index in 0..<(othersDetailsData?.count ?? 0)  {
                    let title = othersDetailsData?[index].title ?? ""
                    let questionData = othersDetailsData?[index].questions
                    let countOfQuestions = questionData?.count ?? 0
                    options.removeAll()
                    for index in 0..<(questionData?.count ?? 0)  {
                        questionId = questionData?[index].id ?? 0
                        questionTitle = questionData?[index].title ?? ""
                        options.append(Option(id: questionId, title: questionTitle))
                    }
                    othersDetails.append(Section(title: title, numberOfQuestions: countOfQuestions, option: options.compactMap({ return $0
                    })))
                    Storage.saveOthersDetails(textInfo: othersDetails)
                }
                completion()
            }
        }
        
    }
    func numberOfRowsInSection() -> [Section]  {
        return othersDetails
    }
    func cellForRowsAt(indexPath: IndexPath) -> [Section] {
        return othersDetails
    }
}
