//
//  OthersViewmodel.swift
//  JustEaseApp
//
//  Created by iOSApp on 30/03/2023.
//

import Foundation
struct Others: Codable {
    var id: Int
    var title: String
}

class OthersViewModel {
    var others: [Others] = []
    func getOthers(numberPerPage: Int, completion: @escaping () -> Void) {
        let othersResource = OthersResource()
        othersResource.getOthers(recordPerPage: numberPerPage) { getOthersApiResponse in
            DispatchQueue.main.async { [self] in
                let totalNumber = getOthersApiResponse?.data.total
                let othersData = getOthersApiResponse?.data.data
                Storage.saveOtherTotalRecord(number: totalNumber ?? 0)
                others.removeAll()
                for index in 0..<(othersData?.count ?? 0) {
                    let otherId = othersData?[index].id ?? 0
                    let otherTitle = othersData?[index].title ?? ""
                    others.append(Others(id: otherId,title: otherTitle))
                    Storage.saveOthers(textInfo: others)
                }
                completion()
            }
        }
    }
    func numberOfRowsInSection(section: Int) -> Int {
        return  others.count
    }
    func cellForRowsAt(indexPath: IndexPath) -> [Others] {
        return others
    }
}
