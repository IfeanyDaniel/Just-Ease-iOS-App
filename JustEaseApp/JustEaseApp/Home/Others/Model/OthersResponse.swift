//
//  OthersResponse.swift
//  JustEaseApp
//
//  Created by iOSApp on 30/03/2023.
//

import Foundation
struct OthersResponse: Decodable {
    let status: Int
    let message: String
    let data: OthersData
}
struct  OthersData: Decodable {
    let current_page: Int
    let data: [ OthersInfo]
    let total : Int
}
struct  OthersInfo: Decodable {
    let id: Int
    let title: String
}
