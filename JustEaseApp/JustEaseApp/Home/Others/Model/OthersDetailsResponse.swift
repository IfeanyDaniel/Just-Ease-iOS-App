//
//  OthersDetailsResponse.swift
//  JustEaseApp
//
//  Created by iOSApp on 30/03/2023.
//
import Foundation
struct OthersDetailsResponse: Decodable {
    let status: Int
    let message: String
    let data: OthersDetailsData
}
struct OthersDetailsData: Decodable {
    let current_page: Int
    let data: [OthersDataInfo]
    let total : Int
}
struct OthersDataInfo: Decodable {
    let id: Int
    let title: String
    let questions: [OthersQuestions]
}
struct OthersQuestions: Decodable {
    let id: Int
    let title: String
}
