//
//  OthersResource.swift
//  JustEaseApp
//
//  Created by iOSApp on 30/03/2023.
//

import Foundation
struct OthersResource {
    func getOthers(recordPerPage: Int, completionHandler: @escaping (_ result: OthersResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let othersUrl = ApiEndpoints.othersEndPoint + "?records_per_page=\(recordPerPage)"
        
        let url = URL(string: othersUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType:  OthersResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}
