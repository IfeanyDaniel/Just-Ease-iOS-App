//
//  othersDetailsResource.swift
//  JustEaseApp
//
//  Created by iOSApp on 30/03/2023.
//

import Foundation
struct OthersDetailsResource {
    func getOthersDetails(id: Int,totalOthersRecord: Int, completionHandler: @escaping (_ result: OthersDetailsResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let othersDetailsUrl = ApiEndpoints.othersDetailsEndPoint + "/\(id)/topics?records_per_page=\(totalOthersRecord)"
        let url = URL(string: othersDetailsUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType:  OthersDetailsResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}
