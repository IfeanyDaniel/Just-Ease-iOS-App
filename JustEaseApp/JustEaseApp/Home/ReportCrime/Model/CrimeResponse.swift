//
//  CrimeCategoryResponse.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/3/23.
//

import Foundation
struct CrimeResponse: Decodable {
    let status: Int
    let message: String
 //   let data: [CrimeData]
}
//struct CrimeData: Decodable {
//    let report_crime_id: Int
//    let user_id: String
//    let report_crime_address: String
//    let report_crime_status: String
//    let crime_details: String
//    let anonymous_status: String
//    let alias_name: String?
//    let longitude: String
//    let latitude: String
//}

