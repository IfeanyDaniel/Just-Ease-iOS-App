//
//  CrimeCategoryModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/3/23.
//

import Foundation
struct CrimeCategoryResponse: Decodable {
    let status: Int
    let message: String
    let data: CrimeCategoryData
}
struct CrimeCategoryData: Decodable {
    let current_page: Int
    let data: [CrimeCategoryInfo]
    let total : Int
}
struct CrimeCategoryInfo: Decodable {
    let id: Int
    let name: String
}
