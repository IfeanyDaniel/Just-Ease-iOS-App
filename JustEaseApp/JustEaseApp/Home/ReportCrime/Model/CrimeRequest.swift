//
//  CrimeRequest.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/3/23.
//

import Foundation
struct CrimeRequest: Encodable {
    let report_crime_category_id: Int
    let report_crime_address: String
    let report_crime_status: String
    let crime_details: String
    let alias_name: String?
    let user_id: Int
    let latitude: String
    let longitude: String
    let postal_code: String?
    let country: String
    let admin_area: String?
    let sub_admin_area: String
    let locality: String?
    let sub_locality: String
    let anonymous_status: String
}
