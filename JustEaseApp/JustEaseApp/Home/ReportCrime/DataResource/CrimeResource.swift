//
//  CrimeResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/6/23.
//

import Foundation
struct CrimeResource {
    func getResponse(crimeRequest: CrimeRequest, completionHandler: @escaping (_ result: CrimeResponse?,_ statusCode: Int) -> Void) {
        let crimeURL = URL(string: ApiEndpoints.reportCrimeEndPoint)!
        let httpUtility = HTTPUtility()
        do {
            let crimePostBody = try JSONEncoder().encode(crimeRequest)
            httpUtility.postAuthorizationResponse(requestUrl: crimeURL, requestBody: crimePostBody, resultType: CrimeResponse.self) { crimeApiResponse, statusCode in
                completionHandler(crimeApiResponse,statusCode)
            }
        } catch let error {
            debugPrint(error)
        }
    }
}
