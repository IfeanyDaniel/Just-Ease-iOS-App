//
//  CrimeCategoryDataResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/3/23.
//

import Foundation
struct CrimeCategoryResource {
    func getCrimeCategoryResponse(completionHandler: @escaping (_ result: CrimeCategoryResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let crimeCategoryUrl = ApiEndpoints.crimeCategoryEndPoint
        print("<<<\(crimeCategoryUrl)")
        let url = URL(string: crimeCategoryUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType:  CrimeCategoryResponse.self) { result in
                completionHandler(result)
                print("<<<\(result)")
            }
        }
    }
}

