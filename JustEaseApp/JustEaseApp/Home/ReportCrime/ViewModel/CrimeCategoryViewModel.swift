//
//  CrimeCategoryViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/3/23.
//
protocol GetAllCrimesCategoryDelegate {
    func didReceiveGetCrimesCategoryResponse(getAllBankResponse: CrimeCategoryResponse?)
}

import Foundation
struct CrimesCategory: Codable {
    var id: Int
    var name: String
}
class CrimeCategoryViewModel {
    var delegate: GetAllCrimesCategoryDelegate?
    var crimesCategory :[CrimesCategory] = []
    func getCrimeCategory(completion: @escaping () -> Void) {
        let crimeCategoryResource = CrimeCategoryResource()
        crimeCategoryResource.getCrimeCategoryResponse { crimeApiResponse in
            DispatchQueue.main.async { [self] in
                let crimes = crimeApiResponse?.data.data
                crimesCategory.removeAll()
                for index in 0..<(crimes?.count ?? 0) {
                    let crimeId = crimes?[index].id ?? 0
                    let crimeNames = crimes?[index].name ?? ""
                    crimesCategory.append(CrimesCategory(id: crimeId, name: crimeNames))
                    Storage.saveCrimeCategory(crime: crimesCategory)
                }
                completion()
            }
        }
    }
}

