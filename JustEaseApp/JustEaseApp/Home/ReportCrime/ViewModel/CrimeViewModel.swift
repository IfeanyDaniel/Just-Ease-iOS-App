//
//  CrimeViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/6/23.
//

import Foundation
protocol ReportCrimeViewModelDelegate {
    func didReceiveReportCrimeResponse(reportCrimeResponse: CrimeResponse?,_ statusCode: Int)
}
class ReportCrimeViewModel {
    var delegate: ReportCrimeViewModelDelegate?
    func getReportCrimeResponse(crimeRequest: CrimeRequest) {
        let reportCrimeResource = CrimeResource()
        reportCrimeResource.getResponse(crimeRequest: crimeRequest) { crimeApiResponse, statusCode in
            if statusCode ==  200 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveReportCrimeResponse(reportCrimeResponse: crimeApiResponse, statusCode)
                }
            }
            if statusCode ==  400 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveReportCrimeResponse(reportCrimeResponse: crimeApiResponse, statusCode)
                }
            }
            if statusCode ==  500 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveReportCrimeResponse(reportCrimeResponse: crimeApiResponse, statusCode)
                }
            }
            if statusCode ==  422 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveReportCrimeResponse(reportCrimeResponse: crimeApiResponse, statusCode)
                }
            }
        }
    }
}
