//
//  CrimeReportSuccess.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/4/23.
//

import UIKit
import FittedSheets

class CrimeSuccessAlert: UIViewController {
    var delegate: ReportDelegate?
    let gif = UIImage.gifImageWithName("check")
    lazy var alertIcon: UIImageView = {
        var imageView = UIImageView()
        imageView = UIImageView(image: gif)
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 50
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    lazy var labelBelowIconButton: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 5
        label.text = "Crime Reported"
        label.textAlignment = .center
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 16)
        label.textColor = textSystemColor
        return label
    }()
    lazy var statement: UILabel = {
        let label = UILabel.labelTextDesign()
        label.font = UIFont(name: AppFonts.silkLight.font ,size: 14)
        label.textColor = textSystemColor
        label.textAlignment = .center
        label.text = "Thank you! the crime has been reported successfully. We will also update others to be aware and be careful"
        return label
    }()
    lazy var closeButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Done", for: .normal)
        button.backgroundColor = AppColors.green.color
        button.setTitleColor(AppColors.white.color, for: .normal)
        button.addTarget(self, action: #selector(didTapOnClose), for: .touchUpInside)
        return button
    }()
   
    @objc func didTapOnClose() {
        dismiss(animated: false){
            self.delegate?.popController()
        }
    }
    @objc func didTapOnBackButton(){
        dismiss(animated: false)
    }
    
    override func viewDidLoad() {
         layoutViews()
    }
    
}

