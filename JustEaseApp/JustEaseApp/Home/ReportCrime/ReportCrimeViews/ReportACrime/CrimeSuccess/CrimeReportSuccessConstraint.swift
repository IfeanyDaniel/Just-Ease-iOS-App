//
//  CrimeReportSuccessConstraint.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/4/23.
//
import UIKit
extension CrimeSuccessAlert  {
    //MARK: - Layout subviews
    func layoutViews() {
        view.backgroundColor =  viewStackColor
        view.addSubview(alertIcon)
        view.addSubview(labelBelowIconButton)
        view.addSubview(closeButton)
        view.addSubview(statement)
        NSLayoutConstraint.activate([
            alertIcon.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            alertIcon.topAnchor.constraint(equalTo: view.topAnchor, constant: 50),
            alertIcon.widthAnchor.constraint(equalToConstant: 100),
            alertIcon.heightAnchor.constraint(equalToConstant: 100),
            
            labelBelowIconButton.topAnchor.constraint(equalTo: alertIcon.bottomAnchor, constant: 20),
            labelBelowIconButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50),
            labelBelowIconButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50),
            
            statement.topAnchor.constraint(equalTo: labelBelowIconButton.bottomAnchor, constant: 10),
            statement.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
            statement.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30),
            
            closeButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20),
            closeButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
            closeButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30),
            closeButton.heightAnchor.constraint(equalToConstant: 55),
            
            ])
    }
}
