//
//  CrimeController.swift
//  JustEaseApp
//
//  Created by iOSApp on 07/03/2023.
//

import UIKit
class CrimeController: UIViewController ,UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate  {
    var delegate: CrimeDelegate?
    var crimesFilterData = [CrimesCategory]()
    var selectedCrime : String = ""
    
    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Cancel", for: .normal)
        button.titleLabel?.font = UIFont(name: AppFonts.silkabold.font, size: 14)
        button.addTarget(self, action: #selector(didTapOnCancel), for: .touchUpInside)
        button.setTitleColor(textSystemColor, for: .normal)
        return button
    }()
    lazy var crimeSearchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.placeholder = " Search crime"
        searchBar.sizeToFit()
        searchBar.isTranslucent = false
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.backgroundColor = textFieldColor
        return searchBar
    }()
    lazy var crimeTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.isUserInteractionEnabled = true
        tableView.rowHeight = 60
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = backgroundSystemColor
        return tableView
    }()
    @objc func didTapOnCancel() {
        dismiss(animated: true)
    }
    func showDone() {
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
          //create left side empty space so that done button set on right side
          let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dropKeyboard))
          toolbar.setItems([flexSpace, doneBtn], animated: false)
          toolbar.sizeToFit()
          //setting toolbar as inputAccessoryView
          self.crimeSearchBar.inputAccessoryView = toolbar
    }
    
    @objc func dropKeyboard() {
        view.endEditing(true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = backgroundSystemColor
        layoutView()
        showDone()
        hideKeyboardWhenTappedAround()
        crimesFilterData = Storage.getCrimeCategory()
        print(crimesFilterData)
        crimeTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        crimeSearchBar.delegate = self
    }
    //-----------------------------------------------------------------------------
    // MARK: - SEARCH BAR
    //----------------------------------------------------------------------------
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        crimesFilterData = []
        if crimeSearchBar.text == "" {
            crimesFilterData = Storage.getCrimeCategory()
        }
        for word in Storage.getCrimeCategory() {
            if word.name.contains(crimeSearchBar.text!) {
                crimesFilterData.append(CrimesCategory(id: word.id, name: word.name))
            }
        }
        self.crimeTableView.reloadData()
    }
}
