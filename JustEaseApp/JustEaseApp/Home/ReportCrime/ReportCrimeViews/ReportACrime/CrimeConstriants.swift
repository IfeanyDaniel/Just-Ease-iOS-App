//
//  CrimeConstriants.swift
//  JustEaseApp
//
//  Created by iOSApp on 07/03/2023.
//

import UIKit
extension CrimeController {
    func layoutView(){
        view.backgroundColor = backgroundSystemColor
        view.addSubview(crimeSearchBar)
        view.addSubview(cancelButton)
        view.addSubview(crimeTableView)
        NSLayoutConstraint.activate([
            cancelButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 20),
            cancelButton.leadingAnchor.constraint(equalTo: crimeSearchBar.trailingAnchor, constant: 5),
            cancelButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            
            crimeSearchBar.topAnchor.constraint(equalTo: view.topAnchor, constant: 10),
            crimeSearchBar.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            crimeSearchBar.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -80),
            
        ])
       crimeTableView.anchorWithConstantsToTop(crimeSearchBar.bottomAnchor,
                                               left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 20)
    }
}
