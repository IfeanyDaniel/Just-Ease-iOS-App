//
//  ReportCrimeConstraints.swift
//  JustEaseApp
//
//  Created by iOSApp on 07/03/2023.
//

import UIKit
extension ReportCrimeController {
    //MARK: - Layout subviews for the scroll views
    func setupScrollView() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
        ])
    }
    //MARK: - Layout subviews
    func layoutViews() {
    
        contentView.addSubview(locationTextField)
        contentView.addSubview(locationButton)
        contentView.addSubview(photoPixLayerStack)
        pictureView.addSubview(photoLayer)
        pictureView.addSubview(firstLabel)
        pictureView.addSubview(uploadIcon)
        pictureView.addSubview(tapLabel)
        
        contentView.addSubview(categoryButton)
        contentView.addSubview(categoryDownIcon)
        contentView.addSubview(crimeTextField)
        contentView.addSubview(crimeDownIcon)
        contentView.addSubview(happeningTextField)
        contentView.addSubview(aliasTextField)
        contentView.addSubview(reportButton)
        
       
        self.navigationItem.setHidesBackButton(true, animated: true)
        view.backgroundColor = .systemBackground
        
        NSLayoutConstraint.activate([
            
            locationButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
            locationButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leadingNumber),
            locationButton.widthAnchor.constraint(equalToConstant: 40),
            locationButton.heightAnchor.constraint(equalToConstant: textFieldHeight + 10),
            
            locationTextField.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
            locationTextField.leadingAnchor.constraint(equalTo: locationButton.trailingAnchor, constant: 0),
            locationTextField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: trailingNumber),
            locationTextField.heightAnchor.constraint(equalToConstant: textFieldHeight + 10),
            
            photoPixLayerStack.topAnchor.constraint(equalTo: locationTextField.bottomAnchor, constant: 30),
            photoPixLayerStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leadingNumber),
            photoPixLayerStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: trailingNumber),
           // photoPixLayerStack.heightAnchor.constraint(equalToConstant: CGFloat(200 + (60 * imageArray.count))),
            
            photoLayer.topAnchor.constraint(equalTo: pictureView.topAnchor, constant: 0),
            photoLayer.leadingAnchor.constraint(equalTo: pictureView.leadingAnchor, constant: 0),
            photoLayer.trailingAnchor.constraint(equalTo: pictureView.trailingAnchor, constant: 0),
            photoLayer.bottomAnchor.constraint(equalTo: pictureView.bottomAnchor, constant: 0),
            
            firstLabel.topAnchor.constraint(equalTo: photoLayer.topAnchor, constant: 5),
            firstLabel.leadingAnchor.constraint(equalTo: photoLayer.leadingAnchor, constant: 20),
            
            uploadIcon.centerXAnchor.constraint(equalTo: photoLayer.centerXAnchor),
            uploadIcon.topAnchor.constraint(equalTo: photoLayer.topAnchor, constant: 70),
            uploadIcon.widthAnchor.constraint(equalToConstant: 25),
            uploadIcon.heightAnchor.constraint(equalToConstant: 25),
            
            tapLabel.centerXAnchor.constraint(equalTo: photoLayer.centerXAnchor),
            tapLabel.topAnchor.constraint(equalTo: uploadIcon.bottomAnchor, constant: 5),
            tapLabel.leadingAnchor.constraint(equalTo: photoLayer.leadingAnchor, constant: 10),
            tapLabel.trailingAnchor.constraint(equalTo: photoLayer.trailingAnchor, constant: -10),
            
            categoryButton.topAnchor.constraint(equalTo: photoPixLayerStack.bottomAnchor, constant: 15),
            categoryButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leadingNumber),
            categoryButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: trailingNumber),
            categoryButton.heightAnchor.constraint(equalToConstant: textFieldHeight),
            
            categoryDownIcon.topAnchor.constraint(equalTo: categoryButton.topAnchor, constant: 20),
            categoryDownIcon.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -40),
            
            crimeTextField.topAnchor.constraint(equalTo: categoryButton.bottomAnchor, constant: 15),
            crimeTextField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leadingNumber),
            crimeTextField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: trailingNumber),
            crimeTextField.heightAnchor.constraint(equalToConstant: textFieldHeight),
            
            crimeDownIcon.topAnchor.constraint(equalTo: crimeTextField.topAnchor, constant: 20),
            crimeDownIcon.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -40),
            
            happeningTextField.topAnchor.constraint(equalTo: crimeTextField.bottomAnchor, constant: 15),
            happeningTextField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leadingNumber),
            happeningTextField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: trailingNumber),
            happeningTextField.heightAnchor.constraint(equalToConstant: 200),
            
            aliasTextField.topAnchor.constraint(equalTo: happeningTextField.bottomAnchor, constant: 15),
            aliasTextField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leadingNumber),
            aliasTextField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: trailingNumber),
            aliasTextField.heightAnchor.constraint(equalToConstant: textFieldHeight),
            
            reportButton.topAnchor.constraint(equalTo: aliasTextField.bottomAnchor, constant: 15),
            reportButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leadingNumber),
            reportButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: trailingNumber),
            reportButton.heightAnchor.constraint(equalToConstant: buttonHeight)
            
        ])
        
    }
}
