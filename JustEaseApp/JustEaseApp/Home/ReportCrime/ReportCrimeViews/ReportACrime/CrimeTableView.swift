//
//  CrimeTableView.swift
//  JustEaseApp
//
//  Created by iOSApp on 07/03/2023.
//

import UIKit
protocol CrimeDelegate {
    func updateCrimeField(with text: String, crimeId: Int)
}
extension CrimeController {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return crimesFilterData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        cell.textLabel?.text = crimesFilterData[indexPath.row].name
        cell.textLabel?.textColor = textSystemColor
        cell.backgroundColor = backgroundSystemColor
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCrime = crimesFilterData[indexPath.row].name
        self.delegate?.updateCrimeField(with: selectedCrime, crimeId: crimesFilterData[indexPath.row].id)
        dismiss(animated: true)
    }
}
