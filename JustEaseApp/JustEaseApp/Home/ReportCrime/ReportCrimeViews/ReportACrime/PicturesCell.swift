//
//  PicturesCell.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/13/23.
//
struct PictureData {
    let pix: String
    let dataSizePix: String
    let pixName: String
}
import UIKit
protocol PictureCellDelegate: AnyObject {
    func onClickDeleteButton(index: Int)
}
class PictureCollectionViewCell: UICollectionViewCell {
    static var identifier: String = "PictureCollectionCell"
    var pictureCellDelegate: PictureCellDelegate?
    var index : IndexPath?    
    lazy var pixSelected:  UIImageView  = {
        let profileImageView =  UIImageView()
        profileImageView.image = UIImage(named: "dan")
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.layer.masksToBounds =  true
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.layer.cornerRadius = 7
        return profileImageView
    }()
    lazy var dataSizeLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text =  "1 MB"
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 12)
        return label
    }()
    lazy var nameLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text =  "IMG_32903.png"
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 12)
        return label
    }()
    lazy var deleteIcon: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 10
        button.setImage(UIImage(systemName: "xmark.circle.fill"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = textSystemColor
        button.addTarget(self, action: #selector(clickDeleteButton(_:)), for: .touchUpInside)
        return button

    }()
    @objc func clickDeleteButton(_ sender: Any){
        guard let indexRow = index?.row else {
            return
        }
        pictureCellDelegate?.onClickDeleteButton(index: indexRow)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
  
    func setUpViews() {
        addSubview(nameLabel)
        addSubview(dataSizeLabel)
        addSubview(pixSelected)
        addSubview(deleteIcon)
        
        NSLayoutConstraint.activate([
            pixSelected.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            pixSelected.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            pixSelected.heightAnchor.constraint(equalToConstant: 40),
            pixSelected.widthAnchor.constraint(equalToConstant: 40),
        
            dataSizeLabel.topAnchor.constraint(equalTo: topAnchor, constant: 17),
            dataSizeLabel.leadingAnchor.constraint(equalTo: pixSelected.leadingAnchor, constant: 60),
            dataSizeLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            
            nameLabel.topAnchor.constraint(equalTo: dataSizeLabel.bottomAnchor, constant: 5),
            nameLabel.leadingAnchor.constraint(equalTo: pixSelected.leadingAnchor, constant: 60),
            nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
           
            deleteIcon.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            deleteIcon.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            
        ])
    }
}
