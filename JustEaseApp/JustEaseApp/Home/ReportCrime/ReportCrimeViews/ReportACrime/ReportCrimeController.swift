//
//  ReportCrimeController.swift
//  JustEaseApp
//
//  Created by iOSApp on 07/03/2023.
//

import UIKit
import FittedSheets
import AVKit
import PhotosUI
import GoogleMaps
import CoreLocation
import MapKit
struct ImageInfo {
    let image : UIImage
    let ImageName : String
    let ImageSize: String
}
class ReportCrimeController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UITextViewDelegate, UIImagePickerControllerDelegate,  UINavigationControllerDelegate, PHPickerViewControllerDelegate, GetAllCrimesCategoryDelegate ,UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout   {
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var imageArray: [ImageInfo] = []
    let statusCrime = ["Ongoing","Completed"]
    var statusCrimePickerView = UIPickerView()
    var selectedCrimeText = ""
    var address = ""
    var crimesViewModel = CrimeCategoryViewModel()
    
    var report_crime_category_id = 0
    var report_crime_address = ""
    var report_crime_status = ""
    var crime_details = ""
    var alias_name = ""
    var user_id = ""
    var latitude = ""
    var longitude = ""
    var postal_code = ""
    var country = ""
    var admin_area = ""
    var sub_admin_area = ""
    var locality = ""
    var sub_locality = ""
    // MARK: - Scroll view
    lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.showsHorizontalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    // MARK: - content view
    lazy var contentView: UIView = {
        let content = UIView()
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    lazy var locationTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.layer.cornerRadius = 0
        textField.layer.borderWidth = 0
        return textField
    }()
    
    lazy var locationButton: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.location.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = textFieldColor
        button.layer.cornerRadius = 0
        return button
    }()
    
    lazy var pictureView: UIView = {
        let content = UIView()
        content.backgroundColor = textFieldColor
        content.translatesAutoresizingMaskIntoConstraints = false
        content.frame.size.height = 200
        content.layer.cornerRadius = 7
        return content
    }()
    
    lazy var photoLayer :  UIImageView  = {
        let profileImageView =  UIImageView()
        profileImageView.image = UIImage(named: "photoLayer")
        profileImageView.contentMode = .scaleToFill
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.isUserInteractionEnabled = true
        profileImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOnPhotoLayer)))
        profileImageView.backgroundColor = textFieldColor
        return profileImageView
    }()
    lazy var firstLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textColor = placeholderSystemGrayColor
        label.font = UIFont(name: AppFonts.silkLight.font ,size: 14)
        label.text = "File Upload"
        return label
    }()
    lazy var uploadIcon: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "photoIcon"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var tapLabel: UILabel = {
        let label = UILabel.boldFirstText(firstText: "Tap to upload files \n", secondText: "You can upload up to 5 files", thirdText: "")
        label.textColor = AppColors.navyBlue.color
        label.textAlignment = .center
        return label
    }()
    lazy var uploadedPixCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 15
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        cv.backgroundColor = backgroundSystemColor
        cv.frame.size.height = CGFloat(60 * imageArray.count)
        cv.isHidden = true
        cv.dataSource = self
        cv.delegate = self
        cv.showsVerticalScrollIndicator = true
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    lazy var photoPixLayerStack: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(pictureView)
        stackView.addArrangedSubview(uploadedPixCollectionView)
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = 2
        return stackView
    }()
    
    lazy var categoryButton: UIButton = {
        let button = UIButton.selectButtonDesign()
        button.setTitle("  Select Crime from category", for: .normal)
        button.addTarget(self, action: #selector(didtapOnCategoryButton), for: .touchUpInside)
        return button
    }()
    
    lazy var categoryDownIcon: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.dropDown.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var crimeTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Status of Crime ",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.inputView = statusCrimePickerView
        textField.layer.borderWidth = 0
        return textField
    }()
    lazy var crimeDownIcon: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.dropDown.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var happeningTextField: UITextView = {
        let tv = UITextView.writeAbletextViewDesign()
        tv.text = "What is happening?"
        return tv
    }()
    
    lazy var aliasTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Input Alias Name (Optional)",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.layer.borderWidth = 0
        return textField
    }()
    
    lazy var reportButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Report Crime", for: .normal)
        button.addTarget(self, action: #selector(didTapReportCrimeButton), for: .touchUpInside)
        return button
    }()
    
    @objc func didtapOnCategoryButton() {
        let controller = CrimeController()
        controller.delegate = self
        present(controller, animated: true)
    }
    @objc func didTapOnPhotoLayer() {
        popUploadSource()
    }
    func popUploadSource(){
        let controller = UploadSourceController()
        controller.delegate = self
        let sheetController = SheetViewController(controller: controller, sizes: [.fixed(250), .fixed(250)])
        sheetController.blurBottomSafeArea = false
        sheetController.handleView.isHidden = true
        sheetController.topCornersRadius = 30
        present(sheetController, animated: false, completion: nil)
    }
    @objc func didTapReportCrimeButton() {
        
        alias_name = aliasTextField.text ?? ""
        report_crime_address = locationTextField.text ?? ""
        report_crime_status = crimeTextField.text ?? ""
        crime_details = happeningTextField.text ?? ""
        
        print(report_crime_category_id)
        print(latitude)
        print(longitude)
        print(crime_details)
        print(report_crime_address)
        print(report_crime_status)
        print(address)
        print(country)
        print(admin_area)
        print(sub_admin_area)
        print(sub_locality)
        if report_crime_category_id != 0 && crimeTextField.text != "" && happeningTextField.text != "" {
            let controller = ReportController()
            controller.delegate = self
            controller.reportButton.isHidden = false
            controller.reportViolationButton.isHidden = true
            controller.label.text = "Report Crime"
            controller.report_crime_category_id = report_crime_category_id
            controller.alias_name = alias_name
            controller.report_crime_address = report_crime_address
            controller.report_crime_status = report_crime_status
            controller.crime_details = crime_details
            controller.country = country
            controller.longitude = longitude
            controller.latitude = latitude
            controller.admin_area = admin_area
            controller.sub_admin_area = sub_admin_area
            controller.locality = locality
            controller.sub_locality = sub_locality
            
            let sheetController = SheetViewController(controller: controller, sizes: [.fixed(400), .fixed(400)])
            sheetController.blurBottomSafeArea = false
            sheetController.handleView.isHidden = true
            sheetController.topCornersRadius = 30
            present(sheetController, animated: false, completion: nil)
        } else {
            Toast.shared.showToastWithTItle("All fields are required", type: .error)
        }
    }
    func showDoneButton(){
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 20))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dropKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.happeningTextField.inputAccessoryView = toolbar
        self.crimeTextField.inputAccessoryView = toolbar
        self.aliasTextField.inputAccessoryView = toolbar
        self.locationTextField.inputAccessoryView = toolbar
    }
    @objc func dropKeyboard(){
        view.endEditing(true)
    }
    func notifyKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil);
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        happeningTextField.text = ""
        happeningTextField.textColor = textSystemColor
        if textView == self.happeningTextField {
           notifyKeyboard()
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.crimeTextField {
            self.statusCrimePickerView.selectRow(0, inComponent: 0, animated: true)
            self.pickerView(self.statusCrimePickerView, didSelectRow: 0, inComponent: 0)
        }
        if textField == self.aliasTextField  {
            notifyKeyboard()
        }
    }
 
   @objc func keyboardWillShow(sender: NSNotification) {
        self.contentView.frame.origin.y = -350 // Move view 150 points upward
   }

   @objc func keyboardWillHide(sender: NSNotification) {
        self.contentView.frame.origin.y = 0 // Move view to original position
   }
   
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        showDoneButton()
        setupScrollView()
        crimeTextField.delegate  = self
        happeningTextField.delegate = self
        aliasTextField.delegate = self
        self.hideKeyboardWhenTappedAround()
        statusCrimePickerView.delegate = self
        popUploadSource()
        crimesViewModel.delegate = self
        crimesViewModel.getCrimeCategory{}
        getlocationOnMap()
        registerCell()
    }
    func registerCell() {
        uploadedPixCollectionView.register(PictureCollectionViewCell.self, forCellWithReuseIdentifier: PictureCollectionViewCell.identifier)
    }
    func getlocationOnMap() {
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            guard let currentLocation = locManager.location else {
                return
            }
            latitude = "\(currentLocation.coordinate.latitude)"
            longitude = "\(currentLocation.coordinate.longitude)"
            let locate = CLLocation(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
            locate.fetchCityAndCountry { streetNumber ,lg , street, city, country, error in
                self.address = "\(streetNumber) \(street) \(city ?? "") \(country ?? "")"
                self.locationTextField.text = self.address
                self.address = "\(street) \(city ?? "") \(country ?? "")"
                self.country = country ?? ""
                self.admin_area = city ?? ""
                self.locality = street
                self.sub_admin_area = street
                self.sub_locality = street
            }

        }
    }
//    func getAddrees() {
//        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
//            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
//            guard let currentLocation = locManager.location else {
//                return
//            }
//            let location = CLLocation(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
//            CLGeocoder().reverseGeocodeLocation(location){ placemarks , error in
//                if error == nil && placemarks!.count > 0 {
//                    guard let placemark = placemarks?.last else {
//                        return
//                    }
//                    let p = CLPlacemark(placemark: (placemarks?[0] as CLPlacemark?)!)
//                    print(p.subThoroughfare ?? "")
//                    print(placemark.subLocality  ?? "")
//                    print(">>\(placemark.locality ?? "")")
//                    print(placemark.subAdministrativeArea ?? "")
//                    print(placemark.subLocality ?? "")
//                    print(placemark.subThoroughfare ?? "")
//                    print(placemark.postalAddress?.postalCode ?? "")
//
//                    self.address = "\(placemark.subThoroughfare ?? "") \(placemark.subLocality  ?? "") \(placemark.locality ?? "") \(placemark.subAdministrativeArea ?? "") \(placemark.subLocality ?? "") \(placemark.postalCode ?? "")"
//                    self.locationTextField.text = self.address
//                }
//
//            }
//        }
//    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
        navigationItem.title = "Report a crime"
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.setHidesBackButton(true, animated: true)
        
    }
  
    // MARK: - View did layout subviews
    override func viewDidLayoutSubviews() {
        scrollView.isScrollEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.contentSize = CGSize(width: 400, height: 1200)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return statusCrime.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return statusCrime[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        crimeTextField.text = statusCrime[row]
    }
    
    func getPhoto() {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true)
    }
    func getPhotoFromLibrary() {
        var controller = PHPickerConfiguration()
        controller.selectionLimit = 5
        controller.filter = .images
        let vc = PHPickerViewController(configuration: controller)
        vc.delegate = self
        present(vc, animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        
        if let image = info[.originalImage] as? UIImage  {
            // print("No image found")
            print("<<<\(image)")
            DispatchQueue.main.async {
                    print(image)
                    if self.imageArray.count < 5 {
                        self.uploadedPixCollectionView.isHidden = false
                        self.uploadedPixCollectionView.frame.size.height = CGFloat(60 * self.imageArray.count)
                        self.photoPixLayerStack.frame.size.height = CGFloat(200 + 60 * self.imageArray.count)
                        self.imageArray.insert(ImageInfo(image: image, ImageName: "mm", ImageSize: "Mb"), at: 0)
                        self.uploadedPixCollectionView.reloadData()
                    } else {
                        Toast.shared.showToastWithTItle("oops! number of images exceeded", type: .error)
                    }
                }
                
        }
        // MARK: - getting and checking the image size
        if let pix = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            
            let imgData = NSData(data: pix.jpegData(compressionQuality: 1)!)
            let imageSize: Int = imgData.count
            let actualImageSize =  Double(imageSize) / 1024.0 / 1024.0
            print("<<\(actualImageSize)")
        }
        if let asset = info[UIImagePickerController.InfoKey.imageURL] as? NSURL{
            print("???\(asset.lastPathComponent)")
        }
        
    }
    
    func proceedWithCameraAccess(){
        // handler in .requestAccess is needed to process user's answer to our request
        AVCaptureDevice.requestAccess(for: .video) { success in
            if success { // if request is granted (success is true)
                DispatchQueue.main.async {
                    self.getPhoto()
                }
            } else { // if request is denied (success is false)
                // Create Alert
                let alert = UIAlertController(title: "Camera", message: "Camera access is absolutely necessary to use this app", preferredStyle: .alert)
                
                // Add "OK" Button to alert, pressing it will bring you to the settings app
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                }))
                // Show the alert with animation
                self.present(alert, animated: true)
            }
        }
    }
    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        dismiss(animated: true)
        for result in results {
            result.itemProvider.loadObject(ofClass: UIImage.self) { object, error in
                DispatchQueue.main.async {
                    if let image = object as? UIImage, let fileName = result.itemProvider.suggestedName, let fileSize = result.itemProvider.preferredPresentationSize as? CGSize {
                        print(image)
                        if self.imageArray.count < 5 {
                            self.uploadedPixCollectionView.isHidden = false
                            self.uploadedPixCollectionView.frame.size.height = CGFloat(60 * self.imageArray.count)
                            self.photoPixLayerStack.frame.size.height = CGFloat(200 + 60 * self.imageArray.count)
                            self.imageArray.insert(ImageInfo(image: image, ImageName: fileName, ImageSize: "\(fileSize)"), at: 0)
                        } else {
                            Toast.shared.showToastWithTItle("oops! number of images exceeded", type: .error)
                        }
                    }
                    self.uploadedPixCollectionView.reloadData()
                }
//                guard let fileName = result.itemProvider.suggestedName else { return }
//                print(fileName)
                
//                if let filesize = result.itemProvider.preferredPresentationSize as? CGSize {
//                    print(filesize)
                //}
            }
        }
        
    }
    
 
}

extension ReportCrimeController : CrimeDelegate, NextScreenDelegate, ReportDelegate  {
    func updateCrimeField(with text: String, crimeId: Int) {
        categoryButton.setTitle("  \(text)", for: .normal)
        categoryButton.setTitleColor(textSystemColor, for: .normal)
        selectedCrimeText = text
        Storage.saveCrimeCategoryId(number: crimeId)
        report_crime_category_id = crimeId
    }
    
    func showCamera() {
        proceedWithCameraAccess()
    }
    
    func showLibrary() {
        getPhotoFromLibrary()
    }
    func didTapOnReport() {
            Loader.shared.showLoader()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
                Loader.shared.hideLoader()
                self.dismiss(animated: false){
                    let controller = CrimeSuccessAlert()
                    controller.delegate = self
                    let sheetController = SheetViewController(controller: controller, sizes: [.fixed(450), .fixed(450)])
                    sheetController.blurBottomSafeArea = false
                    sheetController.topCornersRadius = 30
                    sheetController.handleView.isHidden = true
                    self.present(sheetController, animated: false, completion: nil)
                }
                
            }
    }
//    let pictures: [PictureData] = {
//        let one = PictureData(pix: "dan", dataSizePix: "1 MB", pixName: "IMG_2424JW2.png")
//        
//        let two = PictureData(pix: "dan", dataSizePix: "1 MB", pixName: "IMG_2424JW2.png")
//        
//        let three = PictureData(pix: "dan", dataSizePix: "1 MB", pixName: "IMG_2424JW2.png")
//        
//        let four = PictureData(pix: "dan", dataSizePix: "1 MB", pixName: "IMG_2424JW2.png")
//        
//        return [one,two,three,four]
//    }()
    func popController() {
        navigationController?.popViewController(animated: true)
    }
    func didReceiveGetCrimesCategoryResponse(getAllBankResponse: CrimeCategoryResponse?) {}
    
}
