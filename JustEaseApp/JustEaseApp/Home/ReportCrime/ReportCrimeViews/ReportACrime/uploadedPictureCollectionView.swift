//
//  uploadedPicture.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/13/23.
//

import UIKit
extension ReportCrimeController : PictureCellDelegate{
    func onClickDeleteButton(index: Int) {
       // let arrayIndex = imageArray[index]
        imageArray.remove(at: index)
        uploadedPixCollectionView.reloadData()
        if imageArray.count == 0 {
            self.uploadedPixCollectionView.isHidden = true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            guard let cell = uploadedPixCollectionView.dequeueReusableCell(withReuseIdentifier: PictureCollectionViewCell.identifier, for: indexPath) as? PictureCollectionViewCell else { return UICollectionViewCell() }
        let imagesDetails = imageArray[indexPath.row]
        cell.index = indexPath
        cell.pictureCellDelegate = self
        cell.pixSelected.image =  imagesDetails.image
        cell.nameLabel.text = imagesDetails.ImageName
        cell.dataSizeLabel.text = imagesDetails.ImageSize
        return cell
        }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 40, height: 50)
    }
}

