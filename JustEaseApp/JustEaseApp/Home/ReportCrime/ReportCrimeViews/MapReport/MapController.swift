//
//  MapController.swift
//  JustEaseApp
//
//  Created by iOSApp on 08/03/2023.
//

import MapKit
import UIKit
import GoogleMaps
import CoreLocation
import MapKit

extension CLLocation {
    func fetchCityAndCountry(completion: @escaping (_ streetNumber: String, _ lg: String,_ street: String, _ city: String?, _ country:  String?,_ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(self) { completion($0?.first?.subThoroughfare ?? "", $0?.first?.locality ?? "", $0?.first?.name ?? "", $0?.first?.administrativeArea, $0?.first?.country, $1) }
    }

}
//extension CLLocation {
//    func fetchCityAndCountry(completion: @escaping (_ streetNumber: String, _ lg: String,_ street: String, _ city: String?, _ country:  String?) -> ()) {
//        var locManager = CLLocationManager()
//        guard let currentLocation = locManager.location else {
//            return
//        }
//        var streetNumber = ""
//        var lg = ""
//        var street = ""
//        var city = ""
//        var country = ""
//
//        let location = CLLocation(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
//        CLGeocoder().reverseGeocodeLocation(location){ placemarks , error in
//            if error == nil && placemarks!.count > 0 {
//                guard let placemark = placemarks?.last else {
//                    return
//                }
//                streetNumber = placemark.subThoroughfare ?? ""
//                lg = placemark.locality ?? ""
//                street = placemark.name ?? ""
//                city = placemark.subAdministrativeArea ?? ""
//                country  = placemark.country ?? ""
//                //        { completion($0?.first?.subThoroughfare ?? "", $0?.first?.locality ?? "", $0?.first?.name ?? "", $0?.first?.administrativeArea, $0?.first?.country, $1) }
//            }
//
//        }
//        completion(streetNumber, lg, street, city, country)
//    }
//}

class MapController: UIViewController , UITextFieldDelegate {
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var address = ""
    
    lazy var map: GMSMapView = {
        let map = GMSMapView()
        map.translatesAutoresizingMaskIntoConstraints = false
        map.isMyLocationEnabled = true
        map.isUserInteractionEnabled = true
        return map
    }()
    lazy var cameraIcon: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 30
        button.tintColor = AppColors.white.color
        button.backgroundColor = AppColors.green.color
        button.setImage(AppButtonImages.camera.image, for: .normal)
        button.addTarget(self, action: #selector(didTapOnCamera), for: .touchUpInside)
        return button
    }()
    @objc func didTapOnCamera() {
        navigationController?.pushViewController(ReportCrimeController(), animated:true)
    }
    // MARK: -  Position
    lazy var locationView: UIView = {
        let content = UIView.stackViewHolderDesign()
        content.backgroundColor = backgroundSystemColor
        content.heightAnchor.constraint(equalToConstant: view.frame.size.height / 2 - 100).isActive = true
        content.layer.cornerRadius = 20
        return content
    }()
    lazy var locationTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.layer.cornerRadius = 0
        textField.layer.borderWidth = 0
        return textField
    }()
    
    lazy var locationButton: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.location.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = textFieldColor
        button.layer.cornerRadius = 0
        return button
    }()
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.font = UIFont(name: AppFonts.silkaMedium.font ,size: 14)
        label.textColor = AppColors.green.color
        label.text = "Your Current Location"
        return label
    }()
    lazy var confirmButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Confirm Location", for: .normal)
        button.layer.masksToBounds = true
        button.titleLabel?.font = UIFont(name: AppFonts.silka.font, size: 14)
        button.addTarget(self, action: #selector(didTapConfirmButton), for: .touchUpInside)
        return button
    }()
    @objc func didTapConfirmButton() {
        navigationController?.pushViewController(ReportCrimeController(), animated: true)
    }
    func showDoneButton(){
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dropKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.locationTextField.inputAccessoryView = toolbar
    }
    @objc func dropKeyboard(){
        view.endEditing(true)
    }
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        showDoneButton()
        checkLocationPermission()
        locationTextField.delegate = self
        
        getlocationOnMap()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil);
    }
    
    func getlocationOnMap() {
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            guard let currentLocation = locManager.location else {
                return
            }
            let locate = CLLocation(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
            locate.fetchCityAndCountry { streetNumber, lg , street, city, country, error in
                self.address = "\(streetNumber) \(street) \(city ?? "") \(country ?? "")"
                self.locationTextField.text = self.address
            }
            map.camera = GMSCameraPosition.camera(withLatitude:  currentLocation.coordinate.latitude, longitude:  currentLocation.coordinate.longitude, zoom: 12)
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
            marker.map = map
        }
    }
    @objc func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y = -150 // Move view 150 points upward
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0// Move view to original position
    }
    
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    
    //check location services enabled or not
    func checkLocationPermission() {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                //open setting app when location services are disabled
                openSettingApp(message:NSLocalizedString("Please enable your location to use this service.", comment: ""))
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            @unknown default:
                fatalError()
            }
        } else {
            openSettingApp(message:NSLocalizedString("please enable location services to continue using the app", comment: ""))
        }
    }
    
    //open location settings for app
    func openSettingApp(message: String) {
        let alertController = UIAlertController (title: "JustEase", message:message , preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: NSLocalizedString("settings", comment: ""), style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getlocationOnMap()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
        navigationItem.title = "Report a crime"
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.setHidesBackButton(true, animated: true)
        
    }
    
}

