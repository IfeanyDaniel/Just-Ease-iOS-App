//
//  MapConstraint.swift
//  JustEaseApp
//
//  Created by iOSApp on 08/03/2023.
//

import MapKit
import UIKit
extension MapController {
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(map)
        view.addSubview(locationView)
        view.addSubview(cameraIcon)
        locationView.addSubview(locationTextField)
        locationView.addSubview(locationButton)
        locationView.addSubview(label)
        locationView.addSubview(confirmButton)
        self.navigationItem.setHidesBackButton(true, animated: true)
        view.backgroundColor = backgroundSystemColor
        
        NSLayoutConstraint.activate([
            map.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            map.leadingAnchor.constraint(equalTo:view.leadingAnchor, constant: 0),
            map.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            map.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
            locationView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            locationView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            locationView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            
            label.topAnchor.constraint(equalTo: locationView.topAnchor, constant: 15),
            label.leadingAnchor.constraint(equalTo: locationView.leadingAnchor, constant: leadingNumber),
            
            locationButton.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 20),
            locationButton.leadingAnchor.constraint(equalTo: locationView.leadingAnchor, constant: leadingNumber),
            locationButton.widthAnchor.constraint(equalToConstant: 40),
            locationButton.heightAnchor.constraint(equalToConstant: textFieldHeight + 10),
            
            cameraIcon.topAnchor.constraint(equalTo: locationView.topAnchor, constant: -100),
            cameraIcon.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            cameraIcon.heightAnchor.constraint(equalToConstant: 60),
            cameraIcon.widthAnchor.constraint(equalToConstant: 60),
            
            locationTextField.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 20),
            locationTextField.leadingAnchor.constraint(equalTo: locationButton.trailingAnchor, constant: 0),
            locationTextField.trailingAnchor.constraint(equalTo: locationView.trailingAnchor, constant: trailingNumber),
            locationTextField.heightAnchor.constraint(equalToConstant: textFieldHeight + 10),
            
            confirmButton.bottomAnchor.constraint(equalTo: locationView.bottomAnchor, constant: -50),
            confirmButton.leadingAnchor.constraint(equalTo:  locationView.leadingAnchor, constant: leadingNumber),
            confirmButton.trailingAnchor.constraint(equalTo:  locationView.trailingAnchor, constant: trailingNumber),
            confirmButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            
            
        ])
    }
}
