//
//  UploadSourceCell.swift
//  JustEaseApp
//
//  Created by iOSApp on 08/03/2023.
//

import UIKit
class UploadViewCell: UITableViewCell {
    static var identifier = "cell"
    lazy var nameLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.font = UIFont(name: AppFonts.silkaMedium.font ,size: 14)
        label.textColor = textSystemColor
        return label
    }()
    lazy var icon: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 25
        button.tintColor = AppColors.green.color
        button.backgroundColor = AppColors.lightGreen.color
        return button
    }()
    // MARK: - ... Adding subviews and constraints in the cell
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(nameLabel)
        addSubview(icon)
        
        NSLayoutConstraint.activate([
            
           icon.topAnchor.constraint(equalTo: topAnchor, constant: 10),
           icon.leadingAnchor.constraint(equalTo: leadingAnchor, constant: leadingNumber),
           icon.heightAnchor.constraint(equalToConstant: 50),
           icon.widthAnchor.constraint(equalToConstant: 50),
           
           nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 25),
           nameLabel.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: 10),
           
            
        ])
    }
    required init?(coder: NSCoder) {
        fatalError("init(code:) has not been implemented")
    }
}
