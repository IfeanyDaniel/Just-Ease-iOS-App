//
//  UploadSourceTableView.swift
//  JustEaseApp
//
//  Created by iOSApp on 08/03/2023.
//

import UIKit
extension UploadSourceController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return page.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: UploadViewCell.identifier, for: indexPath) as? UploadViewCell else { return UITableViewCell() }
        let data = page[indexPath.row]
        cell.nameLabel.text = data.title
        cell.icon.setImage(UIImage(named: data.imageName), for: .normal)
        cell.backgroundColor = viewStackColor
        cell.selectionStyle = .none
        return cell
    }
       
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: false)
        let data = page[indexPath.row]
        if data.title ==  "Camera" {
            delegate?.showCamera()
        } else {
             delegate?.showLibrary()
        }
    }
}
