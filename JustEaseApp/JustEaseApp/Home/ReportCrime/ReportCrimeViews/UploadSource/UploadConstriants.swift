//
//  UploadConstriants.swift
//  JustEaseApp
//
//  Created by iOSApp on 08/03/2023.
//


import UIKit
extension  UploadSourceController  {
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(label)
        view.addSubview(tableView)
        view.backgroundColor =  viewStackColor
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 30),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber)
        ])
        tableView.anchorWithConstantsToTop(label.bottomAnchor,
                                                     left: view.leftAnchor, bottom: view.bottomAnchor, right:  view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
    }
}
