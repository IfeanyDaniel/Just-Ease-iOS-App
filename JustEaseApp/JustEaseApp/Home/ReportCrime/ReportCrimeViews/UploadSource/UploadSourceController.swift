//
//  UploadSourceController.swift
//  JustEaseApp
//
//  Created by iOSApp on 08/03/2023.
//

import UIKit
import Foundation
struct UploadData {
    let title: String
    let imageName: String
}

protocol NextScreenDelegate: AnyObject {
    func showCamera()
    func showLibrary()
}
class UploadSourceController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var delegate : NextScreenDelegate?
    
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.font = UIFont(name: AppFonts.silkaMedium.font ,size: 14)
        label.textColor = textSystemColor
        label.text = "Upload from"
        return label
    }()
    lazy var tableView : UITableView = {
        let table = UITableView()
        table.backgroundColor = viewStackColor
        table.rowHeight = 60
        table.sizeToFit()
        table.delegate = self
        table.dataSource = self
        table.isScrollEnabled = false
        table.separatorColor = viewStackColor
        return table
    }()
    let page: [UploadData] = {
        let camera = UploadData(title: "Camera", imageName: "cameraIcon")
        let picture = UploadData(title: "Library", imageName: "library")
        return [camera,picture]
    }()
    func registercCell() {
        tableView.register(UploadViewCell .self, forCellReuseIdentifier: UploadViewCell.identifier)

    }

    override func viewDidLoad() {
         layoutViews()
         registercCell()
    }
    
}
