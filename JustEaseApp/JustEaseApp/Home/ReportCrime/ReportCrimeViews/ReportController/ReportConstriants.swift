//
//  ReportConstriants.swift
//  JustEaseApp
//
//  Created by iOSApp on 09/03/2023.
//


import UIKit
extension ReportController  {
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(label)
        view.addSubview(userButton)
        view.addSubview(userlabel)
        view.addSubview(anonymousButton)
        view.addSubview(anonymouslabel)
        view.addSubview(reportButton)
        view.addSubview(reportViolationButton)
        view.backgroundColor =  viewStackColor
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 30),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber),
            
            userButton.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 15),
            userButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber),
            userButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            userButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            
            userlabel.topAnchor.constraint(equalTo: userButton.bottomAnchor, constant: 10),
            userlabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber),
            
            anonymousButton.topAnchor.constraint(equalTo: userlabel.bottomAnchor, constant: 15),
            anonymousButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber),
            anonymousButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            anonymousButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            
            anonymouslabel.topAnchor.constraint(equalTo: anonymousButton.bottomAnchor, constant: 10),
            anonymouslabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber),
            
            reportButton.topAnchor.constraint(equalTo: view.bottomAnchor, constant: -80),
            reportButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber),
            reportButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            reportButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            
            reportViolationButton.topAnchor.constraint(equalTo: view.bottomAnchor, constant: -80),
            reportViolationButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber),
            reportViolationButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            reportViolationButton.heightAnchor.constraint(equalToConstant: buttonHeight)
            
        ])
    }
}
