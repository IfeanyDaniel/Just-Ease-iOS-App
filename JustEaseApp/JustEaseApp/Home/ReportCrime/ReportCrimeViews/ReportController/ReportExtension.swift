//
//  ReportExtension.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/8/23.
//

import Foundation
extension ReportController: ReportCrimeViewModelDelegate, ReportViolationViewModelDelegate {
    func didReceiveReportCrimeResponse(reportCrimeResponse: CrimeResponse?, _ statusCode: Int) {
        if reportCrimeResponse?.status == 200 {
            Loader.shared.hideLoader()
            delegate?.didTapOnReport()
        }
        if reportCrimeResponse?.status == 400 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("\(reportCrimeResponse?.message ?? "")", type: .error)
        }
    }
    
    func didReceiveReportViolationResponse(reportViolationResponse: ReportViolationResponse?, _ statusCode: Int) {
        if reportViolationResponse?.status == 200 {
            Loader.shared.hideLoader()
            delegateSuccess?.showCrimeSuccess()
        }
        if reportViolationResponse?.status == 400 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("\(reportViolationResponse?.message ?? "")", type: .error)
        }
        if reportViolationResponse?.status == 422 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("\(reportViolationResponse?.message ?? "")", type: .error)
        }
    }

}
