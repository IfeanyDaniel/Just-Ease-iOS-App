//
//  ReportController.swift
//  JustEaseApp
//
//  Created by iOSApp on 09/03/2023.
//

import UIKit
import Foundation

protocol ReportDelegate: AnyObject {
    func didTapOnReport()
    func popController()
}
protocol ReportViolationCrimeDelegate: AnyObject {
    func ShowUserDetails()
}
protocol ReportViolationSuccessDelegate: AnyObject {
    func showCrimeSuccess()
}
class ReportController: UIViewController {
    var delegate:  ReportDelegate?
    var delegateSuccess: ReportViolationSuccessDelegate?
    var reportViolationDelegate: ReportViolationCrimeDelegate?
    var reportCrimeViewModel = ReportCrimeViewModel()
    var reportViolationViewModel = ReportViolationViewModel()
    
    var report_crime_category_id = 0
    var report_crime_address = ""
    var report_crime_status = ""
    var crime_details = ""
    var alias_name = ""
    var user_id = ""
    var latitude = ""
    var longitude = ""
    var postal_code = ""
    var country = ""
    var admin_area = ""
    var sub_admin_area = ""
    var locality = ""
    var sub_locality = ""
    var anonymous_status = ""
    
    var typeOfUser = ""
    
    var report_violation_category_id = 0
    var report_violation_subcategory_id = 0
    var local_government_id = 0
    var nature_of_issue = ""
    var date_of_incident = ""
    var time_of_incident = ""
    var reportIsuue = ""
    var anonymous = 0
    var address = ""
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.font = UIFont(name: AppFonts.silkaMedium.font ,size: 14)
        label.textColor = AppColors.green.color
        return label
    }()
    lazy var userButton: UIButton = {
        let button = UIButton.checkButtonDesign()
        button.setTitle(" A user", for: .normal)
        button.setImage(UIImage(named: "selectedOption"), for: .normal)
        button.addTarget(self, action: #selector(didTapAUserButton), for: .touchUpInside)
        return button
    }()
    lazy var userlabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.font = UIFont(name: AppFonts.silkLight.font ,size: 13)
        label.textColor = textSystemColor
        label.text = "Report with your profile details"
        return label
    }()
    @objc func didTapAUserButton() {
        typeOfUser = "user"
        userButton.setImage(UIImage(named: "selectedOption"), for: .normal)
        anonymousButton.setImage(UIImage(named: "unSelectedOption"), for: .normal)
    }
    lazy var anonymousButton: UIButton = {
        let button = UIButton.checkButtonDesign()
        button.setTitle(" Anonymously", for: .normal)
        button.setImage(UIImage(named: "unSelectedOption"), for: .normal)
        button.addTarget(self, action: #selector(didTapAnonymousButton), for: .touchUpInside)
        return button
    }()
    @objc func didTapAnonymousButton() {
        typeOfUser = "anonymous"
        anonymousButton.setImage(UIImage(named: "selectedOption"), for: .normal)
        userButton.setImage(UIImage(named: "unSelectedOption"), for: .normal)
    }
    lazy var anonymouslabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.font = UIFont(name: AppFonts.silkLight.font ,size: 13)
        label.textColor = textSystemColor
        label.text = "Report anonymously to protect identity"
        return label
    }()
    
    lazy var reportButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Report Crime", for: .normal)
        button.addTarget(self, action: #selector(didTapReportButton), for: .touchUpInside)
        return button
    }()
    
    lazy var reportViolationButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Proceed", for: .normal)
        button.isHidden = true
        button.addTarget(self, action: #selector(didTapReportViolationButton), for: .touchUpInside)
        return button
    }()
    @objc func didTapReportButton() {
        if InternetConnectionManager.isConnectedToNetwork(){
            Loader.shared.showLoader()
            let request = CrimeRequest(report_crime_category_id: report_crime_category_id, report_crime_address: report_crime_address, report_crime_status: report_crime_status, crime_details: crime_details, alias_name: alias_name, user_id: Storage.getUserId(), latitude: latitude, longitude: longitude, postal_code: "1234", country: country, admin_area: admin_area, sub_admin_area: sub_admin_area, locality: locality, sub_locality: sub_locality, anonymous_status: "0")
            print(request)
            reportCrimeViewModel.getReportCrimeResponse(crimeRequest: request)
        } else {
            Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
        }
    }
    @objc func didTapReportViolationButton(){
        if typeOfUser == "user" {
            reportViolationDelegate?.ShowUserDetails()
        } else {
            if InternetConnectionManager.isConnectedToNetwork(){
                Loader.shared.showLoader()
                print(report_violation_category_id)
                let request = ReportViolationRequest(report_violation_category_id: Storage.getViolationCategoryId(), report_violation_subcategory_id: report_violation_subcategory_id, local_government_id: 7, nature_of_issue: nature_of_issue, date_of_incident: date_of_incident, time_of_incident: time_of_incident, details_of_isssue: reportIsuue, user_id: Storage.getUserId(), report_violation_address: address, anonymous_status: 1)
                print(request)
                reportViolationViewModel.getReportViolationResponse(reportViolationRequest: request)
            } else {
                Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
            }

        }
    }
    override func viewDidLoad() {
         layoutViews()
        typeOfUser = "user"
        reportCrimeViewModel.delegate = self
        reportViolationViewModel.delegate = self
    }
    
}
