//
//  VideoRecording.swift
//  JustEaseApp
//
//  Created by iOSApp on 01/06/2023.
//

import UIKit
import AVFoundation

class VideoRecordingViewController: UIViewController, AVCaptureFileOutputRecordingDelegate, PanicViewModelDelegate {


    var captureSession: AVCaptureSession?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var captureMovieOutput: AVCaptureMovieFileOutput?
    var timer: Timer?
    var seconds: Int = 0
    var isTimerRunning: Bool = false
    var panicViewModel = PanicViewModel()
    func startTimer() {
           if !isTimerRunning {
               timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
               isTimerRunning = true
           }
       }
    func pauseTimer() {
          if isTimerRunning {
              timer?.invalidate()
              timer = nil
              isTimerRunning = false
          }
      }
    @objc func updateTimer() {
          seconds += 1
          
          let hours = seconds / 3600
          let minutes = (seconds % 3600) / 60
          let seconds = (seconds % 3600) % 60
          
          timerLabel.text = String(format: "    %02d:%02d:%02d   ", hours, minutes, seconds)
      }
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "In Progress"
        label.textColor = AppColors.green.color
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 12)
        label.textAlignment = .left
        return label
    }()
    lazy var timerLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "   00:00:00  "
        label.textColor = textSystemColor
        label.backgroundColor = viewStackColor
        label.layer.cornerRadius = 3
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 12)
        label.textAlignment = .left
        return label
    }()
    
    lazy var cancelIcon: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(systemName: "xmark"), for: .normal)
        button.layer.cornerRadius = 25
        button.addTarget(self, action: #selector(didTapOnCancelButton), for: .touchUpInside)
        return button
    }()
    lazy var startButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = viewStackColor
        button.setImage(UIImage(systemName: "play.fill"), for: .normal)
        button.layer.cornerRadius = 25
        button.addTarget(self, action: #selector(startRecording(_:)), for: .touchUpInside)
        return button
    }()
    lazy var stopButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = viewStackColor
        button.setImage(UIImage(systemName: "pause.fill"), for: .normal)
        button.layer.cornerRadius = 25
        button.isHidden = true
        button.addTarget(self, action: #selector(stopRecording(_:)), for: .touchUpInside)
        return button
    }()
    lazy var sendButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = viewStackColor
        button.setImage(UIImage(systemName: "paperplane.fill"), for: .normal)
        button.layer.cornerRadius = 25
        button.addTarget(self, action: #selector(uploadButton), for: .touchUpInside)
        return button
    }()
    @objc func uploadButton() {
        captureMovieOutput?.stopRecording()
        dismiss(animated: true) {
            Toast.shared.showToastWithTItle("Sending record...", type: .success)
            let request = PanicRequest(user_id: Storage.getUserId(), file: "video.")
            self.panicViewModel.getPanicResponse(panicRequest: request, videoUrl: Storage.getVideoUrl())
        }
    }
    @objc func didTapOnCancelButton() {
        dismiss(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCaptureSession()
        setupPreviewLayer()
        setupMovieOutput()
        panicViewModel.delegate = self
    }
    
    func setupCaptureSession() {
        captureSession = AVCaptureSession()
        captureSession?.beginConfiguration()
        
        // Configure video input
        guard let videoDevice = AVCaptureDevice.default(for: .video) else {
            print("Failed to get video device")
            return
        }
        
        do {
            let videoInput = try AVCaptureDeviceInput(device: videoDevice)
            if (captureSession?.canAddInput(videoInput) == true) {
                captureSession?.addInput(videoInput)
            }
        } catch {
            print("Failed to create video input")
            return
        }
        
        // Configure audio input
        guard let audioDevice = AVCaptureDevice.default(for: .audio) else {
            print("Failed to get audio device")
            return
        }
        
        do {
            let audioInput = try AVCaptureDeviceInput(device: audioDevice)
            if (captureSession?.canAddInput(audioInput) == true) {
                captureSession?.addInput(audioInput)
            }
        } catch {
            print("Failed to create audio input")
            return
        }
        
        captureSession?.commitConfiguration()
    }
    
    func setupPreviewLayer() {
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.bounds
        view.layer.addSublayer(videoPreviewLayer!)
        view.addSubview(label)
        view.addSubview(timerLabel)
        view.addSubview(cancelIcon)
        view.addSubview(startButton)
        view.addSubview(stopButton)
        view.addSubview(sendButton)
        DispatchQueue.main.async {
            self.captureSession?.startRunning()
        }
        NSLayoutConstraint.activate([
            timerLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 30),
            timerLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0),
          
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 30),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
          
            cancelIcon.topAnchor.constraint(equalTo: view.topAnchor, constant: 30),
            cancelIcon.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            cancelIcon.heightAnchor.constraint(equalToConstant: 50),
            cancelIcon.widthAnchor.constraint(equalToConstant: 50),
            
            startButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -100),
            startButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            startButton.heightAnchor.constraint(equalToConstant: 50),
            startButton.widthAnchor.constraint(equalToConstant: 50),
            
            stopButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -100),
            stopButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            stopButton.heightAnchor.constraint(equalToConstant: 50),
            stopButton.widthAnchor.constraint(equalToConstant: 50),
            
            sendButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -100),
            sendButton.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0),
            sendButton.heightAnchor.constraint(equalToConstant: 50),
            sendButton.widthAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    func setupMovieOutput() {
        captureMovieOutput = AVCaptureMovieFileOutput()
        if (captureSession?.canAddOutput(captureMovieOutput!) == true) {
            captureSession?.addOutput(captureMovieOutput!)
        }
    }
    
    @IBAction func startRecording(_ sender: UIButton) {
        let outputPath = NSTemporaryDirectory() + "output.m4v"
        let outputFileURL = URL(fileURLWithPath: outputPath)
        captureMovieOutput?.startRecording(to: outputFileURL, recordingDelegate: self)
        startButton.isHidden = true
        stopButton.isHidden = false
        startTimer()
    }
    
    @IBAction func stopRecording(_ sender: UIButton) {
        captureMovieOutput?.stopRecording()
        startButton.isHidden = false
        stopButton.isHidden = true
        pauseTimer()
    }
    func convertMOVtoMP4(inputURL: URL, outputURL: URL, completion: @escaping (Bool, Error?) -> Void) {
        let asset = AVAsset(url: inputURL)
        
        guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetMediumQuality) else {
            completion(false, nil)
            return
        }
        
        exportSession.outputFileType = .mp4
        exportSession.outputURL = outputURL
        
        exportSession.exportAsynchronously {
            switch exportSession.status {
            case .completed:
                completion(true, nil)
            case .failed:
                completion(false, exportSession.error)
            case .cancelled:
                completion(false, nil)
            default:
                break
            }
        }
    }
  


//    func convertVideoToData(videoUrl: URL) -> NSMutableData {
//        let body = NSMutableData()
//        let boundary = "Boundary-\(UUID().uuidString)"
//        if let videoData = try? Data(contentsOf: videoUrl) {
//                    body.append("--\(boundary)\r\n".data(using: .utf8)!)
//                    body.append("Content-Disposition: form-data; name=\"video\"; filename=\"video.mp4\"\r\n".data(using: .utf8)!)
//                    body.append("Content-Type: video/mp4\r\n\r\n".data(using: .utf8)!)
//                    body.append(videoData)
//                    body.append("\r\n".data(using: .utf8)!)
//                }
//        print("+++++\(body)")
//        return body
//    }
    func getOutputURL() -> URL? {
        // Get the URL for the app's document directory
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return nil
        }
        
        // Generate a unique file name for the converted .mp4 file
        let outputFileName = "output.m4v" // Replace with your desired file name
        
        // Append the file name to the document directory URL
        let outputURL = documentsDirectory.appendingPathComponent(outputFileName)
        
        return outputURL
    }
    func uploadVideo(url: URL){
        
    }
    func fileOutput(_ output: AVCaptureFileOutput, didStartRecordingTo fileURL: URL, from connections: [AVCaptureConnection]) {
        print("Started recording to \(fileURL)")
    }
    
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        if let error = error {
            print("Recording error: \(error.localizedDescription)")
        } else {
            print("Finished recording: \(outputFileURL)")
            Storage.saveVideoUrl(source: outputFileURL)
//          let man =  convertVideoToData(videoUrl: outputFileURL)
//            print("\(man)")
//            let inputURL = outputFileURL
//            let outputURL = getOutputURL()!
//            convertMOVtoMP4(inputURL: inputURL, outputURL: outputURL) { success, error in
//                if success {
//                    print("Conversion successful\(outputURL)")
//                    Storage.saveVideoUrl(source: outputURL)
//                } else if let error = error {
//                    print("Conversion failed with error: \(error)")
//                } else {
//                    print("Conversion cancelled")
//                }
//            }
        }
    }
    
    func didReceivePanicResponse(panicResponse: PanicResponse?, _ statusCode: Int) {
        if statusCode == 200 {
            Toast.shared.showToastWithTItle("Uploaded successfully...", type: .success)
        } else {
            Toast.shared.showToastWithTItle("Failed to upload...", type: .error)
        }
    }
    
}
