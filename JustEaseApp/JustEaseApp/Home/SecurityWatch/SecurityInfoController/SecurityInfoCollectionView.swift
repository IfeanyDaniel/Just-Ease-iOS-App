//
//  SecurityInfoCollectionView.swift
//  JustEaseApp
//
//  Created by iOSApp on 03/03/2023.
//

import UIKit
import Kingfisher
extension SecurityInfoController {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        searchSecurityWatchViewModel.numberOfRowsInSection().count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = crimeWatchCollectionView.dequeueReusableCell(withReuseIdentifier: SecurityInfoCell.identifier, for: indexPath) as? SecurityInfoCell else { return UICollectionViewCell() }
        let securityWatchData = searchSecurityWatchViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
        cell.commentLabel.text = securityWatchData.crimeDetails
        cell.nameLabel.text = securityWatchData.firstName + " " + securityWatchData.lastName
        cell.messageButton.setTitle("  \(securityWatchData.commentCount)", for: .normal)
        cell.index = indexPath
        cell.securityInfoDelegate = self
        let timeAgo = securityWatchData.createdAt
        cell.timeLabel.text = timeAgo.timeAgoDisplay()
        cell.numberOfLikes = securityWatchData.likesCount
        cell.crimeIsLiked = securityWatchData.isliked
        if securityWatchData.image_url != "" {
            cell.profileImage.kf.setImage(with: URL(string: "\(securityWatchData.image_url)"))
            cell.profileImage.isHidden = false
            cell.defaultImage.isHidden =  true
        } else {
            cell.defaultImage.setTitle("\(securityWatchData.firstName.first!) \(securityWatchData.lastName.first!)", for: .normal)
            cell.defaultImage.isHidden = false
            cell.profileImage.isHidden = true
        }
        print(cell.crimeIsLiked)
        if securityWatchData.isliked == true {
            cell.noLoveButton.isHidden = true
            cell.loveButton.isHidden = false
            cell.loveButton.setTitle(" \(securityWatchData.likesCount)", for: .normal)
        } else {
            cell.loveButton.isHidden = true
            cell.noLoveButton.isHidden = false
            cell.noLoveButton.setTitle(" \(securityWatchData.likesCount)", for: .normal)
        }
        cell.backgroundColor = backgroundSystemColor
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 350)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let securityWatchData = searchSecurityWatchViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
        let controller = SecurityCommentController()
        controller.fullName = securityWatchData.firstName + " " + securityWatchData.lastName
        controller.firstName = securityWatchData.firstName
        controller.lastName = securityWatchData.lastName
        controller.crimeDetails = securityWatchData.crimeDetails
        controller.time = securityWatchData.createdAt.timeAgoDisplay()
        controller.id = securityWatchData.id
        controller.commentCount = securityWatchData.commentCount
        controller.likesCount = securityWatchData.likesCount
        controller.isLiked = securityWatchData.isliked
        controller.imageUrl = securityWatchData.image_url
        navigationController?.pushViewController(controller, animated: true)
    }
}
