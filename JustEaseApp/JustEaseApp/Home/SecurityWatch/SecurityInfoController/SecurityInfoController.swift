//
//  SecurityInfoController.swift
//  JustEaseApp
//
//  Created by iOSApp on 03/03/2023.
//

import UIKit
import GoogleMaps
import CoreLocation
import MapKit
class SecurityInfoController: UIViewController,UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout  {
    var searchSecurityWatchViewModel = SearchSecurityWatchViewModel()
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var createCrimeLikeViewModel = CreateCrimeLikeViewModel()
    var lastIndexPath: IndexPath?
    var liked = ""
    lazy var crimeWatchCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        cv.backgroundColor = backgroundSystemColor
        cv.dataSource = self
        cv.delegate = self
        cv.showsVerticalScrollIndicator = true
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
   
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        self.hideKeyboardWhenTappedAround()
        registercCell()
        fetchCrimeInfo()
        createCrimeLikeViewModel.delegate = self
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    func fetchCrimeInfo() {
        guard let currentLocation = locManager.location else {
            return
        }
        Loader.shared.showLoader()
        self.searchSecurityWatchViewModel.getSecurityWatch(longitude: "\(currentLocation.coordinate.longitude)", latitude: "\(currentLocation.coordinate.latitude)", search_scope: "all_crime", query: "", userId: Storage.getUserId()){
            Loader.shared.hideLoader()
            DispatchQueue.main.async { [self] in
                self.crimeWatchCollectionView.reloadData()
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
        navigationItem.title = "Security Watch"
    }
   
    func registercCell() {
        crimeWatchCollectionView.register(SecurityInfoCell.self, forCellWithReuseIdentifier: SecurityInfoCell.identifier)
    }
    
    func likeButtonClicked(index: IndexPath) {
        print("<<<<<<<<<<")
        let securityWatchData = searchSecurityWatchViewModel.cellForRowsAt(indexPath: index)[index.row]
        if InternetConnectionManager.isConnectedToNetwork(){
            let request = CreateCrimeLikeRequest(user_id: Storage.getUserId())
            print(request)
            print("====\(securityWatchData.id)")
            createCrimeLikeViewModel.getCreateCrimeLikeResponse(reportCrimeId: securityWatchData.id, createCrimeLikeRequest: request)
        } else {
            Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
        }
    }
}

extension SecurityInfoController: SecurityInfoDelegate , CreateCrimeLikeViewModelDelegate {
    func didReceiveCreateCrimeLikeResponse(createCrimeLikeResponse: CreateCrimeLikeResponse?, _ statusCode: Int) {
        if statusCode == 200 {
            if liked == "nice" {
                Toast.shared.showToastWithTItle("👍🏻", type: .success)
            }
        }
        if statusCode == 400 {
            Toast.shared.showToastWithTItle(createCrimeLikeResponse?.message ?? "", type: .error)
        }
    }
  
    func onClickShareButton(index: IndexPath) {
        let securityWatchData = searchSecurityWatchViewModel.cellForRowsAt(indexPath: index)[index.row]
        let shareSheetVC = UIActivityViewController(
            activityItems: [securityWatchData.crimeDetails], applicationActivities: nil
        )
        self.present(shareSheetVC, animated: true)
    }
    
    func onClickLoveButton(index: IndexPath) {
        print("love")
        let securityWatchData = searchSecurityWatchViewModel.cellForRowsAt(indexPath: index)[index.row]
        guard let cell = crimeWatchCollectionView.cellForItem(at: index) as? SecurityInfoCell else {
            return
        }
        if cell.crimeIsLiked == true {
            likeButtonClicked(index: index)
            cell.loveButton.isHidden = true
            cell.noLoveButton.isHidden = false
            cell.noLoveButton.setTitle(" \(securityWatchData.likesCount  - 1)", for: .normal)
            liked = ""
        } else {
            likeButtonClicked(index: index)
            cell.noLoveButton.isHidden =  false
            cell.loveButton.isHidden = true
            cell.noLoveButton.setTitle(" \((securityWatchData.likesCount))", for: .normal)
            liked = ""
        }
    }
    
    func onClickNoLoveButton(index: IndexPath) {
        let securityWatchData = searchSecurityWatchViewModel.cellForRowsAt(indexPath: index)[index.row]
        print("no love")
        guard let cell = crimeWatchCollectionView.cellForItem(at: index) as? SecurityInfoCell else {
            return
        }
        print("no love\(cell.crimeIsLiked)")
       
        if cell.crimeIsLiked == false {
            likeButtonClicked(index: index)
            cell.noLoveButton.isHidden = true
            cell.loveButton.isHidden = false
            cell.loveButton.setTitle(" \((securityWatchData.likesCount) + 1)", for: .normal)
            
            liked = "nice"
        } else {
            likeButtonClicked(index: index)
            cell.noLoveButton.isHidden = true
            cell.loveButton.isHidden = false
            cell.loveButton.setTitle(" \((securityWatchData.likesCount))", for: .normal)

            liked = "nice"
        }
    }
}
