//
//  SecurityInfoCell.swift
//  JustEaseApp
//
//  Created by iOSApp on 03/03/2023.
//

import UIKit
protocol SecurityInfoDelegate: AnyObject {
    func onClickLoveButton(index: IndexPath)
    func onClickNoLoveButton(index: IndexPath)
    func onClickShareButton(index: IndexPath)
}
class SecurityInfoCell: UICollectionViewCell {
    static var identifier: String = "SecurityInfoCell"
    var securityInfoDelegate: SecurityInfoDelegate?
    var index : IndexPath?
    var numberOfLikes = 0
    var crimeIsLiked = false
    var card: Cards? {
        didSet {
            guard let card = card else {
                return
            }
            let forwardImage = card.forwardImage
            defaultImage.setImage(forwardImage, for: .normal)
            
            let cardName = card.title
            nameLabel.text = cardName
        }
    }
    
    lazy var defaultImage: UIButton = {
        let button = UIButton.defaultImageButton()
        button.isHidden = true
        button.layer.cornerRadius = 30
        return button
    }()
    lazy var profileImage:  UIImageView  = {
        let profileImageView =  UIImageView.customImage()
        profileImageView.isHidden = true
        profileImageView.layer.cornerRadius = 30
        return profileImageView
    }()
    lazy var nameLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textAlignment = .left
        label.text = "Ifeanyi Mbata"
        label.textColor = textSystemColor
        label.font = UIFont(name: AppFonts.silkabold.font, size: 12)
        return label
    }()
    lazy var timeLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textAlignment = .left
        label.text = "100 day ago"
        label.textColor = textSystemColor
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 10)
        return label
    }()
    lazy var commentLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textAlignment = .left
        label.textColor = textSystemColor
        label.text = "I love playing footbal with boot in the field"
        label.font = UIFont(name: AppFonts.silkLight.font, size: 12)
        return label
    }()
    lazy var bannerImage :  UIImageView  = {
        let profileImageView =  UIImageView()
        profileImageView.image = UIImage(named: "image")
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.isUserInteractionEnabled = true
        return profileImageView
    }()
    lazy var loveButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.backgroundColor = textFieldColor
        button.titleLabel?.font = UIFont(name:  AppFonts.silkLight.font, size: 12)
        button.setTitleColor(AppColors.green.color, for: .normal)
        button.setImage(UIImage(named: "love"), for: .normal)
        button.setTitle("  1", for: .normal)
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(didTapOnLoveButton(_:)), for: .touchUpInside)
        return button
    }()
    lazy var noLoveButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.backgroundColor = textFieldColor
        button.isHidden = true
        button.titleLabel?.font = UIFont(name:  AppFonts.silkLight.font, size: 12)
        button.setTitleColor(AppColors.green.color, for: .normal)
        button.setImage(UIImage(named: "noLove"), for: .normal)
        button.setTitle("  0", for: .normal)
        button.addTarget(self, action: #selector(didTapOnNoLoveButton(_:)), for: .touchUpInside)
        return button
    }()
    lazy var messageButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.backgroundColor = backgroundSystemColor
        button.setTitleColor(textSystemColor, for: .normal)
        button.layer.cornerRadius = 0
        button.frame.size.width = 55
        button.setTitle("  1", for: .normal)
        button.titleLabel?.font = UIFont(name:  AppFonts.silkLight.font, size: 12)
        button.setImage(AppButtonImages.messageIcon.image, for: .normal)
        return button
    }()
    lazy var shareButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.backgroundColor = backgroundSystemColor
        button.layer.cornerRadius = 0
        button.frame.size.width = 55
        button.setImage(UIImage(named: "share"), for: .normal)
        button.addTarget(self, action: #selector(didTapOnShareButton(_:)), for: .touchUpInside)
        return button
    }()
    @objc func didTapOnShareButton(_ sender: Any){
        guard let indexRow = index else {
            return
        }
        securityInfoDelegate?.onClickShareButton(index: indexRow)
    }
    @objc func didTapOnLoveButton(_ sender: Any){
        guard let indexRow = index else {
            return
        }
        securityInfoDelegate?.onClickLoveButton(index: indexRow)
    }
    @objc func didTapOnNoLoveButton(_ sender: Any){
        guard let indexRow = index else {
            return
        }
        securityInfoDelegate?.onClickNoLoveButton(index: indexRow)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpViews() {
        addSubview(defaultImage)
        addSubview(profileImage)
        addSubview(nameLabel)
        addSubview(timeLabel)
        addSubview(commentLabel)
        addSubview(bannerImage)
        addSubview(loveButton)
        addSubview(messageButton)
        addSubview(shareButton)
        addSubview(noLoveButton)
        
        NSLayoutConstraint.activate([
            defaultImage.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            defaultImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            defaultImage.heightAnchor.constraint(equalToConstant: 60),
            defaultImage.widthAnchor.constraint(equalToConstant: 60),
            
            profileImage.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            profileImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            profileImage.heightAnchor.constraint(equalToConstant: 60),
            profileImage.widthAnchor.constraint(equalToConstant: 60),
            
            nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 30),
            nameLabel.leadingAnchor.constraint(equalTo: defaultImage.trailingAnchor, constant: 10),
            nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            
            timeLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 2),
            timeLabel.leadingAnchor.constraint(equalTo: defaultImage.trailingAnchor, constant: 10),
            timeLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            
            commentLabel.topAnchor.constraint(equalTo:  defaultImage.bottomAnchor, constant: 20),
            commentLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            commentLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            
            bannerImage.topAnchor.constraint(equalTo: defaultImage.bottomAnchor, constant: 100),
            bannerImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            bannerImage.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            bannerImage.heightAnchor.constraint(equalToConstant: 90),
            
            noLoveButton.topAnchor.constraint(equalTo: bannerImage.bottomAnchor, constant: 60),
            noLoveButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            noLoveButton.heightAnchor.constraint(equalToConstant: 40),
            noLoveButton.widthAnchor.constraint(equalToConstant: 60),
            
            messageButton.topAnchor.constraint(equalTo: bannerImage.bottomAnchor, constant: 60),
            messageButton.leadingAnchor.constraint(equalTo: noLoveButton.trailingAnchor, constant: 20),
            messageButton.heightAnchor.constraint(equalToConstant: 40),
            messageButton.widthAnchor.constraint(equalToConstant: 60),
            
            
            shareButton.topAnchor.constraint(equalTo: bannerImage.bottomAnchor, constant: 60),
            shareButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            shareButton.heightAnchor.constraint(equalToConstant: 40),
            shareButton.widthAnchor.constraint(equalToConstant: 60),
            
            loveButton.topAnchor.constraint(equalTo: bannerImage.bottomAnchor, constant: 60),
            loveButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            loveButton.heightAnchor.constraint(equalToConstant: 40),
            loveButton.widthAnchor.constraint(equalToConstant: 60),
            
            
            
        ])
    }
}
