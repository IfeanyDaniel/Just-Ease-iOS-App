//
//  SecurityInfoContraints.swift
//  JustEaseApp
//
//  Created by iOSApp on 03/03/2023.
//

import UIKit
extension SecurityInfoController {
 
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(crimeWatchCollectionView)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = backgroundSystemColor
  
       crimeWatchCollectionView.anchorWithConstantsToTop(view.topAnchor,
                                                     left: view.leftAnchor, bottom: view.bottomAnchor, right:  view.rightAnchor, topConstant: 100, leftConstant: 0, bottomConstant: 50, rightConstant: 0)
    }
}
