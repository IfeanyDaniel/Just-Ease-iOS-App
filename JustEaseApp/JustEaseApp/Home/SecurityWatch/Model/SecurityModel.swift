//
//  SecurityModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/1/23.
//

import Foundation
struct SecurityWatchResponse: Decodable {
    let status: Int
    let message: String
    let data: SecurityWatchData
}
struct SecurityWatchData: Decodable {
    let current_page: Int
    let data: [SecurityWatchInfo]
    let total: Int
}
struct SecurityWatchInfo: Decodable {
    let id: Int
    let user_id: Int
    let report_crime_category_id: Int
    let address: String?
    let report_crime_status: String
    let crime_details: String?
    let anonymous_status: Int
    let alias_name: String?
    let longitude: String
    let latitude: String
    let created_at: String
    let comments_count,report_crime_likes_count: Int
    let is_liked: Bool
    let report_crime_category: ReportCrimeCategory?
    let user: User?
   let report_crime_image: [ReportCrimeImage]
}
struct ReportCrimeCategory : Decodable {
    let id: Int
    let name: String?
}
struct User: Decodable {
    let first_name: String?
    let last_name: String?
    let image_url: String?
}
struct ReportCrimeImage: Codable {
    let report_crime_id: String
    let image_url: String?
}


//"status": 200,
//    "message": "Request was successful",
//    "data": {
//        "current_page": 1,
//        "data": [
//            {
//                "id": 68,
//                "user_id": 33,
//                "report_crime_category_id": 24,
//                "address": "FPFJ+WG8, Oke Arin St, 105101, Lekki, Lagos, Nigeria",
//                "report_crime_status": "Ongoing",
//                "crime_details": "arm robbery",
//                "anonymous_status": 1,
//                "alias_name": null,
//                "longitude": "3.7313931",
//                "latitude": "6.4755943",
//                "created_at": "2023-05-03 16:51:35",
//                "distance": 0.01274398087781606,
//                "comments_count": 0,
//                "report_crime_likes_count": 0,
//                "is_liked": false,
//                "report_crime_category": {
//                    "id": 24,
//                    "name": "Blackmail"
//                },
//                "user": {
//                    "first_name": "Ifeanyi",
//                    "last_name": "Mbata",
//                    "phone_number": "09039270990",
//                    "email": "ifeanyimbata3@gmail.com",
//                    "address": null,
//                    "image_url": null,
//                    "email_verified_at": null,
//                    "longitude": "3.73167",
//                    "latitude": "6.4755271"
//                },
//                "report_crime_image": [
//                    {
//                        "id": 109,
//                        "report_crime_id": "68",
//                        "image_url": "http://kmr-staging.lawpavilion.com/storage/images/report_crime/68/1683132695crime_image1.jpg"
//                    }
//                ],
//                "report_crime_address": {
//                    "id": 89,
//                    "report_crime_id": 68,
//                    "postal_code": "105101",
//                    "country": "Nigeria",
//                    "admin_area": "Lagos",
//                    "sub_admin_area": "Ibeju Lekki",
//                    "locality": "Lekki",
//                    "sub_locality": null
//                }
//            },
