//
//  CreateCrimeComment.swift
//  JustEaseApp
//
//  Created by iOSApp on 31/05/2023.
//
import Foundation
struct CreateCrimeCommentRequest: Encodable {
   let user_id: Int
   let content : String
}
