//
//  lgResponse.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/25/23.
//

import Foundation
struct LgSecurityResponse: Decodable {
    let status: Int
    let message: String
    let data: [LgSecurityData]
}
struct LgSecurityData: Decodable {
    let sub_admin_area: String
}
