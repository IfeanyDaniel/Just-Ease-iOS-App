//
//  CreateCrimeCommentResponse.swift
//  JustEaseApp
//
//  Created by iOSApp on 31/05/2023.
//

import Foundation
struct CreateCrimeCommentResponse: Decodable {
    let status: Int
    let message: String
}
