//
//  StateSecurityResponse.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/25/23.
//

import Foundation
struct StateSecurityResponse: Decodable {
    let status: Int
    let message: String
    let data: [StateSecurityData]
}
struct StateSecurityData: Decodable {
    let admin_area: String
}
