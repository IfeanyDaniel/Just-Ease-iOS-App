//
//  ReadCrimeCommentResponse.swift
//  JustEaseApp
//
//  Created by iOSApp on 31/05/2023.
//

import Foundation
struct ReadCrimeCommentResponse: Decodable {
    let status: Int
    let message: String
    let data: [ReadCrimeCommentData]
}
struct ReadCrimeCommentData: Decodable {
    let id: Int
    let user_id: Int
    let report_crime_category_id: Int
    let report_crime_address: String?
    let report_crime_status: String
    let crime_details: String?
    let anonymous_status: Int
    let alias_name: String?
    let longitude: String
    let latitude: String
    let created_at: String
    let comments_count,report_crime_likes_count,report_crime_view_count,report_crime_share_count: Int
    let is_liked: Bool
    let report_crime_category: ReadCrimeCommentCategory?
    let user: UserReadCrimeComment?
    let report_crime_image: [ReadCrimeCommentImage]
    let comments: [ReadCrimeCommment]
}
struct ReadCrimeCommentCategory : Decodable {
    let id: Int
    let name: String?
}
struct UserReadCrimeComment: Decodable {
    let first_name: String?
    let last_name: String?
}
struct ReadCrimeCommentImage:  Decodable {
    let report_crime_id: String
    let image_url: String?
}

struct ReadCrimeCommment: Decodable {
    let id: Int
    let user_id: Int
    let commentable_type: String
    let commentable_id: Int
    let content: String
    let created_at: String
    let comment_likes_count: Int
    let is_liked: Bool
    let user: UserCommentDetails?
}
struct UserCommentDetails: Decodable {
    let first_name: String
    let last_name: String
    let phone_number: String?
    let email: String?
    let image_url: String?
}
