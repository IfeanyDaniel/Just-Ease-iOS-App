//
//  CreateCommentViewModel.swift
//  JustEaseApp
//
//  Created by iOSApp on 31/05/2023.
//

import Foundation
protocol CreateCrimeCommentDelegate {
    func didReceiveCreateCrimeCommentResponse(createCrimeResponse: CreateCrimeCommentResponse?,_ statusCode: Int)
}
class CreateCrimeCommentViewModel {
    var delegate: CreateCrimeCommentDelegate?
    func getCreateCrimeCommentResponse(reportCrimeId: Int, createCrimeCommentRequest: CreateCrimeCommentRequest) {
        let createCrimeCommentResource = CreateCrimeCommentResource()
        createCrimeCommentResource.getResponse(reportCrimeId: reportCrimeId, createCrimeCommentRequest: createCrimeCommentRequest) { createCrimeCommentApiResponse, statusCode in
            if statusCode ==  200 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateCrimeCommentResponse(createCrimeResponse: createCrimeCommentApiResponse, statusCode)
                }
            }
            if statusCode ==  400 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateCrimeCommentResponse(createCrimeResponse: createCrimeCommentApiResponse, statusCode)
                }
            }
            if statusCode ==  422 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateCrimeCommentResponse(createCrimeResponse: createCrimeCommentApiResponse, statusCode)
                }
            }
            if statusCode ==  500 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateCrimeCommentResponse(createCrimeResponse: createCrimeCommentApiResponse, statusCode)
                }
            }
            if statusCode ==  200 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateCrimeCommentResponse(createCrimeResponse: createCrimeCommentApiResponse, statusCode)
                }
            }
        }
    }
}
