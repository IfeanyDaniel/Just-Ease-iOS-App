//
//  ReadCrimeCommentViewModel.swift
//  JustEaseApp
//
//  Created by iOSApp on 31/05/2023.
//

import Foundation
struct ReadCrimeComment {
    let firstName: String
    let lastName: String
    let image: String
    let commentLikeCount: Int
    let isLiked: Bool
    let createdAt: Date
    let content: String
}
class ReadCrimeCommentViewModel {
    var readCrimeComment :[ReadCrimeComment] = []
    func getCrimeComment(reportCrimeid: Int, userId: Int, completion: @escaping () -> Void ) {
        let readCrimeCommentResource = ReadCrimeCommentResource()
        readCrimeCommentResource.getReadCrimeComment(reportCrimeId: reportCrimeid, user_id: userId) { getReadCrimeCommentApiResponse in
            DispatchQueue.main.async { [self] in
                let readCrimeCommentData = getReadCrimeCommentApiResponse?.data[0].comments
                readCrimeComment.removeAll()
                for index in 0..<(readCrimeCommentData?.count ?? 0) {
                    let firstName = readCrimeCommentData?[index].user?.first_name ?? ""
                    let lastName =  readCrimeCommentData?[index].user?.last_name ?? ""
                    let imageUrl = readCrimeCommentData?[index].user?.image_url ?? ""
                    let commentCount = readCrimeCommentData?[index].comment_likes_count ?? 0
                    let isLiked = readCrimeCommentData?[index].is_liked
                    let dateCreated = readCrimeCommentData?[index].created_at ?? ""
                    let userComment = readCrimeCommentData?[index].content ?? ""
                    readCrimeComment.append(ReadCrimeComment(firstName: firstName, lastName: lastName, image: imageUrl, commentLikeCount: commentCount, isLiked: isLiked ?? false, createdAt: dateCreated.toDate(format: "yyyy-MM-dd HH:mm:ss").adding(minutes: 60), content: userComment))
                    readCrimeComment = readCrimeComment.sorted(by: {$0.createdAt > $1.createdAt})
                }
                completion()
            }
        }
    }
    func numberOfRowsInSection() -> [ReadCrimeComment]  {
        return readCrimeComment
    }
    func cellForRowsAt(indexPath: IndexPath) -> [ReadCrimeComment] {
        return readCrimeComment
    }
}
