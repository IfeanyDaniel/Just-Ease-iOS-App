//
//  StateSecurityViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/25/23.
//


import Foundation
struct StateLocation: Codable {
    var name: String
}
class StateLocationViewModel {
    var stateLocation :[StateLocation] = []
    func getstateLocations(completion: @escaping () -> Void) {
        let stateLocationResource = StateSecurityResource()
        stateLocationResource.getSecurityWatch { stateLocationApiResponse in
            DispatchQueue.main.async { [self] in
                let states = stateLocationApiResponse?.data
                stateLocation.removeAll()
                for index in 0..<(states?.count ?? 0) {
                    let area = states?[index].admin_area ?? ""
                    stateLocation.append(StateLocation(name: area))
                    Storage.saveStateLocation(state: stateLocation)
                }
                completion()
            }
        }
    }
}


