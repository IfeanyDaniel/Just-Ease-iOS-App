//
//  lgViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/25/23.
//

import Foundation
struct LgsLocation: Codable {
    var name: String
}
class LgLocationViewModel {
    var lgLocation :[LgsLocation] = []
    func getLgLocations(state: String, completion: @escaping () -> Void) {
        let lgLocationResource = LgLocationSecurityResource()
        lgLocationResource.getSecurityWatch(state: state) { lgLocationApiResponse in
            DispatchQueue.main.async { [self] in
                let lgs = lgLocationApiResponse?.data
                lgLocation.removeAll()
                for index in 0..<(lgs?.count ?? 0) {
                    let subAreas = lgs?[index].sub_admin_area ?? ""
                    lgLocation.append(LgsLocation(name: subAreas))
                    Storage.saveLocalGovernemnt(lgs: lgLocation)
                }
                completion()
            }
        }
    }
}

