//
//  CreateCrimeLikeViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 6/8/23.
//

import Foundation
protocol CreateCrimeLikeViewModelDelegate {
    func didReceiveCreateCrimeLikeResponse(createCrimeLikeResponse: CreateCrimeLikeResponse?,_ statusCode: Int)
}
class CreateCrimeLikeViewModel {
    var delegate: CreateCrimeLikeViewModelDelegate?
    func getCreateCrimeLikeResponse(reportCrimeId: Int, createCrimeLikeRequest: CreateCrimeLikeRequest) {
        let createCrimeLikeResource = CreateCrimeLikeResource()
        createCrimeLikeResource.getResponse(reportCrimeId: reportCrimeId, creatCrimeLikeRequest: createCrimeLikeRequest) { createCrimeLikeApiResponse, statusCode in
            if statusCode == 200 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateCrimeLikeResponse(createCrimeLikeResponse: createCrimeLikeApiResponse, statusCode)
                }
            }
            if statusCode == 400 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateCrimeLikeResponse(createCrimeLikeResponse: createCrimeLikeApiResponse, statusCode)
                }
            }
            if statusCode == 422 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateCrimeLikeResponse(createCrimeLikeResponse: createCrimeLikeApiResponse, statusCode)
                }
            }
            if statusCode == 500 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveCreateCrimeLikeResponse(createCrimeLikeResponse: createCrimeLikeApiResponse, statusCode)
                }
            }
        }
    }
}
