//
//  SecurityViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/1/23.
//

import Foundation
struct SecurityWatchReportCrime {
    let id : String
    let reportCrimeImage: String?
}
struct SecurityWatch {
    let id : Int
    let userId: Int
    let categoryId: Int
    let address: String
    let crimeStatus: String
    let createdAt: Date
    let reportCrimeCategorId: Int
    let reportCrimeCategoryName: String
    let firstName: String
    let lastName: String
    let reportCrime: [SecurityWatchReportCrime]
}

class SecurityWatchViewModel {
    var securityWatch: [SecurityWatch] = []
    var reportCrimeImages :[SecurityWatchReportCrime] = []
    func getSecurityWatch(longitude: String, latitude: String, search_scope: String, userId: Int, completion: @escaping () -> Void ) {
        let searchSecurityResource = SecurityWatchResource()
        searchSecurityResource.getSearchSecurityWatch(long: longitude, lat: latitude, search_scope: search_scope, user_id: userId) { getSecurityWatchApiResponse in
            DispatchQueue.main.async { [self] in
                let securityWatchTotalCount = getSecurityWatchApiResponse?.data.total ?? 0
                let securityWatchData = getSecurityWatchApiResponse?.data.data
                Storage.saveSecurityWatch(number: securityWatchTotalCount)
                securityWatch.removeAll()
                for index in 0..<(securityWatchData?.count ?? 0) {
                    let  id = securityWatchData?[index].id ?? 0
                    let  userId = securityWatchData?[index].user_id ?? 0
                    let  categoryId = securityWatchData?[index].report_crime_category_id ?? 0
                    let  address = securityWatchData?[index].address ?? ""
                    let  crimeStatus = securityWatchData?[index].report_crime_status ?? ""
                    let  createdAt = securityWatchData?[index].created_at ?? ""
                    let  reportCrimeCategoryId = securityWatchData?[index].report_crime_category?.id ?? 0
                    let reportCrimeCategoryName = securityWatchData?[index].report_crime_category?.name  ?? ""
                    let  firstName = securityWatchData?[index].user?.first_name ?? ""
                    let  lastName = securityWatchData?[index].user?.last_name ?? ""
                    let reportCrimeImage = securityWatchData?[index].report_crime_image
                    
                    for index in  0..<(reportCrimeImage?.count ?? 0) {
                        let imageUrl = reportCrimeImage?[index].image_url ?? ""
                        let imageId = reportCrimeImage?[index].report_crime_id ?? ""
                        reportCrimeImages.append(SecurityWatchReportCrime(id: imageId, reportCrimeImage: imageUrl))
                    }
                    securityWatch.append(SecurityWatch(id: id, userId: userId, categoryId: categoryId, address: address, crimeStatus: crimeStatus, createdAt: createdAt.toDate(format: "yyyy-MM-dd HH:mm:ss").adding(minutes: 60), reportCrimeCategorId: reportCrimeCategoryId, reportCrimeCategoryName: reportCrimeCategoryName, firstName: firstName, lastName: lastName, reportCrime: reportCrimeImages.compactMap({return $0})))
                   
                    securityWatch = securityWatch.sorted(by: {$0.createdAt > $1.createdAt})
                    
                }
                completion()
            }
        }
    }
    func numberOfRowsInSection() -> [SecurityWatch ]  {
        return securityWatch
    }
    func cellForRowsAt(indexPath: IndexPath) -> [SecurityWatch] {
        return securityWatch
    }
}

//import Foundation
//struct ReportCrime {
//    let id : String
//    let reportCrimeImage: String?
//}
//struct SecurityWatch{
//    let id : Int
//    let userId: Int
//    let categoryId: Int
//    let address: String
//    let crimeStatus: String
//    let createdAt: Date
//    let reportCrimeCategorId: Int
//    let reportCrimeCategoryName: String
//    let firstName: String
//    let lastName: String
//    let reportCrime: [ReportCrime]
//}
//
//class SecurityWatchViewModel {
//    var securityWatch :[SecurityWatch] = []
//    var reportCrimeImages :[ReportCrime] = []
//    func getSecurityWatch(completion: @escaping () -> Void ) {
//        let securityWatchResource = SecurityWatchResource()
//        securityWatchResource.getSecurityWatch { getSecurityWatchApiResponse in
//            DispatchQueue.main.async { [self] in
//                let securityWatchTotalCount = getSecurityWatchApiResponse?.data.total ?? 0
//                let securityWatchData = getSecurityWatchApiResponse?.data.data
//                Storage.saveSecurityWatch(number: securityWatchTotalCount)
//                securityWatch.removeAll()
//                for index in 0..<(securityWatchData?.count ?? 0) {
//                    let  id = securityWatchData?[index].id ?? 0
//                    let  userId = securityWatchData?[index].user_id ?? 0
//                    let  categoryId = securityWatchData?[index].report_crime_category_id ?? 0
//                    let  address = securityWatchData?[index].report_crime_address ?? ""
//                    let  crimeStatus = securityWatchData?[index].report_crime_status ?? ""
//                    let  createdAt = securityWatchData?[index].created_at ?? ""
//                    let  reportCrimeCategoryId = securityWatchData?[index].report_crime_category.id ?? 0
//                    let reportCrimeCategoryName = securityWatchData?[index].report_crime_category.name ?? ""
//                    let  firstName = securityWatchData?[index].user?.first_name ?? ""
//                    let  lastName = securityWatchData?[index].user?.last_name ?? ""
//                    let reportCrimeImage = securityWatchData?[index].report_crime_image
//
//                    for index in  0..<(reportCrimeImage?.count ?? 0) {
//                        let imageUrl = reportCrimeImage?[index].image_url ?? ""
//                        let imageId = reportCrimeImage?[index].report_crime_id ?? ""
//                        reportCrimeImages.append(ReportCrime(id: imageId, reportCrimeImage: imageUrl))
//                    }
//                    securityWatch.append(SecurityWatch(id: id, userId: userId, categoryId: categoryId, address: address, crimeStatus: crimeStatus, createdAt: createdAt.toDate(format: "yyyy-MM-dd HH:mm:ss").adding(minutes: 60), reportCrimeCategorId: reportCrimeCategoryId, reportCrimeCategoryName: reportCrimeCategoryName, firstName: firstName, lastName: lastName, reportCrime: reportCrimeImages.compactMap({return $0})))
//
//                    securityWatch = securityWatch.sorted(by: {$0.createdAt > $1.createdAt})
//
//                }
//                completion()
//            }
//        }
//    }
//    func numberOfRowsInSection() -> [SecurityWatch]  {
//        return securityWatch
//    }
//    func cellForRowsAt(indexPath: IndexPath) -> [SecurityWatch] {
//        return securityWatch
//    }
//}
