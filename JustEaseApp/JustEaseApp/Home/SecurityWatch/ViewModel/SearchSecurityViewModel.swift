//
//  SearchSecurityViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/28/23.
//

import Foundation
struct SearchReportCrime {
    let id : String
    let reportCrimeImage: String?
}
struct SearchSecurityWatch {
    let id : Int
    let userId: Int
    let categoryId: Int
    let image_url: String
    let address: String
    let crimeDetails: String
    let crimeStatus: String
    let createdAt: Date
    let commentCount: Int
    let reportCrimeCategorId: Int
    let reportCrimeCategoryName: String
    let firstName: String
    let lastName: String
    let likesCount: Int
    let isliked: Bool
    let reportCrime: [SearchReportCrime]
}

class SearchSecurityWatchViewModel {
    var securityWatch: [SearchSecurityWatch] = []
    var reportCrimeImages :[SearchReportCrime] = []
    func getSecurityWatch(longitude: String, latitude: String, search_scope: String, query: String, userId: Int, completion: @escaping () -> Void ) {
        let searchSecurityResource = SearchSecurityWatchResource()
        searchSecurityResource.getSearchSecurityWatch(long: longitude, lat: latitude, search_scope: search_scope,search_query: query, user_id: userId) { getSecurityWatchApiResponse in
            DispatchQueue.main.async { [self] in
                let securityWatchTotalCount = getSecurityWatchApiResponse?.data.total ?? 0
                let securityWatchData = getSecurityWatchApiResponse?.data.data
                Storage.saveSecurityWatch(number: securityWatchTotalCount)
                securityWatch.removeAll()
                for index in 0..<(securityWatchData?.count ?? 0) {
                    let  id = securityWatchData?[index].id ?? 0
                    let  userId = securityWatchData?[index].user_id ?? 0
                    let  categoryId = securityWatchData?[index].report_crime_category_id ?? 0
                    let  address = securityWatchData?[index].address ?? ""
                    let  crimeStatus = securityWatchData?[index].report_crime_status ?? ""
                    let  createdAt = securityWatchData?[index].created_at ?? ""
                    let  reportCrimeCategoryId = securityWatchData?[index].report_crime_category?.id ?? 0
                    let reportCrimeCategoryName = securityWatchData?[index].report_crime_category?.name ?? ""
                    let  firstName = securityWatchData?[index].user?.first_name ?? ""
                    let  lastName = securityWatchData?[index].user?.last_name ?? ""
                    let reportCrimeImage = securityWatchData?[index].report_crime_image
                    let crimeDetails = securityWatchData?[index].crime_details
                    let commentCount = securityWatchData?[index].comments_count ?? 0
                    let likesCount = securityWatchData?[index].report_crime_likes_count ?? 0
                    let isliked = securityWatchData?[index].is_liked ?? false
                    let userImageUrl = securityWatchData?[index].user?.image_url ?? ""
                    
                    for index in  0..<(reportCrimeImage?.count ?? 0) {
                        let imageUrl = reportCrimeImage?[index].image_url ?? ""
                        let imageId = reportCrimeImage?[index].report_crime_id ?? ""
                        reportCrimeImages.append(SearchReportCrime(id: imageId, reportCrimeImage: imageUrl))
                    }
                    securityWatch.append(SearchSecurityWatch(id: id, userId: userId, categoryId: categoryId, image_url: userImageUrl, address: address, crimeDetails: crimeDetails ?? "", crimeStatus: crimeStatus, createdAt: createdAt.toDate(format: "yyyy-MM-dd HH:mm:ss").adding(minutes: 60), commentCount: commentCount, reportCrimeCategorId: reportCrimeCategoryId, reportCrimeCategoryName: reportCrimeCategoryName, firstName: firstName, lastName: lastName,likesCount: likesCount, isliked: isliked, reportCrime: reportCrimeImages.compactMap({return $0})))
                   
                    securityWatch = securityWatch.sorted(by: {$0.createdAt > $1.createdAt})
                    
                }
                completion()
            }
        }
    }
    func numberOfRowsInSection() -> [SearchSecurityWatch ]  {
        return securityWatch
    }
    func cellForRowsAt(indexPath: IndexPath) -> [SearchSecurityWatch] {
        return securityWatch
    }
}
