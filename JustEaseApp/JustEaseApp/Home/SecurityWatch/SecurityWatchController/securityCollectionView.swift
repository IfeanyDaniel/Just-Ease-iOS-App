//
//  SecurityCollectionView.swift
//  JustEaseApp
//
//  Created by iOSApp on 03/03/2023.
//


import UIKit
extension SecurityWatchController {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == securityCollectionView {
            return  securityWatchViewModel.numberOfRowsInSection().count
        } else {
            return  searchSecurityWatchViewModel.numberOfRowsInSection().count
        }
       
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == securityCollectionView {
            guard let cell = securityCollectionView.dequeueReusableCell(withReuseIdentifier: SecurityCollectionViewCell.identifier, for: indexPath) as? SecurityCollectionViewCell else { return UICollectionViewCell() }
            let securityWatchData = securityWatchViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
            cell.cardLabel.text = securityWatchData.address
            cell.button.setTitle(securityWatchData.reportCrimeCategoryName, for: .normal)
            cell.index = indexPath
            cell.securityCellDelegate = self
            let timeAgo = securityWatchData.createdAt
            cell.daysLabel.text = timeAgo.timeAgoDisplay()
            cell.backgroundColor = backgroundSystemColor
            cell.layer.borderColor = AppColors.green.color.cgColor
            cell.layer.borderWidth = 0.5
            cell.layer.cornerRadius = 5
            cell.layer.shadowOpacity = 0.5
            cell.layer.shadowOffset = .zero
            cell.layer.shadowRadius = 0.2
            return cell
        } else {
            guard let cell = searchSecurityCollectionView.dequeueReusableCell(withReuseIdentifier: SecurityCollectionViewCell.identifier, for: indexPath) as? SecurityCollectionViewCell else { return UICollectionViewCell() }
            let securityWatchData = searchSecurityWatchViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
            cell.cardLabel.text = securityWatchData.address
            cell.button.setTitle(securityWatchData.reportCrimeCategoryName, for: .normal)
            cell.index = indexPath
            cell.securityCellDelegate = self
            let timeAgo = securityWatchData.createdAt
            cell.daysLabel.text = timeAgo.timeAgoDisplay()
            cell.backgroundColor = backgroundSystemColor
            cell.layer.borderColor = AppColors.green.color.cgColor
            cell.layer.borderWidth = 0.5
            cell.layer.cornerRadius = 5
            cell.layer.shadowOpacity = 0.5
            cell.layer.shadowOffset = .zero
            cell.layer.shadowRadius = 0.2
            return cell
        }
       
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 50, height: 150)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        navigationController?.pushViewController(SecurityInfoController(), animated: true)
    }
}
