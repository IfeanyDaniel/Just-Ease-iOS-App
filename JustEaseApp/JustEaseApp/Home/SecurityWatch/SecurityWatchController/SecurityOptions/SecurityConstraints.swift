//
//  SecurityConstraints.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/21/23.
//

import UIKit
extension SecurityOptionsController {
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(tableView)
        view.backgroundColor =  viewStackColor
        tableView.anchorWithConstantsToTop(view.topAnchor,
                                                     left: view.leftAnchor, bottom: view.bottomAnchor, right:  view.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
    }
}
