//
//  SecurityOptionsController.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/21/23.
//

import UIKit
protocol MoveToNextSecurityDelegate: AnyObject {
    func goToChangeLocationScreen()
    func goToMapAnalysisScreen()
}
class SecurityOptionsController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var delegate : MoveToNextSecurityDelegate?
    lazy var tableView : UITableView = {
        let table = UITableView()
        table.backgroundColor = viewStackColor
        table.rowHeight = 60
        table.sizeToFit()
        table.delegate = self
        table.dataSource = self
        table.isScrollEnabled = false
        table.separatorColor = AppColors.lightGray.color
        return table
    }()
    let page: [String] = {
        let location = "Change Location"
        let mapAnalysis = "View on map analysis"
        return [location,mapAnalysis]
    }()
    func registercCell() {
        tableView.register(TableViewCell.self, forCellReuseIdentifier: TableViewCell.identifier)

    }

    override func viewDidLoad() {
         layoutViews()
         registercCell()
    }
    
}
