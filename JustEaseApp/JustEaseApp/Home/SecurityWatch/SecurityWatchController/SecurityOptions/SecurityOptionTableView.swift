//
//  SecurityOptionTableView.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/21/23.
//

import UIKit
extension SecurityOptionsController{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return page.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.identifier, for: indexPath) as? TableViewCell else { return UITableViewCell() }
        cell.nameLabel.text = page[indexPath.row]
        cell.backgroundColor = viewStackColor
        cell.selectionStyle = .none
        return cell
    }
       
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: false)
        if page[indexPath.row] == "Change Location" {
            delegate?.goToChangeLocationScreen()
        } else {
             delegate?.goToMapAnalysisScreen()
        }
    }
}
