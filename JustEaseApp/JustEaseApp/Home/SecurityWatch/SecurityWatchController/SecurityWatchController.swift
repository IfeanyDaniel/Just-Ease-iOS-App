//
//  SecurityWatchController.swift
//  JustEaseApp
//
//  Created by iOSApp on 03/03/2023.
//

import UIKit
import FittedSheets
import GoogleMaps
import CoreLocation
import MapKit
class SecurityWatchController: UIViewController,UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout, UITextFieldDelegate  {
    var securityWatchViewModel = SecurityWatchViewModel()
    var searchSecurityWatchViewModel = SearchSecurityWatchViewModel()
    var timer: Timer?
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Incident around you"
        label.textColor = textSystemColor
        label.font = UIFont(name: AppFonts.silkabold.font, size: 14)
        label.textAlignment = .left
        return label
    }()
   
    lazy var searchTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Search for areas",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.addTarget(self, action: #selector(textFieldValDidChange(_:)), for: .editingChanged)
        textField.autocapitalizationType = .none
        return textField
    }()
    lazy var noContentView : UIView = {
        let content = UIView()
        content.backgroundColor = backgroundSystemColor
        content.isHidden = true
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    lazy var noRecordIcon: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.noBookmarkIcon.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var noContentLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Oops! not found"
        label.textAlignment = .center
        label.font = UIFont(name: AppFonts.silkabold.font, size: 14)
        return label
    }()
    lazy var instructionLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textColor = AppColors.lightNavyBlue.color
        label.text = "Please check back for content."
        label.textAlignment = .center
        return label
    }()
    // MARK: - ... Validation of all search field
    @objc func textFieldValDidChange(_ textField: UITextField) {
        timer?.invalidate()
        //setup timer
        timer = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.searchText), userInfo: nil, repeats: false)
        
    }
    @objc func searchText() {
        guard let currentLocation = locManager.location else {
            return
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.6) { [weak self] in
            if self?.searchTextField.text != "" {
                Loader.shared.showLoader()
                self?.searchSecurityWatchViewModel.getSecurityWatch(longitude: "\(currentLocation.coordinate.longitude)", latitude: "\(currentLocation.coordinate.latitude)", search_scope: "all_crime", query: self?.searchTextField.text ?? "", userId: Storage.getUserId()){
                    Loader.shared.hideLoader()
                    if self?.searchSecurityWatchViewModel.securityWatch.count ?? 0 > 0 {
                            self?.searchSecurityCollectionView.reloadData()
                            self?.searchSecurityCollectionView.isHidden = false
                            self?.securityCollectionView.isHidden = true
                            self?.noContentView.isHidden = true
                    } else {
                        self?.searchSecurityCollectionView.isHidden = true
                        self?.securityCollectionView.isHidden = true
                        self?.noContentView.isHidden = false
                    }
                    
                }
            } else {
                self?.fetchSecurityWatchList()
            }
        }
    }
    lazy var securityCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 15
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        cv.backgroundColor = backgroundSystemColor
        cv.dataSource = self
        cv.delegate = self
        cv.showsVerticalScrollIndicator = true
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    lazy var searchSecurityCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 15
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        cv.backgroundColor = backgroundSystemColor
        cv.dataSource = self
        cv.delegate = self
        cv.showsVerticalScrollIndicator = true
        cv.isHidden = true
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    func showDoneButton(){
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dropKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.searchTextField.inputAccessoryView = toolbar
    }
    @objc func dropKeyboard(){
        view.endEditing(true)
    }
    // Implement the UITextFieldDelegate method to handle text changes
     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         // Check if the text field already contains text
         if let currentText = textField.text {
             // Find the index of the first letter in the current text
             if let firstLetterIndex = currentText.firstIndex(where: { !$0.isWhitespace }) {
                 // Check if the replacement string starts with spaces
                 if string.first == " " && string.prefix(upTo: firstLetterIndex).contains(" ") {
                     // If the replacement string starts with spaces after the first letter, ignore those spaces
                     textField.text = String(currentText.prefix(upTo: firstLetterIndex)) + string.trimmingCharacters(in: .whitespacesAndNewlines)
                     return false
                 }
             }
         }

         // Allow other characters to be entered
         return true
     }
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        showDoneButton()
        layoutViews()
        self.hideKeyboardWhenTappedAround()
        registercCell()
        fetchSecurityWatchList()
        searchTextField.delegate = self
    }
    func fetchSecurityWatchList() {
        guard let currentLocation = locManager.location else {
            return
        }
        Loader.shared.showLoader()
        self.securityWatchViewModel.getSecurityWatch(longitude: "\(currentLocation.coordinate.longitude)", latitude: "\(currentLocation.coordinate.latitude)", search_scope: "crime_near_me", userId: Storage.getUserId()){
            Loader.shared.hideLoader()
            DispatchQueue.main.async { [self] in
                self.searchSecurityCollectionView.isHidden = true
                self.securityCollectionView.isHidden = false
                self.securityCollectionView.reloadData()
            }
        }
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
        navigationItem.title = "Security Watch"
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "slider.horizontal.3"), style: .plain, target: self, action: #selector(goToSecurityOptions))
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationItem.rightBarButtonItem?.tintColor = textSystemColor
    }
    @objc func goToSecurityOptions() {
        let controller = SecurityOptionsController()
        controller.delegate = self
        let sheetController = SheetViewController(controller: controller, sizes: [.fixed(250), .fixed(250)])
        sheetController.blurBottomSafeArea = false
        sheetController.topCornersRadius = 30
        sheetController.handleView.isHidden = true
        self.present(sheetController, animated: false, completion: nil)
    }
    func registercCell() {
        securityCollectionView.register(SecurityCollectionViewCell.self, forCellWithReuseIdentifier: SecurityCollectionViewCell.identifier)
        searchSecurityCollectionView.register(SecurityCollectionViewCell.self, forCellWithReuseIdentifier: SecurityCollectionViewCell.identifier)
    }
}
extension SecurityWatchController: SecurityCellDelegate , MoveToNextSecurityDelegate, DisplayContentDelegate {
    func displaySelectedContent(item: String, searchQuery: String) {
        label.text = item
        Loader.shared.showLoader()
        guard let currentLocation = locManager.location else {
            return
        }
        self.searchSecurityWatchViewModel.getSecurityWatch(longitude: "\(currentLocation.coordinate.longitude)", latitude: "\(currentLocation.coordinate.latitude)", search_scope: "all_crime", query: searchQuery, userId: Storage.getUserId()){
            Loader.shared.hideLoader()
            if self.searchSecurityWatchViewModel.securityWatch.count > 0 {
                    self.searchSecurityCollectionView.reloadData()
                    self.searchSecurityCollectionView.isHidden = false
                    self.securityCollectionView.isHidden = true
                    self.noContentView.isHidden = true
            } else {
                self.searchSecurityCollectionView.isHidden = true
                self.securityCollectionView.isHidden = true
                self.noContentView.isHidden = false
            }
            
        }
    }
    
    func goToChangeLocationScreen() {
        let controller = LocationController()
        controller.delegate = self
        let sheetController = SheetViewController(controller: controller, sizes: [.fixed(450), .fixed(450)])
        sheetController.blurBottomSafeArea = false
        sheetController.topCornersRadius = 30
        sheetController.handleView.isHidden = true
        self.present(sheetController, animated: false, completion: nil)
    }
    
    func goToMapAnalysisScreen() {
        let controller = MapAnalysisController()
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func didTapOnShare(index: IndexPath) {
        if searchTextField.text == "" {
            let securityWatchData = securityWatchViewModel.cellForRowsAt(indexPath: index)[index.row]
            let shareSheetVC = UIActivityViewController(
                activityItems: [securityWatchData.address], applicationActivities: nil
            )
            self.present(shareSheetVC, animated: true)
        } else {
        let securityWatchData = searchSecurityWatchViewModel.cellForRowsAt(indexPath: index)[index.row]
        let shareSheetVC = UIActivityViewController(
            activityItems: [securityWatchData.address], applicationActivities: nil
        )
        self.present(shareSheetVC, animated: true)
    }
    }
}
