//
//  MapAnalysisController.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/21/23.
//

import MapKit
import UIKit
import GoogleMaps
import CoreLocation
import MapKit

class MapAnalysisController: UIViewController , UITextFieldDelegate, CrimeDelegate, GetAllCrimesCategoryDelegate {
    func updateCrimeField(with text: String, crimeId: Int) {
            categoryButton.setTitle("  \(text)", for: .normal)
            categoryButton.setTitleColor(textSystemColor, for: .normal)
    }
    var crimesViewModel = CrimeCategoryViewModel()
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var address = ""
    
    lazy var map: GMSMapView = {
        let map = GMSMapView()
        map.translatesAutoresizingMaskIntoConstraints = false
        map.isMyLocationEnabled = true
        map.isUserInteractionEnabled = true
        return map
    }()
  
    @objc func didTapOnCamera() {
        navigationController?.pushViewController(ReportCrimeController(), animated:true)
    }
    // MARK: -  Position
    lazy var locationView: UIView = {
        let content = UIView.stackViewHolderDesign()
        content.backgroundColor = backgroundSystemColor
        content.heightAnchor.constraint(equalToConstant: view.frame.size.height / 2 - 50).isActive = true
        content.layer.cornerRadius = 20
        return content
    }()
   
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.font = UIFont(name: AppFonts.silkaMedium.font ,size: 14)
        label.textColor = AppColors.green.color
        label.text = "Crime rates in Nigeria"
        return label
    }()
    lazy var categoryButton: UIButton = {
        let button = UIButton.selectButtonDesign()
        button.setTitle("   Select Crime", for: .normal)
        button.addTarget(self, action: #selector(didtapOnCategoryButton), for: .touchUpInside)
        return button
    }()
    @objc func didtapOnCategoryButton() {
            let controller = CrimeController()
            controller.delegate = self
            present(controller, animated: true)
    }
    lazy var categoryDownIcon: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.dropDown.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
   
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        checkLocationPermission()
        getlocationOnMap()
        crimesViewModel.delegate = self
        crimesViewModel.getCrimeCategory{}
    }
    
    func getlocationOnMap() {
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            guard let currentLocation = locManager.location else {
                return
            }
            
            map.camera = GMSCameraPosition.camera(withLatitude:  10, longitude:  8, zoom: 7)
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: 10, longitude: 8)
            marker.map = map
        }
    }
    
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    
    //check location services enabled or not
    func checkLocationPermission() {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                //open setting app when location services are disabled
                openSettingApp(message:NSLocalizedString("Please enable your location to use this service.", comment: ""))
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            @unknown default:
                fatalError()
            }
        } else {
            openSettingApp(message:NSLocalizedString("please enable location services to continue using the app", comment: ""))
        }
    }
    
    //open location settings for app
    func openSettingApp(message: String) {
        let alertController = UIAlertController (title: "JustEase", message:message , preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: NSLocalizedString("settings", comment: ""), style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getlocationOnMap()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        navigationItem.leftBarButtonItem?.tintColor = AppColors.black.color
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.setHidesBackButton(true, animated: true)
        
    }
    func didReceiveGetCrimesCategoryResponse(getAllBankResponse: CrimeCategoryResponse?) {}
}

