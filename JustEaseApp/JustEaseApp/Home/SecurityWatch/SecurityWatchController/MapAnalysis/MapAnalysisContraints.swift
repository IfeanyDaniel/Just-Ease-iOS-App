//
//  MapAnalysisContraints.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/21/23.
//

import UIKit
extension MapAnalysisController {
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(map)
        view.addSubview(locationView)
        categoryButton.addSubview(categoryDownIcon)
        locationView.addSubview(categoryButton)
        locationView.addSubview(label)
        self.navigationItem.setHidesBackButton(true, animated: true)
        view.backgroundColor = backgroundSystemColor
        
        NSLayoutConstraint.activate([
            map.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            map.leadingAnchor.constraint(equalTo:view.leadingAnchor, constant: 0),
            map.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            map.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
            locationView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            locationView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            locationView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            
            label.topAnchor.constraint(equalTo: locationView.topAnchor, constant: 35),
            label.leadingAnchor.constraint(equalTo: locationView.leadingAnchor, constant: leadingNumber),
            
            
            categoryButton.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 30),
            categoryButton.leadingAnchor.constraint(equalTo: locationView.leadingAnchor, constant: leadingNumber),
            categoryButton.trailingAnchor.constraint(equalTo: locationView.trailingAnchor, constant: trailingNumber),
            categoryButton.heightAnchor.constraint(equalToConstant: textFieldHeight),
            
            categoryDownIcon.topAnchor.constraint(equalTo: categoryButton.topAnchor, constant: 20),
            categoryDownIcon.trailingAnchor.constraint(equalTo: categoryButton.trailingAnchor, constant: -20),
            
           
        ])
    }
}
