//
//  SecurityWatchCell.swift
//  JustEaseApp
//
//  Created by iOSApp on 03/03/2023.
//

import UIKit
protocol SecurityCellDelegate: AnyObject {
    func didTapOnShare(index: IndexPath)
}
class SecurityCollectionViewCell: UICollectionViewCell {
    static var identifier: String = "securityCollectionCell"
    var securityCellDelegate: SecurityCellDelegate?
    var index : IndexPath?
    lazy var icon: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "Location"), for: .normal)
        button.tintColor = textSystemColor
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 12.5
        return button
    }()
    lazy var cardLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.numberOfLines = 5
        label.text =  "Abuja Municipal Area Council, Lugbe, Federal Capital Territory"
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 13)
        return label
    }()
    lazy var daysLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.numberOfLines = 1
        label.text =  "100 days ago"
        label.textColor = AppColors.placeholderColor.color
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 10)
        return label
    }()
    lazy var button: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Bribery", for: .normal)
        button.layer.cornerRadius = 5
        button.backgroundColor = greenSystemColor
        button.setTitleColor(AppColors.green.color, for: .normal)
        button.titleLabel?.font = UIFont(name:  AppFonts.silkLight.font, size: 12)
        return button
    }()
    lazy var showButton: UIButton = {
        let button = UIButton.stackButtonDesign()
        button.setImage(UIImage(named: "showIcon"), for: .normal)
        button.tintColor = textSystemColor
        button.addTarget(self, action: #selector(didTapOnShow), for: .touchUpInside)
        return button
    }()
    @objc func didTapOnShow() {
       
    }
    lazy var shareButton: UIButton = {
        let button = UIButton.stackButtonDesign()
        button.setImage(UIImage(named: "shareSmallIcon"), for: .normal)
        button.tintColor = textSystemColor
        button.addTarget(self, action: #selector(didTapOnShare(_:)), for: .touchUpInside)
        return button
    }()
    @objc func didTapOnShare(_ sender: Any){
        guard let indexRow = index else {
            return
        }
        securityCellDelegate?.didTapOnShare(index: indexRow)
    }

    lazy var buttonStack: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(showButton)
        stackView.addArrangedSubview(shareButton)
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 20
        return stackView
    }()
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpViews() {
        addSubview(icon)
        addSubview(cardLabel)
        addSubview(daysLabel)
        addSubview(button)
        addSubview(buttonStack)
        
        NSLayoutConstraint.activate([
            icon.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            icon.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            icon.heightAnchor.constraint(equalToConstant: 25),
            icon.widthAnchor.constraint(equalToConstant: 25),
            
            cardLabel.topAnchor.constraint(equalTo: topAnchor, constant: 22),
            cardLabel.leadingAnchor.constraint(equalTo: icon.leadingAnchor, constant: 40),
            cardLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -100),
            
            daysLabel.topAnchor.constraint(equalTo: topAnchor, constant: 22),
            daysLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            
            button.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20),
            button.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            button.heightAnchor.constraint(equalToConstant: 50),
            button.widthAnchor.constraint(equalToConstant: 130),
            
            buttonStack.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20),
            buttonStack.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            buttonStack.heightAnchor.constraint(equalToConstant: 30),
            buttonStack.widthAnchor.constraint(equalToConstant: 80)
        ])
    }
}
