//
//  ChangeLocationController.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/21/23.
//

import UIKit
protocol DisplayContentDelegate: AnyObject {
    func displaySelectedContent(item: String, searchQuery: String)
}
class LocationController: UIViewController ,UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    let locationIncidents = ["Incident Around You","State","Local Government"]
    var locationIncidentsPickerView = UIPickerView()
    var statePickerView = UIPickerView()
    var lgPickerView = UIPickerView()
    var stateLocationViewModel = StateLocationViewModel()
    var lgLocationViewModel = LgLocationViewModel()
    var delegate : DisplayContentDelegate?
    var selectedItem = ""
    var selectedItemToSearch = ""
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textColor = AppColors.green.color
        label.font = UIFont(name: AppFonts.silkabold.font, size: 18)
        label.textAlignment = .left
        label.text = "Location"
        return label
    }()
    lazy var selectLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.textColor = placeholderSystemGrayColor
        label.text = "Select Location"
        label.numberOfLines = 1
        return label
    }()
    lazy var locationTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.text = "Incident Around You"
        textField.heightAnchor.constraint(equalToConstant: 65.0).isActive = true
        textField.inputView = locationIncidentsPickerView
        return textField
    }()
    lazy var dropDownIcon: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.dropDown.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var stateTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Select State",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.isHidden = true
        textField.inputView = statePickerView
        textField.heightAnchor.constraint(equalToConstant: 65.0).isActive = true
        return textField
    }()
    lazy var stateDropDownIcon: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.dropDown.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var lgTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Select Local Government",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.isHidden = true
        textField.inputView = lgPickerView
        textField.heightAnchor.constraint(equalToConstant: 65.0).isActive = true
        return textField
    }()
    lazy var lgDropDownIcon: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.dropDown.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var textFieldStack: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(locationTextField)
        stackView.addArrangedSubview(stateTextField)
        stackView.addArrangedSubview(lgTextField)
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 8
        return stackView
    }()
    lazy var doneButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Done", for: .normal)
        button.addTarget(self, action: #selector(didTapOnDoneButton), for: .touchUpInside)
        return button
    }()
    @objc func didTapOnDoneButton() {
        dismiss(animated: false) {
            self.delegate?.displaySelectedContent(item: self.selectedItem, searchQuery: self.selectedItemToSearch)
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.locationTextField {
            self.locationIncidentsPickerView.selectRow(0, inComponent: 0, animated: true)
            self.pickerView(self.locationIncidentsPickerView, didSelectRow: 0, inComponent: 0)
        }
        if textField == self.stateTextField {
            self.statePickerView.selectRow(0, inComponent: 0, animated: true)
            self.pickerView(self.statePickerView, didSelectRow: 0, inComponent: 0)
        }
        if textField == self.lgTextField {
            self.lgPickerView.selectRow(0, inComponent: 0, animated: true)
            self.pickerView(self.lgPickerView, didSelectRow: 0, inComponent: 0)
        }
    }
    func showDoneButton(){
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dropKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.locationTextField.inputAccessoryView = toolbar
        self.stateTextField.inputAccessoryView = toolbar
        self.lgTextField.inputAccessoryView = toolbar
    }
    @objc func dropKeyboard(){
        view.endEditing(true)
    }
    override func viewDidLoad() {
         layoutViews()
        showDoneButton()
        locationIncidentsPickerView.delegate = self
        locationIncidentsPickerView.dataSource = self
        
        statePickerView.delegate = self
        statePickerView.delegate = self
        
        lgPickerView.dataSource = self
        lgPickerView.delegate = self
        
        locationTextField.delegate = self
        lgTextField.delegate = self
        getStateLocation()
    }
    func getStateLocation() {
        Loader.shared.showLoader()
        stateLocationViewModel.getstateLocations {
            self.statePickerView.reloadAllComponents()
            Loader.shared.hideLoader()
        }
    }
    func getLgLocation(stateArea: String) {
        Loader.shared.showLoader()
        lgLocationViewModel.getLgLocations(state: stateArea) {
            self.lgPickerView.reloadAllComponents()
            Loader.shared.hideLoader()
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == locationIncidentsPickerView {
            return locationIncidents.count
        } else if pickerView == statePickerView {
            return Storage.getStateLocation().count
        } else {
            return Storage.getLocalGovernemnt().count
        }
       
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == locationIncidentsPickerView {
            return locationIncidents[row]
        } else if pickerView == statePickerView {
            return Storage.getStateLocation()[row].name
        } else {
           return Storage.getLocalGovernemnt()[row].name
        }
       
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == locationIncidentsPickerView {
            locationTextField.text = locationIncidents[row]
            if locationTextField.text == "Incident Around You" {
                stateTextField.isHidden = true
                lgTextField.isHidden = true
                selectedItem = "Incident Around You"
                selectedItemToSearch = ""
            } else if locationTextField.text == "State" {
                stateTextField.isHidden = false
                lgTextField.isHidden = true
                stateTextField.text  = ""
            } else {
                stateTextField.isHidden = false
                lgTextField.isHidden = false
                lgTextField.text = ""
                stateTextField.text  = ""
            }
        } else if pickerView == statePickerView {
            stateTextField.text = Storage.getStateLocation()[row].name
            getLgLocation(stateArea: stateTextField.text ?? "" )
            lgTextField.text = ""
            selectedItem = "Incidents around \(stateTextField.text ?? "") State"
            selectedItemToSearch = stateTextField.text ?? ""
        } else {
            lgTextField.text = Storage.getLocalGovernemnt()[row].name
            selectedItem =  "Incidents around \(lgTextField.text ?? "") local government"
            selectedItemToSearch = lgTextField.text ?? ""
        }
    }
    
}

