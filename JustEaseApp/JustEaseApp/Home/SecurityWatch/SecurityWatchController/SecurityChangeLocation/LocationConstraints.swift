//
//  ChangeLocationConstraints.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/21/23.
//

import UIKit
extension LocationController {
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(label)
        view.addSubview(textFieldStack)
        locationTextField.addSubview(selectLabel)
        locationTextField.addSubview(dropDownIcon)
        stateTextField.addSubview(stateDropDownIcon)
        lgTextField.addSubview(lgDropDownIcon)
        view.addSubview(doneButton)
        view.backgroundColor =  viewStackColor
        
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 30),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            
            selectLabel.topAnchor.constraint(equalTo: locationTextField.topAnchor, constant: 5),
            selectLabel.leadingAnchor.constraint(equalTo: locationTextField.leadingAnchor, constant: 10),
            
            dropDownIcon.topAnchor.constraint(equalTo: locationTextField.topAnchor, constant: 20),
            dropDownIcon.trailingAnchor.constraint(equalTo: locationTextField.trailingAnchor, constant: -20),
            
            stateDropDownIcon.topAnchor.constraint(equalTo: stateTextField.topAnchor, constant: 20),
            stateDropDownIcon.trailingAnchor.constraint(equalTo: stateTextField.trailingAnchor, constant: -20),
            
            lgDropDownIcon.topAnchor.constraint(equalTo: lgTextField.topAnchor, constant: 20),
            lgDropDownIcon.trailingAnchor.constraint(equalTo: lgTextField.trailingAnchor, constant: -20),
            
            textFieldStack.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 20),
            textFieldStack.leadingAnchor.constraint(equalTo:  view.leadingAnchor, constant: leadingNumber),
            textFieldStack.trailingAnchor.constraint(equalTo:  view.trailingAnchor, constant: trailingNumber),

            doneButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -70),
            doneButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber) ,
            doneButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            doneButton.heightAnchor.constraint(equalToConstant: buttonHeight)
        ])
    }
}
