//
//  SecurityResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/1/23.
//

import Foundation
struct SecurityWatchResource {
    func getSearchSecurityWatch(long: String, lat: String, search_scope: String, user_id: Int, completionHandler: @escaping (_ result: SecurityWatchResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let searchSecurityWatchUrl = ApiEndpoints.searchSecurityWatchEndPoint + "?longitude=\(long)&latitude=\(lat)&search_scope=\(search_scope)&user_id=\(user_id)"
        print("<<\(searchSecurityWatchUrl)")
        let url = URL(string: searchSecurityWatchUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType: SecurityWatchResponse.self) { result in
                completionHandler(result)
                print("<<\(result)")
            }
        }
    }
}
