//
//  CreateCrimeLikeResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 6/8/23.
//
import Foundation
struct CreateCrimeLikeResource {
    func getResponse(reportCrimeId: Int, creatCrimeLikeRequest: CreateCrimeLikeRequest, completionHandler: @escaping (_ result: CreateCrimeLikeResponse?,_ statusCode: Int) -> Void) {
        let url = ApiEndpoints.createCrimeLikeEndPoint + "/\(reportCrimeId)/create_report_crime_like"
        print(">>>\(url)")
        let creatCrimeLikeUrl = URL(string: url)!
        let httpUtility = HTTPUtility()
        do {
            let creatCrimeLikePostBody = try JSONEncoder().encode(creatCrimeLikeRequest)
            httpUtility.postAuthorizationResponse(requestUrl: creatCrimeLikeUrl, requestBody: creatCrimeLikePostBody, resultType: CreateCrimeLikeResponse.self) { createCrimeLikeApiResponse, statusCode in
                completionHandler(createCrimeLikeApiResponse,statusCode)
                print(">>>\(createCrimeLikeApiResponse)")
            }
        } catch let error {
            debugPrint(error)
        }
    }
}
