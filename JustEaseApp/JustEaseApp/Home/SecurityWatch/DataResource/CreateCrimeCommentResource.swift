//
//  CreateCrimeCommentResource.swift
//  JustEaseApp
//
//  Created by iOSApp on 31/05/2023.
//
import Foundation
struct CreateCrimeCommentResource {
    func getResponse(reportCrimeId: Int, createCrimeCommentRequest: CreateCrimeCommentRequest, completionHandler: @escaping (_ result: CreateCrimeCommentResponse?,_ statusCode: Int) -> Void) {
        let url = ApiEndpoints.createCrimeCommentEndPoint + "\(reportCrimeId)/create_comment"
        print(">>>\(url)")
        let createCrimeCommentUrl = URL(string: url)!
        let httpUtility = HTTPUtility()
        do {
            let createCrimeCommentPostBody = try JSONEncoder().encode(createCrimeCommentRequest)
            httpUtility.postAuthorizationResponse(requestUrl: createCrimeCommentUrl, requestBody: createCrimeCommentPostBody, resultType: CreateCrimeCommentResponse.self) { createCrimeCommentApiResponse, statusCode in
                completionHandler(createCrimeCommentApiResponse,statusCode)
                print(">>>\(String(describing: createCrimeCommentApiResponse))")
            }
        } catch let error {
            debugPrint(error)
        }
    }
}

