//
//  SearchSecurityResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/28/23.
//

import Foundation
struct SearchSecurityWatchResource {
    func getSearchSecurityWatch(long: String, lat: String, search_scope: String, search_query: String, user_id: Int, completionHandler: @escaping (_ result: SecurityWatchResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let searchSecurityWatchUrl = ApiEndpoints.searchSecurityWatchEndPoint + "?longitude=\(long)&latitude=\(lat)&search_scope=\(search_scope)&search_query=\(search_query)&user_id=\(user_id)"
        print("<<\(searchSecurityWatchUrl)")
        if let url = URL(string: searchSecurityWatchUrl) {
            // Use the valid URL
            do {
                httpUtility.getAPIData(requestURL: url, resultType: SecurityWatchResponse.self) { result in
                    completionHandler(result)
                    print("<<\(result)")
                }
            }
        } else {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Not Found", type: .error)
            // Handle the case where feedsUrl is not a valid URL
            // You can provide a default URL or display an error message
        }
      
    }
}
