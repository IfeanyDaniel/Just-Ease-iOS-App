//
//  StateSecurityResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/25/23.
//

import Foundation
struct StateSecurityResource {
    func getSecurityWatch(completionHandler: @escaping (_ result: StateSecurityResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let securityWatchUrl = ApiEndpoints.securityWatchStateEndPoint
        print("<<\(securityWatchUrl)")
        let url = URL(string: securityWatchUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType: StateSecurityResponse.self) { result in
                completionHandler(result)
                print("<<\(result)")
            }
        }
    }
}
