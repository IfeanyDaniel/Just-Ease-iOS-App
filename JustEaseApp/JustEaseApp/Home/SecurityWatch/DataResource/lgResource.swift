//
//  lgResource.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/25/23.
//

import Foundation
struct LgLocationSecurityResource {
    func getSecurityWatch(state: String, completionHandler: @escaping (_ result: LgSecurityResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let securityWatchLgUrl = ApiEndpoints.lgSecurityWatchEndPoint + "\(state)"
        print("<<\(securityWatchLgUrl)")
        let url = URL(string: securityWatchLgUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType: LgSecurityResponse.self) { result in
                completionHandler(result)
                print("<<\(result)")
            }
        }
    }
}
