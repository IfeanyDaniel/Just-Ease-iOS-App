//
//  ReadCrimeCommentResource.swift
//  JustEaseApp
//
//  Created by iOSApp on 31/05/2023.
//

import Foundation
struct ReadCrimeCommentResource {
    func getReadCrimeComment(reportCrimeId: Int, user_id: Int, completionHandler: @escaping (_ result: ReadCrimeCommentResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let readCrimeCommentUrl = ApiEndpoints.readCrimeCommentEndPoint + "\(reportCrimeId)?user_id=\(user_id)"
        print("<<\(readCrimeCommentUrl)")
        let url = URL(string: readCrimeCommentUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType: ReadCrimeCommentResponse.self) { result in
                completionHandler(result)
                print("<<\(String(describing: result))")
            }
        }
    }
}
