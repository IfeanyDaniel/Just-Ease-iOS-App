//
//  SecurityCommentConstraints.swift
//  JustEaseApp
//
//  Created by iOSApp on 04/03/2023.
//

import UIKit
extension SecurityCommentController {
    func layoutViews() {
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = backgroundSystemColor
        view.addSubview(defaultImage)
        view.addSubview(profileImage)
        view.addSubview(nameLabel)
        view.addSubview(timeLabel)
        
        view.addSubview(commentLabel)
        view.addSubview(bannerImage)
        view.addSubview(loveButton)
        view.addSubview(noLoveButton)
        view.addSubview(messageButton)
        view.addSubview(shareButton)
        
        view.addSubview(commentView)
        
        view.addSubview(profileIcon)
        view.addSubview(defaultProfileIcon)
        view.addSubview(commentButton)
        view.addSubview(commentCollectionView)
        
        NSLayoutConstraint.activate([
            defaultImage.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            defaultImage.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber),
            defaultImage.heightAnchor.constraint(equalToConstant: 60),
            defaultImage.widthAnchor.constraint(equalToConstant: 60),
            
            profileImage.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            profileImage.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber),
            profileImage.heightAnchor.constraint(equalToConstant: 60),
            profileImage.widthAnchor.constraint(equalToConstant: 60),
            
            nameLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 120),
            nameLabel.leadingAnchor.constraint(equalTo: defaultImage.trailingAnchor, constant: 10),
            nameLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            
            timeLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 5),
            timeLabel.leadingAnchor.constraint(equalTo: defaultImage.trailingAnchor, constant: 10),
            timeLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            
            commentLabel.topAnchor.constraint(equalTo: timeLabel.bottomAnchor, constant: 30),
            commentLabel.leadingAnchor.constraint(equalTo:  view.leadingAnchor, constant: leadingNumber),
            commentLabel.trailingAnchor.constraint(equalTo:   view.trailingAnchor, constant: trailingNumber),
            
            bannerImage.topAnchor.constraint(equalTo: commentLabel.bottomAnchor, constant: 80),
            bannerImage.leadingAnchor.constraint(equalTo:   view.leadingAnchor, constant: leadingNumber),
            bannerImage.trailingAnchor.constraint(equalTo:   view.trailingAnchor, constant: trailingNumber),
            bannerImage.heightAnchor.constraint(equalToConstant: 90),
            
            loveButton.topAnchor.constraint(equalTo: bannerImage.bottomAnchor, constant: 60),
            loveButton.leadingAnchor.constraint(equalTo:  view.leadingAnchor, constant: leadingNumber),
            loveButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            loveButton.widthAnchor.constraint(equalToConstant: 60),
            
            noLoveButton.topAnchor.constraint(equalTo: bannerImage.bottomAnchor, constant: 60),
            noLoveButton.leadingAnchor.constraint(equalTo:  view.leadingAnchor, constant: leadingNumber),
            noLoveButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            noLoveButton.widthAnchor.constraint(equalToConstant: 60),
            
            messageButton.topAnchor.constraint(equalTo: bannerImage.bottomAnchor, constant: 60),
            messageButton.leadingAnchor.constraint(equalTo: loveButton.trailingAnchor, constant: leadingNumber),
            messageButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            messageButton.widthAnchor.constraint(equalToConstant: 60),
            
            shareButton.topAnchor.constraint(equalTo: bannerImage.bottomAnchor, constant: 60),
            shareButton.trailingAnchor.constraint(equalTo:  view.trailingAnchor, constant: trailingNumber),
            shareButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            shareButton.widthAnchor.constraint(equalToConstant: 60),
            
            commentView.topAnchor.constraint(equalTo: shareButton.bottomAnchor, constant: 10),
            commentView.trailingAnchor.constraint(equalTo:  view.trailingAnchor, constant: 0),
            commentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            commentView.heightAnchor.constraint(equalToConstant: 70),
            
            profileIcon.topAnchor.constraint(equalTo: commentView.topAnchor, constant: 12),
            profileIcon.leadingAnchor.constraint(equalTo: commentView.leadingAnchor, constant: leadingNumber),
            profileIcon.heightAnchor.constraint(equalToConstant: 40),
            profileIcon.widthAnchor.constraint(equalToConstant: 40),
            
            defaultProfileIcon.topAnchor.constraint(equalTo: commentView.topAnchor, constant: 12),
            defaultProfileIcon.leadingAnchor.constraint(equalTo: commentView.leadingAnchor, constant: leadingNumber),
            defaultProfileIcon.heightAnchor.constraint(equalToConstant: 40),
            defaultProfileIcon.widthAnchor.constraint(equalToConstant: 40),
            
            commentButton.topAnchor.constraint(equalTo: commentView.topAnchor, constant: 12),
            commentButton.leadingAnchor.constraint(equalTo: profileIcon.trailingAnchor, constant: 15),
            commentButton.trailingAnchor.constraint(equalTo: commentView.trailingAnchor, constant: trailingNumber),
            commentButton.heightAnchor.constraint(equalToConstant: 40),
            
        ])
        commentCollectionView.anchorWithConstantsToTop(commentView.bottomAnchor,
                                                       left:  view.leftAnchor, bottom:  view.bottomAnchor, right:    view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
    }
}
