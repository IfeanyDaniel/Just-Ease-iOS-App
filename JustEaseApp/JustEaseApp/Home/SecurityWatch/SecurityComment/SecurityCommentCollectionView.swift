//
//  SecurityCommentCollectionView.swift
//  JustEaseApp
//
//  Created by iOSApp on 04/03/2023.
//

import UIKit
extension SecurityCommentController {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        readCrimeComment.numberOfRowsInSection().count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = commentCollectionView.dequeueReusableCell(withReuseIdentifier: CommentCell.identifier, for: indexPath) as? CommentCell else { return UICollectionViewCell() }
        let comment = readCrimeComment.cellForRowsAt(indexPath: indexPath)[indexPath.row]
        cell.commentLabel.text = comment.content
        cell.nameLabel.text = comment.firstName + " " + comment.lastName
        if comment.image != "" || comment.image != "https://kmr-staging.lawpavilion.com/storage/images/apiusers/" {
            cell.profileImage.kf.setImage(with: URL(string: "\(comment.image)"))
            cell.profileImage.isHidden = false
            cell.defaultImage.isHidden =  true
        } else {
            cell.defaultImage.setTitle("\(comment.firstName.first!) \(comment.lastName.first!)", for: .normal)
            cell.defaultImage.isHidden = false
            cell.profileImage.isHidden = true
        }
        if comment.isLiked == true {
            showlikeButton()
            noLoveButton.setTitle(" \(comment.commentLikeCount)", for: .normal)
        } else {
            showlikeButton()
            noLoveButton.setTitle(" \(comment.commentLikeCount)", for: .normal)
        }
        let timeAgo = comment.createdAt
        cell.timeLabel.text = timeAgo.timeAgoDisplay()
        cell.index = indexPath
        cell.commentDelegate = self
        cell.backgroundColor = textFieldColor
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 130)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
    }
 
}
