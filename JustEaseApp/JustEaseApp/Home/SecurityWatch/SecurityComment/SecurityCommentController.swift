//
//  SecurityCommentController.swift
//  JustEaseApp
//
//  Created by iOSApp on 04/03/2023.
//

import UIKit
import FittedSheets
class SecurityCommentController: UIViewController,UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout  {
    // MARK: - Scroll view
    var fullName = ""
    var firstName = ""
    var lastName = ""
    var time = ""
    var crimeDetails = ""
    var id = 0
    var readCrimeComment = ReadCrimeCommentViewModel()
    var createCrimeLikeViewModel = CreateCrimeLikeViewModel()
    var commentCount = 0
    var likesCount = 0
    var numberOfLikes = 0
    var isLiked = false
    var imageUrl = ""
    lazy var defaultImage: UIButton = {
        let button = UIButton.defaultImageButton()
        button.isHidden = true
        button.layer.cornerRadius = 30
        return button
    }()
    lazy var profileImage:  UIImageView  = {
        let profileImageView =  UIImageView.customImage()
        profileImageView.isHidden = true
        profileImageView.layer.cornerRadius = 30
        return profileImageView
    }()
    lazy var nameLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textAlignment = .left
        label.text = "Ifeanyi Mbata"
        label.textColor = textSystemColor
        label.font = UIFont(name: AppFonts.silkabold.font, size: 12)
        return label
    }()
    lazy var timeLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textAlignment = .left
        label.text = "100 day ago"
        label.textColor = textSystemColor
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 10)
        return label
    }()
    lazy var commentLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textAlignment = .left
        label.textColor = textSystemColor
        label.text = "I love playing footbal with boot in the field"
        label.font = UIFont(name: AppFonts.silkLight.font, size: 12)
        return label
    }()
    lazy var bannerImage :  UIImageView  = {
        let profileImageView =  UIImageView()
        profileImageView.image = UIImage(named: "image")
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.isUserInteractionEnabled = true
        return profileImageView
    }()
    lazy var loveButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.backgroundColor = backgroundSystemColor
        button.titleLabel?.font = UIFont(name:  AppFonts.silkLight.font, size: 12)
        button.setTitleColor(AppColors.green.color, for: .normal)
        button.setImage(UIImage(named: "love"), for: .normal)
        button.setTitle("  1", for: .normal)
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(didTapOnLoveButton), for: .touchUpInside)
        return button
    }()
    func createLike() {
        if InternetConnectionManager.isConnectedToNetwork(){
            let request = CreateCrimeLikeRequest(user_id: Storage.getUserId())
            createCrimeLikeViewModel.getCreateCrimeLikeResponse(reportCrimeId: id, createCrimeLikeRequest: request)
        } else {
            Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
        }
    }
    @objc func didTapOnLoveButton() {
        createLike()
      showUnlikeButton()
        if isLiked == true {
            noLoveButton.setTitle(" \(numberOfLikes - 1)", for: .normal)
        } else {
            noLoveButton.setTitle(" \(numberOfLikes)", for: .normal)
        }
        
    }
    lazy var noLoveButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.backgroundColor = backgroundSystemColor
        button.isHidden = true
        button.titleLabel?.font = UIFont(name:  AppFonts.silkLight.font, size: 12)
        button.setTitleColor(AppColors.green.color, for: .normal)
        button.setImage(UIImage(named: "noLove"), for: .normal)
        button.setTitle("  0", for: .normal)
        button.addTarget(self, action: #selector(didTapOnNoLoveButton), for: .touchUpInside)
        return button
    }()
  
    @objc func didTapOnNoLoveButton() {
        createLike()
       showlikeButton()
        if isLiked == true {
            loveButton.setTitle(" \(numberOfLikes)", for: .normal)
        } else {
            loveButton.setTitle(" \(numberOfLikes + 1)", for: .normal)
        }
    }
    lazy var messageButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.backgroundColor = backgroundSystemColor
        button.setTitleColor(textSystemColor, for: .normal)
        button.layer.cornerRadius = 0
        button.frame.size.width = 55
        button.setTitle("  1", for: .normal)
        button.titleLabel?.font = UIFont(name:  AppFonts.silkLight.font, size: 12)
        button.setImage(AppButtonImages.messageIcon.image, for: .normal)
        button.addTarget(self, action: #selector(didTapMessageutton), for: .touchUpInside)
        return button
    }()
    @objc func didTapMessageutton() {
        
    }
    lazy var shareButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.backgroundColor = backgroundSystemColor
        button.layer.cornerRadius = 0
        button.frame.size.width = 55
        button.setImage(UIImage(named: "share"), for: .normal)
        button.addTarget(self, action: #selector(didTapShareButton), for: .touchUpInside)
        return button
    }()
    @objc func didTapShareButton() {
        let shareSheetVC = UIActivityViewController(
            activityItems: [crimeDetails], applicationActivities: nil
        )
        self.present(shareSheetVC, animated: true)
    }
    lazy var commentView: UIView = {
        let content = UIView()
        content.backgroundColor = textFieldColor
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    lazy var defaultProfileIcon: UIButton = {
        let button = UIButton.defaultImageButton()
        button.layer.cornerRadius = 20
        button.isHidden = true
        button.setTitle(getUserInitial() , for: .normal)
        button.backgroundColor = AppColors.red.color
        return button
    }()
   lazy var profileIcon :  UIImageView  = {
       let profileImageView =  UIImageView.customImage()
       profileImageView.image = UIImage(named: "dan")
       profileImageView.isUserInteractionEnabled = true
       profileImageView.layer.masksToBounds = true
       profileImageView.isHidden = true
       profileImageView.layer.cornerRadius = 20
       return profileImageView
   }()
    lazy var commentButton: UIButton = {
        let button = UIButton.selectButtonDesign()
        button.backgroundColor = commentButtonColor
        button.layer.cornerRadius = 15
        button.setTitle(" Add a comment", for: .normal)
        button.setTitleColor(placeholderSystemGrayColor, for: .normal)
        button.addTarget(self, action: #selector(didTapOnAddComment), for: .touchUpInside)
        return button
    }()
    @objc func didTapOnAddComment() {
        let controller = AddCommentViewController()
        controller.reportCrimeId = id
        controller.delegate = self
        controller.replyButton.isHidden = true
        controller.replyCrimeButton.isHidden = false
        let sheetController = SheetViewController(controller: controller, sizes: [.fixed(200), .fullScreen])
        sheetController.blurBottomSafeArea = false
        sheetController.handleView.isHidden = true
        sheetController.topCornersRadius = 30
        present(sheetController, animated: false, completion: nil)
    }
    lazy var commentCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        cv.backgroundColor = textFieldColor
        cv.dataSource = self
        cv.delegate = self
        cv.showsVerticalScrollIndicator = false
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        self.hideKeyboardWhenTappedAround()
        registercCell()
        getDetails()
        fetchComments()
        createCrimeLikeViewModel.delegate = self
        setProfileIcon()
    }
    func fetchComments() {
        Loader.shared.showLoader()
        readCrimeComment.getCrimeComment(reportCrimeid: id, userId: Storage.getUserId()) {
            Loader.shared.hideLoader()
            DispatchQueue.main.async { [self] in
                self.commentCollectionView.reloadData()
            }
        }
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
   
   
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
    }
    func showlikeButton() {
        noLoveButton.isHidden = true
        loveButton.isHidden = false
        loveButton.setTitle(" \(numberOfLikes)", for: .normal)
        
    }
    func showUnlikeButton() {
        noLoveButton.isHidden = false
        loveButton.isHidden = true
        noLoveButton.setTitle(" \(numberOfLikes)", for: .normal)
    }
    
    func registercCell() {
        commentCollectionView.register(CommentCell.self, forCellWithReuseIdentifier: CommentCell.identifier)
    }
    func setProfileIcon() {
        if Storage.getImageUrl() != ""  {
            profileIcon.kf.setImage(with: URL(string: "\(Storage.getImageUrl())"))
            profileIcon.isHidden = false
            defaultProfileIcon.isHidden = true
        } else {
            defaultProfileIcon.setTitle("\(getUserInitial())", for: .normal)
            profileIcon.isHidden = true
            defaultProfileIcon.isHidden = false
        }
    }
    func getDetails() {
        nameLabel.text = fullName
        timeLabel.text = time
        commentLabel.text = crimeDetails
        if imageUrl != "" {
            profileImage.kf.setImage(with: URL(string: "\(imageUrl)"))
            profileImage.isHidden = false
            defaultImage.isHidden =  true
        } else {
            defaultImage.setTitle("\(firstName.first!) \(lastName.first!)", for: .normal)
            defaultImage.isHidden = false
            profileImage.isHidden = true
        }
        messageButton.setTitle("  \(commentCount)", for: .normal)
        numberOfLikes = likesCount
        if isLiked == true {
            showlikeButton()
            
        } else {
            showUnlikeButton()
        }
    }
}
extension  SecurityCommentController: CommentDelegate, ReloadCommentDelegate, CreateCrimeLikeViewModelDelegate {
    func didReceiveCreateCrimeLikeResponse(createCrimeLikeResponse: CreateCrimeLikeResponse?, _ statusCode: Int) {
        if statusCode == 200 {
        }
        if statusCode == 400 {
            Toast.shared.showToastWithTItle(createCrimeLikeResponse?.message ?? "", type: .error)
        }
    }
    
    func reloadCommentSection() {
        fetchComments()
    }
    
    func onClickLoveButton(index: Int) {
        
    }
    
    func onClickNoLoveButton(index: Int) {
        
    }
    
    
}
