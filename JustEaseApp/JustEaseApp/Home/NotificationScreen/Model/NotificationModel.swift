//
//  NotificationModel.swift
//  JustEaseApp
//
//  Created by Ifeanyi-Mbata on 8/1/23.
//

import Foundation
struct NotificationResponse: Decodable {
    let status: Int
    let message: String
    let data: [NotificationData]
}
struct NotificationData: Decodable {
    let id: Int
    let user_id: Int
    let report_crime_category_id: Int
    let report_crime_address: String?
    let report_crime_status: String
    let crime_details: String?
    let distance: Double?
    let created_at: String
    let anonymous_status: Int
    let is_liked: Bool
    let type: String?
    let report_crime_category: NotificationCategory?
    let user: NotificationUser?
}
struct NotificationCategory : Decodable {
    let id: Int
    let name: String?
}
struct NotificationUser: Decodable {
    let first_name: String?
    let last_name: String?
    let image_url: String?
}
