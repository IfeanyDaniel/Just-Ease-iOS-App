//
//  NotificationViewModel.swift
//  JustEaseApp
//
//  Created by Ifeanyi-Mbata on 8/1/23.
//

import Foundation
struct Notifications{
    let id : Int
    let firstName: String
    let lastName: String
    let address: String
    let crimeDetails: String
    let createdAt: Date
    let imageUrl: String
    let crimeType: String
}

class NotificationViewModel {
    var notificationDetails: [Notifications] = []
    func getAllNotification(userId: Int, completion: @escaping () -> Void ) {
        let notificationResource = NotificationResource()
        notificationResource.getNotificationResponse(user_id: userId) { getNotificationApiResponse in
            DispatchQueue.main.async { [self] in
                let notificationData = getNotificationApiResponse?.data
                notificationDetails.removeAll()
                for index in 0..<(notificationData?.count ?? 0) {
                    let id = notificationData?[index].id ?? 0
                    let firstName = notificationData?[index].user?.first_name ?? ""
                    let lastName = notificationData?[index].user?.last_name ?? ""
                    let reporCrimeLocation = notificationData?[index].report_crime_address ?? ""
                    let crimeDetails = notificationData?[index].crime_details ?? ""
                    let time = notificationData?[index].created_at ?? ""
                    let imageUrl = notificationData?[index].user?.image_url ?? ""
                    let typeOfCrime = notificationData?[index].type ?? ""
                    
                    notificationDetails.append(Notifications(id: id, firstName: firstName, lastName: lastName, address: reporCrimeLocation, crimeDetails: crimeDetails, createdAt: time.toDate(format: "yyyy-MM-dd HH:mm:ss").adding(minutes: 60), imageUrl: imageUrl, crimeType: typeOfCrime))
                    
                    notificationDetails = notificationDetails.sorted(by: {$0.createdAt > $1.createdAt})
                    
                }
                completion()
            }
        }
    }
    func numberOfRowsInSection() -> [Notifications]  {
        return notificationDetails
    }
    func cellForRowsAt(indexPath: IndexPath) -> [Notifications] {
        return notificationDetails
    }
}
