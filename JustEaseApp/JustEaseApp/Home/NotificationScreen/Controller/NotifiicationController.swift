//
//  NotifiicationScreen.swift
//  JustEaseApp
//
//  Created by Ifeanyi-Mbata on 7/30/23.
//

import UIKit
class NotificationController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var notificationViewModel = NotificationViewModel()
    lazy var notificationTableView : UITableView = {
        let table = UITableView()
        table.backgroundColor = backgroundSystemColor
        table.rowHeight = 120
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.sizeToFit()
        table.delegate = self
        table.dataSource = self
        table.separatorColor = AppColors.green.color
        table.showsVerticalScrollIndicator = false
        return table
    }()
    lazy var noContentView : UIView = {
        let content = UIView()
        content.backgroundColor = backgroundSystemColor
        content.isHidden = true
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    
    lazy var noRecordIcon: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.noBookmarkIcon.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var noContentLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "No Notification available"
        label.textAlignment = .center
        label.font = UIFont(name: AppFonts.silkabold.font, size: 14)
        return label
    }()
    lazy var instructionLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textColor = AppColors.lightNavyBlue.color
        label.text = "Please check back for content."
        label.textAlignment = .center
        return label
    }()
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        registercCell()
        getNotification()
    }
    func getNotification() {
        Loader.shared.showLoader()
        notificationViewModel.getAllNotification(userId: Storage.getUserId()) {
            Loader.shared.hideLoader()
            if self.notificationViewModel.notificationDetails.count > 0 {
                DispatchQueue.main.async { [self] in
                    notificationTableView.reloadData()
                }
            } else {
                self.notificationTableView.isHidden = true
                self.showNoContentView()
            }
        }
    }
    func showNoContentView() {
        noContentView.isHidden = false
        notificationTableView.isHidden = true
    }
    
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
        navigationItem.title = "Notification"
    }
    
    func registercCell() {
        notificationTableView.register(NotificationCell.self, forCellReuseIdentifier: NotificationCell.identifier)
    }
}

