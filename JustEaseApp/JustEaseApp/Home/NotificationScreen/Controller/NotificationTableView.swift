//
//  NotificationTableView.swift
//  JustEaseApp
//
//  Created by Ifeanyi-Mbata on 7/30/23.
//

import UIKit
import Kingfisher
extension NotificationController {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationViewModel.numberOfRowsInSection().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NotificationCell.identifier, for: indexPath) as? NotificationCell else { return UITableViewCell() }
        let notificationDetails = notificationViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
        let timeAgo = notificationDetails.createdAt
        cell.nameLabel.text = "\(notificationDetails.firstName) \(notificationDetails.lastName)"
        cell.situationLabel.text = notificationDetails.crimeDetails
        cell.timeLabel.text = timeAgo.timeAgoDisplay()
        cell.alertLabel.text = "\(notificationDetails.crimeType) alert"
        if notificationDetails.imageUrl != "" {
            cell.profileImage.kf.setImage(with: URL(string: "\(notificationDetails.imageUrl)"))
            cell.profileImage.isHidden = false
            cell.defaultImage.isHidden =  true
        } else {
            cell.defaultImage.setTitle("\(notificationDetails.firstName.first!) \(notificationDetails.lastName.first!)", for: .normal)
            cell.defaultImage.isHidden = false
            cell.profileImage.isHidden = true
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}
