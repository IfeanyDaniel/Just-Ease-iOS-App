//
//  NotificationCell.swift
//  JustEaseApp
//
//  Created by Ifeanyi-Mbata on 7/30/23.
//

import UIKit
class NotificationCell: UITableViewCell {
    static var identifier = "notificationCellId"
    lazy var defaultImage: UIButton = {
        let button = UIButton.defaultImageButton()
        button.isHidden = true
        button.layer.cornerRadius = 20
        return button
    }()
    lazy var profileImage:  UIImageView  = {
        let profileImageView =  UIImageView.customImage()
        profileImageView.isHidden = true
        profileImageView.layer.cornerRadius = 20
        return profileImageView
    }()
    var nameLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Ifeanyi Daniel"
        label.font = UIFont(name: AppFonts.silka.font ,size: 11)
        label.textColor = textSystemColor
        return label
    }()
    lazy var alertLabel: UILabel = {
        let label = PaddingLabel(withInsets: 8, 8, 16, 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 10)
        label.textColor = AppColors.black.color
        label.textAlignment = .center
        label.text = "Crime Alert"
        label.backgroundColor = AppColors.lightOrange.color
        label.layer.cornerRadius = 15
        label.layer.masksToBounds = true
        return label
    }()
    lazy var situationLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Robbery is happening in my area"
        label.numberOfLines = 5
        label.textAlignment = .left
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 12)
        label.textColor = textSystemColor
        return label
    }()
    lazy var timeLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "69 days ago"
        label.textColor = textSystemColor
        return label
    }()
    // MARK: - ... Adding subviews and constraints in the cell
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(defaultImage)
        addSubview(profileImage)
        addSubview(nameLabel)
        addSubview(timeLabel)
        addSubview(situationLabel)
        addSubview(alertLabel)
        
        NSLayoutConstraint.activate([
            defaultImage.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            defaultImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            defaultImage.heightAnchor.constraint(equalToConstant: 40),
            defaultImage.widthAnchor.constraint(equalToConstant: 40),
            
            profileImage.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            profileImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            profileImage.heightAnchor.constraint(equalToConstant: 40),
            profileImage.widthAnchor.constraint(equalToConstant: 40),
            
            nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 25),
            nameLabel.leadingAnchor.constraint(equalTo: defaultImage.trailingAnchor, constant: 10),
            
            situationLabel.topAnchor.constraint(equalTo: defaultImage.bottomAnchor, constant: 20),
            situationLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            situationLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -30),
            
            alertLabel.topAnchor.constraint(equalTo: topAnchor, constant: 25),
            alertLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -100),
            
            timeLabel.topAnchor.constraint(equalTo: topAnchor, constant: 25),
            timeLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -1),
            
        ])
    }
    required init?(coder: NSCoder) {
        fatalError("init(code:) has not been implemented")
    }
}
