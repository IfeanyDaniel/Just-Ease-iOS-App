//
//  NotificationResource.swift
//  JustEaseApp
//
//  Created by Ifeanyi-Mbata on 8/1/23.
//
import Foundation
struct NotificationResource {
    func getNotificationResponse(user_id: Int, completionHandler: @escaping (_ result: NotificationResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let notificationUrl = ApiEndpoints.notificationEndPoint + "/\(user_id)"
        print("<<\(notificationUrl)")
        let url = URL(string: notificationUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType: NotificationResponse.self) { result in
                completionHandler(result)
                print("<<\(result)")
            }
        }
    }
}
