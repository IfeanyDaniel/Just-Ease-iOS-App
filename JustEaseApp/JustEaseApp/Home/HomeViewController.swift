//
//  HomeViewController.swift
//  JustEaseApp
//
//  Created by iOSApp on 13/02/2023.
//

import UIKit
import FittedSheets
import CoreLocation
class HomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout , UIGestureRecognizerDelegate, AuthViewModelDelegate  {
    var timer = Timer()
    var feedsViewModel = FeedsViewModel()
    var authViewModel = AuthViewModel()
    
    var timers: Timer?
    var currentIndex = 0
    let animationDuration: TimeInterval = 1.0 // Duration of the animation in seconds
    let timeInterval: TimeInterval = 3.0 // Time interval between each animation update in seconds

    lazy var panicButtton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 30
        button.setTitleColor(AppColors.white.color, for: .normal)
        button.backgroundColor = AppColors.red.color
        button.setTitle("PANIC", for: .normal)
        button.addTarget(self, action: #selector(didTapOnPanicButton), for: .touchUpInside)
        button.titleLabel?.font = UIFont(name:  AppFonts.silkaMedium.font, size: 12)
        button.layer.shadowOpacity = 1
        button.layer.shadowOffset = .zero
        button.isHidden = true
        button.layer.shadowColor = textSystemColor.cgColor
        button.layer.cornerRadius = 30
        return button
    }()
    @objc func didTapOnPanicButton() {
        let controller = DangerController()
        controller.delegate = self
        let sheetController = SheetViewController(controller: controller, sizes: [.fixed(450), .fixed(450)])
        sheetController.blurBottomSafeArea = false
        sheetController.handleView.isHidden = true
        sheetController.topCornersRadius = 30
        present(sheetController, animated: false, completion: nil)
    }
    // MARK: - Scroll view
    
    lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.showsHorizontalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    // MARK: - content view
    lazy var contentView: UIView = {
        let content = UIView()
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    
    lazy var notification: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.notification.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(didTapOnNotification), for: .touchUpInside)
        return button
    }()
    @objc func didTapOnNotification() {
        navigationController?.pushViewController(NotificationController(), animated: true)
    }
    lazy var justEaseLabel: UILabel = {
        let label = UILabel()
        label.text = "JustEase"
        label.textColor = AppColors.green.color
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: AppFonts.silkabold.font, size: 25)
        label.textAlignment = .center
        return label
    }()
    lazy var quickActionViewHolder : UIView = {
        let content = UIView()
        content.backgroundColor = backgroundSystemColor
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    lazy var quickActionCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .horizontal
        cv.backgroundColor = .systemBackground
        cv.dataSource = self
        cv.delegate = self
        cv.showsHorizontalScrollIndicator = false
        cv.isPagingEnabled = true
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    lazy var quickNewsViewHolder : UIView = {
        let content = UIView()
        content.backgroundColor = .clear
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    lazy var quickNewsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 10
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .horizontal
        cv.backgroundColor = .clear
        cv.dataSource = self
        cv.delegate = self
        cv.showsHorizontalScrollIndicator = false
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    var pageControlBottomAnchor: NSLayoutConstraint?
    lazy var pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.pageIndicatorTintColor = AppColors.lightGreen.color
        pc.currentPageIndicatorTintColor = AppColors.green.color
        pc.layer.cornerRadius = 15
        pc.numberOfPages = quickActionNews.count
        pc.translatesAutoresizingMaskIntoConstraints = false
        pc.layer.masksToBounds = true
        return pc
    }()
    lazy var quickLinkLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Quick Links"
        label.textAlignment = .center
        label.font = UIFont(name: AppFonts.silkabold.font ,size: 14)
        return label
    }()
    
    // MARK: - Emergencies
    lazy var firstView: UIView = {
        let content = UIView.viewHolderDesign()
        content.backgroundColor = AppColors.lightOrange.color
        return content
    }()
    lazy var reportIconButton: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.reportIcon.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var reportHeader: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Soro Soke"
        label.textAlignment = .left
        label.textColor = AppColors.black.color
        label.font = UIFont(name: AppFonts.silkabold.font ,size: 12)
        return label
    }()
    lazy var reportLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Report a crime , violation"
        label.textAlignment = .left
        label.textColor = AppColors.black.color
        label.font = UIFont(name: AppFonts.silkaItalic.font ,size: 10)
        return label
    }()
    lazy var reportImage :  UIImageView  = {
        let imageView =  UIImageView.customImage()
        imageView .image = AppButtonImages.reportImage.image
        return imageView 
    }()
    
    // MARK: - Find a lawyer
    lazy var secondView: UIView = {
        let content = UIView.viewHolderDesign()
        content.backgroundColor = AppColors.lightBlue.color
        return content
    }()
    lazy var findIconButton: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.findIcon.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var findHeader: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Find a Lawyer, Find a Police"
        label.textAlignment = .left
        label.textColor = AppColors.black.color
        label.font = UIFont(name: AppFonts.silkabold.font ,size: 12)
        return label
    }()
    lazy var findLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Make a find"
        label.textAlignment = .left
        label.textColor = AppColors.black.color
        label.font = UIFont(name: AppFonts.silkaItalic.font ,size: 10)
        return label
    }()
    lazy var findImage :  UIImageView  = {
        let imageView =  UIImageView.customImage()
        imageView .image = AppButtonImages.findImage.image
        return imageView
    }()
    
    // MARK: - Secuity watch
    lazy var thirdView: UIView = {
        let content = UIView.viewHolderDesign()
        content.backgroundColor = AppColors.lightGreen.color
        return content
    }()
    lazy var securityIconButton: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.securityIcon.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var securityHeader: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Security Watch"
        label.textAlignment = .left
        label.textColor = AppColors.black.color
        label.font = UIFont(name: AppFonts.silkabold.font ,size: 12)
        return label
    }()
    lazy var securityLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Crimes reported near you"
        label.textAlignment = .left
        label.textColor = AppColors.black.color
        label.font = UIFont(name: AppFonts.silkaItalic.font ,size: 10)
        return label
    }()
    lazy var securityImage :  UIImageView  = {
        let imageView =  UIImageView.customImage()
        imageView .image = AppButtonImages.securityImage.image
        return imageView
    }()
    
    // MARK: - Know your representatives
    lazy var fourthView: UIView = {
        let content = UIView.viewHolderDesign()
        content.backgroundColor = AppColors.lightPurple.color
        return content
    }()
    lazy var repIconButton: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.knowRepIcon.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var repHeader: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Know your Rep"
        label.textAlignment = .left
        label.textColor = AppColors.black.color
        label.font = UIFont(name: AppFonts.silkabold.font ,size: 12)
        return label
    }()
    lazy var repLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "know your representatives"
        label.textAlignment = .left
        label.textColor = AppColors.black.color
        label.font = UIFont(name: AppFonts.silkaItalic.font ,size: 10)
        return label
    }()
    lazy var repImage :  UIImageView  = {
        let imageView =  UIImageView.customImage()
        imageView .image = AppButtonImages.knowRepImage.image
        return imageView
    }()
    
    //MARK: - GESTURE ON FOURTH VIEW
    func addGestureRecognizerOnFourthView() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.didTapOnFourthView))
        tapGesture.delegate = self
        view.isUserInteractionEnabled = true
       fourthView.addGestureRecognizer(tapGesture)
    }
    @objc  func didTapOnFourthView() {
        navigationController?.pushViewController(StateController(), animated: true)
    }
    
    //MARK: - GESTURE ON THIRD VIEW
    func addGestureRecognizerOnThirdView() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.didTapOnknowThirdView))
        tapGesture.delegate = self
        view.isUserInteractionEnabled = true
        thirdView.addGestureRecognizer(tapGesture)
    }
   
    @objc func didTapOnknowThirdView() {
        navigationController?.pushViewController(SecurityWatchController(), animated: true)
    }
    
    //MARK: - GESTURE SECOND VIEW
    func addGestureRecognizerOnSecondView() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.didTapOnknowSecondView))
        tapGesture.delegate = self
        view.isUserInteractionEnabled = true
        secondView.addGestureRecognizer(tapGesture)
    }
    @objc func didTapOnknowSecondView() {
        let controller = FindLawyerController()
        controller.delegate = self
        let sheetController = SheetViewController(controller: controller, sizes: [.fixed(160), .fixed(160)])
        sheetController.blurBottomSafeArea = false
        sheetController.handleView.isHidden = true
        sheetController.topCornersRadius = 30
        present(sheetController, animated: false, completion: nil)
    }
    
    //MARK: - GESTURE FIRST VIEW
    func addGestureRecognizerOnFirstView() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.didTapOnknowFirstView))
        tapGesture.delegate = self
        view.isUserInteractionEnabled = true
        firstView.addGestureRecognizer(tapGesture)
    }
    @objc func didTapOnknowFirstView() {
        let controller = SoroSokeController()
        controller.delegate = self
        let sheetController = SheetViewController(controller: controller, sizes: [.fixed(160), .fixed(160)])
        sheetController.blurBottomSafeArea = false
        sheetController.handleView.isHidden = true
        sheetController.topCornersRadius = 30
        present(sheetController, animated: false, completion: nil)
    }
    
    lazy var stackViewHolder : UIStackView = {
        let stackView = UIStackView()
        stackView.backgroundColor = backgroundSystemColor
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.isUserInteractionEnabled = true
        stackView.addArrangedSubview(firstView)
        stackView.addArrangedSubview(secondView)
        stackView.addArrangedSubview(thirdView)
        stackView.addArrangedSubview(fourthView)
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 10
        return stackView
    }()
    //MARK: - Show page control
    func showPageControl(){
        pageControlBottomAnchor = pageControl.anchor(quickNewsCollectionView.bottomAnchor, left: quickNewsCollectionView.leftAnchor, bottom: nil, right: quickNewsCollectionView.rightAnchor, topConstant: -20, leftConstant: 50, bottomConstant: 0, rightConstant: 50, widthConstant: 0, heightConstant: 40)[1]
    }
    //MARK: - scroll view will end dragging
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let pageNumber = Int(targetContentOffset.pointee.x / quickNewsViewHolder.frame.width)
        //MARK: - move current dot color to current position
        quickNewsCollectionView.isPagingEnabled = true
        pageControl.currentPage = pageNumber
    }
    
    let quickActionPage: [QuickActionPage] = {
        let right = QuickActionPage(title: "Rights", imageName: "rightIcon")
        
        let duties = QuickActionPage(title: "Duties", imageName: "dutiesIcon")
        
        let other = QuickActionPage(title: "Others", imageName: "othersIcon")
        
        let constitution = QuickActionPage(title: "Constitution", imageName: "constitutionIcon")
        
        return [right,duties,other,constitution]
    }()
    // MARK: - ONBOARDING SCREENS
    let  quickActionNews: [QuickActionNews] = {
        let first = QuickActionNews(image: "Image", firstLabel: "Did you Know?", secondLabel: "", thirdLabel: "")
        
        let second = QuickActionNews(image: "Image", firstLabel: "Did you Know?", secondLabel: "", thirdLabel: "")
        
        let third = QuickActionNews(image: "Image", firstLabel: "Did you Know?", secondLabel: "", thirdLabel: "")
        
        let fourth = QuickActionNews(image: "Image", firstLabel: "Did you Know?", secondLabel: "", thirdLabel: "")
        
        return [first,second,third,fourth]
    }()
    
    //MARK: - Label below welcome back
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Easy access to justice"
        label.textAlignment = .left
        label.font = UIFont(name: AppFonts.silka.font ,size: 11)
        return label
    }()
    
    @objc func dropKeyboard(){
        view.endEditing(true)
    }
    @objc func userIsInactive() {
        DispatchQueue.main.async {
            let navigationController = UINavigationController.init(rootViewController: Refresh())
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
            self.timer.invalidate()
        }
    }
    
    @objc func appMovedToBackground() {
        timer = Timer.scheduledTimer(timeInterval: 600, target: self, selector: #selector(userIsInactive), userInfo: nil, repeats: true)
    }
    
    @objc func appMovedToActive() {
        timer.invalidate()
    }
    
    func notifyTimerOnBackground() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
    }
    
    func notifyTimerOnActive() {
        let notification = NotificationCenter.default
        notification.addObserver(self, selector: #selector(appMovedToActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        
    }
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        notifyTimerOnActive()
        notifyTimerOnBackground()
        layoutViews()
        setupScrollView()
        self.hideKeyboardWhenTappedAround()
        registerCell()
        showPageControl()
        addGestureRecognizerOnFourthView()
        addGestureRecognizerOnThirdView()
        addGestureRecognizerOnSecondView()
        addGestureRecognizerOnFirstView()
    }
    func startAutoScrolling() {
          // Invalidate the previous timer if any
          timers?.invalidate()
          // Start the new timer
          timers = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(scrollCollectionView), userInfo: nil, repeats: true)
      }

      // Function to stop the auto-scrolling animation (e.g., when view is about to disappear)
      func stopAutoScrolling() {
          timers?.invalidate()
      }

      // Function to scroll the collection view to the next cell
      @objc func scrollCollectionView() {
          currentIndex = (currentIndex + 1) % feedsViewModel.homePageFeed.count
          pageControl.currentPage = currentIndex 
          let indexPath = IndexPath(item: currentIndex, section: 0)
          quickNewsCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
      }

      override func viewWillDisappear(_ animated: Bool) {
          super.viewWillDisappear(animated)
          // Stop the animation when the view is about to disappear
          stopAutoScrolling()
      }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        startAutoScrolling()
        LocationService().startUpdating()
        panicButtton.isHidden = false
        authViewModel.delegate = self
        authenticate()
    }
    func getAllFeeds() {
        Loader.shared.showLoader()
        feedsViewModel.getFeeds {
            Loader.shared.hideLoader()
            DispatchQueue.main.async { [self] in
                quickNewsCollectionView.reloadData()
            }
        }

    }
    func authenticate(){
        if InternetConnectionManager.isConnectedToNetwork(){
            Loader.shared.showLoader()
            let request = AuthRequest(email: LoginDetails().email, password: LoginDetails().password)
            authViewModel.authResponse(authRequest: request)
        } else {
            Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
        }
    }
    func registerCell() {
        quickActionCollectionView.register(QuickActionCollectionViewCell.self, forCellWithReuseIdentifier: QuickActionCollectionViewCell.identifier)
        quickNewsCollectionView.register(QuickNewsCollectionViewCell.self, forCellWithReuseIdentifier: QuickNewsCollectionViewCell.identifier)
        
    }
    // MARK: - View did layout subviews
    override func viewDidLayoutSubviews() {
        scrollView.isScrollEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.contentSize = CGSize(width: 400, height: 1000)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func didReceiveAuthResponse(AuthResponse: AuthResponse?, _ statusCode: Int) {
        if statusCode == 200 {
            if InternetConnectionManager.isConnectedToNetwork(){
                getAllFeeds()
            }
            else {
                Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
            }
        }
        if statusCode == 400 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Something went wrong, Please try again", type: .error)
        }
    }
 }

extension HomeViewController : CollectionViewCellDelegate {
    func onClickCell(index: Int) {
         let card = quickActionPage[index]
        if card.title  == "Rights" {
            navigationController?.pushViewController(RightViewController(), animated: false)
        }
        if card.title  == "Duties" {
            navigationController?.pushViewController(DutiesViewController(), animated: false)
        }
        if card.title  == "Others" {
            navigationController?.pushViewController(OthersController(), animated: false)
        }
        if card.title  == "Constitution" {
            navigationController?.pushViewController(ConstitutionViewController(), animated: false)
        }
    }
}
extension HomeViewController : MoveToNextScreenDelegate, VideoRecordingDelegate {
    func moveToVideoRecordingScreen() {
        present(VideoRecordingViewController(), animated: true)
    }
    
    func goToReportCrimeScreen() {
        navigationController?.pushViewController(MapController(), animated: true)
    }
    
    func goToReportViolationScreen() {
        navigationController?.pushViewController(ReportViolationController(), animated: true)
    }
    
    func goToLawyerScreen() {
        navigationController?.pushViewController(LawyersController(), animated: true)
        Storage.saveRoutingNearByLawyerFrom(source: "")
    }
    
    func goToPoliceScreen() {
        navigationController?.pushViewController(PoliceStationController(), animated: true)
    }
}

//class LocationService: NSObject, CLLocationManagerDelegate{
//    private var locationManager: CLLocationManager!
//    public static var shared = LocationService()
//    public var coordinates: CLLocationCoordinate2D?
//    public func startUpdating(){
//        locationManager = CLLocationManager()
//        locationManager.delegate = self
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager.requestWhenInUseAuthorization()
//        if CLLocationManager.locationServicesEnabled(){
//            locationManager.startUpdatingLocation()
//        }
//    }
//
//}
class LocationService: NSObject, CLLocationManagerDelegate{
    func locationServicesEnabled() async -> Bool {
        CLLocationManager.locationServicesEnabled()
    }
    private var locationManager: CLLocationManager!
    public static var shared = LocationService()
    public var coordinates: CLLocationCoordinate2D?
    public func startUpdating(){
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        Task { [weak self] in
            if ((await self?.locationServicesEnabled()) != nil) {
                locationManager.startUpdatingLocation()
            }
        }
    }
}
