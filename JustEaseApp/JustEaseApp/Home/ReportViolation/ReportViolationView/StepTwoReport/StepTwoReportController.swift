//
//  StepTwoReportController.swift
//  JustEaseApp
//
//  Created by iOSApp on 25/02/2023.
//

import UIKit
import FittedSheets
import GoogleMaps
import CoreLocation
import MapKit
import AVKit
import PhotosUI
class StepTwoReportController: UIViewController, UITextViewDelegate, UIImagePickerControllerDelegate,  UINavigationControllerDelegate, PHPickerViewControllerDelegate, UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout, NextScreenDelegate {
    var imageArray: [ImageInfo] = []
    var lastIndex:IndexPath?
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var headingTitle = ""
    
    var address = ""
    
    var report_violation_category_id = 0
    var report_violation_subcategory_id = 0
    var local_government_id = 0
    var nature_of_issue = ""
    var date_of_incident = ""
    var time_of_incident = ""
    var reportIsuue = ""
    
    // MARK: - Scroll view
    lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.showsHorizontalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    // MARK: - content view
    lazy var contentView: UIView = {
        let content = UIView()
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.font = UIFont(name: AppFonts.silkabold.font ,size: 20)
        label.text = "Transport"
        return label
    }()
    
    lazy var stepsIcon: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "step2"), for: .normal)
        return button
    }()
    lazy var reportTextField: UITextView = {
        let tv = UITextView.writeAbletextViewDesign()
        tv.text = "Details of issue been reported"
        return tv
    }()
    
    lazy var pictureView: UIView = {
        let content = UIView()
        content.backgroundColor = textFieldColor
        content.translatesAutoresizingMaskIntoConstraints = false
        content.layer.cornerRadius = 7
        return content
    }()
    lazy var uploadedPixCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 15
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        cv.backgroundColor = backgroundSystemColor
        cv.frame.size.height = CGFloat(60 * imageArray.count)
        cv.isHidden = true
        cv.dataSource = self
        cv.delegate = self
        cv.showsVerticalScrollIndicator = true
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    lazy var photoPixLayerStack: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(pictureView)
        stackView.addArrangedSubview(uploadedPixCollectionView)
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = 2
        return stackView
    }()
    lazy var photoLayer :  UIImageView  = {
        let profileImageView =  UIImageView()
        profileImageView.image = UIImage(named: "photoLayer")
        profileImageView.contentMode = .scaleToFill
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.isUserInteractionEnabled = true
        profileImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOnPhotoLayer)))
        profileImageView.backgroundColor = textFieldColor
        return profileImageView
    }()
    lazy var firstLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textColor = placeholderSystemGrayColor
        label.font = UIFont(name: AppFonts.silkLight.font ,size: 14)
        label.text = "File Upload"
        return label
    }()
    lazy var uploadIcon: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "photoIcon"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var tapLabel: UILabel = {
        let label = UILabel.boldFirstText(firstText: "Tap to upload files \n", secondText: "You can upload up to 5 files", thirdText: "")
        label.textColor = AppColors.navyBlue.color
        label.textAlignment = .center
        return label
    }()
    @objc func didTapOnPhotoLayer() {
        popUploadSource()
    }
    func popUploadSource(){
        let controller = UploadSourceController()
        controller.delegate = self
        let sheetController = SheetViewController(controller: controller, sizes: [.fixed(250), .fixed(250)])
        sheetController.blurBottomSafeArea = false
        sheetController.handleView.isHidden = true
        sheetController.topCornersRadius = 30
        present(sheetController, animated: false, completion: nil)
    }
    lazy var proceedButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Proceed", for: .normal)
        button.layer.masksToBounds = true
        button.titleLabel?.font = UIFont(name: AppFonts.silka.font, size: 18)
        button.addTarget(self, action: #selector(didTapProceedButton), for: .touchUpInside)
        return button
    }()
    
    @objc func didTapProceedButton() {
        if reportTextField.text == ""  || reportTextField.text == "Details of issue been reported" {
            Toast.shared.showToastWithTItle("Details of the issue cannot be empty", type: .error)
        } else {
            let controller = ReportController()
            controller.reportViolationDelegate = self
            controller.delegateSuccess = self
            controller.reportButton.isHidden = true
            controller.reportViolationButton.isHidden = false
            controller.label.text = "Report Violation as"
            
            controller.report_crime_category_id = report_violation_category_id
            print(report_violation_category_id)
            controller.report_violation_subcategory_id = report_violation_subcategory_id
            controller.local_government_id = local_government_id
            controller.nature_of_issue = nature_of_issue
            controller.date_of_incident = date_of_incident
            controller.time_of_incident = time_of_incident
            controller.reportIsuue = reportTextField.text ?? ""
            controller.anonymous = 0
            controller.address = address
            
            let sheetController = SheetViewController(controller: controller, sizes: [.fixed(400), .fixed(400)])
            sheetController.blurBottomSafeArea = false
            sheetController.handleView.isHidden = true
            sheetController.topCornersRadius = 30
            present(sheetController, animated: false, completion: nil)
        }
    }
    func showDoneButton(){
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dropKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.reportTextField.inputAccessoryView = toolbar
    }
    @objc func dropKeyboard(){
        view.endEditing(true)
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        reportTextField.text = ""
        reportTextField.textColor = textSystemColor
    }
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        showDoneButton()
        setupScrollView()
        reportTextField.delegate  = self
        self.hideKeyboardWhenTappedAround()
        pictureView.addDashBorder()
        label.text = headingTitle
        getAddress()
        registerCell()
    }
    
    func registerCell() {
        uploadedPixCollectionView.register(PictureCollectionViewCell.self, forCellWithReuseIdentifier: PictureCollectionViewCell.identifier)
    }
    func getAddress() {
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            guard let currentLocation = locManager.location else {
                return
            }
            let locate = CLLocation(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
            locate.fetchCityAndCountry { streetNumber ,lg , street, city, country, error in
                self.address = "\(streetNumber) \(street) \(city ?? "") \(country ?? "")"
            }

        }
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    func pop(numberOfTimes: Int) {
        guard let navigationController = navigationController else {
            print("No navigation controller")
            return
        }
        let viewControllers = navigationController.viewControllers
        let index = numberOfTimes + 1
        if viewControllers.count >= index {
            navigationController.popToViewController(viewControllers[viewControllers.count - index], animated: true)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
        
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.setHidesBackButton(true, animated: true)
        
    }
    // MARK: - View did layout subviews
    override func viewDidLayoutSubviews() {
        scrollView.isScrollEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.contentSize = CGSize(width: 400, height: 900)
    }
}

extension StepTwoReportController: ReportViolationCrimeDelegate, ReportViolationSuccessDelegate,  ReportDelegate  {
    func showCrimeSuccess() {
        self.dismiss(animated: false){
            let controller = CrimeSuccessAlert()
            controller.statement.text = "Thank you! your violation had been reported successfully, we are on it and will get back to you with updates as soon as possible."
            controller.labelBelowIconButton.text = "Violation Reported"
            controller.delegate = self
            let sheetController = SheetViewController(controller: controller, sizes: [.fixed(450), .fixed(450)])
            sheetController.blurBottomSafeArea = false
            sheetController.topCornersRadius = 30
            sheetController.handleView.isHidden = true
            self.present(sheetController, animated: false, completion: nil)
        }
    }
    
    func ShowUserDetails() {
        self.dismiss(animated: false){
            let controller = StepThreeController()
            controller.headingTitle = self.headingTitle
            controller.report_violation_category_id = self.report_violation_category_id
            controller.report_violation_subcategory_id = self.report_violation_subcategory_id
            controller.local_government_id = self.local_government_id
            controller.nature_of_issue = self.nature_of_issue
            controller.date_of_incident = self.date_of_incident
            controller.time_of_incident = self.time_of_incident
            controller.reportIsuue = self.reportTextField.text ?? ""
            controller.anonymous = 1
            controller.address = self.address
            self.navigationController?.pushViewController(controller, animated: false)
          
        }
        
    }
    func getPhoto() {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true)
    }
    func getPhotoFromLibrary() {
        var controller = PHPickerConfiguration()
        controller.selectionLimit = 5
        controller.filter = .images
        let vc = PHPickerViewController(configuration: controller)
        vc.delegate = self
        present(vc, animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        
        if let image = info[.originalImage] as? UIImage  {
            // print("No image found")
            print("<<<\(image)")
            DispatchQueue.main.async {
                    print(image)
                    if self.imageArray.count < 5 {
                        self.uploadedPixCollectionView.isHidden = false
                        self.uploadedPixCollectionView.frame.size.height = CGFloat(60 * self.imageArray.count)
                        self.photoPixLayerStack.frame.size.height = CGFloat(200 + 60 * self.imageArray.count)
                        self.imageArray.insert(ImageInfo(image: image, ImageName: "mm", ImageSize: "Mb"), at: 0)
                        self.uploadedPixCollectionView.reloadData()
                    } else {
                        Toast.shared.showToastWithTItle("oops! number of images exceeded", type: .error)
                    }
                }
                
        }
        // MARK: - getting and checking the image size
        if let pix = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            
            let imgData = NSData(data: pix.jpegData(compressionQuality: 1)!)
            let imageSize: Int = imgData.count
            let actualImageSize =  Double(imageSize) / 1024.0 / 1024.0
            print("<<\(actualImageSize)")
        }
        if let asset = info[UIImagePickerController.InfoKey.imageURL] as? NSURL{
            print("???\(asset.lastPathComponent)")
        }
        
    }
    
    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        dismiss(animated: true)
        for result in results {
            result.itemProvider.loadObject(ofClass: UIImage.self) { object, error in
                DispatchQueue.main.async {
                    if let image = object as? UIImage, let fileName = result.itemProvider.suggestedName, let fileSize = result.itemProvider.preferredPresentationSize as? CGSize {
                        print(image)
                        if self.imageArray.count < 5 {
                            self.uploadedPixCollectionView.isHidden = false
                            self.uploadedPixCollectionView.frame.size.height = CGFloat(60 * self.imageArray.count)
                            self.photoPixLayerStack.frame.size.height = CGFloat(200 + 60 * self.imageArray.count)
                            self.imageArray.insert(ImageInfo(image: image, ImageName: fileName, ImageSize: "\(fileSize)"), at: 0)
                        } else {
                            Toast.shared.showToastWithTItle("oops! number of images exceeded", type: .error)
                        }
                    }
                    self.uploadedPixCollectionView.reloadData()
                }
            }
        }
        
    }
    
    func proceedWithCameraAccess(){
        // handler in .requestAccess is needed to process user's answer to our request
        AVCaptureDevice.requestAccess(for: .video) { success in
            if success { // if request is granted (success is true)
                DispatchQueue.main.async {
                    self.getPhoto()
                }
            } else { // if request is denied (success is false)
                // Create Alert
                let alert = UIAlertController(title: "Camera", message: "Camera access is absolutely necessary to use this app", preferredStyle: .alert)
                
                // Add "OK" Button to alert, pressing it will bring you to the settings app
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                }))
                // Show the alert with animation
                self.present(alert, animated: true)
            }
        }
    }
    func didTapOnReport() {}
    
    func popController() {
        self.pop(numberOfTimes: 3)
    }
    func showCamera() {
        proceedWithCameraAccess()
    }
    
    func showLibrary() {
        getPhotoFromLibrary()
    }
    
}
