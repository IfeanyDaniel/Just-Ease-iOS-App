//
//  StepOneReportConstraint.swift
//  JustEaseApp
//
//  Created by iOSApp on 25/02/2023.
//

import UIKit
extension StepTwoReportController {
    //MARK: - Layout subviews for the scroll views
    func setupScrollView() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 20),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
        ])
    }
    //MARK: - Layout subviews
    func layoutViews() {
        
        view.addSubview(label)
        contentView.addSubview(stepsIcon)
        contentView.addSubview(reportTextField)
        contentView.addSubview(proceedButton)
        contentView.addSubview(photoPixLayerStack)
        pictureView.addSubview(photoLayer)
        photoLayer.addSubview(firstLabel)
        photoLayer.addSubview(uploadIcon)
        photoLayer.addSubview(tapLabel)
        contentView.addSubview(proceedButton)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        view.backgroundColor = backgroundSystemColor
        
        NSLayoutConstraint.activate([
            
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingNumber),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingNumber),
            
            stepsIcon.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            stepsIcon.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leadingNumber),
            stepsIcon.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: trailingNumber),
            stepsIcon.heightAnchor.constraint(equalToConstant: 40),
            
            reportTextField.topAnchor.constraint(equalTo: stepsIcon.bottomAnchor, constant: 30),
            reportTextField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leadingNumber),
            reportTextField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: trailingNumber),
            reportTextField.heightAnchor.constraint(equalToConstant: 200),
            
            photoPixLayerStack.topAnchor.constraint(equalTo: reportTextField.bottomAnchor, constant: 30),
            photoPixLayerStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leadingNumber),
            photoPixLayerStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: trailingNumber),
           // photoPixLayerStack.heightAnchor.constraint(equalToConstant: CGFloat(200 + (60 * imageArray.count))),
            
            photoLayer.topAnchor.constraint(equalTo: pictureView.topAnchor, constant: 0),
            photoLayer.leadingAnchor.constraint(equalTo: pictureView.leadingAnchor, constant: 0),
            photoLayer.trailingAnchor.constraint(equalTo: pictureView.trailingAnchor, constant: 0),
            photoLayer.bottomAnchor.constraint(equalTo: pictureView.bottomAnchor, constant: 0),
            
            firstLabel.topAnchor.constraint(equalTo: photoLayer.topAnchor, constant: 5),
            firstLabel.leadingAnchor.constraint(equalTo: photoLayer.leadingAnchor, constant: 20),
            
            uploadIcon.centerXAnchor.constraint(equalTo: photoLayer.centerXAnchor),
            uploadIcon.topAnchor.constraint(equalTo: photoLayer.topAnchor, constant: 70),
            uploadIcon.widthAnchor.constraint(equalToConstant: 25),
            uploadIcon.heightAnchor.constraint(equalToConstant: 25),
            
            tapLabel.centerXAnchor.constraint(equalTo: photoLayer.centerXAnchor),
            tapLabel.topAnchor.constraint(equalTo: uploadIcon.bottomAnchor, constant: 5),
            tapLabel.leadingAnchor.constraint(equalTo: photoLayer.leadingAnchor, constant: 10),
            tapLabel.trailingAnchor.constraint(equalTo: photoLayer.trailingAnchor, constant: -10),
            
            proceedButton.topAnchor.constraint(equalTo: photoPixLayerStack.bottomAnchor, constant: 50),
            proceedButton.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 25),
            proceedButton.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -25),
            proceedButton.heightAnchor.constraint(equalToConstant: buttonHeight)
            
        ])
    }
}
