//
//  ReportViolationController.swift
//  JustEaseApp
//
//  Created by iOSApp on 25/02/2023.
//

import UIKit
class ReportViolationController: UIViewController,UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
    var reportViolationCategoriesViewModel = ReportViolationCategoriesViewModel()
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Report a Violation"
        label.textColor = AppColors.green.color
        label.font = UIFont(name: AppFonts.silkabold.font, size: 25)
        label.textAlignment = .left
        return label
    }()
   
    lazy var violationCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        cv.backgroundColor = backgroundSystemColor
        cv.dataSource = self
        cv.delegate = self
        cv.showsVerticalScrollIndicator = false
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    
//    let cards: [Card] = {
//        let health = Card(image: "health", title: "HEALTH", color: AppColors.lightOrange.color)
//
//        let infrastructure = Card(image: "infrastructure", title: "INFRASTRUCTURE", color: AppColors.lightPurple.color)
//
//        let environment = Card(image: "environment", title: "ENVIRONMENT", color: AppColors.lightPink.color)
//
//        let securityIsuues = Card(image: "security", title: "SECURTY ISSUES", color: AppColors.lightBlue.color)
//
//        let transport = Card(image: "transport", title: "TRANSPORT", color: AppColors.lightMilky.color)
//
//        let housing = Card(image: "health", title: "HOUSING", color: AppColors.lightOrange.color)
//
//        let consumer = Card(image:"health", title: "CONSUMER", color: AppColors.lightOrange.color)
//
//        let aviation = Card(image: "health", title: "AVIATION", color: AppColors.lightOrange.color)
//
//        let labour = Card(image: "health", title: "LABOUR", color: AppColors.lightOrange.color)
//
//        let power = Card(image: "health", title: "POWER(ELECTRICITY)", color: AppColors.lightOrange.color)
//
//        let bankingFinance = Card(image: "health", title: "BANKING&FINANCE", color: AppColors.lightOrange.color)
//
//        return [health,infrastructure ,environment,securityIsuues,transport,housing,consumer,aviation,labour,power,bankingFinance]
//    }()
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        registercCell()
       getReportCategories()
    }
    func getReportCategories() {
        Loader.shared.showLoader()
        reportViolationCategoriesViewModel.getReportViolationCategories {
            Loader.shared.hideLoader()
            self.violationCollectionView.reloadData()
        }
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
        
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.setHidesBackButton(true, animated: true)
       
    }
   
    func registercCell() {
        violationCollectionView.register(ReportViolationCell.self, forCellWithReuseIdentifier: ReportViolationCell.identifier)
    }
}
extension ReportViolationController: ReportViolationDelegate {
    func onClickReportViolationForwardButton(index: IndexPath) {
        let category = reportViolationCategoriesViewModel.cellForRowsAt(indexPath: index)[index.item]
        let controller = StepOneReportController()
        controller.reportType = category.subCategories
        controller.reportCategory = category.name
        controller.reportCategoryId = category.id
        Storage.saveViolationCategory(id: category.id)
        print(category.id)
        navigationController?.pushViewController(controller, animated: true)
    }
}
