//
//  ReportViolationCell.swift
//  JustEaseApp
//
//  Created by iOSApp on 25/02/2023.
//

import UIKit
protocol ReportViolationDelegate: AnyObject {
    func onClickReportViolationForwardButton(index: IndexPath)
}
class ReportViolationCell: UICollectionViewCell {
    static var identifier: String = "ReportViolationCell"
    var reportViolationDelegate: ReportViolationDelegate?
    var index : IndexPath?
    var card: Cards? {
        didSet {
            guard let card = card else {
                return
            }
            let forwardImage = card.forwardImage
            image.setImage(forwardImage, for: .normal)
            
            let cardName = card.title
            cardLabel.text = cardName
        }
    }
    
    lazy var cardLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textAlignment = .center
        label.textColor = AppColors.black.color
        label.font = UIFont(name: AppFonts.silkaMedium.font, size: 12)
        return label
    }()
    
    lazy var image: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(clickContactForwardButton(_:)), for: .touchUpInside)
        return button
    }()
    @objc func clickContactForwardButton(_ sender: Any){
//        guard let indexRow = index?.row else {
//            return
//        }
        reportViolationDelegate?.onClickReportViolationForwardButton(index: index!)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpViews() {
        addSubview(cardLabel)
        addSubview(image)
        NSLayoutConstraint.activate([
             image.topAnchor.constraint(equalTo: topAnchor, constant: 40),
             image.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -30),
             image.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 30),
             
            cardLabel.topAnchor.constraint(equalTo: image.bottomAnchor, constant: 16),
            cardLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            cardLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
          
        ])
    }
}
