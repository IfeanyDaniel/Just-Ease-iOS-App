//
//  ReportViolationConstriant.swift
//  JustEaseApp
//
//  Created by iOSApp on 25/02/2023.
//

import UIKit
extension ReportViolationController {
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(label)
        view.addSubview(violationCollectionView)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = backgroundSystemColor
        
        NSLayoutConstraint.activate([
    
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 25),
            
            
        ])
        violationCollectionView.anchorWithConstantsToTop(label.bottomAnchor,
                                                     left: view.leftAnchor, bottom: view.bottomAnchor, right:  view.rightAnchor, topConstant: 20, leftConstant: 25, bottomConstant: 30, rightConstant: 25)
    }
}
