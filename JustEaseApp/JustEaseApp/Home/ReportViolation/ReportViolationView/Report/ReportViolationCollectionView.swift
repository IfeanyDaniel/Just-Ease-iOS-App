//
//  ReportViolationColletionView.swift
//  JustEaseApp
//
//  Created by iOSApp on 25/02/2023.
//


import UIKit
extension ReportViolationController {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        reportViolationCategoriesViewModel.numberOfRowsInSection().count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = violationCollectionView.dequeueReusableCell(withReuseIdentifier: ReportViolationCell.identifier, for: indexPath) as? ReportViolationCell else { return UICollectionViewCell() }
        let category = reportViolationCategoriesViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.row]
       // let card =  cards[indexPath.row]
        cell.index = indexPath
        cell.reportViolationDelegate = self
        cell.cardLabel.text = category.name
        cell.backgroundColor = category.color
        cell.image.setImage(UIImage(named: category.imageName), for: .normal)
        cell.layer.cornerRadius = 10
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.size.width / 2 - 30, height: 150)
    }
    
    func collectionView(collectionView: UICollectionView,
            layout collectionViewLayout: UICollectionViewLayout,
            minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 2.0
    }

    func collectionView(collectionView: UICollectionView, layout
            collectionViewLayout: UICollectionViewLayout,
            minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 2.0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let category = reportViolationCategoriesViewModel.cellForRowsAt(indexPath: indexPath)[indexPath.item]
        let controller = StepOneReportController()
        controller.reportType = category.subCategories
        controller.reportCategory = category.name
        controller.reportCategoryId = category.id
        Storage.saveViolationCategory(id: category.id)
        print(category.id)
        navigationController?.pushViewController(controller, animated: true)

    }
}
