//
//  StepOnecollectionView.swift
//  JustEaseApp
//
//  Created by iOSApp on 25/02/2023.
//

import UIKit
extension StepOneReportController {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return reportType.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = violationTypeCollectionView.dequeueReusableCell(withReuseIdentifier: ViolationTypeCell.identifier, for: indexPath) as? ViolationTypeCell else { return UICollectionViewCell() }
        cell.cardLabel.text = reportType[indexPath.row].name
        cell.violationTypeDelegate = self
        cell.index = indexPath
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: contentView.frame.width - 50, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard lastIndex != indexPath else { return }
        if let index = lastIndex {
            let cell = violationTypeCollectionView.cellForItem(at: index) as? ViolationTypeCell
            cell?.isSelected = false
            cell?.optionButton.setImage(UIImage(named: "unSelectedOption"), for: .normal)
        }
        let cell = violationTypeCollectionView.cellForItem(at: indexPath) as? ViolationTypeCell
        cell?.isSelected = true
        lastIndex = indexPath
        cell?.optionButton.setImage(UIImage(named: "selectedOption"), for: .normal)
        subCategoryId =  reportType[indexPath.item].id
        print(subCategoryId)
        
    }
    
}
