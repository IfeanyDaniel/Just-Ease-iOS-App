//
//  StepOneConstriant.swift
//  JustEaseApp
//
//  Created by iOSApp on 25/02/2023.
//

import UIKit
extension StepOneReportController {
    //MARK: - Layout subviews for the scroll views
    func setupScrollView() {
      view.addSubview(scrollView)
      scrollView.addSubview(contentView)
      NSLayoutConstraint.activate([
        scrollView.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 20),
        scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
        scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
        
        contentView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
        contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
        contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
        contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
        
      ])
    }
    //MARK: - Layout subviews
    func layoutViews() {
    
        view.addSubview(label)
        contentView.addSubview(stepsIcon)
        contentView.addSubview(LgaButton)
        contentView.addSubview(lgaDropDownIcon)
        contentView.addSubview(labelBelowLga)
        contentView.addSubview(violationTypeCollectionView)
        contentView.addSubview(issueTextField)
        contentView.addSubview(dateTextField)
        contentView.addSubview(timeTextField)
        contentView.addSubview(proceedButton)
        
      self.navigationItem.setHidesBackButton(true, animated: true)
      view.backgroundColor = backgroundSystemColor
      
      NSLayoutConstraint.activate([
        
        label.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
        label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 25),
        label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -25),
        
        stepsIcon.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
        stepsIcon.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 25),
        stepsIcon.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -25),
        stepsIcon.heightAnchor.constraint(equalToConstant: 40),
        
        LgaButton.topAnchor.constraint(equalTo: stepsIcon.bottomAnchor, constant: 20),
        LgaButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 25),
        LgaButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -25),
        LgaButton.heightAnchor.constraint(equalToConstant: 65),
        
        lgaDropDownIcon.topAnchor.constraint(equalTo: LgaButton.topAnchor, constant: 18),
        lgaDropDownIcon.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -40),
        
        labelBelowLga.topAnchor.constraint(equalTo: LgaButton.bottomAnchor, constant: 20),
        labelBelowLga.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 25),
        labelBelowLga.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -25),
        
        violationTypeCollectionView.topAnchor.constraint(equalTo: labelBelowLga.bottomAnchor, constant: 20),
        violationTypeCollectionView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 25),
        violationTypeCollectionView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -25),
        violationTypeCollectionView.heightAnchor.constraint(equalToConstant: CGFloat(reportType.count * 40)),
        
        issueTextField.topAnchor.constraint(equalTo: violationTypeCollectionView.bottomAnchor, constant: 20),
        issueTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 25),
        issueTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -25),
        issueTextField.heightAnchor.constraint(equalToConstant: textFieldHeight),
        
        dateTextField.topAnchor.constraint(equalTo: issueTextField.bottomAnchor, constant: 20),
        dateTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 25),
        dateTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -25),
        dateTextField.heightAnchor.constraint(equalToConstant: textFieldHeight),
        
        timeTextField.topAnchor.constraint(equalTo: dateTextField.bottomAnchor, constant: 20),
        timeTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 25),
        timeTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -25),
        timeTextField.heightAnchor.constraint(equalToConstant: textFieldHeight),
        
        proceedButton.topAnchor.constraint(equalTo: timeTextField.bottomAnchor, constant: 20),
        proceedButton.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 25),
        proceedButton.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -25),
        proceedButton.heightAnchor.constraint(equalToConstant: buttonHeight),
        
      ])
    }
}
