//
//  StepOneCell.swift
//  JustEaseApp
//
//  Created by iOSApp on 25/02/2023.
//

import UIKit
protocol ViolationTypeDelegate: AnyObject {
    func onClickViolationTypeButton(index: IndexPath)
}
class ViolationTypeCell: UICollectionViewCell {
    static var identifier: String = "ViolationTypenCell"
    var violationTypeDelegate: ViolationTypeDelegate?
    var index : IndexPath?
    var card: Cards? {
        didSet {
            guard let card = card else {
                return
            }
            let cardName = card.title
            cardLabel.text = cardName
        }
    }
    
    lazy var cardLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textAlignment = .left
        label.textColor = textSystemColor
        label.font = UIFont(name: AppFonts.silkLight.font, size: 14)
        return label
    }()
    
    lazy var optionButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "unSelectedOption"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(clickOptionButton(_:)), for: .touchUpInside)
        return button
    }()
    @objc func clickOptionButton(_ sender: Any){
        guard let indexRow = index else {
            return
        }
        violationTypeDelegate?.onClickViolationTypeButton(index: indexRow)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpViews() {
        addSubview(cardLabel)
        addSubview(optionButton)
        NSLayoutConstraint.activate([
            optionButton.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            optionButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            optionButton.heightAnchor.constraint(equalToConstant: 25),
            optionButton.widthAnchor.constraint(equalToConstant: 25),
            
            cardLabel.topAnchor.constraint(equalTo: topAnchor, constant: 13),
            cardLabel.leadingAnchor.constraint(equalTo: optionButton.leadingAnchor, constant: 30),
            
        ])
    }
}
