//
//  StepOneReportController.swift
//  JustEaseApp
//
//  Created by iOSApp on 25/02/2023.
//

import UIKit
import CoreLocation
import GoogleMaps
import CoreLocation
import MapKit
class StepOneReportController: UIViewController,UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout, UITextFieldDelegate {
    var lastIndex:IndexPath?
    let dateFormatter = DateFormatter()
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var subCategoryId = 0
    var getDate = ""
    var getTime = ""
    
    
    // MARK: - Scroll view
    var reportCategory = ""
    var reportCategoryId = 0
    
    var lg_id = 0
    
    var reportType : [SubCategories] = [] 
    lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.showsHorizontalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    // MARK: - content view
    lazy var contentView: UIView = {
        let content = UIView()
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.font = UIFont(name: AppFonts.silkabold.font ,size: 20)
        label.text = "Transport"
        return label
    }()
    
    lazy var stepsIcon: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "step1"), for: .normal)
        return button
    }()
    
    lazy var LgaButton: UIButton = {
        let button = UIButton.selectButtonDesign()
        button.setTitle(" Select a Local Government Area", for: .normal)
        button.addTarget(self, action: #selector(didtapOnLgaButton), for: .touchUpInside)
        return button
    }()
    lazy var lgaDropDownIcon: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.dropDown.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    //MARK: - Label below welcome back
    lazy var labelBelowLga: UILabel = {
        let label = UILabel.labelTextDesign()
        label.font = UIFont(name: AppFonts.silkaMedium.font ,size: 14)
        label.text = "What kind of Violation are you reporting?"
        return label
    }()
    
    lazy var violationTypeCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = backgroundSystemColor
        collectionView.showsVerticalScrollIndicator = false
        collectionView.isUserInteractionEnabled = true
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    lazy var issueTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Nature of issue",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
     // textField.addTarget(self, action: #selector(validateViews(_:)), for: .editingChanged)
        return textField
    }()
    lazy var dateTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Date of incident",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        return textField
    }()
    lazy var timeTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Time of incident",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        return textField
    }()
    lazy var proceedButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Proceed", for: .normal)
        button.layer.masksToBounds = true
        button.titleLabel?.font = UIFont(name: AppFonts.silka.font, size: 18)
        button.addTarget(self, action: #selector(didTapProceedButton), for: .touchUpInside)
        return button
    }()
    func registercCell() {
        violationTypeCollectionView.register(ViolationTypeCell.self, forCellWithReuseIdentifier: ViolationTypeCell.identifier)
    }
    @objc func didtapOnLgaButton() {
        
    }
    @objc func didTapProceedButton() {
        if issueTextField.text == "" && dateTextField.text == "" && timeTextField.text == "" {
            Toast.shared.showToastWithTItle("Please all fields are required", type: .error)
        } else if subCategoryId == 0 {
            Toast.shared.showToastWithTItle("Please select the violation you are reporting", type: .error)
        }  else {
            let controller = StepTwoReportController()
            controller.headingTitle = reportCategory
            controller.report_violation_category_id = reportCategoryId
            print(reportCategoryId)
            controller.report_violation_subcategory_id =  subCategoryId
            controller.local_government_id = lg_id
            controller.nature_of_issue = issueTextField.text ?? ""
            controller.time_of_incident = getTime
            controller.date_of_incident = getDate
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    func showDoneButton(){
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dropKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.issueTextField.inputAccessoryView = toolbar
        self.dateTextField.inputAccessoryView = toolbar
        self.timeTextField.inputAccessoryView = toolbar
    }
    @objc func dropKeyboard(){
        view.endEditing(true)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {

        if textField == self.issueTextField  {
            notifyKeyboard()
        }
        if textField == self.dateTextField && textField == self.timeTextField {
            notifyKeyboard()
        }
    }
    func notifyKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil);
    }
    
      @objc func keyboardWillShow(sender: NSNotification) {
           self.contentView.frame.origin.y = -350 // Move view 150 points upward
      }

      @objc func keyboardWillHide(sender: NSNotification) {
           self.contentView.frame.origin.y = 0 // Move view to original position
      }
    @objc func datePickerDone() {
        dateTextField.resignFirstResponder()
    }
    // MARK: - SET START DATE
    func setIncidentDate() {
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.preferredDatePickerStyle = .wheels
        dateTextField.inputView = datePickerView
        datePickerView.frame = CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 200)
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        let doneButton = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(self.datePickerDone))
        let toolBar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 35))
        toolBar.setItems([UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil), doneButton], animated: true)
        dateTextField.inputAccessoryView = toolBar
    }
    @objc func handleDatePicker(sender: UIDatePicker) {
        let actualDateSender =  DateFormatter()
        actualDateSender.dateFormat = "yyyy-MM-dd"
        getDate = actualDateSender.string(from: sender.date)
        
        let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.dateFormat = "EEEE, MMM d, yyyy"
        dateTextField.text = dateFormatter.string(from: sender.date)
    }
   
    func setIncidentTime() {
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .time
        datePickerView.preferredDatePickerStyle = .wheels
        timeTextField.inputView = datePickerView
        datePickerView.frame = CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 200)
        datePickerView.addTarget(self, action: #selector(handleTimePicker(sender:)), for: .valueChanged)
        let doneButton = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(self.timeDatePickerDone))
        let toolBar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 35))
        toolBar.setItems([UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil), doneButton], animated: true)
      
        timeTextField.inputAccessoryView = toolBar
    }
    @objc func timeDatePickerDone() {
        timeTextField.resignFirstResponder()
    }
    @objc func handleTimePicker(sender: UIDatePicker) {
        let actualDateSender =  DateFormatter()
        actualDateSender.dateFormat = "hh:mm:ss a"
        getTime = actualDateSender.string(from: sender.date)
        timeTextField.text = actualDateSender.string(from: sender.date)
    }
   
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        setupScrollView()
        self.hideKeyboardWhenTappedAround()
        registercCell()
        showDoneButton()
        displayData()
        issueTextField.delegate = self
        timeTextField.delegate = self
        dateTextField.delegate = self
        setIncidentDate()
        setIncidentTime()
        displayData()
        getLgaArea()
    }
    func displayData() {
        DispatchQueue.main.async { [self] in
            self.label.text = reportCategory
            self.violationTypeCollectionView.reloadData()
        }
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: false)
        reportType.removeAll()
    }
    func getLgaArea() {
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            guard let currentLocation = locManager.location else {
                return
            }
            let location = CLLocation(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
            CLGeocoder().reverseGeocodeLocation(location){ placemarks , error in
                if error == nil && placemarks!.count > 0 {
                    guard let placemark = placemarks?.last else {
                        return
                    }
                    self.LgaButton.setTitle(" \(placemark.locality ?? "")", for: .normal)
                    self.LgaButton.setTitleColor(textSystemColor, for: .normal)
                }
                
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
        
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.setHidesBackButton(true, animated: true)
        
    }
    
    // MARK: - View did layout subviews
    override func viewDidLayoutSubviews() {
        scrollView.isScrollEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.contentSize = CGSize(width: 400, height: 1250)
    }
}
extension StepOneReportController : ViolationTypeDelegate {
    func onClickViolationTypeButton(index: IndexPath) {
        guard lastIndex != index else { return }
        if let index = lastIndex {
            let cell = violationTypeCollectionView.cellForItem(at: index) as? ViolationTypeCell
            cell?.isSelected = false
            cell?.optionButton.isHidden = false
            cell?.optionButton.setImage(UIImage(named: "unSelectedOption"), for: .normal)
        }
        let cell = violationTypeCollectionView.cellForItem(at: index) as? ViolationTypeCell
        cell?.isSelected = true
        lastIndex = index
        cell?.optionButton.setImage(UIImage(named: "selectedOption"), for: .normal)
        let data =  reportType[index.item]
        subCategoryId =  reportType[index.item].id
        print(data)
        print(subCategoryId)
    }
    
}
