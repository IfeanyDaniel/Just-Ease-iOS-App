//
//  StepThreeReportController.swift
//  JustEaseApp
//
//  Created by iOSApp on 25/02/2023.
//

import UIKit
import FittedSheets
class StepThreeController: UIViewController {
    // MARK: - Scroll view
    var report_violation_category_id = 0
    var report_violation_subcategory_id = 0
    var local_government_id = 0
    var nature_of_issue = ""
    var date_of_incident = ""
    var time_of_incident = ""
    var reportIsuue = ""
    var anonymous = 0
    var address = ""
    
    var userProfileViewModel = UserProfileViewModel()
    var reportViolationViewModel = ReportViolationViewModel()
    var headingTitle = ""
    lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.showsHorizontalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    // MARK: - content view
    lazy var contentView: UIView = {
        let content = UIView()
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.font = UIFont(name: AppFonts.silkabold.font ,size: 20)
        label.text = "Transport"
        return label
    }()
    
    lazy var stepsIcon: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "step3"), for: .normal)
        return button
    }()
    lazy var firstLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.textColor = placeholderSystemGrayColor
        label.text = "First Name"
        label.numberOfLines = 1
        return label
    }()
    lazy var firstNameTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.addTarget(self, action: #selector(validateViews(_:)), for: .editingChanged)
        return textField
    }()
    //MARK: - AlL VALIDATION LABEL
    lazy var firstNameValidationLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        return label
    }()
    
    lazy var lastNameLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.textColor = placeholderSystemGrayColor
        label.text = "Last Name"
        label.numberOfLines = 1
        return label
    }()
    lazy var lastNameTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.addTarget(self, action: #selector(validateViews(_:)), for: .editingChanged)
        return textField
    }()
    lazy var lastNameValidationLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        return label
    }()
    
    lazy var emailLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.textColor = placeholderSystemGrayColor
        label.text = "Email Address"
        label.numberOfLines = 1
        return label
    }()
    lazy var emailAddressTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.keyboardType = .emailAddress
        textField.addTarget(self, action: #selector(validateViews(_:)), for: .editingChanged)
        textField.autocapitalizationType = .none
        textField.autocorrectionType = .no
        return textField
    }()
    lazy var emailValidationLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.numberOfLines = 1
        return label
    }()
    
    lazy var phoneLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.textColor = placeholderSystemGrayColor
        label.text = "Phone Number"
        label.numberOfLines = 1
        return label
    }()
    lazy var phoneTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.keyboardType = .numberPad
        return textField
    }()
    
    lazy var locationLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.textColor = placeholderSystemGrayColor
        label.text = "Location"
        label.numberOfLines = 1
        return label
    }()
    lazy var locationTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        return textField
    }()
    lazy var reportButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Report Violation", for: .normal)
        button.layer.masksToBounds = true
        button.titleLabel?.font = UIFont(name: AppFonts.silka.font, size: 18)
        button.addTarget(self, action: #selector(didTapReportButton), for: .touchUpInside)
        return button
    }()
    // MARK: - Login function
    @objc func didTapReportButton(){
        if InternetConnectionManager.isConnectedToNetwork(){
            Loader.shared.showLoader()
            print(report_violation_category_id)
            let request = ReportViolationRequest(report_violation_category_id: Storage.getViolationCategoryId(), report_violation_subcategory_id: report_violation_subcategory_id, local_government_id: 7, nature_of_issue: nature_of_issue, date_of_incident: date_of_incident, time_of_incident: time_of_incident, details_of_isssue: reportIsuue, user_id: Storage.getUserId(), report_violation_address: address, anonymous_status: 1)
            print(request)
            reportViolationViewModel.getReportViolationResponse(reportViolationRequest: request)
        } else {
            Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
        }
    
    }
    func showReportSuccess() {
        let controller = CrimeSuccessAlert()
        controller.statement.text = "Thank you! your violation had been reported successfully, we are on it and will get back to you with updates as soon as possible."
        controller.labelBelowIconButton.text = "Violation Reported"
        controller.delegate = self
        let sheetController = SheetViewController(controller: controller, sizes: [.fixed(450), .fixed(450)])
        sheetController.blurBottomSafeArea = false
        sheetController.topCornersRadius = 30
        sheetController.handleView.isHidden = true
        self.present(sheetController, animated: false, completion: nil)
    }
    func showDoneButton(){
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dropKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.firstNameTextField.inputAccessoryView = toolbar
        self.lastNameTextField.inputAccessoryView = toolbar
        self.emailAddressTextField.inputAccessoryView = toolbar
        self.phoneTextField.inputAccessoryView = toolbar
        self.locationTextField.inputAccessoryView = toolbar
    }
    @objc func dropKeyboard(){
        view.endEditing(true)
    }
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        showDoneButton()
        layoutViews()
        setupScrollView()
        self.hideKeyboardWhenTappedAround()
        label.text = headingTitle
        userProfileViewModel.delegate = self
        userProfileViewModel.getUserProfileData(userID: Storage.getUserId())
        reportViolationViewModel.delegate = self
    }
    @objc func goBack() {
        pop(numberOfTimes: 2)
    }
    @objc  func didTapOnCancel() {
        navigationController?.popViewController(animated: true)
    }
    @objc func validateViews(_ textField: UITextField) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.validateFirstNameField()
            self.validateLastNameField()
            self.validateEmail()
        }
    }
    func pop(numberOfTimes: Int) {
        guard let navigationController = navigationController else {
            print("No navigation controller")
            return
        }
        let viewControllers = navigationController.viewControllers
        let index = numberOfTimes + 1
        if viewControllers.count >= index {
            navigationController.popToViewController(viewControllers[viewControllers.count - index], animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
        
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.setHidesBackButton(true, animated: true)
        
    }
    // MARK: - View did layout subviews
    override func viewDidLayoutSubviews() {
        scrollView.isScrollEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.contentSize = CGSize(width: 400, height: 900)
    }
}
