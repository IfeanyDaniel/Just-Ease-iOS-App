//
//  StepThreeExtension.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/8/23.
//

import Foundation
extension StepThreeController: UserProfileDelegate , ReportDelegate, ReportViolationViewModelDelegate {
    func didTapOnReport() {}
    func popController() {
        self.dismiss(animated: false){
            self.pop(numberOfTimes: 4)
        }
    }
    
    func didReceiveUserProfileResponse(userProileResponse: LoginResponse?) {
        if userProileResponse?.status ==  200 {
            Loader.shared.hideLoader()
            firstNameTextField.text = userProileResponse?.data?.first_name
            lastNameTextField.text = userProileResponse?.data?.last_name
            emailAddressTextField.text = userProileResponse?.data?.email
            locationTextField.text = userProileResponse?.data?.address
           // setTitle("  \(userProileResponse?.data?.address ?? "")", for: .normal)
            phoneTextField.text = userProileResponse?.data?.phone_number
        }
        if userProileResponse?.status ==  422 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Something went wrong", type: .error)
        }
    }
    
    func didReceiveReportViolationResponse(reportViolationResponse: ReportViolationResponse?, _ statusCode: Int) {
        if reportViolationResponse?.status == 200 {
            Loader.shared.hideLoader()
            showReportSuccess()
        }
        if reportViolationResponse?.status == 400 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("\(reportViolationResponse?.message ?? "")", type: .error)
        }
        if reportViolationResponse?.status == 422 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("\(reportViolationResponse?.message ?? "")", type: .error)
        }
    }
}
