//
//  CreateReportViolationModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/7/23.
//
import Foundation
struct ReportViolationResponse: Decodable {
    let status: Int
    let message: String
  //  let data: [ReportViolationData]
}
//struct ReportViolationData: Decodable {
//    let report_violation_id: Int
//    let user_id: String
//    let anonymous_status: String
//    let details_of_isssue: String
//    let report_violation_address: String
//    let date_of_incident: String
//    let longitude: Double
//    let latitude: Double
//}

