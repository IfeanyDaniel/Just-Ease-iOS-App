//
//  CreateReportViolationRequest.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/7/23.
//

import Foundation
struct ReportViolationRequest: Encodable {
    let report_violation_category_id: Int
    let report_violation_subcategory_id: Int
    let local_government_id: Int
    let nature_of_issue: String
    let date_of_incident: String
    let time_of_incident: String
    let details_of_isssue: String
    let user_id: Int
    let report_violation_address: String
    let anonymous_status: Int
}
