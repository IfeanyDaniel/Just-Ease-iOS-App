//
//  ReportViolationCategoryModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/7/23.
//

import Foundation
struct ReportViolationCategoriesResponse: Decodable {
    let status: Int
    let message: String
    let data: ReportViolationCategories
}
struct ReportViolationCategories: Decodable {
    let current_page: Int
    let data: [ReportViolationSubCategories]
    let total : Int
}
struct ReportViolationSubCategories: Decodable {
    let id: Int
    let name: String
    let report_violation_sub_categories: [SubCategoriesInfo]
}
struct SubCategoriesInfo: Decodable {
    let id: Int
    let report_violation_category_id: Int
    let name: String
}
