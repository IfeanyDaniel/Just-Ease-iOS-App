//
//  ReportViolationCategoryViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/7/23.
//
import Foundation
import UIKit
struct SubCategories {
    let id : Int
    let name: String
    let categoryId : Int
}
struct Categories {
    let id: Int
    let name : String
    let imageName: String
    let color: UIColor
    let subCategories : [SubCategories]
}
class ReportViolationCategoriesViewModel {
    var reportViolationCategories :[Categories] = []
    var subCategories :[SubCategories]  = []
    func getReportViolationCategories(completion: @escaping () -> Void ) {
        let reportViolationCategoriesResource = ReportViolationCategoriesResource()
        reportViolationCategoriesResource.getReportCategoriesResponse { reportViolationCategoriesApiResponse in
            DispatchQueue.main.async { [self] in
                let totalCategories =  reportViolationCategoriesApiResponse?.data.total ?? 0
                let totalCategoriesData = reportViolationCategoriesApiResponse?.data.data
                Storage.saveReportCategoryTotalRecord(number: totalCategories)
                reportViolationCategories.removeAll()
                for index in 0..<(totalCategoriesData?.count ?? 0) {
                    let categoryId = totalCategoriesData?[index].id ?? 0
                    let categoryName = totalCategoriesData?[index].name ?? ""
                    let subCategory = totalCategoriesData?[index].report_violation_sub_categories
                    subCategories.removeAll()
                    for index in 0..<(subCategory?.count ?? 0) {
                        let id = subCategory?[index].id ?? 0
                        let categoryId = subCategory?[index].report_violation_category_id ?? 0
                        let categoryName = subCategory?[index].name ?? ""
                        subCategories.append(SubCategories(id: id, name: categoryName, categoryId: categoryId))
                    } 
                    if categoryName == "INFRASTRUCTURE" {
                        reportViolationCategories.append(Categories(id: categoryId, name: categoryName, imageName: "infrastructure", color: AppColors.lightPurple.color, subCategories: subCategories.compactMap({return $0})))
                    } else if categoryName == "ENVIRONMENT" {
                        reportViolationCategories.append(Categories(id: categoryId, name: categoryName, imageName: "environment", color: AppColors.lightPink.color, subCategories: subCategories.compactMap({return $0})))
                    } else if categoryName == "SECURTY ISSUES" {
                        reportViolationCategories.append(Categories(id: categoryId, name: categoryName, imageName: "security", color: AppColors.lightBlue.color, subCategories: subCategories.compactMap({return $0})))
                    } else if categoryName == "TRANSPORT" {
                        reportViolationCategories.append(Categories(id: categoryId, name: categoryName, imageName: "transport", color: AppColors.lightMilky.color, subCategories: subCategories.compactMap({return $0})))
                    } else {
                        reportViolationCategories.append(Categories(id: categoryId, name: categoryName, imageName: "health", color: AppColors.lightOrange.color, subCategories: subCategories.compactMap({return $0})))
                    }
                    // sorting the result uusing login
                    self.reportViolationCategories =  self.reportViolationCategories.sorted(by: {$0.id < $1.id})
                    print(">>\(reportViolationCategories)")
                }
                completion()
            }
        }
    }
    func numberOfRowsInSection() -> [Categories]  {
        return reportViolationCategories
    }
    func cellForRowsAt(indexPath: IndexPath) -> [Categories] {
        return reportViolationCategories
    }
}
