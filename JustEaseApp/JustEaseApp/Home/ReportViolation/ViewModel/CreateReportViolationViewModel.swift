//
//  CreateReportViolationViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/7/23.
//

import Foundation
protocol ReportViolationViewModelDelegate {
    func didReceiveReportViolationResponse(reportViolationResponse: ReportViolationResponse?,_ statusCode: Int)
}
class ReportViolationViewModel {
    var delegate: ReportViolationViewModelDelegate?
    func getReportViolationResponse(reportViolationRequest: ReportViolationRequest) {
        let reportViolationResource = ReportViolationResource()
        reportViolationResource.getResponse(createReportViolationRequest: reportViolationRequest) { reportViolationApiResponse, statusCode in
            if statusCode ==  200 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveReportViolationResponse(reportViolationResponse: reportViolationApiResponse, statusCode)
                }
            }
            if statusCode ==  400 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveReportViolationResponse(reportViolationResponse: reportViolationApiResponse, statusCode)
                }
            }
            if statusCode ==  500 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveReportViolationResponse(reportViolationResponse: reportViolationApiResponse, statusCode)
                }
            }
            if statusCode ==  422 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveReportViolationResponse(reportViolationResponse: reportViolationApiResponse, statusCode)
                }
            }
        }
    }
}
