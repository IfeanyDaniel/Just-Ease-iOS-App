//
//  ReportViolationCategories.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/7/23.
//

import Foundation
struct ReportViolationCategoriesResource {
    func getReportCategoriesResponse(completionHandler: @escaping (_ result: ReportViolationCategoriesResponse?) -> Void) {
        let httpUtility = HTTPUtility()
        let reportViolationCategoriesUrl = ApiEndpoints.reportViolationCategoriesEndPoint
        let url = URL(string: reportViolationCategoriesUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType: ReportViolationCategoriesResponse.self) { result in
                completionHandler(result)
            }
        }
    }
}
