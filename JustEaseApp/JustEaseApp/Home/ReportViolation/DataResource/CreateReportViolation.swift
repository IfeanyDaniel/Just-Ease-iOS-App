//
//  CreateReportViolation.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 5/7/23.
//

import Foundation
struct ReportViolationResource {
    func getResponse(createReportViolationRequest: ReportViolationRequest, completionHandler: @escaping (_ result: ReportViolationResponse?,_ statusCode: Int) -> Void) {
        let reportViolationURL = URL(string: ApiEndpoints.createReportViolationEndPoint)!
        let httpUtility = HTTPUtility()
        do {
            let reportViolationPostBody = try JSONEncoder().encode(createReportViolationRequest)
            httpUtility.postAuthorizationResponse(requestUrl:  reportViolationURL, requestBody:  reportViolationPostBody, resultType: ReportViolationResponse.self) { reportViolationApiResponse, statusCode in
                completionHandler(reportViolationApiResponse,statusCode)
                print("\(reportViolationApiResponse)\(statusCode)")
            }
        } catch let error {
            debugPrint(error)
        }
    }
}
