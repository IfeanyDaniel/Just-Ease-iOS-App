//
//  SignUpController.swift
//  JustEaseApp
//
//  Created by iOSApp on 09/02/2023.
//


import UIKit
import Lottie
class SignUpViewController: UIViewController {
    var signUpViewModel = SignUpViewModel()
    var authViewModel = AuthViewModel()
    var registerDeviceViewModel = RegisterDeviceViewModel()
    // MARK: - Scroll view
    lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.showsHorizontalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    // MARK: - content view
    lazy var contentView: UIView = {
        let content = UIView()
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    
    lazy var heyLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Hey there!👋🏾"
        label.textColor = AppColors.green.color
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 25)
        label.textAlignment = .left
        return label
    }()
    
    //MARK: - Label below welcome back
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Create an account to get started."
        return label
    }()
    lazy var firstNameTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "First Name",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.addTarget(self, action: #selector(validateViews(_:)), for: .editingChanged)
        return textField
    }()
    //MARK: - AlL VALIDATION LABEL
    lazy var firstNameValidationLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        return label
    }()
    
    lazy var lastNameTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Last Name",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.addTarget(self, action: #selector(validateViews(_:)), for: .editingChanged)
        return textField
    }()
    lazy var lastNameValidationLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        return label
    }()
    lazy var phoneNumberTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Phone Number (Optional)",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.keyboardType = .numberPad
        return textField
    }()
    lazy var emailAddressTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Email",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.addTarget(self, action: #selector(validateViews(_:)), for: .editingChanged)
        textField.keyboardType = .emailAddress
        textField.autocapitalizationType = .none
        textField.autocorrectionType = .no
        return textField
    }()
    
    lazy var emailValidationLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.numberOfLines = 1
        return label
    }()
    
    lazy var passwordTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Password",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.addTarget(self, action: #selector(validateViews(_:)), for: .editingChanged)
        textField.isSecureTextEntry = true
        textField.autocapitalizationType = .none
        return textField
    }()
    
    lazy var passwordValidationLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.numberOfLines = 1
        return label
    }()
    
    lazy var signUpButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Sign Up", for: .normal)
        button.addTarget(self, action: #selector(didTapSignUpButton), for: .touchUpInside)
        button.isEnabled = false
        button.backgroundColor = AppColors.grayBorderColor.color
        return button
    }()
    lazy var openEyeButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didTapOnOpenEyeButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setBackgroundImage(AppButtonImages.focus.image, for: .normal)
        button.isHidden = true
        return button
    }()
    
    lazy var closeEyeButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didTapOnCloseEyeButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setBackgroundImage(AppButtonImages.noFocus.image, for: .normal)
        return button
    }()
    lazy var ownAccountButton: UIButton = {
        let button = UIButton()
        button.setTitle("Already have an account?  ", for: .normal)
        button.setTitleColor(textSystemColor, for: .normal)
        button.titleLabel?.font = UIFont(name:  AppFonts.silka.font, size: 16)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var signInButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Sign In", for: .normal)
        button.setTitleColor(AppColors.green.color, for: .normal)
        button.backgroundColor = AppColors.lightGreen.color
        button.titleLabel?.font = UIFont(name: AppFonts.silka.font, size: 12)
        button.addTarget(self, action: #selector(didTapOnSignIn), for: .touchUpInside)
        return button
    }()
    // MARK: - password visibility function
    @objc func didTapOnOpenEyeButton() {
        openEyeButton.isHidden = true
        closeEyeButton.isHidden = false
        passwordTextField.isSecureTextEntry = true
    }
    
    @objc func didTapOnCloseEyeButton() {
        passwordTextField.isSecureTextEntry = false
        openEyeButton.isHidden = false
        closeEyeButton.isHidden = true
        
    }
    // MARK: - Login function
    @objc func didTapSignUpButton(){
        if InternetConnectionManager.isConnectedToNetwork(){
            Loader.shared.showLoader()
            let request = AuthRequest(email: LoginDetails().email, password: LoginDetails().password)
            authViewModel.authResponse(authRequest: request)
        } else {
            Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
        }
    }
    
    @objc func didTapOnSignIn() {
        navigationController?.pushViewController(LoginViewController(), animated: true)
    }
    func showDoneButton(){
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dropKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.firstNameTextField.inputAccessoryView = toolbar
        self.lastNameTextField.inputAccessoryView = toolbar
        self.phoneNumberTextField.inputAccessoryView = toolbar
        self.emailAddressTextField.inputAccessoryView = toolbar
        self.passwordTextField.inputAccessoryView = toolbar
    }
    @objc func dropKeyboard(){
        view.endEditing(true)
    }
    @objc func validAllFields() {
        checkingAllFields()
    }
    func checkingAllFields() {
        if  firstNameTextField.text != "" && lastNameTextField.text != "" && emailAddressTextField.text != "" && passwordTextField.text != "" && emailAddressTextField.layer.borderColor != UIColor.red.cgColor && passwordTextField.layer.borderColor != UIColor.red.cgColor && lastNameTextField.layer.borderColor != UIColor.red.cgColor && firstNameTextField.layer.borderColor != UIColor.red.cgColor {
            signUpButton.isEnabled = true
            signUpButton.backgroundColor = AppColors.green.color
        } else {
            signUpButton.backgroundColor = AppColors.grayBorderColor.color
            signUpButton.isEnabled = false
        }
    }
    // MARK: - ... Validation of all textfield
    @objc func validateViews(_ textField: UITextField) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.validateFirstNameField()
            self.validateLastNameField()
            self.validatePasswordField()
            self.validateEmail()
            self.validAllFields()
        }
    }
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        showDoneButton()
        layoutViews()
        setupScrollView()
        self.hideKeyboardWhenTappedAround()
        authViewModel.delegate = self
        registerDeviceViewModel.delegate = self
    }
    
    // MARK: - View did layout subviews
    override func viewDidLayoutSubviews() {
        scrollView.isScrollEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.contentSize = CGSize(width: 400, height: 900)
    }
}

