//
//  SignUpValidations.swift
//  JustEaseApp
//
//  Created by iOSApp on 12/02/2023.
//

import UIKit
extension SignUpViewController {
    // MARK: - ... Validation of first name text field
    func validateFirstNameField() {
        let text = firstNameTextField.text
        let nameRegEx  = "^(?=.{2,100}$)[A-Za-zÀ-ú][A-Za-zÀ-ú'-]+(?: [A-Za-zÀ-ú'-]+)* *$"
        let textTest = NSPredicate(format: "SELF MATCHES %@", nameRegEx)
        let firstnameResult = textTest.evaluate(with: text)
        if (firstnameResult == true && text?.contains(" ") == false) || firstNameTextField.text == "" {
            firstNameValidationLabel.text = ""
            firstNameTextField.layer.borderColor = AppColors.lightGray.color.cgColor
            firstNameTextField.layer.borderWidth = 1
        } else {
            firstNameValidationLabel.text = "Name must be more than 2 character with no space "
            firstNameValidationLabel.textColor = AppColors.red.color
            firstNameTextField.layer.borderColor = UIColor.red.cgColor
            firstNameTextField.layer.borderWidth = 1
        }
    }
    // MARK: - ... Validation of last name text field
    func validateLastNameField() {
        let text = lastNameTextField.text
        let nameRegEx  = "^(?=.{2,100}$)[A-Za-zÀ-ú][A-Za-zÀ-ú'-]+(?: [A-Za-zÀ-ú'-]+)* *$"
        let textTest = NSPredicate(format: "SELF MATCHES %@",nameRegEx)
        let lastnameResult = textTest.evaluate(with: text)
        if (lastnameResult == true && text?.contains(" ") == false) || lastNameTextField.text == ""{
            lastNameValidationLabel.text = ""
            lastNameTextField.layer.borderColor = AppColors.lightGray.color.cgColor
            lastNameTextField.layer.borderWidth = 1
            signUpButton.isEnabled = true
        } else {
            lastNameValidationLabel.text = "Name must be more than 2 character with no space "
            lastNameValidationLabel.textColor = AppColors.red.color
            lastNameTextField.layer.borderColor = UIColor.red.cgColor
            lastNameTextField.layer.borderWidth = 1
        }
    }
    // MARK: - ... Validation of email format

    func validateEmail() {
        guard let text = emailAddressTextField.text else { return }
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        let emailResult = emailPredicate.evaluate(with: text)
        if emailResult == true || emailAddressTextField.text == "" {
            emailValidationLabel.text = ""
            emailAddressTextField.layer.borderColor = AppColors.lightGray.color.cgColor
            emailAddressTextField.layer.borderWidth = 1
        } else {
            emailValidationLabel.text = "Please enter a valid email address"
            emailValidationLabel.textColor = AppColors.red.color
            emailAddressTextField.layer.borderColor = UIColor.red.cgColor
            emailAddressTextField.layer.borderWidth = 1
        }
    }
    // MARK: - ... Validation of password text field
    func validatePasswordField() {
        guard let text = passwordTextField.text else { return }
        let capitalLetterRegEx  =  "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&<>*~:`-]).{8,}$"
        let textTest = NSPredicate(format: "SELF MATCHES %@", capitalLetterRegEx)
        let passwordValidility = textTest.evaluate(with: text)
        if passwordValidility == true || passwordTextField.text == "" {
            passwordValidationLabel.text = ""
            passwordTextField.layer.borderColor = AppColors.lightGray.color.cgColor
            passwordTextField.layer.borderWidth = 1
            signUpButton.isEnabled = true
        }  else {
            passwordValidationLabel.text = "1 upper case, 1 special character , 1 number and minimum of 8 characters"
            passwordValidationLabel.textColor = AppColors.red.color
            passwordTextField.layer.borderColor = UIColor.red.cgColor
            passwordTextField.layer.borderWidth = 1
            signUpButton.isEnabled = false
        }
    }

    
}
//struct ValidationResult {
//    let success: Bool
//    let error : String?
//}
//
//struct SignUpValidation {
//    
//    func validate(signUpRequest: SignUpRequest) -> ValidationResult {
//        
//        if (signUpRequest.first_name.isEmpty) {
//            return ValidationResult(success: false, error: "First name field cannot be empty!")
//        }
//        
//        if (signUpRequest.last_name.isEmpty) {
//            return ValidationResult(success: false, error: "Last name field cannot be empty!")
//        }
//        
//        if (signUpRequest.email.isEmpty) {
//            return ValidationResult(success: false, error: "Email field cannot be empty!")
//        }
//        
//        if (signUpRequest.password.isEmpty) {
//            return ValidationResult(success: false, error: "Password us field cannot be empty")
//        }
//        return ValidationResult(success: true, error: nil)
//    }
//}
