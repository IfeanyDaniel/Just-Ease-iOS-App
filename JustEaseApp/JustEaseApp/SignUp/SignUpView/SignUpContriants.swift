//
//  SignUpContriants.swift
//  JustEaseApp
//
//  Created by iOSApp on 09/02/2023.
//

import UIKit
extension SignUpViewController {
    //MARK: - Layout subviews for the scroll views
    func setupScrollView() {
      view.addSubview(scrollView)
      scrollView.addSubview(contentView)
      NSLayoutConstraint.activate([
        scrollView.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 0),
        scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
        scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
        
        contentView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
        contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
        contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
        contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
        
      ])
    }
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(heyLabel)
        view.addSubview(label)
        contentView.addSubview(firstNameTextField)
        contentView.addSubview(firstNameValidationLabel)
        contentView.addSubview(lastNameTextField)
        contentView.addSubview(lastNameValidationLabel)
        contentView.addSubview(phoneNumberTextField)
        contentView.addSubview(emailAddressTextField)
        contentView.addSubview(emailValidationLabel)
        contentView.addSubview(passwordTextField)
        contentView.addSubview(passwordValidationLabel)
        contentView.addSubview(signInButton)
        contentView.addSubview(signUpButton)
        contentView.addSubview(ownAccountButton)
        contentView.addSubview(closeEyeButton)
        contentView.addSubview(openEyeButton)
      
        
      self.navigationItem.setHidesBackButton(true, animated: true)
      self.navigationController?.isNavigationBarHidden = true
      view.backgroundColor = .systemBackground
      
      NSLayoutConstraint.activate([
        heyLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
        heyLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
        
        label.topAnchor.constraint(equalTo: heyLabel.bottomAnchor, constant: 20),
        label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
        
        firstNameTextField.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 30),
        firstNameTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 20),
        firstNameTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -20),
        firstNameTextField.heightAnchor.constraint(equalToConstant: 65),
        
        firstNameValidationLabel.topAnchor.constraint(equalTo: firstNameTextField.bottomAnchor, constant: 2),
        firstNameValidationLabel.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 20),
        
        lastNameTextField.topAnchor.constraint(equalTo: firstNameValidationLabel.bottomAnchor, constant: 20),
        lastNameTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 20),
        lastNameTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -20),
        lastNameTextField.heightAnchor.constraint(equalToConstant: 65),
        
        lastNameValidationLabel.topAnchor.constraint(equalTo: lastNameTextField.bottomAnchor, constant: 2),
        lastNameValidationLabel.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 20),
        
        phoneNumberTextField.topAnchor.constraint(equalTo: lastNameValidationLabel.bottomAnchor, constant: 20),
        phoneNumberTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 20),
        phoneNumberTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -20),
        phoneNumberTextField.heightAnchor.constraint(equalToConstant: 65),
        
        emailAddressTextField.topAnchor.constraint(equalTo: phoneNumberTextField.bottomAnchor, constant: 15),
        emailAddressTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 20),
        emailAddressTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -20),
        emailAddressTextField.heightAnchor.constraint(equalToConstant: 65),
        
        emailValidationLabel.topAnchor.constraint(equalTo: emailAddressTextField.bottomAnchor, constant: 2),
        emailValidationLabel.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 20),
        
        passwordTextField.topAnchor.constraint(equalTo: emailValidationLabel.bottomAnchor, constant: 15),
        passwordTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 20),
        passwordTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -20),
        passwordTextField.heightAnchor.constraint(equalToConstant: 65),
        
        passwordValidationLabel.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 2),
        passwordValidationLabel.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 20),
        
        signUpButton.topAnchor.constraint(equalTo: passwordValidationLabel.bottomAnchor, constant: 30),
        signUpButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
        signUpButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor , constant: -20),
        signUpButton.heightAnchor.constraint(equalToConstant: buttonHeight),
    
        closeEyeButton.topAnchor.constraint(equalTo: passwordTextField.topAnchor, constant: 15),
        closeEyeButton.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -30),
        
        openEyeButton.topAnchor.constraint(equalTo: passwordTextField.topAnchor, constant: 15),
        openEyeButton.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -30),
        
        ownAccountButton.topAnchor.constraint(equalTo: signUpButton.topAnchor, constant: 100),
        ownAccountButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor, constant: -(view.frame.width / 32 + 20) ),
        
        signInButton.topAnchor.constraint(equalTo: signUpButton.topAnchor, constant: 100),
        signInButton.leadingAnchor.constraint(equalTo: ownAccountButton.trailingAnchor, constant: 2),
        signInButton.heightAnchor.constraint(equalToConstant: 30),
        signInButton.widthAnchor.constraint(equalToConstant: 90),
        
        
        
        
      ])
    }
}
