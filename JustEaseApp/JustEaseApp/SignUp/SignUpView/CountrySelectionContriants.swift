//
//  CountrySelectionContriants.swift
//  JustEaseApp
//
//  Created by iOSApp on 12/02/2023.
//

import UIKit
extension CountrySelectionController {
    //MARK: - Layout subviews for the scroll views
    func setupScrollView() {
      view.addSubview(scrollView)
      scrollView.addSubview(contentView)
      NSLayoutConstraint.activate([
        scrollView.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 0),
        scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
        scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
        
        contentView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
        contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
        contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
        contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
        
      ])
    }
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(heyLabel)
        view.addSubview(label)
        contentView.addSubview(countryTextField)
        contentView.addSubview(countryrDropDownIcon)
        contentView.addSubview(proceedButton)
      
        
      self.navigationItem.setHidesBackButton(true, animated: true)
      self.navigationController?.isNavigationBarHidden = true
      view.backgroundColor = .systemBackground
      
      NSLayoutConstraint.activate([
        heyLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
        heyLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
        
        label.topAnchor.constraint(equalTo: heyLabel.bottomAnchor, constant: 20),
        label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
        
        countryTextField.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 30),
        countryTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 20),
        countryTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -20),
        countryTextField.heightAnchor.constraint(equalToConstant: 65),
        
        countryrDropDownIcon.topAnchor.constraint(equalTo: countryTextField.topAnchor, constant: 10),
        countryrDropDownIcon.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -40),
        
        proceedButton.topAnchor.constraint(equalTo: countryTextField.bottomAnchor, constant: 30),
        proceedButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
        proceedButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor , constant: -20),
        proceedButton.heightAnchor.constraint(equalToConstant: 50),
    
    
        
      ])
    }
}
