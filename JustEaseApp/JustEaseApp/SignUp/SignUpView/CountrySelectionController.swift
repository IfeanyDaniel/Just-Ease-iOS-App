//
//  CountrySelectionController.swift
//  JustEaseApp
//
//  Created by iOSApp on 12/02/2023.
//

import UIKit
class CountrySelectionController: UIViewController {
    // MARK: - Scroll view
    lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.showsHorizontalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    // MARK: - content view
    lazy var contentView: UIView = {
        let content = UIView()
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    
    lazy var heyLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Hey there!👋🏾"
        label.textColor = AppColors.green.color
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 25)
        label.textAlignment = .left
        return label
    }()
    
    //MARK: - Label below welcome back
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Select a country to get started.."
        return label
    }()
    lazy var countryTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Select a country",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
      //  textField.addTarget(self, action: #selector(validateViews(_:)), for: .editingChanged)
        return textField
    }()
    lazy var countryrDropDownIcon: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.dropDown.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var proceedButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Proceed", for: .normal)
        button.addTarget(self, action: #selector(didTapProceedButton), for: .touchUpInside)
        return button
    }()
    @objc func didTapProceedButton() {
        
    }
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        setupScrollView()
        self.hideKeyboardWhenTappedAround()
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
    }
    // MARK: - View will appear
    
    // MARK: - View did layout subviews
    override func viewDidLayoutSubviews() {
        scrollView.isScrollEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.contentSize = CGSize(width: 400, height: 200)
    }
}
