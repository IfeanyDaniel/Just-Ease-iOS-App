//
//  SignUpExtension.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/03/2023.
//

import Foundation
extension SignUpViewController : AuthViewModelDelegate, SignUpViewModelDelegate, RegisterDeviceViewModelDelegate {
    func didReceiveRegisterDeviceResponse(registerDeviceResponse: RegisterDeviceResponse?, _ statusCode: Int) {
        if statusCode == 200 {
            
        }
    }
    func didReceiveSignUpResponse(signUpResponse: SignUpResponse?, _ statusCode: Int) {
        if statusCode == 200 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Registration successful", type: .success)
            navigationController?.pushViewController(TabBarViewController(), animated: true)
            UserDefaults.standard.setValue(true, forKey: "onboarded")
            UserDefaults.standard.setValue("loggedIn", forKey: "LogInState")
            UserDefaults.standard.synchronize()
            let tokenRequest = RegisterDeviceRequest(device_token: Storage.getDeviceToken(), user_id: "\(Storage.getUserId())")
            registerDeviceViewModel.getRegisterDeviceResponse(registerDeviceRequest: tokenRequest)
        }
        if statusCode == 422 {
            Loader.shared.hideLoader()
            guard let errorMessage = signUpResponse?.message else {
                return
            }
            Toast.shared.showToastWithTItle(errorMessage, type: .error)
            
        }
    }
    
    func didReceiveAuthResponse(AuthResponse: AuthResponse?, _ statusCode: Int) {
        if statusCode == 200 {
            signUpViewModel.delegate = self
            if InternetConnectionManager.isConnectedToNetwork(){
                guard let firstName = firstNameTextField.text, let lastName = lastNameTextField.text, let password = passwordTextField.text, let phoneNumber = phoneNumberTextField.text , let email = emailAddressTextField.text else {
                    return }
                Loader.shared.showLoader()
                let request =  SignUpRequest(first_name: firstName, last_name: lastName, password: password, email: email, phone_number: phoneNumber)
                signUpViewModel.getSignUpResponse(signUpRequest: request)
            }
            else {
                Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
            }
        }
        if statusCode == 400 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Something went wrong, Please try again", type: .error)
        }
    }
}
