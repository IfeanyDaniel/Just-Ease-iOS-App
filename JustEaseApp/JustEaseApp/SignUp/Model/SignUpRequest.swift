//
//  SignUpRequest.swift
//  JustEaseApp
//
//  Created by iOSApp on 21/03/2023.
//

import Foundation
struct SignUpRequest: Encodable {
   let first_name: String
   let last_name: String
   let password: String
   let email: String
   let phone_number: String?
}
