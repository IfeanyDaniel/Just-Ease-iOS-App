//
//  SignUpResponse.swift
//  JustEaseApp
//
//  Created by iOSApp on 21/03/2023.
//

import Foundation
struct SignUpResponse: Decodable {
    let message: String?
    let data: [SignupData]?
    
}
struct SignupData: Decodable {
    let user_id: Int
    let first_name: String
    let last_name:  String
    let phone_number:  String?
    let address: String?
    let image_url: String?
    let email:  String?
    
}

