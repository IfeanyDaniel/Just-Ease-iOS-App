//
//  SignUpViewModel.swift
//  JustEaseApp
//
//  Created by iOSApp on 21/03/2023.
//
import Foundation
protocol SignUpViewModelDelegate {
    func didReceiveSignUpResponse(signUpResponse: SignUpResponse?,_ statusCode: Int)
}
class SignUpViewModel {
    var delegate: SignUpViewModelDelegate?
    func getSignUpResponse(signUpRequest: SignUpRequest) {
        let signUpResource = SignUpResource()
        signUpResource.getResponse(signUpRequest: signUpRequest) { signUpApiResponse, statusCode in
            if statusCode ==  200 {
                let userID = signUpApiResponse?.data?[0].user_id
                Storage.saveUserId(number: userID ?? 0)
                let firstname = signUpApiResponse?.data?[0].first_name
                Storage.saveFirstName(data: firstname ?? "")
                let lastname = signUpApiResponse?.data?[0].last_name
                Storage.saveLastName(data: lastname ?? "")
                DispatchQueue.main.async {
                    self.delegate?.didReceiveSignUpResponse(signUpResponse: signUpApiResponse, statusCode)
                }
            }
            if statusCode ==  400 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveSignUpResponse(signUpResponse: signUpApiResponse, statusCode)
                }
            }
            if statusCode ==  500 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveSignUpResponse(signUpResponse: signUpApiResponse, statusCode)
                }
            }
            if statusCode ==  422 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveSignUpResponse(signUpResponse: signUpApiResponse, statusCode)
                }
            }
        }
    }
}

