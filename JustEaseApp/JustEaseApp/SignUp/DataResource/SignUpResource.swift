//
//  SignUpResource.swift
//  JustEaseApp
//
//  Created by iOSApp on 21/03/2023.
//

import Foundation
struct SignUpResource {
    func getResponse(signUpRequest: SignUpRequest, completionHandler: @escaping (_ result: SignUpResponse?,_ statusCode: Int) -> Void) {
        let signUpURL = URL(string: ApiEndpoints.signupEndPoint)!
        let httpUtility = HTTPUtility()
        do {
            let signUpPostBody = try JSONEncoder().encode(signUpRequest)
            httpUtility.postAuthorizationResponse(requestUrl: signUpURL, requestBody: signUpPostBody, resultType: SignUpResponse.self) { signUpApiResponse, statusCode in
                completionHandler(signUpApiResponse,statusCode)
            }
        } catch let error {
            debugPrint(error)
        }
    }
}
