//
//  EndPoints.swift
//  JustEaseApp
//
//  Created by iOSApp on 19/03/2023.
//

import Foundation
struct ApiEndpoints   {
   static let baseURL = "https://kmr-staging.lawpavilion.com/api/" // staging
   // static let baseURL = "https://api-lr.lawpavilion.com/api/" // live
    
    static let authEndPoint = "\(baseURL)client_login"
    static let signupEndPoint = "\(baseURL)users/register"
    static let loginEndPoint = "\(baseURL)users/login"
    static let forgotPasswordEndPoint = "\(baseURL)users/forgot"
    static let resetPasswordEndPoint = "\(baseURL)users/reset"
    static let constitutionEndPoint = "\(baseURL)sections"
    static let constitutionDetailsEndPoint = "\(baseURL)sections"
    static let rightEndPoint = "\(baseURL)rights"
    static let rightDetailsEndPoint = "\(baseURL)rights"
    static let questionEndPoint = "\(baseURL)questions"
    static let dutiesEndPoint = "\(baseURL)duties"
    static let dutiesDetailsEndPoint = "\(baseURL)duties"
    static let othersEndPoint = "\(baseURL)provisions"
    static let othersDetailsEndPoint = "\(baseURL)provisions"
    static let stateEndPoint = "\(baseURL)representative/all/state"
    static let townEndPoint = "\(baseURL)representative/town"
    static let lgaEndPoint = "\(baseURL)representative/town"
    static let repEndPoint = "\(baseURL)representative/local-government"
    static let stateSearchEndPoint = "\(baseURL)representative/state_search?state_title"
  //  static let townSearchEndPoint = "\(baseURL)representative/town_search?town_title" // live
    static let townSearchEndPoint = "\(baseURL)representative/state/town/search?town_title" // staging
    static let lgSearchEndPoint = "\(baseURL)representative/lga_search?lga_title"
    static let nearLawywersEndPoint = "\(baseURL)lawyers/all/lawyers_near_me"
    static let lawywersDirectoryEndPoint = "\(baseURL)lawyers/all/lawyers"
    static let practiceAreasEndPoint = "\(baseURL)practice-areas"
    static let filterbypracticeAreasEndPoint = "\(baseURL)lawyers/all/location"
    static let feedEndPoint = "\(baseURL)feeds/read"
    static let feedSearchEndPoint = "\(baseURL)feeds/search"
    static let policeEndPoint = "\(baseURL)police_stations/all/police_stations"
   // static let securityEndPoint = "\(baseURL)security_watch/read"
    static let securityEndPoint = "\(baseURL)security_watch/search"
    static let userProfileEndPoint = "\(baseURL)users/read_user"
    static let editProfileEndPoint = "\(baseURL)users/update"
    static let crimeCategoryEndPoint = "\(baseURL)report_crime/report_crime_categories"
    static let reportCrimeEndPoint = "\(baseURL)report_crime/create"
    static let reportViolationCategoriesEndPoint  = "\(baseURL)report_violations/report_violation_categories"
    static let createReportViolationEndPoint = "\(baseURL)report_violations/create"
    static let feedDetailEndPoint = "\(baseURL)feeds/read"
    static let createFeedLikeEndPoint = "\(baseURL)feeds"
    static let createFeedCommentEndPoint = "\(baseURL)feeds"
    static let createFeedShareEndPoint = "\(baseURL)feeds"
    static let addContactEndPoint = "\(baseURL)panic_contact"
    static let fetchPanicContactEndPoint = "\(baseURL)panic_contact"
    static let securityWatchStateEndPoint = "\(baseURL)security_watch/address/state"
    static let lgSecurityWatchEndPoint = "\(baseURL)security_watch/address/lga/"
    static let searchSecurityWatchEndPoint = "\(baseURL)security_watch/search"
    static let createPanicEndPoint = "\(baseURL)panic"
    static let createCrimeCommentEndPoint = "\(baseURL)security_watch/"
    static let readCrimeCommentEndPoint = "\(baseURL)security_watch/read/"
    static let createCrimeLikeEndPoint = "\(baseURL)security_watch"
    static let registerDeviceEndPoint = "\(baseURL)users/create_device_token"
    static let logOutEndPoint = "\(baseURL)client_logout"
    static let notificationEndPoint = "\(baseURL)report_crime/notification"
}
//  /api/security_watch/:report_crime_id/create_report_crime_like
//https://kmr-staging.lawpavilion.com/api/client_logout
//https://api-lr.lawpavilion.com/api/feeds/read/:feed_id?user_id=1370
//https://api-lr.lawpavilion.com/api/feeds/:feed_id/create_feed_like
//api/feeds/:feed_id/create_comment
//'https://kmr-staging.lawpavilion.com/api/feeds/1/create_feed_share'
//https://api-lr.lawpavilion.com/api/panic_contact?user_id=1370
//api/panic_contact/3
//https://kmr-staging.lawpavilion.com/api/security_watch/address/lga/lagos
//https://kmr-staging.lawpavilion.com/api/security_watch/search?longitude=45.0&latitude=-34&search_scope=all_crime&user_id=13

//https://kmr-staging.lawpavilion.com/api/security_watch/search?longitude=3.731280233420415&latitude=6.4755706787109375&search_scope=crime_near_me&user_id=33
//https://kmr-staging.lawpavilion.com//api/security_watch/search?longitude=45.0&latitude=-34&search_scope=all_crime&search_query=Ajegunla&user_id=33
//https://kmr-staging.lawpavilion.com/api/security_watch/:report_crime_id/create_comment
//https://kmr-staging.lawpavilion.com/api/security_watch/read/:report_crime_id?user_id=33
//
//'address="Lekki Phase 4"' \
//--form 'image_url=@"/C:/Users/PIPA/Pictures/lawpavilion_spending_pattern.png"' \
//--form 'longitude="89.0"' \
//--form 'latitude="-35"'
