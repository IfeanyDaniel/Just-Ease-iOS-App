//
//  OnBoardingCell.swift
//  JustEaseApp
//
//  Created by iOSApp on 08/02/2023.
//

import UIKit
class OnboardingCell: UICollectionViewCell {
     static var identifier = "cellId"
    var page: Page? {
        didSet {
            guard let page = page else {
                return
            }
            
            var imageName = page.imageName
            if UIDevice.current.orientation.isLandscape {
                imageName += "_landscape"
            }
            
            imageView.image = UIImage(named: imageName)
            var color : UIColor {
                return UIColor { (trait) -> UIColor in
                    switch trait.userInterfaceStyle {
                    case .dark:
                        return UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 1.00)
                    default:
                        return  UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 1.00)
                    }
                }
            }
            
            let attributedText = NSMutableAttributedString(string: page.title, attributes: convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont(name: "HelveticaNeue-Bold", size: 18) as Any, convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): color]))
            
            attributedText.append(NSAttributedString(string: "\n\n\(page.message)", attributes: convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont(name: "HelveticaNeue-Light", size: 14) as Any, convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): color])))
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = .center
            
            let length = attributedText.string.count
            attributedText.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: length))
            
            textView.attributedText = attributedText
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    lazy var imageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.image = UIImage(named: "")
        iv.layer.masksToBounds = true
        iv.layer.cornerRadius = 0
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var textView: UITextView = {
        let tv = UITextView()
        tv.text = "SAMPLE TEXT FOR NOW"
        tv.isEditable = false
        tv.backgroundColor = .systemBackground
        tv.contentInset = UIEdgeInsets(top: 24, left: 0, bottom: 0, right: 0)
        tv.font = UIFont(name: "HelveticaNeue", size: 1.0)
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    func setupViews() {
        addSubview(imageView)
        addSubview(textView)
        imageView.anchorWithConstantsToTop(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant:55, bottomConstant: 300, rightConstant:55)
        
        textView.anchorWithConstantsToTop(imageView.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 30, leftConstant: 25, bottomConstant: 20, rightConstant: 25)
        
        textView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.3).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
    guard let input = input else { return nil }
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
    return input.rawValue
}

