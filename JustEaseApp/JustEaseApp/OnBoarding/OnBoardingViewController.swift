//
//  ViewController.swift
//  JustEaseApp
//
//  Created by iOSApp on 05/02/2023.
//

import UIKit
class OnBoardingController: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    lazy var onBoardingcollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        cv.isPagingEnabled = true
        cv.backgroundColor = .clear
        cv.dataSource = self
        cv.delegate = self
        cv.showsHorizontalScrollIndicator = false
        return cv
    }()
    
    lazy var pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.pageIndicatorTintColor = .lightGray
        pc.currentPageIndicatorTintColor = UIColor(red: 0.29, green: 0.65, blue: 0.50, alpha: 1.00)
        pc.numberOfPages = self.pages.count
        pc.translatesAutoresizingMaskIntoConstraints = false
        return pc
    }()
    
    lazy var nextButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.layer.masksToBounds = true
        button.titleLabel?.font = UIFont(name: AppFonts.silka.font, size: 18)
        button.setTitle("Next", for: .normal)
        button.addTarget(self, action: #selector(moveToNextPage), for: .touchUpInside)
        return button
    }()
    lazy var getStartedButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.layer.masksToBounds = true
        button.isHidden = true
        button.titleLabel?.font = UIFont(name: AppFonts.silka.font, size: 18)
        button.setTitle("Get Started", for: .normal)
        button.addTarget(self, action: #selector(goToSignInScreen), for: .touchUpInside)
        return button
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        layoutViews()
        registerCells()
        increaseSizeOfPageControlIndicator()
        pageControl.currentPageIndicatorTintColor = UIColor(red: 0.29, green: 0.65, blue: 0.50, alpha: 1.00)
        UserDefaults.standard.setValue(false, forKey: "onboarded")
        UserDefaults.standard.setValue("notLoggedIn", forKey: "LogInState")
    }
    @objc func didTapGetStartedButton() {
        goToSignInScreen()
    }
    // MARK: - ONBOARDING SCREENS
    let pages: [Page] = {
        let firstPage = Page(title: "JustEase", message: "On JustEase you access the law and know your rights.", imageName: "onboardingImage1")
        
        let secondPage = Page(title: "Report a Violation", message: "Report violations to the relevant enforcement agencies.", imageName: "onboardingImage2")
        
        let thirdPage = Page(title: "Find a Lawyer", message: "On JustEase you access the law and know your rights.", imageName: "onboardingImage3")
        
        return [firstPage, secondPage, thirdPage]
    }()
    //MARK: - increase the size of pagae control indicator
    func increaseSizeOfPageControlIndicator(){
        pageControl.currentPageIndicatorTintColor = AppColors.green.color
        (0..<pageControl.numberOfPages).forEach { (index) in
            let activePageIconImage = UIImage(named: "mainDot")
            let otherPageIconImage = UIImage(named: "otherDot")
            let pageIcon = index == pageControl.currentPage ? activePageIconImage : otherPageIconImage
                pageControl.setIndicatorImage(pageIcon, forPage: index)
        }
    }
    //MARK: - scroll view will end dragging
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let pageNumber = Int(targetContentOffset.pointee.x / view.frame.width)
        increaseSizeOfPageControlIndicator()
        //MARK: - move current dot color to current position
        onBoardingcollectionView.isPagingEnabled = true
        pageControl.currentPage = pageNumber
        increaseSizeOfPageControlIndicator()
        if pageNumber  == 0 {
            getStartedButton.isHidden = true
            nextButton.isHidden = false
        } else if pageNumber  == 1  {
            getStartedButton.isHidden = true
            nextButton.isHidden = false
        }  else {
            getStartedButton.isHidden = false
            nextButton.isHidden = true
        }
    }
    //MARK: -cell registration
    func registerCells(){
        onBoardingcollectionView.register(OnboardingCell.self, forCellWithReuseIdentifier: OnboardingCell.identifier)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    @objc func moveToNextPage() {
        if pageControl.currentPage  == 2 {
            getStartedButton.isHidden = false
            nextButton.isHidden = true
        } else  {
            getStartedButton.isHidden = true
            nextButton.isHidden = false
            nextPage()
        }
    }
    func nextPage() {
        let nextIndex = pageControl.currentPage + 1
        if nextIndex == 2 {
            getStartedButton.isHidden = false
            nextButton.isHidden = true
        }
        let indexPath = IndexPath(item: nextIndex, section: 0)
        pageControl.currentPage = nextIndex
        increaseSizeOfPageControlIndicator()
        onBoardingcollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        onBoardingcollectionView.isPagingEnabled = true
    }
    @objc func goToSignInScreen() {
        navigationController?.pushViewController(LoginViewController(), animated: true)
    }
}

