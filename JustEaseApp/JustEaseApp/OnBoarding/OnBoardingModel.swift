//
//  OnBoardingModel.swift
//  JustEaseApp
//
//  Created by iOSApp on 08/02/2023.
//

import Foundation
struct Page {
    let title: String
    let message: String
    let imageName: String
}
