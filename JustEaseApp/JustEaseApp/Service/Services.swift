//
//  Services.swift
//  JustEaseApp
//
//  Created by iOSApp on 19/03/2023.
//

import Foundation
struct HTTPUtility {
    func getauthorizationKey() -> String {
        let token = "\(Storage.getUserToken())"
        let authorizationKey = "\(Storage.getTokenType()) ".appending(token)
        return authorizationKey
    }
    func postAuthorizationResponse<T: Decodable>(requestUrl: URL, requestBody: Data, resultType: T.Type, completion:@escaping(_ result: T?,_ statusCode:Int)-> Void) {
        
        var urlRequest = URLRequest(url: requestUrl)
        urlRequest.httpMethod = "post"
        urlRequest.httpBody = requestBody
        urlRequest.addValue("application/json", forHTTPHeaderField: "content-type")
        urlRequest.addValue( getauthorizationKey(), forHTTPHeaderField: "Authorization")
 
        URLSession.shared.dataTask(with: urlRequest) { (data, httpUrlResponse, error) in
            guard let httpResponse  = httpUrlResponse as? HTTPURLResponse else { return  }
            if (data != nil && data?.count != 0) {
                let decoder = JSONDecoder()
                do {
                    if httpResponse.statusCode == 400 {
                        let response = try decoder.decode(T.self, from: data!)
                        completion(response, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 200 {
                        let response = try decoder.decode(T.self, from: data!)
                        completion(response, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 201 {
                        let response = try decoder.decode(T.self, from: data!)
                        completion(response, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 401 {
                        completion(nil, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 500 {
                        completion(nil, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 422 {
                        let response = try decoder.decode(T.self, from: data!)
                        completion(response, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 413 {
                        completion(nil, httpResponse.statusCode)
                    }
                } catch let error {
                    debugPrint(error.localizedDescription)
                }
            }
        }.resume()
    }
    
    func post<T: Decodable>(requestUrl: URL, requestBody: Data, resultType: T.Type, completion:@escaping(_ result: T?,_ statusCode:Int)-> Void) {
        
        var urlRequest = URLRequest(url: requestUrl)
        urlRequest.httpMethod = "post"
        urlRequest.httpBody = requestBody
        urlRequest.addValue("application/json", forHTTPHeaderField: "content-type")
        if urlRequest.cachePolicy == .reloadIgnoringLocalCacheData {
            let urlCache = URLSession.shared.configuration.urlCache
            urlCache?.removeCachedResponse(for: urlRequest)
        }
        URLSession.shared.dataTask(with: urlRequest) { (data, httpUrlResponse, error) in
            guard let httpResponse  = httpUrlResponse as? HTTPURLResponse else { return  }
            if (data != nil && data?.count != 0) {
                let decoder = JSONDecoder()
                do {
                    if httpResponse.statusCode == 400 {
                        let response = try decoder.decode(T.self, from: data!)
                        completion(response, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 200 {
                        let response = try decoder.decode(T.self, from: data!)
                        completion(response, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 401 {
                        let response = try decoder.decode(T.self, from: data!)
                        completion(response, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 201 {
                        let response = try decoder.decode(T.self, from: data!)
                        completion(response, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 500 {
                        completion(nil, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 404 {
                        completion(nil, httpResponse.statusCode)
                    }
                } catch let error {
                    debugPrint(error.localizedDescription)
                }
            }
        }.resume()
    }
    
    func getAPIData<T: Decodable>(requestURL: URL, resultType: T.Type, completion: @escaping (_ result: T?) -> Void) {
        var requestURL = URLRequest(url: requestURL, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalCacheData, timeoutInterval: 60)
        requestURL.httpMethod = "get"
        requestURL.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        requestURL.addValue("application/json", forHTTPHeaderField: "Accept")
        requestURL.addValue(getauthorizationKey(), forHTTPHeaderField: "Authorization")
        print(getauthorizationKey())
        if requestURL.cachePolicy == .reloadIgnoringLocalCacheData {
            let urlCache = URLSession.shared.configuration.urlCache
            urlCache?.removeCachedResponse(for: requestURL)
        }
        URLSession.shared.dataTask(with: requestURL) { (responseData, httpURLResponse, error) in
            
            if (error == nil && responseData != nil && responseData?.count != 0) {
                let decoder = JSONDecoder()
                do {
                    let result = try decoder.decode(T.self, from: responseData!)
                    completion(result)
                } catch let error{
                    debugPrint("error occured while decoding === \(error)")
                }
            }
        }.resume()
    }

    func deleteAPIData<T: Decodable>(requestURL: URL, resultType: T.Type, completion: @escaping (_ result: T?,_ statusCode:Int) -> Void) {
        var requestURL = URLRequest(url: requestURL, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalCacheData, timeoutInterval: 60)
        requestURL.httpMethod = "delete"
        requestURL.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        requestURL.addValue("application/json", forHTTPHeaderField: "Accept")
        requestURL.addValue(getauthorizationKey(), forHTTPHeaderField: "Authorization")
        print(getauthorizationKey())
        if requestURL.cachePolicy == .reloadIgnoringLocalCacheData {
            let urlCache = URLSession.shared.configuration.urlCache
            urlCache?.removeCachedResponse(for: requestURL)
        }
        URLSession.shared.dataTask(with: requestURL) { (responseData, httpURLResponse, error) in
            guard let httpResponse  = httpURLResponse as? HTTPURLResponse else { return  }
            if (error == nil && responseData != nil && responseData?.count != 0) {
                let decoder = JSONDecoder()
                do {
                    if httpResponse.statusCode == 200 {
                        let response = try decoder.decode(T.self, from: responseData!)
                        completion(response, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode > 399 {
                        completion(nil, httpResponse.statusCode)
                    }
                } catch let error{
                    debugPrint("error occured while decoding === \(error)")
                }
            }
        }.resume()
    }
    func postVideoResponse<T: Decodable>(requestUrl: URL, videoUrl: URL, requestBody: Data, resultType: T.Type, completion:@escaping(_ result: T?,_ statusCode:Int)-> Void) {
        
        var urlRequest = URLRequest(url: requestUrl)
        urlRequest.httpMethod = "post"
        urlRequest.httpBody = requestBody
        urlRequest.setValue("video/mp4", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue( getauthorizationKey(), forHTTPHeaderField: "Authorization")
        URLSession.shared.uploadTask(with: urlRequest, fromFile: videoUrl) { (data, httpUrlResponse, error) in
            guard let httpResponse  = httpUrlResponse as? HTTPURLResponse else { return  }
            if (data != nil && data?.count != 0) {
                let decoder = JSONDecoder()
                do {
                    if httpResponse.statusCode == 400 {
                        let response = try decoder.decode(T.self, from: data!)
                        completion(response, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 200 {
                        let response = try decoder.decode(T.self, from: data!)
                        completion(response, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 201 {
                        let response = try decoder.decode(T.self, from: data!)
                        completion(response, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 401 {
                        completion(nil, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 500 {
                        completion(nil, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 422 {
                        let response = try decoder.decode(T.self, from: data!)
                        completion(response, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 413 {
                        completion(nil, httpResponse.statusCode)
                    }
                } catch let error {
                    debugPrint(error.localizedDescription)
                }
            }
        }.resume()
    }
    
    func postWithImageAuthorizationResponse<T: Decodable>(requestUrl: URL, requestBody: Data, resultType: T.Type, completion:@escaping(_ result: T?,_ statusCode:Int)-> Void) {
        
        var urlRequest = URLRequest(url: requestUrl)
        urlRequest.httpMethod = "post"
        urlRequest.httpBody = requestBody
        urlRequest.addValue("application/json", forHTTPHeaderField: "content-type")
        urlRequest.addValue(getauthorizationKey(), forHTTPHeaderField: "Authorization")
       // urlRequest.setValue("image/jpeg", forHTTPHeaderField: "Content-Type")
        URLSession.shared.dataTask(with: urlRequest) { (data, httpUrlResponse, error) in
            guard let httpResponse  = httpUrlResponse as? HTTPURLResponse else { return  }
            if (data != nil && data?.count != 0) {
                let decoder = JSONDecoder()
                do {
                    if httpResponse.statusCode == 400 {
                        let response = try decoder.decode(T.self, from: data!)
                        completion(response, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 200 {
                        let response = try decoder.decode(T.self, from: data!)
                        completion(response, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 201 {
                        let response = try decoder.decode(T.self, from: data!)
                        completion(response, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 401 {
                        completion(nil, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 500 {
                        completion(nil, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 422 {
                        let response = try decoder.decode(T.self, from: data!)
                        completion(response, httpResponse.statusCode)
                    }
                    if httpResponse.statusCode == 413 {
                        completion(nil, httpResponse.statusCode)
                    }
                } catch let error {
                    debugPrint(error.localizedDescription)
                }
            }
        }.resume()
    }
}
