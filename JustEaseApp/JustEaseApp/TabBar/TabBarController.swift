//
//  TabBarController.swift
//  JustEaseApp
//
//  Created by iOSApp on 14/02/2023.
//

import UIKit
class TabBarViewController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.clipsToBounds = true
        // MARK: Create instance of viewControllers
        let homeViewController =  HomeViewController() 
        let feedsViewController = UINavigationController(rootViewController: FeedsViewController())
        let bookMarkController = BookMarkController()
        let settingsController = UINavigationController(rootViewController: SettingsController())
        // MARK: Assign viewController to tab Bar
        self.setViewControllers([homeViewController,feedsViewController,bookMarkController,settingsController], animated: true)
        
        guard let items = self.tabBar.items else { return }
        let images = ["home", "feed", "bookmark", "settings"]
        let title = ["Home", "Feeds", "Bookmarks", "Settings"]
        for item in 0...3 {
            items[item].image = UIImage(named: images[item])
            items[item].title = title[item]
        }
        // MARK: Changing the tint Color
        self.tabBar.tintColor = AppColors.green.color
        self.tabBar.layer.backgroundColor = AppColors.green.color.cgColor
        tabBar.backgroundColor =  .systemBackground
        homeViewController.tabBarItem.selectedImage = UIImage(named: "homeSelected")
        feedsViewController.tabBarItem.selectedImage = UIImage(named: "feedsSelected")
        bookMarkController.tabBarItem.selectedImage = UIImage(named: "bookmarkSelected")
        settingsController.tabBarItem.selectedImage = UIImage(named: "settingsSelected")
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.hidesBackButton = true
    }
}


