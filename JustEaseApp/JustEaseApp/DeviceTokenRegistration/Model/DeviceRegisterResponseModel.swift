//
//  DeviceRegisterResponseModel.swift
//  JustEaseApp
//
//  Created by Ifeanyi-Mbata on 7/30/23.
//

import Foundation
struct RegisterDeviceResponse: Decodable {
    let message: String?
}

