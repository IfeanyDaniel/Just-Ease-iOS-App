//
//  DeviceRegisterRequestModel.swift
//  JustEaseApp
//
//  Created by Ifeanyi-Mbata on 7/30/23.
//


import Foundation
struct RegisterDeviceRequest: Encodable {
 let device_token: String
 let user_id: String
}
