//
//  DeviceRegisterDataResource.swift
//  JustEaseApp
//
//  Created by Ifeanyi-Mbata on 7/30/23.
//

import Foundation
struct RegisterDeviceResource {
    func getResponse(registerDeviceRequest: RegisterDeviceRequest, completionHandler: @escaping (_ result: RegisterDeviceResponse?,_ statusCode: Int) -> Void) {
        let registerDeviceURL = URL(string: ApiEndpoints.registerDeviceEndPoint)!
        let httpUtility = HTTPUtility()
        do {
            let registerDevicePostBody = try JSONEncoder().encode(registerDeviceRequest)
            httpUtility.postAuthorizationResponse(requestUrl: registerDeviceURL, requestBody: registerDevicePostBody, resultType: RegisterDeviceResponse.self) { registerDeviceApiResponse, statusCode in
                completionHandler(registerDeviceApiResponse,statusCode)
            }
        } catch let error {
            debugPrint(error)
        }
    }
}
