//
//  DeviceRegisterViewModel.swift
//  JustEaseApp
//
//  Created by Ifeanyi-Mbata on 7/30/23.
//

import Foundation
protocol RegisterDeviceViewModelDelegate {
    func didReceiveRegisterDeviceResponse(registerDeviceResponse: RegisterDeviceResponse?,_ statusCode: Int)
}
class RegisterDeviceViewModel {
    var delegate:RegisterDeviceViewModelDelegate?
    func getRegisterDeviceResponse(registerDeviceRequest: RegisterDeviceRequest) {
        let registerDeviceResource = RegisterDeviceResource()
        registerDeviceResource.getResponse(registerDeviceRequest: registerDeviceRequest) { registerDeviceApiResponse, statusCode in
            if statusCode ==  200 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveRegisterDeviceResponse(registerDeviceResponse: registerDeviceApiResponse, statusCode)
                }
            }
            if statusCode >= 400 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveRegisterDeviceResponse(registerDeviceResponse: registerDeviceApiResponse, statusCode)
                }
            }
        }
    }
}

