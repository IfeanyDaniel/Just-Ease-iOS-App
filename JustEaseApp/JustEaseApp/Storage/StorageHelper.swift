//
//  StorageHelper.swift
//  JustEaseApp
//
//  Created by iOSApp on 19/03/2023.
//

import Foundation
import KeychainSwift
struct Storage {
    //MARK: - SAVE LOGIN DETAILS
    static let keychain = KeychainSwift()
    static let userTokenKey = "userToken"
    static let userTokenType = "tokenType"
    static let passwordKey = "password"
    static let userEmailKey = "userEmail"
    static let otpKey = "otp"
    static let sectionKey = "section"
    static let rightKey = "right"
    static let dutiesKey = "duties"
    static let othersKey = "others"
    static let rightDetailsKey = "rightDetails"
    static let dutiesDetailsKey = "dutiesDetails"
    static let othersDetailsKey = "othersDetails"
    static let lawyersKey = "lawyers"
    static let lawyersDirectoryKey = "lawyersDirectory"
    static let policeStationKey = "policeStation"
    static let routingKey = "routingPracticeAreas"
    static let securityWatchKey = "securityWatchKey"
    static let filterByKeywordKey = "filterByKeywordKey"
    static let crimeCategoryIdKey = "crimeCategoryIdKey"
    static let violationCategoryIdKey = "violationCategoryIdKey"
    static let reportCategoryKey = "reportCategoryKey"
    static let videoUrl = "videoUrlKey"
    // USER STORAGE
    static let firstNameKey = "firstNameKey "
    static let lastNameKey = "lastNameKey "
    static let emailKey = "emailKey"
    static let phoneNumberKey = "phoneNumberKey"
    static let locationKey = "locationKey"
    static let userIDKey = "userIdKey"
    static let stateKey = "stateKey"
    static let lgsKey = "lgsKey"
    static let deviceToken = "deviceToken"
    static let imageuRL = "imageUrl"
    
    static let allRightKey = "allRight"
    static let allRightDetailsKey = "allRightDetails"
    static let allDutiesKey = "allDuties"
    static let allDutiesDetailsKey = "allDutiesDetails"
    static let allOthersKey = "allOthers"
    static let allOtherDetailsKey = "allOthersDetails"
    
   
    static func saveUserToken(userToken: String) {
        keychain.set(userToken, forKey: userTokenKey)
    }
    static func getUserToken() -> String {
        return keychain.get(userTokenKey) ?? ""
    }
    static func saveTokenType(name: String) {
        keychain.set(name, forKey: userTokenType)
    }
    static func getTokenType() -> String {
        return keychain.get(userTokenType) ?? ""
    }
    static func savePassword(data: String) {
        keychain.set(data, forKey: passwordKey)
    }
    static func  getPassword() -> String {
        return keychain.get(passwordKey) ?? ""
    }
    static func saveUserEmail(data: String) {
        keychain.set(data, forKey: userEmailKey)
    }
    static func  getUserEmail() -> String {
        return keychain.get(userEmailKey) ?? ""
    }
    static func saveOtp(data: String) {
        keychain.set(data, forKey: otpKey)
    }
    static func  getOtp() -> String {
        return keychain.get(otpKey) ?? ""
    }
    static func saveSectionTotalRecord(number: Int) {
        UserDefaults.standard.setValue(number, forKey: sectionKey)
    }
    static func getSectionTotalRecord() -> Int {
        return UserDefaults.standard.integer(forKey: sectionKey)
    }
    static func saveDutiesTotalRecord(number: Int) {
        UserDefaults.standard.setValue(number, forKey: dutiesKey)
    }
    static func getDutiesTotalRecord() -> Int {
        return UserDefaults.standard.integer(forKey: dutiesKey)
    }
    static func saveRightTotalRecord(number: Int) {
        UserDefaults.standard.setValue(number, forKey: rightKey)
    }
    static func getRightTotalRecord() -> Int {
        return UserDefaults.standard.integer(forKey: rightKey)
    }
    static func saveOtherTotalRecord(number: Int) {
        UserDefaults.standard.setValue(number, forKey: othersKey)
    }
    static func getOthersTotalRecord() -> Int {
        return UserDefaults.standard.integer(forKey: othersKey)
    }
    static func saveRightDetailsTotalRecord(number: Int) {
        UserDefaults.standard.setValue(number, forKey: rightDetailsKey)
    }
    static func getRightDetailsTotalRecord() -> Int {
        return UserDefaults.standard.integer(forKey: rightDetailsKey)
    }
    static func saveDutiesDetailsTotalRecord(number: Int) {
        UserDefaults.standard.setValue(number, forKey: dutiesDetailsKey)
    }
    static func getDutiesDetailsTotalRecord() -> Int {
        return UserDefaults.standard.integer(forKey: dutiesDetailsKey)
    }
    static func saveOthersDetailsTotalRecord(number: Int) {
        UserDefaults.standard.setValue(number, forKey: othersDetailsKey)
    }
    static func getOthersDetailsTotalRecord() -> Int {
        return UserDefaults.standard.integer(forKey: othersDetailsKey)
    }
    static func saveLawyersTotalRecord(number: Int) {
        UserDefaults.standard.setValue(number, forKey: lawyersKey)
    }
    static func getLawyersTotalRecord() -> Int {
        return UserDefaults.standard.integer(forKey: lawyersKey)
    }
    static func saveLawyersDirectoryTotal(number: Int) {
        UserDefaults.standard.setValue(number, forKey: lawyersDirectoryKey)
    }
    static func getLawyersDirectoryTotal() -> Int {
        return UserDefaults.standard.integer(forKey: lawyersDirectoryKey)
    }
    static func saveStationTotalRecord(number: Int) {
        UserDefaults.standard.setValue(number, forKey: policeStationKey)
    }
    static func getStationTotalRecord() -> Int {
        return UserDefaults.standard.integer(forKey: policeStationKey)
    }
    static func saveRoutingNearByLawyerFrom(source: String) {
        UserDefaults.standard.setValue(source, forKey: routingKey)
    }
    static func getRoute() -> String {
        return UserDefaults.standard.string(forKey: routingKey) ?? ""
    }
    static func saveVideoUrl(source: URL) {
        UserDefaults.standard.set(source, forKey: videoUrl)
    }
    static func getVideoUrl() -> URL{
        return UserDefaults.standard.url(forKey: videoUrl)!
    }
    static func saveSecurityWatch(number: Int) {
        UserDefaults.standard.setValue(number, forKey: securityWatchKey)
    }
    static func getSecurityWatch() -> Int {
        return UserDefaults.standard.integer(forKey:securityWatchKey)
    }
    
    static func saveFilterByKeyword(data: String) {
        UserDefaults.standard.setValue(data, forKey: filterByKeywordKey)
    }
    static func getFilterByKeyword() -> String {
        return UserDefaults.standard.string(forKey:filterByKeywordKey) ?? ""
    }
    static func saveCrimeCategoryId(number: Int) {
        UserDefaults.standard.setValue(number, forKey: crimeCategoryIdKey)
    }
    static func getCrimeCategoryId() -> Int {
        return UserDefaults.standard.integer(forKey:  crimeCategoryIdKey)
    }
    static func saveReportCategoryTotalRecord(number: Int) {
        UserDefaults.standard.setValue(number, forKey: reportCategoryKey)
    }
    static func getReportCategoryTotalRecord() -> Int {
        return UserDefaults.standard.integer(forKey: reportCategoryKey)
    }
    static func saveViolationCategory(id: Int) {
        UserDefaults.standard.setValue(id, forKey: violationCategoryIdKey)
    }
    static func getViolationCategoryId() -> Int {
        return UserDefaults.standard.integer(forKey: violationCategoryIdKey)
    }
    // SAVE USER PROFILE DETAILS
    static func saveUserId(number: Int) {
        UserDefaults.standard.setValue(number, forKey: userIDKey)
    }
    static func getUserId() -> Int {
        return UserDefaults.standard.integer(forKey: userIDKey)
    }
    static func saveImage(url: String) {
        UserDefaults.standard.setValue(url, forKey: imageuRL)
    }
    static func getImageUrl() -> String {
        return UserDefaults.standard.string(forKey: imageuRL) ?? ""
    }
    static func saveDevice(token: String) {
        UserDefaults.standard.setValue(token, forKey: deviceToken)
    }
    static func getDeviceToken() -> String {
        return UserDefaults.standard.string(forKey: deviceToken) ?? ""
    }
    static func saveFirstName(data: String) {
        UserDefaults.standard.setValue(data, forKey: firstNameKey)
    }
    static func getFirstName() -> String {
        return UserDefaults.standard.string(forKey: firstNameKey) ?? ""
    }
    static func saveEmail(data: String) {
        UserDefaults.standard.setValue(data, forKey: userEmailKey)
    }
    static func getEmail() -> String {
        return UserDefaults.standard.string(forKey: userEmailKey) ?? ""
    }
    static func saveLastName(data: String) {
        UserDefaults.standard.setValue(data, forKey: lastNameKey)
    }
    static func getLasttName() -> String {
        return UserDefaults.standard.string(forKey: lastNameKey) ?? ""
    }
    static func saveUserEditLocation(data: String) {
        UserDefaults.standard.setValue(data, forKey: locationKey)
    }
    static func getUserLocation() -> String {
        return UserDefaults.standard.string(forKey: locationKey) ?? ""
    }
    //MARK: - save crime category details
    static func saveCrimeCategory(crime: [CrimesCategory]) {
        let crime = try! PropertyListEncoder().encode(crime)
        UserDefaults.standard.set(crime, forKey: "crimes")
    }
    
    static func getCrimeCategory() -> [CrimesCategory] {
        guard let fetchedData = UserDefaults.standard.data(forKey: "crimes") else { return []}
        let fetchedCrimes = try! PropertyListDecoder().decode([CrimesCategory].self, from: fetchedData)
        return fetchedCrimes
    }
    //MARK: - save all rights
    static func saveRight(textInfo: [Right]) {
        let allRight = try! PropertyListEncoder().encode(textInfo)
        UserDefaults.standard.set(allRight, forKey: allRightKey)
    }
    
    //MARK: - get all rights
    static func getAllRight() -> [Right] {
        guard let fetchedData = UserDefaults.standard.data(forKey: allRightKey) else { return []}
        let fetchedRight = try! PropertyListDecoder().decode([Right].self, from: fetchedData)
        return fetchedRight
    }
    //MARK: - save all right details
    static func saveRightDetails(textInfo: [Section]) {
        let allRight = try! PropertyListEncoder().encode(textInfo)
        UserDefaults.standard.set(allRight, forKey: allRightDetailsKey)
    }
    
    //MARK: - get all rights details
    static func getAllRightDetails() -> [Section] {
        guard let fetchedData = UserDefaults.standard.data(forKey: allRightDetailsKey) else { return []}
        let fetchedRight = try! PropertyListDecoder().decode([Section].self, from: fetchedData)
        return fetchedRight
    }
    //MARK: - save all duties
    static func saveDuties(textInfo: [Duties]) {
        let allRight = try! PropertyListEncoder().encode(textInfo)
        UserDefaults.standard.set(allRight, forKey: allDutiesKey)
    }
    
    //MARK: - get all duties
    static func getAllDuties() -> [Duties] {
        guard let fetchedData = UserDefaults.standard.data(forKey: allDutiesKey) else { return []}
        let fetchedDuties = try! PropertyListDecoder().decode([Duties].self, from: fetchedData)
        return fetchedDuties
    }
    //MARK: - save all duties details
    static func saveDutiesDetails(textInfo: [Section]) {
        let allDuties = try! PropertyListEncoder().encode(textInfo)
        UserDefaults.standard.set(allDuties, forKey: allDutiesDetailsKey)
    }
    
    //MARK: - get all others details
    static func getAllDutiesDetails() -> [Section] {
        guard let fetchedData = UserDefaults.standard.data(forKey: allDutiesDetailsKey) else { return []}
        let fetchedDuties = try! PropertyListDecoder().decode([Section].self, from: fetchedData)
        return fetchedDuties
    }
    //MARK: - save all duties
    static func saveOthers(textInfo: [Others]) {
        let allRight = try! PropertyListEncoder().encode(textInfo)
        UserDefaults.standard.set(allRight, forKey: allOthersKey)
    }
    
    //MARK: - get all duties
    static func getAllOthers() -> [Others] {
        guard let fetchedData = UserDefaults.standard.data(forKey: allOthersKey) else { return []}
        let fetchedOther = try! PropertyListDecoder().decode([Others].self, from: fetchedData)
        return fetchedOther
    }
    //MARK: - save all others details
    static func saveOthersDetails(textInfo: [Section]) {
        let allOther = try! PropertyListEncoder().encode(textInfo)
        UserDefaults.standard.set(allOther, forKey: allOtherDetailsKey)
    }
    
    //MARK: - get all others details
    static func getAllOthersDetails() -> [Section] {
        guard let fetchedData = UserDefaults.standard.data(forKey: allOtherDetailsKey) else { return []}
        let fetchedOther = try! PropertyListDecoder().decode([Section].self, from: fetchedData)
        return fetchedOther
    }
    
    //MARK: - save state location
    static func saveStateLocation(state: [StateLocation]) {
        let states = try! PropertyListEncoder().encode(state)
        UserDefaults.standard.set(states, forKey: stateKey)
    }
    
    static func getStateLocation() -> [StateLocation] {
        guard let fetchedData = UserDefaults.standard.data(forKey: stateKey) else { return []}
        let fetchedStates = try! PropertyListDecoder().decode([StateLocation].self, from: fetchedData)
        return fetchedStates
    }
    
    //MARK: - save location govern location
    static func saveLocalGovernemnt(lgs: [LgsLocation]) {
        let lgs = try! PropertyListEncoder().encode(lgs)
        UserDefaults.standard.set(lgs, forKey: lgsKey)
    }
    
    static func getLocalGovernemnt() -> [LgsLocation] {
        guard let fetchedData = UserDefaults.standard.data(forKey: lgsKey) else { return []}
        let fetchedLgs = try! PropertyListDecoder().decode([LgsLocation].self, from: fetchedData)
        return fetchedLgs
    }
    
    static func removeUserToken() {
        keychain.clear()
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            if key != "userEmail" {
                defaults.removeObject(forKey: key)
            }
        }
    }
}
