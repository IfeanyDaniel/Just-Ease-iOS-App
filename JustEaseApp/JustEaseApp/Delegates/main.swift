//
//  main.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/2/23.
//

import UIKit
UIApplicationMain (
    CommandLine.argc, CommandLine.unsafeArgv,
    NSStringFromClass(CustomApplication.self),
    NSStringFromClass(AppDelegate.self)
)
