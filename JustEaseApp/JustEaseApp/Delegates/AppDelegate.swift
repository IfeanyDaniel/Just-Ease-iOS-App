//
//  AppDelegate.swift
//  JustEaseApp
//
//  Created by iOSApp on 05/02/2023.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import Contacts
import AddressBook
import Photos
import AVKit
import FirebaseCore
import FirebaseMessaging
extension UIWindow {
    open override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            Constant.lightHaptic()
            if #available(iOS 13.0, *) {
                if traitCollection.userInterfaceStyle == .dark {
                    overrideUserInterfaceStyle = .light
                } else {
                    overrideUserInterfaceStyle = .dark
                }
            }
        }
    }
}

class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        GMSPlacesClient.provideAPIKey("AIzaSyCc-0BFtbOdZJPDNIHGpVhH-6hRJuugwNg")
        GMSServices.provideAPIKey("AIzaSyCc-0BFtbOdZJPDNIHGpVhH-6hRJuugwNg")
        Messaging.messaging().delegate = self
        registerForPushNotifications()
        request {
            
        }
        return true
    }
    // MARK: Register app for push notificaitons
        func registerForPushNotifications() {
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
              [weak self] granted, error in
              print("Permission granted: \(granted)")
             // guard granted else { return }
                if error == nil {
                    self?.getNotificationSettings()
                }

          }
        }

    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            print("Notification settings: \(settings)")
            switch settings.authorizationStatus {
            case .authorized:
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            case .denied:
                print("Notifications Denied")

            case .notDetermined:
                print("Notifications not determined")
            default:
                break
            }

        }
    }
    func request(completion: @escaping ()->()?) {
        if #available(iOS 9.0, *) {
            let store = CNContactStore()
            store.requestAccess(for: .contacts, completionHandler: { (granted, error) in
                DispatchQueue.main.async {
                    completion()
                }
            })
        } else {
            let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
            ABAddressBookRequestAccessWithCompletion(addressBookRef) {
                (granted: Bool, error: CFError?) in
                DispatchQueue.main.async() {
                    completion()
                }
            }
        }
    }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        Messaging.messaging().token { token, error in
            if let error = error {
                print("Error fetching FCM registration token: \(error)")
            } else if let token = token {
               // UserDefaultUtility.saveNotificationToken(ofUser: token)
                print("FCM registration token: \(token)")
                Storage.saveDevice(token: token)
            }
        }
        let dataDict: [String: String] = ["token": fcmToken ?? ""]
        NotificationCenter.default.post(
            name: Notification.Name("FCMToken"),
            object: nil,
            userInfo: dataDict
        )
    }
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return UIDevice.current.userInterfaceIdiom == .phone ? .portrait : .all
    }
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
      
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
       
    }
    func applicationWillTerminate(_ application: UIApplication) {
        let top = UIApplication.shared.keyWindow?.rootViewController
        top?.modalPresentationStyle = .fullScreen
        top?.present(Refresh(), animated: false, completion: nil)
    }
    
}

class CustomApplication: UIApplication {
    private var timerToDetectInactivity: Timer?
    var window: UIWindow?
    private var timeIdleSecond: TimeInterval {
        return 10 * 60 // 10 min.
    }
    override func sendEvent(_ event: UIEvent) {
        super.sendEvent(event)
        if let touches = event.allTouches {
            for touch in touches where touch.phase == UITouch.Phase.began{
                self.resetTimer()
            }
        }
        
    }
    func restartTimer() {
        if let timerToDetectInactivity = timerToDetectInactivity {
            timerToDetectInactivity.invalidate()
        }
        
        timerToDetectInactivity = Timer.scheduledTimer(timeInterval: timeIdleSecond,
                                                       target: self,
                                                       selector: #selector(CustomApplication.showAlert),
                                                       userInfo: nil,
                                                       repeats: true
        )
    }
    // reset the timer because there was user event
    private func resetTimer() {
        restartTimer()
    }
    @objc func appMovedToActive() {
        restartTimer()
    }
    @objc func showAlert() {
        let state = UIApplication.shared.applicationState
        if state == .active {
            let top = UIApplication.shared.keyWindow?.rootViewController
            top?.modalPresentationStyle = .overCurrentContext
            
            top?.present(Refresh(), animated: false, completion: nil)
        }
    }
}
