//
//  SceneDelegate.swift
//  JustEaseApp
//
//  Created by iOSApp on 05/02/2023.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        var navigationController = UINavigationController()
        //MARK: - Checking if a user has not onboarded
        if UserDefaults.standard.bool(forKey: "onboarded") == false {
            navigationController = UINavigationController(rootViewController: OnBoardingController())
        }
        //MARK: - If a user has onboarded
        else  {
            //MARK: - checking if the user has logged In
            let logInString = UserDefaults.standard.string(forKey: "LogInState") ?? ""
            if  logInString == "notLoggedIn" {
                navigationController = UINavigationController(rootViewController: LoginViewController())
            }
            else {
                navigationController = UINavigationController(rootViewController: Loading())
            }
        }
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {}
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    
    
}

