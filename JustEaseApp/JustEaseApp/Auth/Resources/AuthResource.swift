//
//  AuthResource.swift
//  JustEaseApp
//
//  Created by iOSApp on 19/03/2023.
//

import Foundation
struct AuthResource {
    func getResponse(authRequest: AuthRequest, completionHandler: @escaping (_ result: AuthResponse?,_ statusCode: Int) -> Void) {
        
        let authURL = URL(string: ApiEndpoints.authEndPoint)!
        let httpUtility = HTTPUtility()
        do {
            let authPostBody = try JSONEncoder().encode(authRequest)
            httpUtility.post(requestUrl: authURL, requestBody: authPostBody, resultType: AuthResponse.self) { authResponse, statusCode in
                completionHandler(authResponse,statusCode)
            }
        } catch let error {
            debugPrint(error)
        }
    }
}
