//
//  ViewModel.swift
//  JustEaseApp
//
//  Created by iOSApp on 19/03/2023.
//

import Foundation
protocol AuthViewModelDelegate {
    func didReceiveAuthResponse(AuthResponse: AuthResponse?,_ statusCode: Int)
}
class AuthViewModel {
    var delegate: AuthViewModelDelegate?
    func authResponse(authRequest: AuthRequest) {
        let authResource = AuthResource()
        authResource.getResponse(authRequest: authRequest) { authApiResponse, statusCode in
            if statusCode == 200 {
                let accessCode = authApiResponse?.access_token
                let tokenType = authApiResponse?.token_type
                Storage.saveUserToken(userToken: accessCode ?? "")
                Storage.saveTokenType(name: tokenType ?? "")
                DispatchQueue.main.async {
                    self.delegate?.didReceiveAuthResponse(AuthResponse: authApiResponse, statusCode)
                }
            }
            if statusCode == 400 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveAuthResponse(AuthResponse: authApiResponse, statusCode)
                }
            }
        }
    }
}


