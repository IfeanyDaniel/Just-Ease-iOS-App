//
//  AuthRequest.swift
//  JustEaseApp
//
//  Created by iOSApp on 19/03/2023.
//

import Foundation
struct AuthRequest: Encodable {
    let email: String
    let password: String
}
