//
//  AuthRespons.swift
//  JustEaseApp
//
//  Created by iOSApp on 19/03/2023.
//

import Foundation
struct AuthResponse: Codable {
    let access_token: String
    let token_type: String
    let expires_in: Int
}
