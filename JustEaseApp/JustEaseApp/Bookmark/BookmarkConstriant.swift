//
//  BookmarkConstriant.swift
//  JustEaseApp
//
//  Created by iOSApp on 15/02/2023.
//

import UIKit
extension BookMarkController {
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(label)
        view.addSubview(bookmarkCollectionView)
        view.addSubview(noContentView)
        noContentView.addSubview(noBoomarkIcon)
        noContentView.addSubview(noBookmarkLabel)
        noContentView.addSubview(instructionLabel)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = backgroundSystemColor
       
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 80),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            
            noContentView.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 30),
            noContentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            noContentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            noContentView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
        
            noBoomarkIcon.centerXAnchor.constraint(equalTo: noContentView.centerXAnchor),
            noBoomarkIcon.topAnchor.constraint(equalTo: noContentView.topAnchor, constant: 150),
            noBoomarkIcon.widthAnchor.constraint(equalToConstant: 200),
            noBoomarkIcon.heightAnchor.constraint(equalToConstant: 200),
            
            noBookmarkLabel.topAnchor.constraint(equalTo: noBoomarkIcon.bottomAnchor, constant: 20),
            noBookmarkLabel.leadingAnchor.constraint(equalTo: noContentView.leadingAnchor, constant: 20),
            noBookmarkLabel.trailingAnchor.constraint(equalTo:noContentView.trailingAnchor, constant: -20),
            
            instructionLabel.topAnchor.constraint(equalTo: noBookmarkLabel.bottomAnchor, constant: 20),
            instructionLabel.leadingAnchor.constraint(equalTo: noContentView.leadingAnchor, constant: 20),
            instructionLabel.trailingAnchor.constraint(equalTo: noContentView.trailingAnchor, constant: -20),
            
        ])
        bookmarkCollectionView.anchorWithConstantsToTop(label.bottomAnchor,
                                                     left: view.leftAnchor, bottom: view.bottomAnchor, right:  view.rightAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 20, rightConstant: 0)
    }
}
