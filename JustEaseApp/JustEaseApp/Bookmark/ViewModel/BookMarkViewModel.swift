//
//  BookMarkViewModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/3/23.
//

struct DetailsListViewModel {
    let title: String
    let note: String
    let isBookMark: String
    
    // MARK: - INITIALIZING
    init(with model: DetailList){
        title = model.title ?? ""
        note = model.note ?? ""
        isBookMark = model.isBookMarked ?? ""
    }
}
