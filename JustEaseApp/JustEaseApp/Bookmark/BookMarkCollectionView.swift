//
//  BookMarkCollectionView.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/2/23.
//
import UIKit
import Foundation
extension BookMarkController {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        list.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = bookmarkCollectionView.dequeueReusableCell(withReuseIdentifier: CardCollectionViewCell.identifier, for: indexPath) as? CardCollectionViewCell else { return UICollectionViewCell() }
        let details = list[indexPath.row]
        cell.configure(with: DetailsListViewModel(with: details))
        cell.backgroundColor = backgroundSystemColor
        cell.layer.borderColor = AppColors.green.color.cgColor
        cell.layer.borderWidth = 0.5
        cell.layer.cornerRadius = 5
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = .zero
        cell.layer.shadowRadius = 0.2
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 50, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let details = list[indexPath.row]
        let controller = InfoController()
        controller.headerlabel.text = details.title
        controller.note.text = details.note
        controller.isBookmark = details.isBookMarked ?? ""
        controller.listIndex = indexPath.row
        navigationController?.pushViewController(controller, animated: true)
    }
}
