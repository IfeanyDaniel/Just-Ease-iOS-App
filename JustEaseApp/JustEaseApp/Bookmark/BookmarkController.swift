//
//  BookmarkController.swift
//  JustEaseApp
//
//  Created by iOSApp on 15/02/2023.
//

import UIKit
import RealmSwift
class BookMarkController: UIViewController, UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    let realm =  try! Realm()
    lazy var list: Results<DetailList> = {realm.objects(DetailList.self) }()
    let selecetedDetailList : DetailList? = nil
    
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Bookmarks"
        label.textColor = AppColors.green.color
        label.font = UIFont(name: AppFonts.silkabold.font, size: 25)
        label.textAlignment = .left
        return label
    }()
    lazy var bookmarkCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 15
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        cv.backgroundColor = backgroundSystemColor
        cv.dataSource = self
        cv.delegate = self
        cv.showsVerticalScrollIndicator = true
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    
    lazy var noContentView : UIView = {
        let content = UIView()
        content.backgroundColor = backgroundSystemColor
        content.isHidden = true
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    lazy var noBoomarkIcon: UIButton = {
        let button = UIButton()
        button.setImage(AppButtonImages.noBookmarkIcon.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var noBookmarkLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "No bookmark available"
        label.textAlignment = .center
        label.font = UIFont(name: AppFonts.silkabold.font, size: 14)
        return label
    }()
    lazy var instructionLabel: UILabel = {
        let label = UILabel.labelTextDesign()
        label.textColor = AppColors.lightNavyBlue.color
        label.text = "Try bookmark a feeds, right, or duties to see a content here."
        label.textAlignment = .center
        return label
    }()
    func showBookmark() {
        if list.count == 0 {
            noContentView.isHidden = false
            bookmarkCollectionView.isHidden = true
        } else {
            noContentView.isHidden = true
            bookmarkCollectionView.isHidden = false
        }
        
    }
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        registercCell()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showBookmark()
        bookmarkCollectionView.reloadData()
        self.navigationController?.isNavigationBarHidden = true
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.setHidesBackButton(true, animated: true)
    }
    func registercCell() {
        bookmarkCollectionView.register(CardCollectionViewCell.self, forCellWithReuseIdentifier: CardCollectionViewCell.identifier)
    }
}
