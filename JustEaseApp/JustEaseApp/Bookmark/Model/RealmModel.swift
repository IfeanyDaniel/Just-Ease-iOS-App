//
//  RealmModel.swift
//  JustEaseApp
//
//  Created by MACBOOK PRO on 4/3/23.
//

import UIKit
import RealmSwift
class DetailList: Object {
  @objc dynamic var title: String? = ""
  @objc dynamic var note: String? = ""
  @objc dynamic var isBookMarked: String? = ""
}
