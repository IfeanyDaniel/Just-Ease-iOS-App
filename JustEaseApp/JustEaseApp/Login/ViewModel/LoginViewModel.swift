//
//  LoginViewModel.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/03/2023.
//


import Foundation
protocol LoginDelegate {
    func didReceiveLoginResponse(loginResponse: LoginResponse?)
}
class LoginViewModel {
    var delegate: LoginDelegate?
    func  getLoginResponse(email: String, paswword: String) {
        let loginResource = LoginResource()
        loginResource.getLoginResponse(email: email, paswword: paswword) { getLoginApiResponse in
            if getLoginApiResponse.status ==  200 {
                let userID = getLoginApiResponse.data?.user_id
                Storage.saveUserId(number: userID ?? 0)
                let firstname = getLoginApiResponse.data?.first_name
                Storage.saveFirstName(data: firstname ?? "")
                Storage.saveImage(url: getLoginApiResponse.data?.image_url ?? "")
                let lastname = getLoginApiResponse.data?.last_name
                Storage.saveLastName(data: lastname ?? "")
                DispatchQueue.main.async {
                    self.delegate?.didReceiveLoginResponse(loginResponse: getLoginApiResponse)
                }
            }
            if getLoginApiResponse.status ==  422 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveLoginResponse(loginResponse: getLoginApiResponse)
                }
            }
        }
    }
}
