//
//  LoginResource.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/03/2023.
//

import Foundation
struct LoginResource {
    func getLoginResponse(email: String, paswword: String, completionHandler: @escaping (_ result: LoginResponse) -> Void) {
        let httpUtility = HTTPUtility()
        let loginUrl = "\(ApiEndpoints.loginEndPoint)?email=\(email)&password=\(paswword)"
        let url = URL(string: loginUrl)!
        do {
            httpUtility.getAPIData(requestURL: url, resultType: LoginResponse.self) { result in
                completionHandler(result!)
            }
        }
    }
}
