//
//  LoginController.swift
//  JustEaseApp
//
//  Created by iOSApp on 08/02/2023.
//


import UIKit

class LoginViewController: UIViewController {
   var loginViewModel = LoginViewModel()
   var authViewModel = AuthViewModel()
   var registerDeviceViewModel = RegisterDeviceViewModel()
    // MARK: - Scroll view
    lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.showsHorizontalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    // MARK: - content view
    lazy var contentView: UIView = {
        let content = UIView()
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    
    lazy var heyLabel: UILabel = {
        let label = UILabel()
        label.text = "Hey there!👋🏾"
        label.textColor = AppColors.green.color
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: AppFonts.silkabold.font, size: 25)
        label.textAlignment = .center
        return label
    }()
    
    //MARK: - Label below welcome back
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Sign in to continue"
        return label
    }()
    
    lazy var emailAddressTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Email",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.text = Storage.getEmail()
        textField.keyboardType = .emailAddress
        textField.autocapitalizationType = .none
        textField.autocorrectionType = .no
        return textField
    }()
    
    lazy var passwordTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "Password",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.isSecureTextEntry = true
        textField.autocapitalizationType = .none
        return textField
    }()
    
    lazy var forgetPasswordButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Forgot Password", for: .normal)
        button.layer.masksToBounds = true
        button.backgroundColor = AppColors.lightRed.color
        button.setTitleColor(AppColors.red.color, for: .normal)
        button.titleLabel?.font = UIFont(name: AppFonts.silkLight.font, size: 14)
        button.addTarget(self, action: #selector(didTapForgetPasswordButton), for: .touchUpInside)
        return button
    }()
    lazy var signInButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Sign In", for: .normal)
        button.layer.masksToBounds = true
        button.titleLabel?.font = UIFont(name: AppFonts.silka.font, size: 18)
        button.addTarget(self, action: #selector(didTapSignInButton), for: .touchUpInside)
        return button
    }()
    lazy var openEyeButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didTapOnOpenEyeButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setBackgroundImage(AppButtonImages.focus.image, for: .normal)
        button.isHidden = true
        return button
    }()
    
    lazy var closeEyeButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didTapOnCloseEyeButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setBackgroundImage(AppButtonImages.noFocus.image, for: .normal)
        return button
    }()
    lazy var ownAccountButton: UIButton = {
        let button = UIButton()
        button.setTitle("Don’t have an account?  ", for: .normal)
        button.setTitleColor(textSystemColor, for: .normal)
        button.titleLabel?.font = UIFont(name: AppFonts.silka.font, size: 16)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var signUpButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Sign Up", for: .normal)
        button.setTitleColor(AppColors.green.color, for: .normal)
        button.backgroundColor = AppColors.lightGreen.color
        button.titleLabel?.font = UIFont(name: AppFonts.silka.font, size: 12)
        button.addTarget(self, action: #selector(didTapOnSignUp), for: .touchUpInside)
        return button
    }()
    // MARK: - password visibility function
    @objc func didTapOnOpenEyeButton() {
        openEyeButton.isHidden = true
        closeEyeButton.isHidden = false
        passwordTextField.isSecureTextEntry = true
    }
    
    @objc func didTapOnCloseEyeButton() {
        passwordTextField.isSecureTextEntry = false
        openEyeButton.isHidden = false
        closeEyeButton.isHidden = true
        
    }
    // MARK: - Login function
    @objc func didTapSignInButton(){
        if InternetConnectionManager.isConnectedToNetwork(){
            Loader.shared.showLoader()
            let request = AuthRequest(email: LoginDetails().email, password: LoginDetails().password)
            authViewModel.authResponse(authRequest: request)
        } else {
            Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
        }
    }
    @objc func didTapForgetPasswordButton() {
        navigationController?.pushViewController(ForgotPasswordViewController(), animated: true)
    }
    @objc func didTapOnSignUp() {
        navigationController?.pushViewController(SignUpViewController(), animated: true)
    }
    func showDoneButton(){
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dropKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.emailAddressTextField.inputAccessoryView = toolbar
        self.passwordTextField.inputAccessoryView = toolbar
    }
    @objc func dropKeyboard(){
        view.endEditing(true)
    }
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        showDoneButton()
        layoutViews()
        setupScrollView()
        self.hideKeyboardWhenTappedAround()
        authViewModel.delegate = self
        loginViewModel.delegate = self
        registerDeviceViewModel.delegate = self
    }

    // MARK: - View will appear
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = .systemBackground
    }
    
    // MARK: - View did layout subviews
    override func viewDidLayoutSubviews() {
        scrollView.isScrollEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.contentSize = CGSize(width: 400, height: 700)
    }
}

