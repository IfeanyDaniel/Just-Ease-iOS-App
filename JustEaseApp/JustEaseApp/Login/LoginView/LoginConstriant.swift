//
//  LoginConstriant.swift
//  JustEaseApp
//
//  Created by iOSApp on 08/02/2023.
//

import UIKit
extension LoginViewController {
    //MARK: - Layout subviews for the scroll views
    func setupScrollView() {
      view.addSubview(scrollView)
      scrollView.addSubview(contentView)
      NSLayoutConstraint.activate([
        scrollView.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 0),
        scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
        scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
        
        contentView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
        contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
        contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
        contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
        
      ])
    }
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(heyLabel)
        view.addSubview(label)
        contentView.addSubview(emailAddressTextField)
        contentView.addSubview(passwordTextField)
        contentView.addSubview(forgetPasswordButton)
        contentView.addSubview(signInButton)
        contentView.addSubview(signUpButton)
        contentView.addSubview(ownAccountButton)
        contentView.addSubview(closeEyeButton)
        contentView.addSubview(openEyeButton)
      
        
      self.navigationItem.setHidesBackButton(true, animated: true)
      self.navigationController?.isNavigationBarHidden = true
      view.backgroundColor = .systemBackground
      
      NSLayoutConstraint.activate([
        heyLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
        heyLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
        
        label.topAnchor.constraint(equalTo: heyLabel.bottomAnchor, constant: 20),
        label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
        
        emailAddressTextField.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 30),
        emailAddressTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 20),
        emailAddressTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -20),
        emailAddressTextField.heightAnchor.constraint(equalToConstant: 65),
        
        passwordTextField.topAnchor.constraint(equalTo: emailAddressTextField.bottomAnchor, constant: 15),
        passwordTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 20),
        passwordTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -20),
        passwordTextField.heightAnchor.constraint(equalToConstant: 65),
        
        forgetPasswordButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 30),
        forgetPasswordButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor , constant: -20),
        forgetPasswordButton.heightAnchor.constraint(equalToConstant: 30),
        forgetPasswordButton.widthAnchor.constraint(equalToConstant: 150),
        
        signInButton.topAnchor.constraint(equalTo: forgetPasswordButton.bottomAnchor, constant: 40),
        signInButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
        signInButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor , constant: -20),
        signInButton.heightAnchor.constraint(equalToConstant: buttonHeight),
    
        
        closeEyeButton.topAnchor.constraint(equalTo: passwordTextField.topAnchor, constant: 15),
        closeEyeButton.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -30),
        
        openEyeButton.topAnchor.constraint(equalTo: passwordTextField.topAnchor, constant: 15),
        openEyeButton.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -30),
        
        ownAccountButton.topAnchor.constraint(equalTo: signInButton.topAnchor, constant: 100),
        ownAccountButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor, constant: -(view.frame.width / 32 + 20) ),
        
        signUpButton.topAnchor.constraint(equalTo: signInButton.topAnchor, constant: 100),
        signUpButton.leadingAnchor.constraint(equalTo: ownAccountButton.trailingAnchor, constant: 2),
        signUpButton.heightAnchor.constraint(equalToConstant: 30),
        signUpButton.widthAnchor.constraint(equalToConstant: 90),
        
      ])
    }
}
