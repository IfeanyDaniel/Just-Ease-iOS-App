//
//  LoginExtension.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/03/2023.
//

import UIKit
extension LoginViewController: AuthViewModelDelegate , LoginDelegate , RegisterDeviceViewModelDelegate {
    func didReceiveRegisterDeviceResponse(registerDeviceResponse: RegisterDeviceResponse?, _ statusCode: Int) {
        if statusCode == 200 {
            
        }
    }
    
    func didReceiveLoginResponse(loginResponse: LoginResponse?) {
        if loginResponse?.status == 200 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Login succesfully", type: .success)
            navigationController?.pushViewController(TabBarViewController(), animated: true)
            UserDefaults.standard.setValue(true, forKey: "onboarded")
            UserDefaults.standard.setValue("loggedIn", forKey: "LogInState")
            let logInString = UserDefaults.standard.string(forKey: "LogInState") ?? ""
            Storage.saveEmail(data: emailAddressTextField.text ?? "")
            print("<<<<\(logInString)")
            UserDefaults.standard.synchronize()
            let request = RegisterDeviceRequest(device_token: Storage.getDeviceToken(), user_id: "\(Storage.getUserId())")
            registerDeviceViewModel.getRegisterDeviceResponse(registerDeviceRequest: request)
        }
        if loginResponse?.status  == 422 {
            Loader.shared.hideLoader()
            guard let errorMessage = loginResponse?.message else {
                return
            }
            Toast.shared.showToastWithTItle(errorMessage, type: .error)
            
        }
    }
    
    func didReceiveAuthResponse(AuthResponse: AuthResponse?, _ statusCode: Int) {
        if statusCode == 200 {
            if InternetConnectionManager.isConnectedToNetwork(){
                guard let password = passwordTextField.text, let email = emailAddressTextField.text else {
                    return }
                Loader.shared.showLoader()
                loginViewModel.getLoginResponse(email: email, paswword: password)
            }
            else {
                Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
            }
        }
        if statusCode == 400 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Something went wrong, Please try again", type: .error)
        }
    }
}
