//
//  LoginResponse.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/03/2023.
//

import Foundation
struct LoginResponse: Decodable {
    let status: Int
    let message: String?
    let data: LoginData?
    
}
struct LoginData: Decodable {
    let user_id: Int
    let first_name: String
    let last_name:  String
    let phone_number:  String?
    let address: String?
    let image_url: String?
    let email:  String?
    
}
