//
//  ResetPasswordViewModel.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/03/2023.
//

import Foundation
protocol ResetPasswordViewModelDelegate {
    func didReceiveResetPasswordResponse(resetPasswordResponse: ResetPasswordResponse?,_ statusCode: Int)
}

class ResetPasswordViewModel {
    var delegate: ResetPasswordViewModelDelegate?
    func getResetPasswordResponse(resetPasswordRequest: ResetPasswordRequest) {
        let resetPasswordResource = ResetPasswordResource()
        resetPasswordResource.getResponse(resetPasswordRequest: resetPasswordRequest) { resetPasswordApiResponse, statusCode in
            if statusCode ==  200 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveResetPasswordResponse(resetPasswordResponse: resetPasswordApiResponse, statusCode)
                }
            }
            if statusCode ==  422 {
                DispatchQueue.main.async {
                    self.delegate?.didReceiveResetPasswordResponse(resetPasswordResponse: resetPasswordApiResponse, statusCode)
                }
            }
        }
    }
}
