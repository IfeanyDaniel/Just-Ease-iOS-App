//
//  ResetPasswordSuccess.swift
//  JustEaseApp
//
//  Created by iOSApp on 09/02/2023.
//

import UIKit

class SuccessController: UIViewController {
    
    // MARK: - Scroll view
    lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.showsHorizontalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    // MARK: - content view
    lazy var contentView: UIView = {
        let content = UIView()
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    
    lazy var heyLabel: UILabel = {
        let label = UILabel()
        label.text = "Hey there!👋🏾"
        label.textColor = AppColors.green.color
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: AppFonts.silkabold.font, size: 25)
        label.textAlignment = .center
        return label
    }()
    
    //MARK: - Label below welcome back
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Password reset successfully."
        label.textAlignment = .center
        return label
    }()
    
    lazy var successImageButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(AppButtonImages.checkSuccess.image, for: .normal)
        button.layer.cornerRadius = 45
        return button
    }()
    //MARK: - Label below welcome back
    lazy var labelBelow: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Password Successfully Changed"
        label.textAlignment = .center
        return label
    }()
    
    lazy var loginButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Login now", for: .normal)
        button.layer.cornerRadius = 15
        button.addTarget(self, action: #selector(didTapLoginButton), for: .touchUpInside)
        return button
    }()

    @objc func didTapLoginButton() {
        navigationController?.pushViewController(LoginViewController(), animated: true)
    }
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        setupScrollView()

    }
   
    // MARK: - View did layout subviews
    override func viewDidLayoutSubviews() {
        scrollView.isScrollEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.contentSize = CGSize(width: 400, height: 200)
    }
}
