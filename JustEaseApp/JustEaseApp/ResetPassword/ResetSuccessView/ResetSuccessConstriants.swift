//
//  ResetPasswordConstriants.swift
//  JustEaseApp
//
//  Created by iOSApp on 09/02/2023.
//


import UIKit
extension  SuccessController {
    //MARK: - Layout subviews for the scroll views
    func setupScrollView() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
        ])
    }
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(heyLabel)
        view.addSubview(label)
        contentView.addSubview(successImageButton)
        contentView.addSubview(labelBelow)
        contentView.addSubview(loginButton)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = .systemBackground
        
        NSLayoutConstraint.activate([
            heyLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            heyLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            
            label.topAnchor.constraint(equalTo: heyLabel.bottomAnchor, constant: 30),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            
            successImageButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 120),
            successImageButton.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 50),
            successImageButton.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -50),
            successImageButton.heightAnchor.constraint(equalToConstant: 90),
            successImageButton.widthAnchor.constraint(equalToConstant: 90),
            
            labelBelow.topAnchor.constraint(equalTo: successImageButton.bottomAnchor, constant: 30),
            labelBelow.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            labelBelow.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            
            loginButton.topAnchor.constraint(equalTo: labelBelow.bottomAnchor, constant: 50),
            loginButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            loginButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor , constant: -20),
            loginButton.heightAnchor.constraint(equalToConstant: 50),
            
            
        ])
    }
}
