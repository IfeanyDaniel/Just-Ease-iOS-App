//
//  ResetPasswordDataResource.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/03/2023.
//

import Foundation
struct ResetPasswordResource {
    func getResponse(resetPasswordRequest: ResetPasswordRequest, completionHandler: @escaping (_ result: ResetPasswordResponse?,_ statusCode: Int) -> Void) {
        let resetPasswordURL = URL(string: ApiEndpoints.resetPasswordEndPoint)!
        let httpUtility = HTTPUtility()
        do {
            let resetPasswordPostBody = try JSONEncoder().encode(resetPasswordRequest)
            httpUtility.postAuthorizationResponse(requestUrl: resetPasswordURL, requestBody: resetPasswordPostBody, resultType: ResetPasswordResponse.self) { signUpApiResponse, statusCode in
                completionHandler(signUpApiResponse,statusCode)
            }
        } catch let error {
            debugPrint(error)
        }
    }
}
