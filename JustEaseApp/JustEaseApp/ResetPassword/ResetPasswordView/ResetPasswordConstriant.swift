//
//  ResetPasswordConstriant.swift
//  JustEaseApp
//
//  Created by iOSApp on 09/02/2023.
//

import UIKit
extension ResetPasswordViewController {
    //MARK: - Layout subviews for the scroll views
    func setupScrollView() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
        ])
    }
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(heyLabel)
        view.addSubview(label)
        contentView.addSubview(passwordTextField)
        contentView.addSubview(passwordValidationLabel)
        contentView.addSubview(resetPasswordButton)
        contentView.addSubview(openEyeButton)
        contentView.addSubview(closeEyeButton)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = backgroundSystemColor
        
        NSLayoutConstraint.activate([
            heyLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            heyLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            
            label.topAnchor.constraint(equalTo: heyLabel.bottomAnchor, constant: 30),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            
            passwordTextField.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
            passwordTextField.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 20),
            passwordTextField.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -20),
            passwordTextField.heightAnchor.constraint(equalToConstant: 65),
            
            passwordValidationLabel.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 2),
            passwordValidationLabel.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 20),
            passwordValidationLabel.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -20),
            
            closeEyeButton.topAnchor.constraint(equalTo: passwordTextField.topAnchor, constant: 15),
            closeEyeButton.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -30),
            
            openEyeButton.topAnchor.constraint(equalTo: passwordTextField.topAnchor, constant: 15),
            openEyeButton.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -30),
            
            resetPasswordButton.topAnchor.constraint(equalTo: passwordValidationLabel.bottomAnchor, constant: 30),
            resetPasswordButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            resetPasswordButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor , constant: -20),
            resetPasswordButton.heightAnchor.constraint(equalToConstant: 50),
            
            
        ])
    }
}
