//
//  ResetPasswordExtension.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/03/2023.
//

import Foundation
extension ResetPasswordViewController : AuthViewModelDelegate, ResetPasswordViewModelDelegate {
    func didReceiveResetPasswordResponse(resetPasswordResponse: ResetPasswordResponse?, _ statusCode: Int) {
        if statusCode == 200 {
            Loader.shared.hideLoader()
            navigationController?.pushViewController(SuccessController(), animated: true)
        }
        if statusCode == 422 {
            Loader.shared.hideLoader()
            guard let errorMessage = resetPasswordResponse?.message else {
                return
            }
            Toast.shared.showToastWithTItle(errorMessage, type: .error)
            
        }
    }
    
    func didReceiveAuthResponse(AuthResponse: AuthResponse?, _ statusCode: Int) {
        if statusCode == 200 {
            if InternetConnectionManager.isConnectedToNetwork(){
                Loader.shared.showLoader()
                let request =  ResetPasswordRequest(token: Storage.getOtp(), password: passwordTextField.text!, password_confirmation: passwordTextField.text!, email: Storage.getUserEmail())
                resetPasswordViewModel.getResetPasswordResponse(resetPasswordRequest: request)
            }
            else {
                Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
            }
        }
        if statusCode == 400 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Something went wrong, Please try again", type: .error)
        }
    }
}
