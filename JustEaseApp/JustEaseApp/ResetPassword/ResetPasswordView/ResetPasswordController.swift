//
//  ResetPasswordController.swift
//  JustEaseApp
//
//  Created by iOSApp on 09/02/2023.
//

import UIKit
class ResetPasswordViewController: UIViewController {
    // MARK: - Scroll view
    var resetPasswordViewModel = ResetPasswordViewModel()
    var authViewModel = AuthViewModel()
    lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.showsHorizontalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    // MARK: - content view
    lazy var contentView: UIView = {
        let content = UIView()
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    
    lazy var heyLabel: UILabel = {
        let label = UILabel()
        label.text = "Hey there!👋🏾"
        label.textColor = AppColors.green.color
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: AppFonts.silkabold.font, size: 25)
        label.textAlignment = .center
        return label
    }()
    
    //MARK: - Label below welcome back
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Kindly input a new password you would remember"
        return label
    }()
    
    lazy var passwordTextField: paddedTextField = {
        let textField = paddedTextField.textFieldDesign()
        textField.attributedPlaceholder = NSAttributedString(
            string: "New Password",
            attributes: [NSAttributedString.Key.foregroundColor: placeholderSystemGrayColor]
        )
        textField.addTarget(self, action: #selector(validateViews(_:)), for: .editingChanged)
        textField.isSecureTextEntry = true
        textField.autocapitalizationType = .none
        return textField
    }()
    lazy var passwordValidationLabel: UILabel = {
        let label = UILabel.validationLabelDesign()
        label.numberOfLines = 2
        return label
    }()
   
    lazy var resetPasswordButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Reset Password", for: .normal)
        button.layer.masksToBounds = true
        button.titleLabel?.font = UIFont(name: AppFonts.silka.font, size: 18)
        button.isEnabled = false
        button.backgroundColor = AppColors.grayBorderColor.color
        button.addTarget(self, action: #selector(didTapResetPasswordButton), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Login function
    @objc func didTapResetPasswordButton(){
        if InternetConnectionManager.isConnectedToNetwork(){
            Loader.shared.showLoader()
            let request = AuthRequest(email: LoginDetails().email, password: LoginDetails().password)
            authViewModel.authResponse(authRequest: request)
        } else {
            Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
        }
    }
    
    func showDoneButton(){
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dropKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        //setting toolbar as inputAccessoryView
        self.passwordTextField.inputAccessoryView = toolbar
    }
    @objc func dropKeyboard(){
        view.endEditing(true)
    }
    // MARK: - ... Validation of all textfield
    @objc func validateViews(_ textField: UITextField) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.validatePasswordField()
        }
    }
    lazy var openEyeButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didTapOnOpenEyeButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setBackgroundImage(AppButtonImages.focus.image, for: .normal)
        button.isHidden = true
        return button
    }()
    
    lazy var closeEyeButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didTapOnCloseEyeButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setBackgroundImage(AppButtonImages.noFocus.image, for: .normal)
        return button
    }()
    // MARK: - password visibility function
    @objc func didTapOnOpenEyeButton() {
        openEyeButton.isHidden = true
        closeEyeButton.isHidden = false
        passwordTextField.isSecureTextEntry = true
    }
    
    @objc func didTapOnCloseEyeButton() {
        passwordTextField.isSecureTextEntry = false
        openEyeButton.isHidden = false
        closeEyeButton.isHidden = true
        
    }
    // MARK: - ... Validation of password text field
    func validatePasswordField() {
        guard let text = passwordTextField.text else { return }
        let capitalLetterRegEx  =  "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&<>*~:`-]).{8,}$"
        let textTest = NSPredicate(format: "SELF MATCHES %@", capitalLetterRegEx)
        let passwordValidility = textTest.evaluate(with: text)
        if passwordValidility == true || passwordTextField.text == "" {
            passwordValidationLabel.text = ""
            passwordTextField.layer.borderColor = AppColors.lightGray.color.cgColor
            passwordTextField.layer.borderWidth = 1
            resetPasswordButton.isEnabled = true
            resetPasswordButton.backgroundColor = AppColors.green.color
        }  else {
            passwordValidationLabel.text = "1 upper case, 1 special character , 1 number and minimum of 8 characters"
            passwordValidationLabel.textColor = AppColors.red.color
            passwordTextField.layer.borderColor = UIColor.red.cgColor
            passwordTextField.layer.borderWidth = 1
            resetPasswordButton.isEnabled = false
            resetPasswordButton.backgroundColor = AppColors.grayBorderColor.color
        }
    }

    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        showDoneButton()
        layoutViews()
        setupScrollView()
        self.hideKeyboardWhenTappedAround()
        authViewModel.delegate = self
        resetPasswordViewModel.delegate = self
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
    }
    // MARK: - View did layout subviews
    override func viewDidLayoutSubviews() {
        scrollView.isScrollEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.contentSize = CGSize(width: 400, height: 300)
    }
}
