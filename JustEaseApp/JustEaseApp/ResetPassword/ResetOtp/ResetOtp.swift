//
//  ResetOtpPassword.swift
//  JustEaseApp
//
//  Created by iOSApp on 09/02/2023.
//

import Foundation
import UIKit
class ResetOtpViewController: UIViewController {
    // MARK: - Scroll view
    var resetPasswordViewModel = ResetPasswordViewModel()
    var authViewModel = AuthViewModel()
    var otp = ""
    lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.showsHorizontalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    // MARK: - content view
    lazy var contentView: UIView = {
        let content = UIView()
        content.translatesAutoresizingMaskIntoConstraints = false
        return content
    }()
    
    lazy var heyLabel: UILabel = {
        let label = UILabel()
        label.text = "Hey there!👋🏾"
        label.textColor = AppColors.green.color
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: AppFonts.silkabold.font, size: 25)
        label.textAlignment = .center
        return label
    }()
    
    //MARK: - Label below welcome back
    lazy var label: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Kindly Input the code sent to your email"
        return label
    }()
    
    lazy var otp1TextField: paddedOtpTextField = {
        let textField = paddedOtpTextField.textFieldOtpDesign()
        textField.addTarget(self, action: #selector(self.changeCharacter), for: .editingChanged)
        return textField
    }()
    lazy var otp2TextField: paddedOtpTextField = {
        let textField = paddedOtpTextField.textFieldOtpDesign()
        textField.addTarget(self, action: #selector(self.changeCharacter), for: .editingChanged)
        return textField
    }()
    lazy var otp3TextField: paddedOtpTextField = {
        let textField = paddedOtpTextField.textFieldOtpDesign()
        textField.addTarget(self, action: #selector(self.changeCharacter), for: .editingChanged)
        return textField
    }()
    lazy var otp4TextField: paddedOtpTextField = {
        let textField = paddedOtpTextField.textFieldOtpDesign()
        textField.addTarget(self, action: #selector(self.changeCharacter), for: .editingChanged)
        return textField
    }()
    lazy var otp5TextField: paddedOtpTextField = {
        let textField = paddedOtpTextField.textFieldOtpDesign()
        textField.addTarget(self, action: #selector(self.changeCharacter), for: .editingChanged)
        return textField
    }()
    lazy var otp6TextField: paddedOtpTextField = {
        let textField = paddedOtpTextField.textFieldOtpDesign()
        textField.addTarget(self, action: #selector(self.changeCharacter), for: .editingChanged)
        return textField
    }()
    
    func inputtingText(in textField: UITextField) {
        textField.layer.borderColor = AppColors.green.color.cgColor
        textField.layer.borderWidth = 0.3
        textField.backgroundColor = AppColors.lightGreen.color
        textField.textColor = AppColors.green.color
    }
    
    func removingText(in textField: UITextField) {
        textField.layer.borderColor = AppColors.lightGray.color.cgColor
        textField.layer.borderWidth = 1
        textField.backgroundColor = textFieldColor
        textField.textColor = textFieldTextColor
        
    }
    
    @objc func changeCharacter(textField: UITextField) {
        if textField.text!.utf8.count == 1 {
            switch textField {
            case otp1TextField:
                otp2TextField.becomeFirstResponder()
                 inputtingText(in: otp1TextField)
            case otp2TextField:
                otp3TextField.becomeFirstResponder()
                inputtingText(in: otp2TextField)
            case otp3TextField:
                otp4TextField.becomeFirstResponder()
                inputtingText(in: otp3TextField)
            case otp4TextField:
                otp5TextField.becomeFirstResponder()
                inputtingText(in: otp4TextField)
            case otp5TextField:
                otp6TextField.becomeFirstResponder()
                inputtingText(in: otp5TextField)
            case otp6TextField:
                inputtingText(in: otp6TextField)
                print("OTP \(otp1TextField.text!)\(otp2TextField.text!)\(otp3TextField.text!)\(otp4TextField.text!)\(otp5TextField.text!)\(otp6TextField.text!)")
                otp = ">>>\(otp1TextField.text!)\(otp2TextField.text!)\(otp3TextField.text!)\(otp4TextField.text!)\(otp5TextField.text!)\(otp6TextField.text!)"
                print("@@@\(otp)")
                
                proceedButton.isEnabled = true
                proceedButton.backgroundColor = AppColors.green.color
            default:
                break
            }
        } else if ((textField.text?.isEmpty) != nil) {
            switch textField {
            case otp6TextField:
                otp5TextField.becomeFirstResponder()
            proceedButton.isEnabled = false
            proceedButton.backgroundColor = AppColors.grayBorderColor.color
                removingText(in: otp6TextField)
            case otp5TextField:
                otp4TextField.becomeFirstResponder()
                removingText(in: otp5TextField)
            case otp4TextField:
                otp3TextField.becomeFirstResponder()
                removingText(in: otp4TextField)
            case otp3TextField:
                otp2TextField.becomeFirstResponder()
                removingText(in: otp3TextField)
            case otp2TextField:
                otp1TextField.becomeFirstResponder()
                removingText(in: otp2TextField)
            case otp1TextField:
                removingText(in: otp1TextField)
            default:
                break
            }
        }
    }
    lazy var stackViewHolder : UIStackView = {
        let stackView = UIStackView()
        stackView.backgroundColor = backgroundSystemColor
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.isUserInteractionEnabled = true
        stackView.addArrangedSubview(otp1TextField)
        stackView.addArrangedSubview(otp2TextField)
        stackView.addArrangedSubview(otp3TextField)
        stackView.addArrangedSubview(otp4TextField)
        stackView.addArrangedSubview(otp5TextField)
        stackView.addArrangedSubview(otp6TextField)
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 11
        return stackView
    }()
    //MARK: - Label below welcome back
    lazy var labelBelow: UILabel = {
        let label = UILabel.labelTextDesign()
        label.text = "Didn't receieve a code? Resend in 2:59"
        label.isHidden = true
        return label
    }()
    
    lazy var proceedButton: UIButton = {
        let button = UIButton.actionButtonDesign()
        button.setTitle("Proceed", for: .normal)
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(didTapProceedPasswordButton), for: .touchUpInside)
        button.isEnabled = false
        button.backgroundColor = AppColors.grayBorderColor.color
        return button
    }()
    
    // MARK: - Login function
    @objc func didTapProceedPasswordButton(){
        if InternetConnectionManager.isConnectedToNetwork(){
            Loader.shared.showLoader()
            let request = AuthRequest(email: LoginDetails().email, password: LoginDetails().password)
            authViewModel.authResponse(authRequest: request)
        } else {
            Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
        }
    }
    
    // MARK: - View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        setupScrollView()
        self.hideKeyboardWhenTappedAround()
        resetPasswordViewModel.delegate = self
        authViewModel.delegate = self
    }
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: AppButtonImages.backArrow.image, style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.setHidesBackButton(true, animated: true)
        navigationItem.leftBarButtonItem?.tintColor = textSystemColor
    }
    // MARK: - View did layout subviews
    override func viewDidLayoutSubviews() {
        scrollView.isScrollEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.contentSize = CGSize(width: 400, height: 300)
    }
}
