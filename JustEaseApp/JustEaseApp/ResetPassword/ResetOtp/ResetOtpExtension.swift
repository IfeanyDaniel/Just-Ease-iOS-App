//
//  ResetPasswordExtension.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/03/2023.
//

import Foundation
extension ResetOtpViewController : AuthViewModelDelegate, ResetPasswordViewModelDelegate {
    func didReceiveResetPasswordResponse(resetPasswordResponse: ResetPasswordResponse?, _ statusCode: Int) {
//        if statusCode == 200 {
//            Loader.shared.hideLoader()
//            let controller = ResetPasswordViewController()
//            navigationController?.pushViewController(controller, animated: true)
//        }
        if resetPasswordResponse?.message == "The password confirmation does not match." {
            Loader.shared.hideLoader()
            let controller = ResetPasswordViewController()
            navigationController?.pushViewController(controller, animated: true)
           
        } else {
            Loader.shared.hideLoader()
            guard let errorMessage = resetPasswordResponse?.message else {
                return
            }
            Toast.shared.showToastWithTItle(errorMessage, type: .error)
            otp1TextField.text = ""
            removingText(in: otp1TextField)
            otp2TextField.text = ""
            removingText(in: otp2TextField)
            otp3TextField.text = ""
            removingText(in: otp3TextField)
            otp4TextField.text = ""
            removingText(in: otp4TextField)
            otp5TextField.text = ""
            removingText(in: otp5TextField)
            otp6TextField.text = ""
            removingText(in: otp6TextField)
        }
    }
    
    func didReceiveAuthResponse(AuthResponse: AuthResponse?, _ statusCode: Int) {
        if statusCode == 200 {
            if InternetConnectionManager.isConnectedToNetwork(){
                Loader.shared.showLoader()
                Storage.saveOtp(data: "\(otp1TextField.text!)\(otp2TextField.text!)\(otp3TextField.text!)\(otp4TextField.text!)\(otp5TextField.text!)\(otp6TextField.text!)")
                let request =  ResetPasswordRequest(token: Storage.getOtp(), password: Storage.getPassword(), password_confirmation: "", email:  Storage.getUserEmail())
                resetPasswordViewModel.getResetPasswordResponse(resetPasswordRequest: request)
            }
            else {
                Toast.shared.showToastWithTItle(noInternetMessage, type: .error)
            }
        }
        if statusCode == 400 {
            Loader.shared.hideLoader()
            Toast.shared.showToastWithTItle("Something went wrong, Please try again", type: .error)
        }
    }
}
