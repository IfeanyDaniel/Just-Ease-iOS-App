//
//  ResetOtpConstriants.swift
//  JustEaseApp
//
//  Created by iOSApp on 09/02/2023.
//

import UIKit
extension ResetOtpViewController {
    //MARK: - Layout subviews for the scroll views
    func setupScrollView() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            
        ])
    }
    //MARK: - Layout subviews
    func layoutViews() {
        view.addSubview(heyLabel)
        view.addSubview(label)
        contentView.addSubview(stackViewHolder)
        contentView.addSubview(labelBelow)
        contentView.addSubview(proceedButton)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = backgroundSystemColor
        
        NSLayoutConstraint.activate([
            heyLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            heyLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            
            label.topAnchor.constraint(equalTo: heyLabel.bottomAnchor, constant: 30),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            
            stackViewHolder.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
            stackViewHolder.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: 20),
            stackViewHolder.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor, constant: -20),
            stackViewHolder.heightAnchor.constraint(equalToConstant: 65),
            
            labelBelow.topAnchor.constraint(equalTo: stackViewHolder.bottomAnchor, constant: 30),
            labelBelow.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            labelBelow.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            
            proceedButton.topAnchor.constraint(equalTo: labelBelow.bottomAnchor, constant: 30),
            proceedButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            proceedButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor , constant: -20),
            proceedButton.heightAnchor.constraint(equalToConstant: 50),
            
            
        ])
    }
}
