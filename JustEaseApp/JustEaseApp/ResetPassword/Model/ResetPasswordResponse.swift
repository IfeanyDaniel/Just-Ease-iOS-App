//
//  ResetPasswordResponse.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/03/2023.
//

import Foundation
struct ResetPasswordResponse: Decodable {
    let status: Int 
    let message: String
}
