//
//  ResetPasswordRequest.swift
//  JustEaseApp
//
//  Created by iOSApp on 22/03/2023.
//

import Foundation
struct ResetPasswordRequest: Encodable {
    let token: String
    let password: String
    let password_confirmation: String
    let email: String
}
